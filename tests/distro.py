#!/usr/bin/python3

import unittest

import test

from buildservice import distribution

class DistroTestCase(test.TestCase):
	"""
		Tests everything around the Distro object
	"""
	def test_create(self):
		"""
			Tests whether we can create a distribution
		"""
		with self.db.transaction():
			distro = self.backend.distros.create("Random Test Distribution 2", "test", 2)

		# Check that we got the correct type back
		self.assertIsInstance(distro, distribution.Distribution)

		# Check if the values got set correct
		self.assertEqual(distro.name, "Random Test Distribution 2")
		self.assertEqual(distro.slug, "test-2")
		self.assertEqual(distro.tag, "test2")

	def test_default(self):
		"""
			Tests whether we can access the default distribution
		"""
		self.assertIsInstance(self.distro, distribution.Distribution)


if __name__ == "__main__":
	unittest.main()
