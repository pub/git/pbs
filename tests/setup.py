#!/usr/bin/python3

import os
import unittest

import test

class SetupTestCase(test.TestCase):
	async def test_pakfire(self):
		"""
			This test checks whether we can launch into a local Pakfire instance
		"""
		async with self.backend.pakfire() as p:
			pass

	async def test_unlink(self):
		"""
			Tests whether the unlink() function works correctly
		"""
		path = self.backend.path("tmp/parent/file")

		# Fetch the parent directory
		parent = os.path.dirname(path)

		# Create the parent directory
		os.mkdir(parent)

		# Create an empty file
		with open(path, "w") as f:
			pass

		# Check if the file exists
		self.assertTrue(os.path.exists(path))

		# Try to unlink the file
		await self.backend.unlink(path)

		# Check that the file is gone
		self.assertFalse(os.path.exists(path))

		# Check that the parent directory is gone, too
		self.assertFalse(os.path.exists(parent))

		# Check that the base path is still there
		self.assertTrue(os.path.exists(self.backend.basepath))

		# Try to delete some file outside the base path
		with self.assertRaises(OSError):
			await self.backend.unlink("/tmp/file-that-should-not-exist")

		# Try to leave the base path through a relative thing
		with self.assertRaises(OSError):
			await self.backend.unlink("%s/../file-that-should-not-exist" % self.backend.basepath)


if __name__ == "__main__":
	unittest.main()
