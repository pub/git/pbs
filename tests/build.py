#!/usr/bin/python3

import io
import unittest

import test

from buildservice import builds
from buildservice import jobs

class BuildTestCase(test.TestCase):
	"""
		Tests everything around builds
	"""
	async def test_create_from_source(self):
		"""
			Tests whether we can create a build from a source package
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the build
		with self.db.transaction():
			build = await self._create_build(path)

		# Check if any jobs have been created
		self.assertTrue(build.jobs)

		# Check if a job for each architecture has been generated
		self.assertEqual([j.arch for j in build.jobs], self.distro.arches)

	async def test_create_from_binary(self):
		"""
			Tests whether we can create a build from a binary package
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.x86_64.pfm")

		with self.db.transaction():
			package = await self._create_package(path)

			# Create the build
			with self.assertRaises(RuntimeError):
				build = self.backend.builds.create(self.repo, package, owner=self.user)

	async def test_delete(self):
		"""
			Tests whether a build can be deleted properly
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the build
		with self.db.transaction():
			build = await self._create_build(path, owner=self.user)

		# It should be possible for the owner to delete the build
		self.assertTrue(build.can_be_deleted(self.user))

		# It should not be possible for anybody else to delete the build
		self.assertFalse(build.can_be_deleted())

		# Check how many builds we have
		self.assertEqual(len(self.backend.builds), 1)

		# Delete the build
		with self.db.transaction():
			await build.delete(self.user)

		# There should now be no more builds
		self.assertEqual(len(self.backend.builds), 0)

		# We should not be able to fetch the build again
		self.assertIsNone(self.backend.builds.get_by_uuid(build.uuid))

		# But we should be able to fetch the build by its ID
		self.assertEqual(self.backend.builds.get_by_id(build.id), build)

	async def test_queue(self):
		"""
			Check if all jobs appear correctly in the queue
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the build
		with self.db.transaction():
			build = await self._create_build(path, owner=self.user)

		# Fetch jobs
		job1, job2 = build.jobs

		self.assertIsInstance(job1, jobs.Job)
		self.assertIsInstance(job2, jobs.Job)

		# There should be no jobs in the queue yet, because the dependency check
		# has not been finished, yet
		self.assertEqual(len(self.backend.jobs.queue), 0)

		# Pretend the install check was successful
		for job in build.jobs:
			job._set_attribute("installcheck_succeeded", True)

		# There should now be two jobs in the queue
		self.assertEqual(len(self.backend.jobs.queue), 2)

		# Assign a job to a builder
		job1.assign(self.builder)

		# There should be one job left
		self.assertEqual(len(self.backend.jobs.queue), 1)

		# Let the job fail
		await job1.finished(success=False)

		# There should still be only one job
		self.assertEqual(len(self.backend.jobs.queue), 1)

		# Assign the second job
		job2.assign(self.builder)

		# The queue should now be empty
		self.assertEqual(len(self.backend.jobs.queue), 0)

		# Pretend the job finished successfully
		await job2.finished(success=True)

		# The queue should still be empty
		self.assertEqual(len(self.backend.jobs.queue), 0)

	async def test_watchers(self):
		"""
			Tests whether we can add and remove a watcher to a build
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the build
		with self.db.transaction():
			build = await self._create_build(path)

		# Add the default user as watcher
		with self.db.transaction():
			build.add_watcher(self.user)

		# Check if the watcher has been added
		self.assertIn(self.user, build.watchers)

		# Remove the default user as watcher
		with self.db.transaction():
			build.remove_watcher(self.user)

		# Remove the default user as watcher (again)
		with self.db.transaction():
			build.remove_watcher(self.user)

		# Check if the watcher has been removed
		self.assertNotIn(self.user, build.watchers)

	async def test_comments(self):
		"""
			Tests creating comments
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the build
		with self.db.transaction():
			build = await self._create_build(path)

		# Create a new comment
		comment = build.comment(self.user, "This is a test comment")

		# Check if the type is correct
		self.assertIsInstance(comment, builds.Comment)

		# Check if the comment is correctly cached
		self.assertIn(comment, build.comments)

		# Award some points
		build.add_points(2)

		# The score should now be 2
		self.assertEqual(build.score, 2)

		# Take away some points
		build.add_points(-4)

		# The score should now be -2
		self.assertEqual(build.score, -2)

	async def test_run(self):
		"""
			This test creates a build and tries to emulate a job being completed.
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the build
		with self.db.transaction():
			build = await self._create_build(path)

		# Assign all jobs to the default builder
		for job in build.jobs:
			job.assign(self.builder)

			# This job should now be running
			self.assertTrue(job.is_running())

			# The builder should match
			self.assertEqual(job.builder, self.builder)

		path = self.source_path("tests/data/beep-1.3-2.ip3.x86_64.pfm")

		# Upload a package
		#package = await self._create_upload(path)

		# Upload the log
		logfile = await self._create_upload(
			self.source_path("tests/data/beep-1.3-2.ip3.x86_64.log"),
		)

		# Pretend that all jobs finished
		for job in build.jobs:
			await job.finished(
				success=True,
				#packages=[package],
				logfile=logfile,
			)

	async def test_fail(self):
		"""
			This test creates a build and pretends a job failed
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the build
		with self.db.transaction():
			build = await self._create_build(path, owner=self.user)

		# Assign all jobs to the default builder
		for job in build.jobs:
			job.assign(self.builder)

		# Pick the first job
		job = build.jobs[0]

		# Upload the log
		logfile = await self._create_upload(
			self.source_path("tests/data/beep-1.3-2.ip3.x86_64.log"),
		)

		# Pretend that the job has failed
		await job.finished(
			success=False,
			logfile=logfile,
		)

	async def test_abort(self):
		"""
			This test tries to abort a job
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the build
		with self.db.transaction():
			build = await self._create_build(path, owner=self.user)

		# Assign all jobs to the default builder
		for job in build.jobs:
			job.assign(self.builder)

		# Abort all jobs
		for job in build.jobs:
			await job.abort(self.user)

			self.assertTrue(job.is_aborted())
			self.assertIsNotNone(job.aborted_at)

	async def test_log(self):
		"""
			This test creates a build and tries to access the log
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the build
		with self.db.transaction():
			build = await self._create_build(path)

		# Assign all jobs to the default builder
		for job in build.jobs:
			job.assign(self.builder)

			# This job should now be running
			self.assertTrue(job.is_running())

			# The builder should match
			self.assertEqual(job.builder, self.builder)

		path = self.source_path("tests/data/beep-1.3-2.ip3.x86_64.pfm")

		# Pick the first job
		job = build.jobs[0]

		# Check that there is no log
		with self.assertRaises(FileNotFoundError):
			await job.open_log()

		# Upload the log
		logfile = await self._create_upload(
			self.source_path("tests/data/beep-1.3-2.ip3.x86_64.log"),
		)

		# Pretend that the job has failed
		await job.finished(
			success=False,
			logfile=logfile,
		)

		# Check if we have a log
		self.assertTrue(job.has_log())

		# Check if the log path is set
		self.assertIsNotNone(job.log_path)

		# Check if log size is set
		self.assertGreater(job.log_size, 0)

		# Check if the digest has been set
		self.assertIsNotNone(job.log_digest_blake2s)

		# Try to open the log file again
		with await job.open_log() as f:
			self.assertIsInstance(f, io.IOBase)

		# Fetch the last 100 lines of the log
		lines = await job.tail_log(100)

		# Check if we received the correct type
		self.assertIsInstance(lines, list)

		# Check if the result had the correct length
		self.assertEqual(len(lines), 100)



if __name__ == "__main__":
	unittest.main()
