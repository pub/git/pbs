#!/usr/bin/python3

import datetime
import os
import pakfire
import unittest

import test

from buildservice import packages

class PackageTestCase(test.TestCase):
	"""
		Tests everything around packages
	"""
	async def test_create(self):
		"""
			Tests whether we can create/import a package
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.x86_64.pfm")

		# Create the package
		with self.db.transaction():
			package = await self._create_package(path)

		# Check whether the package has been copied to the correct place
		self.assertTrue(os.path.exists(package.path))
		self.assertTrue(package.path.startswith(self.backend.path("packages")))

		# Check metadata
		self.assertEqual(package.name, "beep")
		self.assertEqual(package.evr, "1.3-2.ip3")
		self.assertEqual(package.arch, "x86_64")
		self.assertEqual(package.groups, [])
		self.assertEqual(package.packager, None)
		self.assertEqual(package.license, "GPLv2+")
		self.assertEqual(package.url, "http://www.johnath.com/beep/")
		self.assertEqual(package.summary, "Beep the PC speaker any number of ways.")
		self.assertEqual(package.description,
			"""Beep allows the user to control the PC speaker with precision, allowing
different sounds to indicate different events. While it can be run quite happily
on the commandline, it's intended place of residence is within shell/perl
scripts, notifying the user when something interesting occurs. Of course, it has
no notion of what's interesting, but it's real good at that notifying part.""")
		self.assertEqual(package.size, 17856)
		self.assertEqual(package.uuid, "0d35b9fa-5254-4a16-ba5b-a171362ade5d")
		# XXX BUILD ID MISSING
		self.assertEqual(package.build_host, "cornelius.ipfire.org")
		self.assertEqual(package.build_time, datetime.datetime(2016, 10, 21, 9, 46, 54))
		self.assertEqual(package.filesize, 30720)

	async def test_create_source(self):
		path = self.source_path("tests/data/beep-1.3-2.ip3.src.pfm")

		# Create the package
		with self.db.transaction():
			package = await self._create_package(path)

		# Check metadata
		self.assertEqual(package.name, "beep")
		self.assertEqual(package.evr, "1.3-2.ipfire3")
		self.assertEqual(package.arch, "src")

	async def test_double_import(self):
		path = self.source_path("tests/data/beep-1.3-2.ip3.x86_64.pfm")

		# Import the same package twice
		with self.db.transaction():
			package1 = await self._create_package(path)
			package2 = await self._create_package(path)

		# The package should not have been imported again, and therefore have the same ID
		self.assertEqual(package1.id, package2.id)

	async def test_delete(self):
		path = self.source_path("tests/data/beep-1.3-2.ip3.x86_64.pfm")

		# Create the package
		with self.db.transaction():
			package = await self._create_package(path)

		# Store the package path for later
		path = package.path

		# Delete the package
		with self.db.transaction():
			await package.delete()

		# Check if the payload has been removed
		self.assertFalse(os.path.exists(path))

	async def test_files(self):
		"""
			Tests whether the filelist could be loaded correctly
		"""
		path = self.source_path("tests/data/beep-1.3-2.ip3.x86_64.pfm")

		# Create the package
		with self.db.transaction():
			package = await self._create_package(path)

		# Check if the filelist is of the correct type
		self.assertIsInstance(package.files, list)
		for file in package.files:
			self.assertIsInstance(file, packages.File)

		# Check if we have the correct paths
		self.assertIn("/usr/bin/beep", [file.path for file in package.files])

		# Fetch a specific file
		file = package.get_file("/usr/bin/beep")
		self.assertIsInstance(file, packages.File)

		# Try to read a file
		payload = await file.get_payload()
		self.assertIsInstance(payload, bytes)


if __name__ == "__main__":
	unittest.main()
