#!/usr/bin/python3

import configparser
import functools
import os
import socket
import tempfile
import unittest

from pakfire._pakfire import Archive

from buildservice import Backend
from buildservice import builds
from buildservice import database
from buildservice import misc
from buildservice import packages
from buildservice import uploads

class TestCase(unittest.IsolatedAsyncioTestCase):
	"""
		This is the base class for all tests
	"""
	def source_path(self, path):
		try:
			source_path = os.environ["abs_top_srcdir"]
		except KeyError as e:
			raise ValueError("source path is not set") from e

		return os.path.join(source_path, path)

	def _setup_database(self):
		"""
			Creates a new database and imports the default schema
		"""
		# Path to the schema
		schema = self.source_path("src/database.sql")

		# Load the schema
		with open(schema) as f:
			schema = f.read()

		# Database Server & Name
		host = os.environ.get("DB_HOST", socket.gethostname())
		name = os.environ.get("DB_NAME", "pakfiretest")

		# Credentials
		username = os.environ.get("DB_USER", "pakfiretest")
		password = os.environ.get("DB_PASS", "pakfiretest")

		# Connect to the database
		db = database.Connection(host, name, username, password)

		with db.transaction():
			# Drop anything existing
			db.execute("DROP SCHEMA public CASCADE")

			# Re-create the schema
			db.execute("CREATE SCHEMA public")
			db.execute(schema)

		# Return the credentials
		return {
			"name"     : name,
			"hostname" : host,
			"user"     : username,
			"password" : password,
		}

	async def asyncSetUp(self):
		# Create a new temporary directory
		self.testdir = tempfile.TemporaryDirectory()

		# Create a configuration file
		conf = configparser.ConfigParser()

		# Set the base path
		conf["global"] = {
			"basepath" : self.testdir.name,
		}

		# Setup the database
		conf["database"] = self._setup_database()

		with tempfile.NamedTemporaryFile("w") as f:
			# Write configuration
			conf.write(f)

			# Flush
			f.flush()

			# Initialize the backend
			self.backend = Backend(f.name, test=True)

		# Create handle to the database
		self.db = self.backend.db

		# Create some default objects
		with self.db.transaction():
			await self._create_default_objects()

	async def asyncTearDown(self):
		# Dump any messages in the message queue
		for message in self.backend.messages.queue:
			print(message.message)

		# Dump a listing of all temporary files
		with os.scandir(self.testdir.name) as listing:
			for entry in sorted(listing, key=lambda e: e.path):
				print("  %s" % entry.path)

		# Removing any temporary files
		self.testdir.cleanup()

	async def _create_default_objects(self):
		"""
			Creates some random objects that are created by default so
			that we won't have to start from scratch each time...
		"""
		# Create a builder
		self.builder = self.backend.builders.create("test-builder01.ipfire.org")

		# Create a user
		self.user = self.backend.users.create("tester", _attrs={
			"cn"    : [b"Joe Tester"],
			"email" : [b"joe.tester@ipfire.org"],
		})

		# Set storage quota
		self.user.storage_quota = 104857600 # 100 MiB

		# Create a distribution
		self.distro = self.backend.distros.create("Default Test Distribution", "test", 1)

		# Enable this distribution for aarch64 & x86_64
		self.distro.arches = ["aarch64", "x86_64"]

		# Create a repository
		self.repo = await self.backend.repos.create(self.distro, "Default Test Repository")

	async def _create_package(self, path):
		"""
			Helper function to import a package from path
		"""
		# Check if the file exists
		self.assertTrue(os.path.exists(path))

		# Upload the file
		upload = await self._create_upload(path)

		# Create the package
		package = await self.backend.packages.create(upload, distro=self.distro)

		# Check if we received the correct type
		self.assertIsInstance(package, packages.Package)

		return package

	async def _create_build(self, path, repo=None, owner=None):
		"""
			Helper function to import a build from path
		"""
		# Use the default repository if none set
		if repo is None:
			repo = self.repo

		# Import the package first
		package = await self._create_package(path)

		# Create the build
		build = await self.backend.builds.create(repo, package, owner=owner)

		# Check if we received the correct type
		self.assertIsInstance(build, builds.Build)

		return build

	async def _create_upload(self, path, **kwargs):
		"""
			Helper function to create an upload from path
		"""
		# Create the upload object
		upload = await self.backend.uploads.create_from_local(path, **kwargs)

		# Check if received the correct type
		self.assertIsInstance(upload, uploads.Upload)

		return upload
