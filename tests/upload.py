#!/usr/bin/python3

import io
import os
import unittest

import test

from buildservice import uploads
from buildservice import users

class UploadTestCase(test.TestCase):
	"""
		Tests everything around uploads
	"""
	async def test_create_delete(self):
		"""
			Tests whether uploads can be created and deleted
		"""
		# Create the upload object
		upload = self.backend.uploads.create("test.blob", size=0, owner=self.user)

		self.assertIsInstance(upload, uploads.Upload)
		self.assertEqual(upload.filename, "test.blob")

		# Check if the upload file exists
		self.assertTrue(os.path.exists(upload.path))

		# Delete the upload
		await upload.delete()

		# Check if the file has been removed
		self.assertFalse(os.path.exists(upload.path))

	async def test_write_too_large_file(self):
		"""
			Creates an upload of a certain size, but then tries to write more data
		"""
		payload = io.BytesIO(b"012345678901234567890123456789")

		with self.db.transaction():
			upload = await self.backend.uploads.create("test.blob", size=20, owner=self.user)

		# Try to write more than 20 bytes
		with self.assertRaises(OverflowError):
			await upload.copyfrom(payload)

	async def test_check_digest(self):
		"""
			Creates an upload and checks if the digest matches
		"""
		payload = io.BytesIO(b"01234567890123456789")

		with self.db.transaction():
			upload = await self.backend.uploads.create("test.blob", size=20, owner=self.user)

		# Write some payload
		await upload.copyfrom(payload)

		digest = bytes.fromhex("185c728c3fccb51875d74e21fec19f4cdfad8d5aa347a7a75c06473"
			"af6f73835b5a00515a34f0e09725d5b1e0715ce43a1a57d966a92400efd215e45dd19c09c")

		# Check the digest
		self.assertTrue(await upload.check_digest("blake2b", digest))

	async def test_quota(self):
		"""
			Tries to create an upload that exceeds the quota
		"""
		# Create an upload that is 200 MiB large
		with self.db.transaction():
			with self.assertRaises(users.QuotaExceededError):
				await self.backend.uploads.create(
					"test.blob",
					size=209715200,
					owner=self.user,
				)


if __name__ == "__main__":
	unittest.main()
