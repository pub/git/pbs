#!/usr/bin/python3

import unittest

import test

from buildservice import keys
from buildservice import repository

class RepoTestCase(test.TestCase):
	"""
		Tests everything around the Repo object
	"""
	async def test_create(self):
		"""
			Tests whether we can create a repository
		"""
		with self.db.transaction():
			repo = await self.backend.repos.create(self.distro, "Random Test Repository")

		# Check that we got the correct type back
		self.assertIsInstance(repo, repository.Repository)

		# Check if the values got set correct
		self.assertEqual(repo.name, "Random Test Repository")
		#self.assertEqual(repo.slug, "random-test-repository")

		# Check if a key was generated
		self.assertIsInstance(repo.key, keys.Key)

	def test_default(self):
		"""
			Tests whether we can access the default repository
		"""
		self.assertIsInstance(self.repo, repository.Repository)

	async def test_write(self):
		"""
			Tests writing a repository
		"""
		await self.repo.write()


if __name__ == "__main__":
	unittest.main()
