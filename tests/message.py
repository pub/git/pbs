#!/usr/bin/python3

import unittest

import test

class MessageTestCase(test.TestCase):
	"""
		Tests everything around messages
	"""
	async def test_create_from_source(self):
		"""
			Test whether we can send a message from template
		"""
		with self.db.transaction():
			self.user._send_welcome_email()

		# There should now be one message in the queue
		self.assertEqual(len(self.backend.messages.queue), 1)


if __name__ == "__main__":
	unittest.main()
