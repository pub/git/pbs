#!/usr/bin/python3

import unittest

import test

from buildservice import builders

class BuilderTestCase(test.TestCase):
	"""
		Tests everything around builders
	"""
	async def test_create(self):
		"""
			Tests whether we can create a repository
		"""
		with self.db.transaction():
			builder = self.backend.builders.create("test-builder02.ipfire.org")

		# Check that we got the correct type back
		self.assertIsInstance(builder, builders.Builder)

	def test_default(self):
		"""
			Tests whether we can access the default builder
		"""
		self.assertIsInstance(self.builder, builders.Builder)


if __name__ == "__main__":
	unittest.main()
