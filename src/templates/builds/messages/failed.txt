To: {{ recipient.email_to }}
Subject: [{{ build }}] {{ _("Build Failed") }}
Message-Id: <build-{{ build.uuid }}-failed@pakfire.ipfire.org>

{{ _("Hello %s,") % recipient }}

{{ _("This is an automated email to let you know that this build has failed:") }}

	/builds/{{ build.uuid }}

{{ _("Jobs") }}:

{% for job in build.jobs %}
	* {{ "%8s" % job.arch }} - {% if job.is_running() %}{{ _("Running") }}{% elif job.has_failed() %}{{ _("Failed") }}{% elif job.has_finished() %}{{ _("Finished") }}{% else %}{{ _("Pending") }}{% end %}{% end %}

{{ _("Sincerely,") }}
-{{ _("The Pakfire Build Service") }}
