{##############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
##############################################################################}

{% from "users/macros.html" import Avatar with context %}

{% macro JobList(jobs, show_arch_only=False, show_controls=False, show_type=True) %}
	<article class="panel">
		{% for job in jobs %}
			<div class="panel-block is-block">
				<div class="level">
					<div class="level-left">
						<div class="level-item">
							<span class="icon is-medium">
								{# Halted #}
								{% if job.is_halted() %}
									<i class="fa-solid fa-pause fa-2x"></i>

								{# Queued #}
								{% elif job.is_queued() %}
									<i class="fa-solid fa-hourglass-start fa-2x"></i>

								{# Dependency Problems #}
								{% elif job.is_pending(installcheck=False) %}
									<i class="fa-solid fa-arrows-turn-to-dots has-text-warning fa-2x"></i>

								{# Pending #}
								{% elif job.is_pending() %}
									<i class="fa-solid fa-hourglass-start fa-2x"></i>

								{# Running #}
								{% elif job.is_running() %}
									<i class="fa-solid fa-spinner fa-spin fa-2x"></i>

								{# Aborted #}
								{% elif job.is_aborted() %}
									<i class="fa-solid fa-xmark fa-2x"></i>

								{# Failed #}
								{% elif job.has_failed() %}
									<i class="fa-solid fa-xmark has-text-danger fa-2x"></i>

								{# Finished #}
								{% elif job.has_finished() %}
									<i class="fa-solid fa-check has-text-success fa-2x"></i>
								{% endif %}
							</span>
						</div>

						<div class="level-item">
							<div>
								<h6 class="title is-6">
									{% if show_arch_only %}
										{{ job.arch }}
									{% elif job.is_deleted() %}
										<span class="icon-text">
											<span>{{ job }}</span>

											<span class="icon has-text-grey" title="{{ _("Deleted") }}">
												<i class="fa-solid fa-trash"></i>
											</span>
										</span>
									{% else %}
										<a href="/builds/{{ job.build.uuid }}">
											{{ job }}
										</a>
									{% endif %}
								</h6>

								{# Running #}
								{% if job.is_running() %}
									<p class="has-text-grey">
										<small>
											{{ _("Started %s...") % job.started_at | format_date(shorter=True) }}
										</small>
									</p>

								{# Aborted #}
								{% elif job.is_aborted() %}
									<p class="has-text-danger">
										<small>
											{% set args = {
												"when" : job.aborted_at | format_date(shorter=True),
												"who"  : job.aborted_by,
											} %}

											{% if job.aborted_by %}
												{{ _("Was aborted %(when)s by %(who)s") % args }}
											{% else %}
												{{ _("Was automatically aborted %(when)s") % args }}
											{% endif %}
										</small>
									</p>

								{# Failed #}
								{% elif job.has_failed() %}
									<p class="has-text-danger">
										<small>
											{% set args = {
												"duration" : job.duration | format_time,
												"when"     : job.finished_at | format_date(shorter=True),
											} %}

											{{ _("Failed %(when)s after %(duration)s") % args }}
										</small>
									</p>

								{# Finished #}
								{% elif job.has_finished() %}
									<p class="has-text-success">
										<small>
											{% set args = {
												"duration" : job.duration | format_time,
												"when"     : job.finished_at | format_date(shorter=True),
											} %}

											{{ _("Finished %(when)s in %(duration)s") % args }}
										</small>
									</p>

								{# Dependency Problems #}
								{% elif job.is_pending(installcheck=False) %}
									<p>
										<small>
											<ul class="has-text-warning">
												{% for line in job.message.splitlines() %}
													<li>
														{{ job.message }}
													</li>
												{% endfor %}
											</ul>
										</small>
									</p>
								{% endif %}
							</div>
						</div>
					</div>

					<div class="level-right">
						{# Show controls? #}
						{% if show_controls %}
							{# Retry? #}
							{% if job.can_be_retried() %}
								<div class="level-item">
									<a class="button is-small is-warning" href="/jobs/{{ job.uuid }}/retry">
										<span class="icon">
											<i class="fa-solid fa-repeat"></i>
										</span>

										<span>
											{{ _("Retry") }}
										</span>
									</a>
								</div>

							{# Abort? #}
							{% elif job.is_running() %}
								<div class="level-item">
									<a class="button is-small is-dark" href="/jobs/{{ job.uuid }}/abort">
										<span class="icon">
											<i class="fa-solid fa-stop"></i>
										</span>

										<span>
											{{ _("Abort") }}
										</span>
									</a>
								</div>
							{% endif %}
						{% endif %}

						{# Logs #}
						{% if job.show_log() %}
							<div class="level-item">
								{% if job.preceeding_jobs %}
									<div class="dropdown is-up is-right">
										<div class="dropdown-trigger">
											<a class="button is-text" aria-haspopup="true"
													aria-controls="dropdown-logs-{{ job.uuid }}">
												<span class="icon">
													<i class="fa-solid fa-file-lines"></i>
												</span>

												<span class="icon is-small">
													<i class="fas fa-angle-up" aria-hidden="true"></i>
												</span>
											</a>
										</div>

										<div class="dropdown-menu" id="dropdown-logs-{{ job.uuid }}" role="menu">
											<div class="dropdown-content">
												{% for j in job.all_jobs | reverse %}
													<a class="dropdown-item" href="/jobs/{{ j.uuid }}/log"
															{% if not j.has_log() %}disabled{% endif %}>
														{{ j.created_at | format_date }}
													</a>
												{% endfor %}
											</div>
										</div>
									</div>
								{% else %}
									<a class="button is-text" href="/jobs/{{ job.uuid }}/log">
										<span class="icon">
											<i class="fa-solid fa-file-lines"></i>
										</span>
									</a>
								{% endif %}
							</div>
						{% endif %}

						{# Show type? #}
						{% if show_type %}
							{# Scratch Build? #}
							{% if job.build.is_scratch() %}
								<span class="level-item">
									<a href="{{ job.build.owner.link }}">
										<figure class="image is-24x24">
											{{ Avatar(job.build.owner, size=24) }}
										</figure>
									</a>
								</span>

							{# Test Build? #}
							{% elif job.build.is_test() %}
								<span class="level-item">
									<span class="icon" title="{{ _("Test Build") }}">
										<i class="fa-solid fa-flask"></i>
									</span>
								</span>

							{# Release Build? #}
							{% else %}
								<span class="level-item">
									<span class="icon" title="{{ _("Release Build") }}">
										<i class="fa-solid fa-box"></i>
									</span>
								</span>
							{% endif %}
						{% endif %}
					</div>
				</div>
			</div>
		{% endfor %}
	</article>
{% endmacro %}

{% macro JobLogStream(job, small=False, limit=None) %}
	<ul class="jobs-log-stream {% if small %}is-small{% endif %}"
		data-uuid="{{ job.uuid }}" {% if limit %}data-limit="{{ limit }}"{% endif %}></ul>
{% endmacro %}
