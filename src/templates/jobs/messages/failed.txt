To: {{ recipient.email_to }}
Subject: [{{ job.name }}] {{ _("Build Job Failed") }}
Message-Id: <job-{{ job.uuid }}-failed@pakfire.ipfire.org>
In-Reply-To: <build-{{ job.build.uuid }}-failed@pakfire.ipfire.org>
References: <build-{{ job.build.uuid }}-failed@pakfire.ipfire.org>

{{ _("Hello %s,") % recipient }}

{{ _("This is an automated email to let you know that this build job has failed:") }}

	/builds/{{ build.uuid }}

{% if log %}
{{ _("Below, you will find an excerpt of the log, which might be helpful looking for the problem.") }}
{% end %}

{{ _("Sincerely,") }}
-{{ _("The Pakfire Build Service") }}

{# Show the log if we have one #}
{% if log %}
	---- {{ _("The last line of the log", "The last %(n)s lines of the log", len(log)) % { "n" : len(log) } }} ----

{% for line in log %}{{ line }}{% end %}
{% end %}
