#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import asyncio
import base64
import datetime
import io
import json
import logging
import mimetypes
import urllib.parse

from . import base
from .decorators import *

# Setup logging
log = logging.getLogger("pbs.bugzilla")

TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

class BugzillaError(Exception):
	pass

class BugzillaInvalidObjectError(BugzillaError):
	pass

class BadRequestError(Exception):
	pass

class Bugzilla(base.Object):
	def init(self, api_key=None):
		if api_key is None:
			api_key = self.backend.config.get("bugzilla", "api-key")

		# Store the API key
		self.api_key = api_key

	@property
	def url(self):
		"""
			Returns the base URL of a Bugzilla instance
		"""
		return self.backend.config.get("bugzilla", "url")

	async def whoami(self):
		"""
			Returns the email address of the logged in user
		"""
		response = await self._request("GET", "/rest/whoami")

		# Return the email address
		return response.get("name")

	async def get_products(self):
		"""
			Returns a dictionary with all products and versions
		"""
		products = {}

		# Fetch all products
		response = await self._request("GET", "/rest/product", { "type" : "accessible" })

		# Walk through all products
		for product in response.get("products", []):
			# Fetch name
			name = product.get("name")

			# Fetch versions
			versions = [v.get("name") for v in product.get("versions")]

			# Add the product and versions
			products[name] = versions

		return products

	@property
	async def fields(self):
		"""
			Fetch all fields
		"""
		return await self._request("GET", "/rest/field/bug")

	def make_url(self, *args, **kwargs):
		"""
			Composes a URL based on the base URL
		"""
		url = urllib.parse.urljoin(self.url, *args)

		# Append any query arguments
		if kwargs:
			url = "%s?%s" % (url, urllib.parse.urlencode(kwargs))

		return url

	async def _request(self, method, url, data=None):
		if data is None:
			data = {}

		# Headers
		headers = {
			# Authenticate all requests
			"X-BUGZILLA-API-KEY" : self.api_key,
		}

		# Make the URL
		url = self.make_url(url)

		# Fallback authentication because some API endpoints
		# do not accept the API key in the header
		data |= { "api_key" : self.api_key }

		# Encode body
		body = None

		# For GET requests, append query arguments
		if method == "GET":
			if data:
				url = "%s?%s" % (url, urllib.parse.urlencode(data))

		# For POST/PUT encode all arguments as JSON
		elif method in ("POST", "PUT"):
			headers |= {
				"Content-Type" : "application/json",
			}

			body = json.dumps(data)

		# Send the request and wait for a response
		res = await self.backend.httpclient.fetch(url, method=method,
			headers=headers, body=body)

		# Decode JSON response
		body = json.loads(res.body)

		# Check for any errors
		if "error" in body:
			# Fetch code and message
			code, message = body.get("code"), body.get("message")

			# 51 - Invalid object
			if code == 51:
				raise BugzillaInvalidObjectError(message)

			# Handle any so far unhandled errors
			else:
				raise BugzillaError(message)

		# Return an empty response
		return body

	def bug_url(self, id):
		"""
			Creates a URL to a bug report
		"""
		return self.make_url("/show_bug.cgi", id=id)

	def enter_url(self, component, product=None, version=None):
		"""
			Creates an URL to create a new bug for package
		"""
		return self.make_url("/enter_bug.cgi", product=product, version=version,
			component=component)

	def list_url(self, component, product=None, version=None):
		"""
			Creates an URL to list all bugs for package
		"""
		return self.make_url("/buglist.cgi", product=product, version=version,
			component=component)

	async def version(self):
		"""
			Returns the version number of Bugzilla
		"""
		response = await self._request("GET", "/rest/version")

		return response.get("version")

	async def search(self, only_open_bugs=True, **kwargs):
		# Add filters to only have open bugs
		if only_open_bugs:
			kwargs |= {
				"resolution" : "---",
			}

		# Send request
		response = await self._request("GET", "/rest/bug", data=kwargs)

		# Parse the response
		bugs = [Bug(self.backend, self, data) for data in response.get("bugs")]

		# Sort and return in reverse order
		return sorted(bugs, reverse=True)

	async def get_bugs(self, bugs):
		"""
			Fetches multiple bugs concurrently
		"""
		tasks = []

		async with asyncio.TaskGroup() as tg:
			for bug in bugs:
				tg.create_task(
					self.get_bug(bug),
				)

		# Return the result from all tasks
		return [task.result() for task in tasks]

	async def get_bug(self, bug):
		"""
			Fetches one bug
		"""
		log.debug("Fetching bug %s" % bug)

		response = await self._request("GET", "/rest/bug/%s" % bug)

		for data in response.get("bugs"):
			return Bug(self.backend, self, data)

	async def create_bug(self, product, component, version, summary, **kwargs):
		data = {
			# Mandatory fields
			"product"         : product,
			"component"       : component,
			"version"         : version,
			"summary"         : summary,
		} | kwargs

		# Create the bug
		response = await self._request("POST", "/rest/bug", data=data)

		# Fetch the bug ID
		id = response.get("id")
		if not id:
			raise RuntimeError("No bug ID received")

		# Return the Bug object
		return await self.get_bug(id)


class Bug(base.Object):
	def init(self, bugzilla, data):
		self.bugzilla = bugzilla
		self.data = data

	def __repr__(self):
		return "<%s #%s>" % (self.__class__.__name__, self.id)

	def __str__(self):
		return "#%s" % self.id

	def __eq__(self, other):
		if isinstance(other, self.__class__):
			return self.id == other.id

		raise NotImplemented

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.created_at < other.created_at

		raise NotImplemented

	@property
	def id(self):
		return self.data.get("id")

	@property
	def url(self):
		return self.bugzilla.make_url("/show_bug.cgi", id=self.id)

	@property
	def created_at(self):
		t = self.data.get("creation_time")

		return datetime.datetime.strptime(t, TIME_FORMAT)

	@property
	def summary(self):
		return self.data.get("summary")

	@property
	def component(self):
		return self.data.get("component")

	@property
	def creator(self):
		creator = self.data.get("creator")

		return self.backend.users.get_by_email(creator)

	@property
	def assignee(self):
		assignee = self.data.get("assigned_to")

		return self.backend.users.get_by_email(assignee)

	@property
	def status(self):
		return self.data.get("status")

	@property
	def resolution(self):
		return self.data.get("resolution")

	@property
	def severity(self):
		return self.data.get("severity")

	def is_closed(self):
		return not self.data["is_open"]

	@property
	def keywords(self):
		return self.data.get("keywords")

	# Update

	async def _update(self, data):
		"""
			Low-level updater
		"""
		response = await self.bugzilla._request("PUT", "/rest/bug/%s" % self.id, data=data)

		# XXX Update data from response

	async def update(self, comment=None, **data):
		"""
			Updates the bug
		"""
		# Comment
		if comment:
			data |= {
				"comment" : {
					"body" : comment,
				}
			}

		return await self._update(data)

	# Close

	async def close(self, resolution, comment=None):
		args = {
			"status"     : "CLOSED",
			"resolution" : resolution,
			"comment"    : comment,
		}

		log.debug("Closing bug #%s" % self.id)

		# Send the request
		await self.update(**args)

	# Attachments

	async def attach(self, filename, data, summary, content_type=None, is_patch=False):
		"""
			Attaches something to an existing bug
		"""
		# Guess the mimetype if none was provided
		if content_type is None:
			content_type, encoding = mimetypes.guess_type(filename)

		# Assemble the request
		data = {
			# Mandatory fields
			"ids"          : self.id,
			"summary"      : summary,
			"file_name"    : filename,
			"data"         : self._encode_to_base64(data),
			"content_type" : content_type,
		}

		# Is this a patch?
		if is_patch:
			data |= { "is_patch" : is_patch }

		# Send request
		await self.bugzilla._request("POST", "/rest/bug/%s/attachment" % self.id, data=data)

	@staticmethod
	def _encode_to_base64(data):
		"""
			This function takes a file-like object, a bytes-like object or string
			and returns it encoded as base64.
		"""
		# Read all data from file handles
		if isinstance(data, io.IOBase):
			data = data.read()

		# Convert to bytes()
		if not isinstance(data, bytes):
			data = data.encode()

		# Encode to base64
		data = base64.b64encode(data)

		# Convert to string
		return data.decode()
