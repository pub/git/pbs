#!/usr/bin/python

import asyncio
import datetime
import functools
import itertools
import logging
import os
import re

import sqlalchemy
from sqlalchemy import Column, ForeignKey, Index
from sqlalchemy import Boolean, DateTime, Integer, Text, UUID

from . import base
from . import builds
from . import database
from . import jobs
from . import packages
from . import repos
from . import users

from .i18n import N_
from .constants import *
from .decorators import *

# Setup logging
log = logging.getLogger("pbs.builds")

class Builds(base.Object):
	async def get_by_uuid(self, uuid):
		stmt = (
			sqlalchemy
			.select(Build)
			.where(
				Build.deleted_at == None,
				Build.uuid == uuid,
			)
		)

		return await self.db.fetch_one(stmt)

	async def get_latest_by_name(self, name):
		"""
			Returns the latest build that matches the package name
		"""
		stmt = (
			sqlalchemy
			.select(Build)
			.where(
				Build.deleted_at == None,

				# Don't include test builds
				Build.test == False,
			)

			# Join packages and filter by package name
			.join(packages.Package)
			.where(
				packages.Package.name == name,
			)

			# Pick the latest build
			.order_by(
				Build.created_at.desc(),
			)
			.limit(1)
		)

		return await self.db.fetch_one(stmt)

	async def get(self, name=None, user=None, distro=None, repo=None, scratch=None,
			limit=None, offset=None):
		"""
			Returns all builds by this name
		"""
		source_packages = sqlalchemy.orm.aliased(Build.pkg)

		stmt = (
			sqlalchemy
			.select(Build)
			.join(
				source_packages,
			)
			.where(
				Build.deleted_at == None,

				# Always filter out any test builds
				Build.test == False,
			)

			# Order by creation date
			.order_by(
				Build.created_at.desc(),
			)

			# Limit & Offset
			.limit(limit)
			.offset(offset)
		)

		# Scratch builds only?
		if scratch is True:
			stmt = stmt.where(
				Build.owner != None,
			)

		# Exclude scratch builds?
		elif scratch is False:
			stmt = stmt.where(
				Build.owner == None,
			)

		# Optionally filter by name
		if name:
			stmt = stmt.where(
				source_packages.c.deleted_at == None,
				source_packages.c.name == name,
			)

		# Optionally filter by user
		if user:
			stmt = stmt.where(
				Build.owner == user,
			)

		# Optionally filter by distro
		if distro:
			# XXX This cannot access distro
			stmt = stmt.where(
				source_packages.c.distro_id == distro.id,
			)

		# Optionally filter by repo
		if repo:
			stmt = (
				stmt
				.join(
					repos.RepoBuild,
					repos.RepoBuild.build_id == Build.id,
				).where(
					repos.RepoBuild.removed_at == None,
					repos.RepoBuild.repo == repo,
				)
				.order_by(
					repos.RepoBuild.added_at.desc(),
				)
			)

		return await self.db.fetch_as_list(stmt)

	def get_release_builds_by_name(self, name, limit=None):
		cte = (
			sqlalchemy.select(
				Build,

				# Number all builds per distribution so that we can filter out
				# the first N builds later
				sqlalchemy.func.row_number()
				.over(
					partition_by = packages.Package.distro_id,
					order_by     = Build.created_at.desc(),
				)
				.label("_number"),
			)
			.join(Build.pkg)
			.where(
				Build.deleted_at == None,
				Build.owner_id == None,
				packages.Package.deleted_at == None,
				packages.Package.name == name,
			)
			.cte("_builds")
		)

		stmt = (
			sqlalchemy.select(Build)
			.select_from(cte)
			.order_by(cte.c._number)
			.limit(limit)
		)

		return self.db.fetch(stmt)

	def get_scratch_builds_by_name(self, name, limit=None):
		cte = (
			sqlalchemy.select(
				Build,

				# Number all builds per distribution so that we can filter out
				# the first N builds later
				sqlalchemy.func.row_number()
				.over(
					partition_by = Build.owner_id,
					order_by     = Build.created_at.desc(),
				)
				.label("_number"),
			)
			.join(Build.pkg)
			.where(
				Build.deleted_at == None,
				Build.owner_id != None,
				packages.Package.deleted_at == None,
				packages.Package.name == name,
			)
			.cte("_builds")
		)

		stmt = (
			sqlalchemy.select(Build)
			.select_from(cte)
			.order_by(cte.c._number)
			.limit(limit)
		)

		return self.db.fetch(stmt)

	async def get_by_package_uuids(self, uuids):
		"""
			Returns a list of builds that contain the given packages
		"""
		source_packages = sqlalchemy.orm.aliased(packages.Package)
		binary_packages = sqlalchemy.orm.aliased(packages.Package)

		stmt = (
			sqlalchemy
			.select(
				Build,
			)
			.distinct()
			.join(
				source_packages,
				source_packages.id == Build.pkg_id,
			)
			.join(
				jobs.Job,
				jobs.Job.build_id == Build.id,
			)
			.join(
				jobs.JobPackage,
				jobs.JobPackage.job_id == jobs.Job.id,
			)
			.join(
				binary_packages,
				binary_packages.id == jobs.JobPackage.pkg_id,
			)
			.where(
				Build.deleted_at == None,
				jobs.Job.deleted_at == None,
				source_packages.deleted_at == None,
				binary_packages.deleted_at == None,
			)
			.where(
				sqlalchemy.or_(
					source_packages.uuid.in_(uuids),
					binary_packages.uuid.in_(uuids),
				),
			)
		)

		return await self.db.fetch_as_set(stmt)

	async def create(self, repo, package, owner=None, group=None, test=False,
			disable_test_builds=False, timeout=None):
		"""
			Creates a new build based on the given distribution and package
		"""
		# Fail if this is not a source package
		if not package.is_source():
			raise RuntimeError("Can only build source packages, not %s" % package.arch)

		# Check if the package exists
		if not package.path or not await self.backend.exists(package.path):
			raise RuntimeError("Package %s does not exist (path = %s)" % (package, package.path))

		# Set a default timeout
		if timeout is None:
			timeout = datetime.timedelta(hours=3)

		# Insert the build into the database
		build = await self.db.insert(
			Build,
			repo                = repo,
			pkg                 = package,
			owner               = owner,
			group               = group,
			test                = test,
			disable_test_builds = disable_test_builds,
		)

		# Create all jobs
		await build._create_jobs(timeout=timeout)

		if not build.is_test():
			# Deprecate previous builds
			await build._deprecate_others()

			# Add watchers
			await build._add_watchers()

			# Add the build into its repository
			await repo.add_build(build, user=owner)

		return build

	async def launch(self, builds):
		"""
			Called to launch all given builds
		"""
		jobs = []

		# Collect all jobs from all builds
		for build in builds:
			jobs += build.jobs

		# Launch all jobs
		await self.backend.jobs.launch(jobs)

	# Groups

	async def get_group_by_uuid(self, uuid):
		"""
			Fetch a group by its UUID
		"""
		stmt = (
			sqlalchemy
			.select(BuildGroup)
			.where(
				BuildGroup.deleted_at == None,
				BuildGroup.uuid == uuid,
			)
		)

		return await self.db.fetch_one(stmt)

	async def create_group(self, owner=None, tested_build=None):
		"""
			Creates a new Build Group
		"""
		group = await self.db.insert(
			BuildGroup,
			created_by   = owner,
			tested_build = tested_build,
		)

		return group


class Build(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "builds"

	def __str__(self):
		return "%s %s" % (self.pkg.name, self.pkg.evr)

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.pkg < other.pkg or self.created_at < other.created_at

		return NotImplemented

	# ID

	id = Column(Integer, primary_key=True)

	@property
	def url(self):
		return "/builds/%s" % self.uuid

	@property
	def bugzilla_fields(self):
		fields = {
			# Component
			"component" : self.pkg.name,

			# Send URL to build
			"url"       : self.backend.url_to(self.url),
		}

		# Add the distribution fields
		fields |= self.distro.bugzilla_fields

		return fields

	def can_be_deleted(self, user=None):
		"""
			Returns True if this build can be deleted
		"""
		# This must be a scratch build
		if not self.is_scratch():
			return False

		# The user must have according permissions
		if self.has_perm(user):
			return True

		# You can't touch this
		return False

	async def delete(self, deleted_by):
		"""
			Deletes this build including all jobs,
			packages and the source package.
		"""
		# Check if we can actually delete
		if not self.can_be_deleted(deleted_by):
			raise PermissionError

		# Delete all test builds
		tests = await self.get_tests()
		if tests:
			await tests.delete(deleted_by=deleted_by)

		# Delete all jobs
		for job in self.alljobs:
			await job.delete(deleted_by=deleted_by)

		# Mark as deleted
		await super().delete(deleted_by=deleted_by)

		# Delete source package
		await self.pkg.delete(deleted_by=deleted_by)

		# Unless this was a test build, all repositories
		# this build has been in have been changed.
		if not self.is_test():
			await self._update_repos(build=True)

	# UUID

	uuid = Column(UUID, unique=True, nullable=False,
		server_default=sqlalchemy.func.gen_random_uuid())

	# Package ID

	pkg_id = Column(Integer, ForeignKey("packages.id"), nullable=False)

	# Package

	pkg = sqlalchemy.orm.relationship("Package", foreign_keys=[pkg_id], lazy="selectin")

	@property
	def name(self):
		return "%s-%s" % (self.pkg.name, self.pkg.evr)

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Date

	@property
	def date(self):
		return self.created_at.date()

	# Finished At

	finished_at = Column(DateTime(timezone=False))

	# Owner ID

	owner_id = Column(Integer, ForeignKey("users.id"))

	# Owner

	owner = sqlalchemy.orm.relationship("User", foreign_keys=[owner_id], lazy="joined")

	# Is this a scratch build?

	def is_scratch(self):
		# We must have an owner
		if self.owner is None:
			return False

		# Test builds don't count as scratch builds
		if self.is_test():
			return False

		return True

	# Repo ID

	repo_id = Column(Integer, ForeignKey("repositories.id"), nullable=False)

	# Repo

	repo = sqlalchemy.orm.relationship("Repo", lazy="selectin")

	# Distro

	@functools.cached_property
	def distro(self):
		return self.repo.distro

	# Group ID

	group_id = Column(Integer, ForeignKey("build_groups.id"))

	# Group

	group = sqlalchemy.orm.relationship(
		"BuildGroup", foreign_keys=[group_id], back_populates="builds", lazy="selectin",
	)

	# Severity

	severity = Column(Text)

	# Commit ID

	commit_id = Column(Integer, ForeignKey("source_commits.id"))

	# Commit

	commit = sqlalchemy.orm.relationship(
		"SourceCommit", foreign_keys=[commit_id], lazy="selectin",
	)

	def has_perm(self, user):
		"""
			Check, if the given user has the right to perform administrative
			operations on this build.
		"""
		if user is None:
			return False

		# Admins always have permission
		if user.is_admin():
			return True

		# Owners have permission
		if self.owner == user:
			return True

		# No permission
		return False

	# Priority

	priority = Column(Integer, nullable=False, default=0)

	# Arches

	@property
	def arches(self):
		"""
			Returns a list of all supported arches for this build
		"""
		# If the package doesn't filter anything, report everything the distro supports
		if not self.pkg.build_arches:
			return self.distro.arches

		# Handle the special case of "noarch"
		if "noarch" in self.pkg.build_arches:
			return ["noarch"]

		# Otherwise we return all supported arches
		return [arch for arch in self.distro.arches if arch in self.pkg.build_arches]

	# Jobs - This fetches all jobs that have ever existed for this build

	alljobs = sqlalchemy.orm.relationship("Job", back_populates="build", lazy="joined")

	@property
	def jobs(self):
		"""
			Returns the current set of jobs
		"""
		for job in self.alljobs:
			# Skip any superseeded jobs
			if job.is_superseeded():
				continue

			yield job

	async def _create_jobs(self, **kwargs):
		"""
			Called after a build has been created and creates all jobs
		"""
		# Create the jobs
		for arch in self.arches:
			await self.backend.jobs.create(self, arch, **kwargs)

	async def _job_finished(self, job):
		"""
			Called when a job finished.

			This function will determine the status of the build and run and actions.
		"""
		# Nothing to do, if this build has already finished
		if self.has_finished():
			return

		# If all jobs have finished, the build has finished
		if all((j.has_finished() for j in self.jobs)):
			return await self.finished(success=True)

		# If there are any failed jobs, the build has failed
		elif any((j.has_failed() for j in self.jobs)):
			return await self.finished(success=False)

	# Packages

	@property
	def packages(self):
		"""
			Returns a list of all built packages
		"""
		# Collect all packages
		packages = itertools.chain(*(job.packages for job in self.jobs))

		packages = { pkg.nevra : pkg for pkg in packages }

		# Return as list
		return packages.values()

	## Comment stuff

	async def comment(self, **kwargs):
		"""
			Submits a comment
		"""
		# Create a new comment
		comment = await self.db.insert(
			BuildComment, build=self, **kwargs,
		)

		# Notify
		await comment.notify()

	comments = sqlalchemy.orm.relationship(
		"BuildComment", back_populates="build", lazy="selectin",
	)

	# Deleted By ID

	deleted_by_id = Column(Integer, ForeignKey("users.id"))

	# Deleted By

	deleted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deleted_by_id], lazy="selectin",
	)

	# Add Points

	async def add_points(self, points, user=None):
		"""
			Add points (can be negative)
		"""
		# Log points
		await self.db.insert(
			BuildPoint, build=self, points=points, user=user,
		)

		# Update the cache
		self.points += points

	# Points

	points = Column(Integer, nullable=False, default=0)

	## Watchers

	async def get_watchers(self):
		stmt = (
			sqlalchemy

			# Select all build watchers
			.select(BuildWatcher)
			.where(
				BuildWatcher.deleted_at == None,
				BuildWatcher.build == self,
			)
		)

		return await self.db.fetch_as_set(stmt)

	async def watched_by(self, user):
		"""
			Checks if this build is being watched by user.

			Returns the BuildWatcher object.
		"""
		stmt = (
			sqlalchemy
			.select(BuildWatcher)
			.where(
				BuildWatcher.build == self,
				BuildWatcher.user == user,
				BuildWatcher.deleted_at == None,
			)
		)

		return await self.db.fetch_one(stmt)

	async def add_watcher(self, user):
		"""
			Adds a watcher to this build
		"""
		# Don't add the same watcher more than once
		# SQLAlchemy does not have an elegant way to do an upsert here or to ignore
		# and additional inserts, so we will have to check ourselves. Oh well.
		if await self.watched_by(user):
			return

		# Add the new watcher
		return await self.db.insert(
			BuildWatcher,
			build = self,
			user  = user,
		)

	async def remove_watcher(self, user):
		"""
			Removes a watcher from this build
		"""
		watcher = await self.watched_by(user)

		# If we have found a watcher, we will delete it
		if watcher:
			await watcher.delete()

	async def _add_watchers(self):
		"""
			Called when a new build is created and automatically adds any watchers
		"""
		# Add the owner of the build (if any)
		if self.owner:
			await self.add_watcher(self.owner)

		# XXX extract any watchers from the commit message (if any)

		# XXX add any permanent watchers

	# Actions

	async def finished(self, success):
		"""
			Called when this build has successfully finished
		"""
		builds = None

		# Log this
		if success:
			log.info("Build %s has finished" % self)
		else:
			log.error("Build %s has failed" % self)

		# Mark as finished
		self.finished_at = sqlalchemy.func.current_timestamp()

		# Flush and refresh
		await self.db.flush_and_refresh(self)

		# Mark as failed if the build was not successful
		if not success:
			self.failed = True

		# Award some negative points on failure
		if not success:
			await self.add_points(-1)

		# Notify everyone this build has finished...
		if success:
			# Send a push notification
			await self._send_push_message(
				title = N_("Build Successful"),
				body  = "%s" % self,
			)

			# Send an email
			await self._send_email("builds/messages/finished.txt")
		# ... or that it has failed
		else:
			# Send a push notification
			await self._send_push_message(
				title = N_("Build Failed"),
				body  = "%s" % self,
			)

			# Send an email
			await self._send_email("builds/messages/failed.txt")

		# Create any test builds
		if success:
			builds = await self.create_test_builds()

		# Notify the group that build has finished
		if self.group:
			await self.group._build_finished(self)

		# Notify release monitoring that this build has finished
		if self.monitoring_release:
			await self.monitoring_release._build_finished()

		return builds

	# Finished?

	def has_finished(self):
		"""
			Returns True if this build has finished
		"""
		if self.finished_at:
			return True

		return False

	# Failed

	failed = Column(Boolean, nullable=False, default=False)

	# Failed?

	def has_failed(self):
		"""
			Returns True if this build has failed
		"""
		return self.has_finished() and self.failed is True

	# Successful?

	def is_successful(self):
		"""
			Returns True if this build was successful
		"""
		return self.has_finished() and self.failed is False

	async def _send_push_message(self, *args, exclude=None, **kwargs):
		"""
			Convenience function which sends a push notification to everybody
		"""
		watchers = await self.get_watchers()

		for watcher in watchers:
			# Skip some people
			if exclude and watcher.user in exclude:
				continue

			# Send a push notification
			await watcher.user.send_push_message(*args, **kwargs)

	async def _send_email(self, *args, exclude=None, **kwargs):
		"""
			Convenience function which sends an email to everybody who would care
		"""
		watchers = await self.get_watchers()

		for watcher in watchers:
			# Skip some people
			if exclude and watcher.user in exclude:
				continue

			# Send an email to the user
			await watcher.user.send_email(*args, build=self, **kwargs)

	# Repos

	repos = sqlalchemy.orm.relationship(
		"Repo",
		secondary = "repository_builds",
		primaryjoin = """and_(
			RepoBuild.build_id == Build.id,
			RepoBuild.removed_at == None
		)""",
		viewonly  = True,
		lazy      = "selectin",
	)

	async def _update_repos(self, build=False):
		"""
			This method should be called if the repositories should be updated
		"""
		# Remaster the build repository?
		if build:
			await self.repo.changed()

		# Remaster all other repositories
		for repo in self.repos:
			await repo.changed()

	@property
	def next_repo(self):
		for repo in self.repos:
			return self.distro.get_next_repo(repo)

	def can_be_approved(self, user=None):
		if not user or not user.is_admin():
			return False

		if self.owner:
			return False

		# XXX Disabled because of the next_repo bullshit
		return False

		# This can only be approved if there is another repository
		if self.next_repo:
			return True

		return False

	async def approve(self, user):
		"""
			Moves this build to the next stage
		"""
		# Fetch the next repository
		next_repo = self.next_repo

		# Remove the build from all repositories
		for repo in self.repos:
			await repo.remove_build(build=self, user=user)

		# Add it to the next repository
		await next_repo.add_build(build=self, user=user)

	# Bugs

	async def _find_bug(self, bug_id):
		"""
			Fetches a specific bug
		"""
		stmt = (
			sqlalchemy
			.select(
				BuildBug,
			)
			.where(
				BuildBug.build == self,
				BuildBug.bug_id == bug_id,
				BuildBug.removed_at == None,
			)
		)

		return await self.db.fetch_one(stmt)

	async def add_bug(self, bug_id, added_by=None):
		"""
			Adds a new bug to this build
		"""
		bug = await self.db.insert(
			BuildBug,
			build    = self,
			bug_id   = bug_id,
			added_by = added_by,
		)

		# Log action
		log.info("Added %s to %s" % (bug, self))

		return bug

	async def remove_bug(self, bug_id, removed_by=None):
		"""
			Removes a bug from this build
		"""
		bug = await self._find_bug(bug_id)

		# Nothing to do if we couldn't find the bug
		if not bug:
			return

		# Remove it
		bug.remove(removed_by=removed_by)

		# Log action
		log.info("Removed %s from %s" % (bug, self))

		return bug

	async def get_bugs(self):
		"""
			Fetch all bugs from Bugzilla
		"""
		stmt = (
			sqlalchemy
			.select(
				BuildBug.bug_id,
			)
			.where(
				BuildBug.build == self,
				BuildBug.removed_at == None,
			)
		)

		# Fetch all bug IDs
		bug_ids = [b.bug_id async for b in self.db.select(stmt)]

		# Fetch the bugs from Bugzilla
		return await self.backend.bugzilla.get_bugs(bug_ids)

	# Monitoring Release

	@functools.cached_property
	async def monitoring_release(self):
		"""
			Returns the Monitoring Release
		"""
		return await self.backend.monitorings._get_release("""
			SELECT
				*
			FROM
				release_monitoring_releases
			WHERE
				build_id = %s
			""", self.id,

			# Populate cache
			build=self,
		)

	# Deprecation

	async def _deprecate_others(self):
		"""
			Called when this build is being created.

			This method will find similar builds and automatically deprecate them.
		"""
		builds = await self.backend.builds._get_builds("""
			SELECT
				builds.*

			-- Walk through all builds
			FROM
				builds

			-- Find all packages belonging to the builds
			LEFT JOIN
				packages ON builds.pkg_id = packages.id

			-- Find all packages that match the name
			LEFT JOIN
				packages other_packages ON
					other_packages.name = packages.name
				AND
					other_packages.distro_id = packages.distro_id

			-- Find all other builds (excluding ourselves)
			LEFT JOIN
				builds other_builds ON
					other_builds.id <> builds.id
				AND
					other_builds.pkg_id = other_packages.id
				AND
					other_builds.repo_id = builds.repo_id

			-- Start with this build
			WHERE
				other_builds.id = %s

			-- Exclude any deleted packages
			AND
				packages.deleted_at IS NULL
			AND
				other_packages.deleted_at IS NULL

			-- Exclude any deleted builds
			AND
				builds.deleted_at IS NULL

			-- Exclude everything that is already deprecated
			AND
				builds.deprecated_at IS NULL
			""", self.id,
		)

		# Deprecate all builds
		for build in builds:
			await build.deprecate(build=self)

	async def deprecate(self, build=None, user=None):
		"""
			Called when a build needs to be deprecated
		"""
		if build:
			log.debug("Deprecating %s by %s" % (self, build))
		else:
			log.debug("Deprecating %s" % self)

		# Set user from build if not set
		if not user and build:
			user = build.owner

		# Mark as deprecated
		await self._set_attribute_now("deprecated_at")
		if user:
			await self._set_attribute("deprecated_by", user)

		# Store the build
		if build:
			self.deprecating_build = build

	# Deprecated At

	deprecated_at = Column(DateTime(timezone=False))

	# Deprecated?

	def is_deprecated(self):
		if self.deprecated_at:
			return True

		return False

	@functools.cached_property
	async def deprecated_by(self):
		if self.data.deprecated_by:
			return await self.backend.users.get_by_id(self.data.deprecated_by)

	# Deprecated By ID

	deprecated_by_id = Column(Integer, ForeignKey("users.id"))

	# Deprecated By

	deprecated_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deprecated_by_id], lazy="selectin",
	)

	# Deprecating Build ID

	deprecating_build_id = Column(Integer, ForeignKey("builds.id"))

	deprecating_build = sqlalchemy.orm.relationship(
		"Build", foreign_keys=[deprecating_build_id],
	)

	@functools.cached_property
	async def deprecated_builds(self):
		"""
			Returns a list of builds that were deprecated by this build
		"""
		builds = await self.backend.builds._get_builds("""
			SELECT
				*
			FROM
				builds
			WHERE
				deleted_at IS NULL
			AND
				deprecated_by = %s
			ORDER BY
				created_at
			""", self.id,
		)

		return list(builds)

	# Reverse Requires

	async def reverse_requires(self):
		"""
			Returns a list of builds that reverse-depend on this build
		"""
		log.debug("Calculating reverse requires for %s..." % self)

		# Fetch reverse requires for all jobs
		reverse_requires = [await job._reverse_requires() for job in self.jobs]

		# Join all builds together
		return itertools.chain(*reverse_requires)

	# Tests Builds

	test = Column(Boolean, nullable=False, default=False)

	# Test?

	def is_test(self):
		return self.test

	# Disable Test Builds?

	disable_test_builds = Column(Boolean, nullable=False, default=False)

	async def create_test_builds(self):
		"""
			This creates test builds for this build
		"""
		# You cannot create any test builds for test builds
		if self.is_test():
			return

		# Don't create any test builds if requested to be disabled
		if self.disable_test_builds:
			log.debug("Creating test builds has been disabled for %s" % self)
			return

		builds = {}

		# Map all builds by their name
		for build in await self.reverse_requires():
			try:
				builds[build.pkg.name].append(build)
			except KeyError:
				builds[build.pkg.name] = [build]

		# End here if there are no builds to create
		if not builds:
			return

		# Fetch all watchers
		watchers = await self.get_watchers()

		# Create a build group for all tests
		group = await self.backend.builds.create_group(
			owner        = self.owner,
			tested_build = self,
		)

		# Create a test build only for the latest version of each package
		for name in builds:
			build = max(builds[name])

			# Create a new test build
			build = await self.backend.builds.create(
				# Use the same build repository
				repo    = self.repo,
				package = build.pkg,
				owner   = self.owner,
				group   = group,
				test    = True,
			)

			# Add all watchers of the build to the test builds
			for watcher in watchers:
				await build.add_watcher(watcher.user)

		# Return the group
		return group

	async def _test_builds_finished(self, success):
		"""
			Called when all test builds have finished
		"""
		if not success:
			# Take away more points
			self.add_points(-2)

			# Send an email on fail
			await self._send_email("builds/messages/test-builds-failed.txt",
				build=self, test_builds=self.test_builds)

	# Tests

	async def get_tests(self):
		"""
			Returns the test group
		"""
		stmt = (
			sqlalchemy
			.select(
				BuildGroup,
			)
			.where(
				BuildGroup.deleted_at == None,
				BuildGroup.tested_build == self,
			)
		)

		return await self.db.fetch_one(stmt)

	# Clone!

	async def clone(self, owner, repo=None):
		"""
			Creates a clone of this build
		"""
		# Clone into the same repository if none given
		if repo is None:
			repo = self.repo

		# Create a new build from the same package
		clone = await self.backend.builds.create(
			# Build the same package
			package = self.pkg,

			# Owner
			owner   = owner,

			# Repository
			repo    = repo,
		)

		log.info("Cloned %s as %s" % (self, clone))

		return clone


class BuildGroup(database.Base, database.SoftDeleteMixin):
	__tablename__ = "build_groups"

	def __str__(self):
		return "%s" % self.uuid

	def __iter__(self):
		"""
			Returns an iterator that sorts the builds
		"""
		builds = sorted(self.builds, key=self._sort_builds)

		return iter(builds)

	def __bool__(self):
		return True

	def __len__(self):
		return len(self.builds)

	# ID

	id = Column(Integer, primary_key=True)

	# UUID

	uuid = Column(UUID, nullable=False, server_default=sqlalchemy.func.gen_random_uuid())

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Created By ID

	created_by_id = Column(Integer, ForeignKey("users.id"))

	# Created By

	created_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[created_by_id], lazy="selectin",
	)

	# Deleted By ID

	deleted_by_id = Column(Integer, ForeignKey("users.id"))

	# Deleted By

	deleted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deleted_by_id], lazy="selectin",
	)

	# Finished At

	finished_at = Column(DateTime(timezone=False), nullable=False)

	# Failed

	failed = Column(Boolean, nullable=False, default=False)

	# Builds

	builds = sqlalchemy.orm.relationship(
		"Build", foreign_keys=[Build.group_id], back_populates="group", lazy="joined",
	)

	@staticmethod
	def _sort_builds(build):
		# Move failed builds to the top
		if build.has_failed():
			return (1, build)

		# After that put successfully finished builds
		elif build.is_successful():
			return (2, build)

		# After that put anything else
		else:
			return (3, build)

	@property
	def successful_builds(self):
		"""
			Returns all successful builds in this group
		"""
		return [b for b in self.builds if b.is_successful()]

	@property
	def failed_builds(self):
		"""
			Returns all failed builds in this group
		"""
		return [b for b in self.builds if b.has_failed()]

	# Tested Build ID

	tested_build_id = Column(Integer, ForeignKey("builds.id"))

	# Tested Build

	tested_build = sqlalchemy.orm.relationship(
		"Build", foreign_keys=[tested_build_id], lazy="joined",
	)

	# Test?

	def is_test(self):
		"""
			Returns True if this is a test group
		"""
		if self.tested_build:
			return True

		return False

	# Delete

	async def delete(self, deleted_by=None):
		"""
			Deletes this group
		"""
		# Delete all builds in this group
		for build in self.builds:
			await build.delete(deleted_by=deleted_by)

		# Delete the group itself
		await super().delete(deleted_by=deleted_by)

	# Functions to find out whether this was all successful/failed

	def has_failed(self):
		"""
			Returns True if at least one build has failed
		"""
		return self.failed is True

	def is_successful(self):
		"""
			Returns True if all jobs have been successful
		"""
		return self.has_finished() and self.failed is False

	def has_finished(self):
		"""
			Returns True if all builds have finished
		"""
		if self.finished_at:
			return True

		return False

	def all_finished(self):
		"""
			Returns True if all builds have finished
		"""
		return all((b.has_finished() for b in self.builds))

	async def _build_finished(self, build):
		"""
			Called when a build has finished
		"""
		# Fail this group if one build has failed
		if build.has_failed():
			await self.failed()

		# Finished when all builds have finished
		if self.all_finished():
			await self.finished()

	async def finished(self):
		"""
			Called when all builds in this group have finished
		"""
		# Only call once
		if self.has_finished():
			return

		log.info("Build group %s has finished" % self)

		# Mark as finished
		self.finished_at = sqlalchemy.func.current_timestamp()

		# Call the build that has created this test group
		if self.tested_build:
			await self.tested_build._test_builds_finished(success=not self.has_failed())

	async def failed(self):
		"""
			Called when this group is considered failed
		"""
		# Do nothing if already failed
		if self.has_failed():
			return

		log.error("Build group %s has failed" % self)

		# Mark as failed
		self.failed = True


class BuildBug(database.Base):
	__tablename__ = "build_bugs"

	def __str__(self):
		return "#%s" % self.bug_id

	# ID

	id = Column(Integer, primary_key=True)

	# Build ID

	build_id = Column(Integer, ForeignKey("builds.id"), index=True, nullable=False)

	# Build

	build = sqlalchemy.orm.relationship("Build")

	# Bug ID

	bug_id = Column(Integer, nullable=False)

	# Added At

	added_at = Column(DateTime(timezone=False), nullable=False,
		default=sqlalchemy.func.current_timestamp())

	# Added By ID

	added_by_id = Column(Integer, ForeignKey("users.id"), nullable=False)

	# Added By

	added_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[added_by_id], lazy="joined", innerjoin=True,
	)

	# Removed At

	removed_at = Column(DateTime(timezone=False))

	# Removed By ID

	removed_by_id = Column(Integer, ForeignKey("users.id"))

	# Removed ID

	removed_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[removed_by_id], lazy="selectin",
	)

	# Remove!

	def remove(self, removed_by=None):
		"""
			Remove the bug from this build
		"""
		self.removed_at = sqlalchemy.func.current_timestamp()

		# Optionally store the user
		if removed_by:
			self.removed_by = removed_by


class BuildComment(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "build_comments"

	# ID

	id = Column(Integer, primary_key=True)

	# Build ID

	build_id = Column(Integer, ForeignKey("builds.id"), index=True, nullable=False)

	# Build

	build = sqlalchemy.orm.relationship("Build", back_populates="comments")

	# User ID

	user_id = Column(Integer, ForeignKey("users.id"), index=True, nullable=False)

	# User

	user = sqlalchemy.orm.relationship("User")

	# Text

	text = Column(Text, nullable=False, default="")

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Notify!

	async def notify(self):
		"""
			Notifies all watchers about this comment (except the user who posted it)
		"""
		await self.build._send_email("builds/messages/comment.txt",
			exclude=[self.user], build=self.build, comment=self)


class BuildPoint(database.Base, database.BackendMixin):
	__tablename__ = "build_points"

	# Build ID

	build_id = Column(Integer, ForeignKey("builds.id"), primary_key=True, nullable=False)

	# Build

	build = sqlalchemy.orm.relationship("Build", foreign_keys=[build_id], lazy="selectin")

	# Created At

	created_at = Column(DateTime(timezone=False), primary_key=True,
		nullable=False, server_default=sqlalchemy.func.current_timestamp())

	# Points

	points = Column(Integer, nullable=False)

	# User ID

	user_id = Column(Integer, ForeignKey("users.id"))

	# User

	user = sqlalchemy.orm.relationship("User", foreign_keys=[user_id], lazy="selectin")


class BuildWatcher(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "build_watchers"

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.added_at < other.added_at or self.user < other.user

		return NotImplemented

	# Build ID

	build_id = Column(Integer, ForeignKey("builds.id"), primary_key=True, nullable=False)

	# Build

	build = sqlalchemy.orm.relationship("Build", lazy="selectin")

	# User ID

	user_id = Column(Integer, ForeignKey("users.id"), primary_key=True, nullable=False)

	# User

	user = sqlalchemy.orm.relationship("User", lazy="joined", innerjoin=True)

	# Added At

	added_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())
