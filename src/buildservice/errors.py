

class BuilderNotOnlineError(Exception):
	"""
		Raised when a builder is unexpectedly not online
	"""
	pass

class CommandExecutionError(Exception):
	"""
		Raised when the executed command at Backend.command() returned an error
	"""
	def __init__(self, returncode, stderr=None):
		self.returncode = returncode
		self.stderr = stderr


class NoSuchDistroError(Exception):
	"""
		Raised when a certain distribution could not be found
	"""
	pass
