###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2023 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import asyncio
import collections
import datetime
import logging
import uuid

from . import base

# Setup logging
log = logging.getLogger("pbs.logstreamer")

BUFFER_MAX_SIZE = 2048

class LogStreams(base.Object):
	streams = {}

	async def open(self, job):
		"""
			Opens a new stream
		"""
		# Fail if a stream already exists
		if job.uuid in self.streams:
			raise ValueError("Stream for %s exists" % job)

		# Create a new stream
		stream = LogStream(self.backend, job.uuid)

		# Register the stream
		self.streams[job.uuid] = stream

		# Turn on streaming
		await job.enable_log_streaming()

		return stream

	async def close(self, uuid):
		"""
			Closes the stream for a job
		"""
		# Fetch the job
		job = await self.backend.jobs.get_by_uuid(uuid)

		# Turn off the log stream
		if job:
			await job.disable_log_streaming()

		try:
			del self.streams[uuid]
		except KeyError:
			return

	async def join(self, job, consumer, **kwargs):
		"""
			Joins the stream for the given job
		"""
		# Fetch the stream
		try:
			stream = self.streams[job.uuid]

		# If the stream does not exist, open a new one
		except KeyError:
			stream = await self.open(job)

		# Join the stream
		await stream.join(consumer, **kwargs)

		return stream

	async def log(self, message):
		"""
			Receives a raw log message
		"""
		job_id = uuid.UUID(message.get("job_id"))

		try:
			stream = self.streams[job_id]

		# Close the session if nobody is listening
		except KeyError:
			return await self.close(job_id)

		# Fetch the payload
		data = message.get("data")

		# Extract all fields
		timestamp = data.get("timestamp")
		priority  = data.get("priority")
		line      = data.get("line")

		# Parse the timestamp
		timestamp = datetime.datetime.fromisoformat(timestamp)

		# Process the message
		await stream.message(timestamp, priority, line)


class LogStream(base.Object):
	levels = {
		logging.DEBUG   : "DEBUG",
		logging.INFO    : "INFO",
		logging.WARNING : "WARNING",
		logging.ERROR   : "ERROR",
	}

	def init(self, uuid):
		self.uuid = uuid

		# Lock when buffer is being modified
		self._lock = asyncio.Lock()

		# Buffer for messages
		self.buffer = collections.deque(maxlen=BUFFER_MAX_SIZE)

		# Consumers
		self.consumers = []

	def __repr__(self):
		return "<%s %s>" % (self.__class__.__name__, self.uuid)

	async def close(self):
		"""
			Called to close all connections to consumers
		"""
		# De-register the stream
		await self.backend.logstreams.close(self.uuid)

		# Close all connections to consumers
		for consumer in self.consumers:
			consumer.close()

	async def join(self, consumer, limit=None):
		"""
			Called when a consumer wants to receive the stream
		"""
		# Store a reference to the consumer
		self.consumers.append(consumer)

		log.debug("%s has joined the stream for %s" % (consumer, self.uuid))

		# Select all messages we want to send
		async with self._lock:
			buffer = collections.deque(self.buffer, limit)

		# Send all messages
		for message in buffer:
			await consumer.message(message)

	def leave(self, consumer):
		"""
			Called when a consumer wants to leave the stream
		"""
		try:
			self.consumers.remove(consumer)
		except IndexError:
			pass

		log.debug("%s has left the stream for %s" % (consumer, self.uuid))

		# Close the stream if there are no consumers left
		if not self.consumers:
			self.backend.run_task(self.close)

	async def message(self, timestamp, level, line):
		# Translate the level
		try:
			level = self.levels[level]
		except KeyError:
			level = "UNKNOWN"

		# Form a message object that we will send to the consumers
		m = {
			"timestamp" : timestamp.isoformat(),
			"level"     : level,
			"message"   : line,
		}

		# Append the message to the buffer
		async with self._lock:
			self.buffer.append(m)

		# Send the message to all consumers
		async with asyncio.TaskGroup() as tasks:
			for c in self.consumers:
				await c.message(m)
