###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import asyncio
import json
import logging
import tarfile

# Setup logging
log = logging.getLogger("pbs.registry")

class OCIImage(object):
	"""
		Takes an Image object and opens it
	"""
	def __init__(self, backend, image):
		self.backend = backend
		self.image   = image

	async def open(self):
		"""
			Opens the image as a tar file
		"""
		return await asyncio.to_thread(self._open, self.image.path)

	def _open(self, path):
		log.debug("Opening %s..." % path)

		# Open the tar file
		return tarfile.open(path)

	# Get Manifest

	async def get_manifests(self):
		"""
			Returns the manifests of index.json in OCI images
		"""
		with await self.open() as f:
			# Open index.json
			f = f.extractfile("index.json")

			# Parse the JSON object
			index = json.load(f)

			# Return all manifests
			return index.get("manifests")

	# Get Blob

	async def get_blob(self, reference):
		"""
			Returns the blob as a file-like object or None if not found
		"""
		# Split the reference
		type, _, digest = reference.partition(":")

		# Make the path
		path = "blobs/%s/%s" % (type, digest)

		# Log action
		log.debug("Looking up %s in %s" % (path, self.image))

		# Open the tar file
		f = await self.open()

		# Extract the blob
		try:
			return f.extractfile(path)
		except KeyError:
			pass
