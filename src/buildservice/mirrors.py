#!/usr/bin/python

import asyncio
import datetime
import functools
import ipaddress
import logging
import os.path
import random
import socket
import tornado.netutil
import urllib.parse

import location

import sqlalchemy
from sqlalchemy import ARRAY, Boolean, Column, DateTime, Double, ForeignKey, Integer, Text
from sqlalchemy.dialects.postgresql import INET

from . import base
from . import database
from . import httpclient

from .decorators import *

# Setup logging
log = logging.getLogger("pbs.mirrors")

class Mirrors(base.Object):
	def __aiter__(self):
		stmt = (
			sqlalchemy
			.select(Mirror)
			.where(
				Mirror.deleted_at == None,
			)

			# Order them by hostname
			.order_by(Mirror.hostname)
		)

		# Fetch the mirrors
		return self.db.fetch(stmt)

	async def get_by_hostname(self, hostname):
		stmt = (
			sqlalchemy
			.select(Mirror)
			.where(
				Mirror.deleted_at == None,

				# Match by hostname
				Mirror.hostname == hostname,
			)
		)

		return await self.db.fetch_one(stmt)

	async def create(self, hostname, path, owner, contact, notes, user=None, check=True):
		"""
			Creates a new mirror
		"""
		# Create the new mirror
		mirror = await self.db.insert(
			Mirror,
			hostname   = hostname,
			path       = path,
			owner      = owner,
			contact    = contact,
			notes      = notes,
			created_by = user,
		)

		log.info("Mirror %s has been created" % mirror)

		# Perform the first check
		if check:
			await mirror.check(force=True)

		return mirror

	async def get_mirrors_for_address(self, address):
		"""
			Returns all mirrors in random order with preferred mirrors first
		"""
		def __sort(mirror):
			# Generate some random value for each mirror
			r = random.random()

			# Add the preference score
			r += mirror.is_preferred_for_address(address)

			return r

		# Fetch all mirrors and shuffle them, but put preferred mirrors first
		return sorted([mirror async for mirror in self], key=__sort, reverse=True)

	@functools.cached_property
	def location(self):
		"""
			The location database
		"""
		return location.Database("/var/lib/location/database.db")

	@functools.cached_property
	def resolver(self):
		"""
			A DNS resolver
		"""
		return tornado.netutil.ThreadedResolver()

	async def check(self, *args, **kwargs):
		"""
			Runs the mirror check for all mirrors
		"""
		# Check all mirrors concurrently
		async with asyncio.TaskGroup() as tg:
			async for mirror in self:
				tg.create_task(
					mirror.check(*args, **kwargs),
				)


class Mirror(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "mirrors"

	def __str__(self):
		return self.hostname

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.hostname < other.hostname

		return NotImplemented

	# ID

	id = Column(Integer, primary_key=True)

	# Hostname

	hostname = Column(Text, unique=True, nullable=False)

	# XXX Must be unique over non-deleted items

	# Path

	path = Column(Text, nullable=False)

	# URL

	@property
	def url(self):
		return self.make_url()

	def make_url(self, *paths):
		url = "https://%s%s/" % (
			self.hostname,
			self.path
		)

		# Merge the path together
		path = os.path.join(*paths)

		# Remove any leading slashes
		if path.startswith("/"):
			path = path[1:]

		return urllib.parse.urljoin(url, path)

	# Last Check Success - True if the last check was successful

	last_check_success = Column(Boolean, nullable=False, default=False)

	# Last Check At

	last_check_at = Column(DateTime(timezone=False))

	# Error Message when the check has been unsuccessful

	error = Column(Text, nullable=False, default="")

	# Created At

	created_at = Column(
		DateTime(timezone=False), nullable=False, server_default=sqlalchemy.func.current_timestamp(),
	)

	# Created By ID

	created_by_id = Column(Integer, ForeignKey("users.id"), nullable=False)

	# Created By

	created_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[created_by_id], lazy="joined", innerjoin=True,
	)

	# Deleted By ID

	deleted_by_id = Column(Integer, ForeignKey("users.id"))

	# Deleted By

	deleted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deleted_by_id], lazy="selectin",
	)

	def has_perm(self, user):
		# Anonymous users have no permission
		if not user:
			return False

		# Admins have all permissions
		return user.is_admin()

	# Owner

	owner = Column(Text, nullable=False)

	# Contact

	contact = Column(Text, nullable=False)

	# Notes

	notes = Column(Text, nullable=False, default="")

	# Country Code

	country_code = Column(Text)

	# ASN

	asn = Column(Integer)

	# Addresses IPv6

	addresses_ipv6 = Column(ARRAY(INET), nullable=False, default=[])

	def supports_ipv6(self):
		"""
			Returns True if this mirror supports IPv6
		"""
		if self.addresses_ipv6:
			return True

		return False

	# Addresses IPv4

	addresses_ipv4 = Column(ARRAY(INET), nullable=False, default=[])

	def supports_ipv4(self):
		"""
			Returns True if this mirror supports IPv4
		"""
		if self.addresses_ipv4:
			return True

		return False

	# Addresses

	@property
	def addresses(self):
		"""
			All addresses of the mirror, regardless of family
		"""
		return self.addresses_ipv6 + self.addresses_ipv4

	async def _update_country_code_and_asn(self):
		"""
			Updates the country code of this mirror
		"""
		# Resolve the hostname
		try:
			addresses = await self.backend.mirrors.resolver.resolve(self.hostname, port=443)

		# XXX Catch this!
		except socket.gaierror as e:
			# Name or service not known
			if e.errno == -2:
				log.error("Could not resolve %s: %s" % (self, e))
				return

			# Raise anything else
			raise e

		print(addresses)

		# Store all IP addresses
		self.addresses_ipv6 = [
			address[0] for family, address in addresses if family == socket.AF_INET6
		]
		self.addresses_ipv4 = [
			address[0] for family, address in addresses if family == socket.AF_INET
		]

		# Lookup the country code and ASN
		for address in self.addresses:
			network = self.backend.mirrors.location.lookup(address)

			# Try the next IP address if we didn't find any data
			if not network or not network.country_code:
				continue

			# Store the country code
			self.country_code = network.country_code

			# Store the ASN
			self.asn = network.asn

			# Once is enough
			break

	def is_preferred_for_address(self, address):
		"""
			Returns a score of how much the mirror is preferred for this address
		"""
		# Lookup the client
		network = self.backend.mirrors.location.lookup("%s" % address)

		# Return if we could not find the client
		if not network:
			return 0

		first_address = ipaddress.ip_address(network.first_address)
		last_address  = ipaddress.ip_address(network.last_address)

		# Check if the mirror is on the same network
		for address in self.addresses:
			# Skip incompatible families
			if isinstance(address, ipaddress.IPv6Address):
				if not network.family == socket.AF_INET6:
					continue
			elif isinstance(address, ipaddress.IPv4Address):
				if not network.family == socket.AF_INET:
					continue

			# Check if the address is within the network
			if first_address <= address <= last_address:
				return 4

		# If the AS matches, we will prefer this
		if self.asn and self.asn == network.asn:
			return 3

		# If the mirror and client are in the same country, we prefer this
		if self.country_code and self.country_code == network.country_code:
			return 2

		# Check if we are on the same continent
		if self._continent_match(self.country_code, network.country_code):
			return 1

		return 0

	def _continent_match(self, cc1, cc2):
		"""
			Checks if the two given country codes are on the same continent
		"""
		country1 = self.backend.mirrors.location.get_country(cc1)
		country2 = self.backend.mirrors.location.get_country(cc2)

		# If we are missing either country, we don't know
		if not country1 or not country2:
			return False

		# Return True if both countries are on the same continent
		return country1.continent_code == country2.continent_code

	# Check

	async def check(self, force=False):
		t = datetime.datetime.utcnow()

		# Ratelimit checks somewhat
		if not force:
			# Check mirrors that are up only once an hour
			if self.last_check_success is True:
				if self.last_check_at + datetime.timedelta(hours=1) > t:
					log.debug("Skipping check for %s" % self)
					return

			# Check mirrors that are down once every 15 minutes
			elif self.last_check_success is False:
				if self.last_check_at + datetime.timedelta(minutes=15) > t:
					log.debug("Skipping check for %s" % self)
					return

		log.debug("Running mirror check for %s" % self.hostname)

		# Wrap this into one large transaction
		async with await self.db.transaction():
			# Update the country code & ASN
			await self._update_country_code_and_asn()

			# Make URL for .timestamp
			url = self.make_url(".timestamp")

			response = None
			error = None

			# Was this check successful?
			success = False

			# When was the last sync?
			timestamp = None

			try:
				response = await self.backend.httpclient.fetch(
					url,

					# Allow a moment to connect and get a response
					connect_timeout=10,
					request_timeout=10,
				)

				# Try to parse the response
				try:
					timestamp = int(response.body)

				except (TypeError, ValueError) as e:
					log.error("%s responded with an invalid timestamp")

					raise ValueError("Invalid timestamp received") from e

				# Convert into datetime
				timestamp = datetime.datetime.utcfromtimestamp(timestamp)

			# Catch anything that isn't 200 OK
			except httpclient.HTTPError as e:
				log.error("%s: %s" % (self, e))
				error = "%s" % e

			# Catch DNS Errors
			except socket.gaierror as e:
				# Name or service not known
				if e.code == -2:
					log.error("Could not resolve %s: %s" % (self, e))

					# Store the error
					error = "%s" % e

				# Raise anything else
				else:
					raise e

			# Success!
			else:
				# This check was successful!
				success = True

			# Log this check
			await self.db.insert(
				MirrorCheck,
				mirror_id     = self.id,
				success       = success,
				response_time = response.request_time if response else None,
				http_status   = response.code if response else None,
				last_sync_at  = timestamp,
				error         = error,
			)

			# Update the main table
			self.last_check_at      = sqlalchemy.func.current_timestamp()
			self.last_check_success = success
			self.last_sync_at       = timestamp
			self.error              = error

	async def get_uptime_since(self, t):
		# Convert timedeltas to absolute time
		if isinstance(t, datetime.timedelta):
			t = datetime.datetime.utcnow() - t

		# CTE with uptimes
		uptimes = sqlalchemy.select(
			MirrorCheck.success,
			sqlalchemy.func.least(
				sqlalchemy.func.lead(
					MirrorCheck.checked_at,
					1,
					sqlalchemy.func.current_timestamp(),
				).over(
					order_by=MirrorCheck.checked_at.asc(),
				)
				-
				MirrorCheck.checked_at,
				sqlalchemy.text("INTERVAL '1 hour'"),
			).label("uptime"),
		).where(
			MirrorCheck.mirror_id == self.id,
			MirrorCheck.checked_at >= t,
		).cte("uptimes")

		# Check the percentage of how many checks have been successful
		stmt = sqlalchemy.select(
			(
				sqlalchemy.func.extract(
					"epoch",
					sqlalchemy.func.sum(
						uptimes.c.uptime,
					).filter(
						uptimes.c.success == True,
					),
				)
				/
				sqlalchemy.func.extract(
					"epoch",
					sqlalchemy.func.sum(
						uptimes.c.uptime
					),
				)
			).label("uptime")
		).select_from(uptimes)

		# Run the statement
		return await self.db.select_one(stmt, "uptime")

	async def serves_file(self, path):
		"""
			Checks if this mirror serves the file
		"""
		# XXX Skip this if the mirror is not online

		# Make the URL
		url = self.make_url(path)

		# Make the cache key
		cache_key = "file-check:%s" % url

		# Check if we have something in the cache
		serves_file = await self.backend.cache.get(cache_key)

		# Nothing in cache, let's run the check
		if serves_file is None:
			serves_file = await self._serves_file(url, cache_key=cache_key)

		return serves_file

	async def _serves_file(self, url, cache_key=None):
		serves_file = None

		# Send a HEAD request for the URL
		try:
			response = await self.backend.httpclient.fetch(
				url, method="HEAD", follow_redirects=True,

				# Don't allow too much time for the mirror to respond
				connect_timeout=5, request_timeout=5,

				# Ensure the server responds to all types or requests
				headers = {
					"Accept"        : "*/*",
					"Cache-Control" : "no-cache",
				}
			)

		# Catch any HTTP errors
		except tornado.httpclient.HTTPClientError as e:
			log.error("Mirror %s returned %s for %s" % (self, e.code, url))
			serves_file = False

		# If there was no error, we assume this file can be downloaded
		else:
			log.debug("Mirror %s seems to be serving %s")
			serves_file = True

		# Store positive responses in the cache for 24 hours
		# and negative responses for six hours.
		if cache_key:
			if serves_file:
				ttl = 86400 # 24h
			else:
				ttl = 21600 # 6h

			# Store in cache
			await self.backend.cache.set(cache_key, serves_file, ttl)

		return serves_file


class MirrorCheck(database.Base):
	"""
		An object that represents a single mirror check
	"""
	__tablename__ = "mirror_checks"

	# Mirror ID

	mirror_id = Column(Integer, ForeignKey("mirrors.id"), primary_key=True, nullable=False)

	# Checked At

	checked_at = Column(DateTime(timezone=None), primary_key=True, nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Success

	success = Column(Boolean, nullable=False)

	# Response Time

	response_time = Column(Double)

	# HTTP Status

	http_status = Column(Integer)

	# Last Sync At

	last_sync_at = Column(DateTime(timezone=None))

	# Error

	error = Column(Text)
