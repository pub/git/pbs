###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging

import sqlalchemy
from sqlalchemy import Column, Computed, ForeignKey
from sqlalchemy import ARRAY, DateTime, Integer, Text

from . import base
from . import database
from . import misc
from . import releases
from . import repos
from . import sources

# Setup logging
log = logging.getLogger("pbs.distros")

class Distros(base.Object):
	def __aiter__(self):
		stmt = (
			sqlalchemy
			.select(Distro)
			.where(
				Distro.deleted_at == None,
			)

			# Order them by name
			.order_by(Distro.name)
		)

		# Fetch the distros
		return self.db.fetch(stmt)

	async def get_by_slug(self, slug):
		stmt = (
			sqlalchemy
			.select(Distro)
			.where(
				Distro.deleted_at == None,

				# Select by slug
				Distro.distro_id + "-" + Distro.version_id.cast(Text) == slug,
			)
		)

		return await self.db.fetch_one(stmt)

	async def get_by_tag(self, tag):
		stmt = (
			sqlalchemy
			.select(Distro)
			.where(
				Distro.deleted_at == None,
				Distro.tag == tag,
			)
		)

		return await self.db.fetch_one(stmt)

	async def create(self, name, distro_id, version_id, **kwargs):
		"""
			Create a new distribution
		"""
		return await self.db.insert(
			Distro,
			name       = name,
			distro_id  = distro_id,
			version_id = version_id,
			**kwargs,
		)


class Distro(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "distributions"

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.name < other.name or self.version < other.version

		return NotImplemented

	def __str__(self):
		return "%s %s" % (self.name, self.version)

	def write_config(self, config, local=False, vendor=None, contact=None):
		# Make distribution configuration
		config["distro"] = {
			"name"       : self.name,
			"id"         : self.distro_id,
			"version_id" : self.version_id,
			"codename"   : self.codename,
			"slogan"     : self.slogan,
			"vendor"     : vendor or self.vendor,
			"contact"    : contact or self.contact,
		}

		# Add any custom configuration
		if self.custom_config:
			config.read_string(self.custom_config)

	# ID

	id = Column(Integer, primary_key=True)

	# Name

	name = Column(Text, nullable=False)

	# Distro ID

	distro_id = Column(Text, nullable=False)

	# Version

	@property
	def version(self):
		return "%s" % self.version_id

	# Version ID

	version_id = Column(Integer, nullable=False)

	# Slug

	@property
	def slug(self):
		return "%s-%s" % (self.distro_id, self.version_id)

	# Slogan

	slogan = Column(Text, nullable=False)

	# Codename

	codename = Column(Text, nullable=False)

	# Description

	description = Column(Text, nullable=False)

	# Arches

	arches = Column(ARRAY(Text), nullable=False)

	# Vendor

	vendor = Column(Text, nullable=False)

	# Contact

	contact = Column(Text, nullable=False)

	# Tag

	tag = Column(Text, Computed(distro_id + version_id), unique=True)

	# Pakfire

	def pakfire(self, **kwargs):
		"""
			Returns a Pakfire configuration for this distribution
			without any repositories (useful for dist)
		"""
		return self.backend.pakfire(distro=self, **kwargs)

	# Custom Configuration

	custom_config = Column(Text, nullable=False)

	# Bugzilla Product

	bugzilla_product = Column(Text, nullable=False)

	# Bugzilla Version

	bugzilla_version = Column(Text, nullable=False)

	# Bugzilla Fields

	@property
	def bugzilla_fields(self):
		"""
			Short hand to convey all fields for Bugzilla
		"""
		return {
			"product" : self.bugzilla_product,
			"version" : self.bugzilla_version,
		}

	# Permissions

	def has_perm(self, user):
		# Anonymous users have no permissions
		if not user:
			return False

		# Must be admin
		return user.is_admin()

	# Repos

	async def get_repos(self):
		"""
			Returns all repositories of this distribution
		"""
		stmt = (
			sqlalchemy
			.select(repos.Repo)
			.where(
				repos.Repo.deleted_at == None,
				repos.Repo.distro == self,
				repos.Repo.owner == None,
			)
		)

		return await self.db.fetch_as_set(stmt)

	async def get_repo(self, slug):
		"""
			Returns a specific repository by its slug
		"""
		stmt = (
			sqlalchemy
			.select(repos.Repo)
			.where(
				repos.Repo.deleted_at == None,
				repos.Repo.distro == self,
				repos.Repo.owner == None,
				repos.Repo.slug == slug,
			)
		)

		return await self.db.fetch_one(stmt)

	def get_next_repo(self, repo):
		"""
			Returns the next repository if there is one
		"""
		for r in sorted(self.repos, key=lambda r: r.priority):
			if r.priority <= repo.priority:
				continue

			return r

	# Builds

	async def get_builds(self, **kwargs):
		"""
			Returns all builds in this distribution
		"""
		return await self.backend.builds.get(distro=distro, scratch=False, **kwargs)

	# Sources

	async def get_sources(self):
		"""
			Returns a list of all sources
		"""
		stmt = (
			sqlalchemy
			.select(
				sources.Source,
			)
			.select_from(repos.Repo)
			.join(
				sources.Source,
				sources.Source.repo_id == repos.Repo.id,
			)
			.where(
				repos.Repo.deleted_at == None,
				repos.Repo.distro == self,
				repos.Repo.owner == None,

				sources.Source.deleted_at == None,
			)
			.order_by(
				sources.Source.name,
				sources.Source.url,
			)
		)

		return await self.db.fetch_as_list(stmt)

	# Releases

	async def get_releases(self, limit=None, offset=None):
		"""
			Fetches all releases of this distribution
		"""
		stmt = (
			sqlalchemy
			.select(
				releases.Release,
			)
			.where(
				releases.Release.deleted_at == None,
				releases.Release.distro == self,
			)
			.order_by(
				releases.Release.published_at.desc(),
				releases.Release.created_at.desc(),
			)
		)

		return await self.db.fetch_as_list(stmt)

	async def get_release(self, slug):
		stmt = (
			sqlalchemy
			.select(
				releases.Release,
			)
			.where(
				releases.Release.deleted_at == None,
				releases.Release.distro == self,
				releases.Release.slug == slug,
			)
		)

		return await self.db.fetch_one(stmt)

	# Latest Release

	async def get_latest_release(self):
		"""
			Returns the latest published release
		"""
		stmt = (
			sqlalchemy
			.select(
				releases.Release,
			)
			.where(
				releases.Release.deleted_at == None,
				releases.Release.distro == self,
				releases.Release.published_at <= sqlalchemy.func.current_timestamp(),
			)
			.order_by(
				releases.Release.published_at.desc(),
			)
			.limit(1)
		)

		return await self.db.fetch_one(stmt)

	async def create_release(self, name, **kwargs):
		"""
			Creates a new release
		"""
		# Create a slug
		slug = misc.normalize(name)

		# Create a new Release
		release = await self.db.insert(
			releases.Release,
			distro = self,
			name   = name,
			slug   = slug,
			**kwargs,
		)

		# XXX create image jobs

		return release
