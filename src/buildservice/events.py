#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import functools
import logging

import sqlalchemy
from sqlalchemy import Column, ForeignKey, DateTime, Integer, Text

from . import base
from . import builders
from . import builds
from . import database
from . import distros
from . import jobs
from . import mirrors
from . import releases
from . import releasemonitoring as monitorings
from . import repos

# Setup logging
log = logging.getLogger("pbs.events")

# Priorities (the higher, the more important)
# ERROR          : 10
# WARNING        : 8
# MAJOR INFO     : 5
# MINOR INFO     : 4
# DEBUG          : 1

class Events(base.Object):
	@functools.cached_property
	def events(self):
		"""
			This returns a massive CTE that creates this thing on the fly
		"""
		events = []

		def TYPE(t):
			return sqlalchemy.literal(t).label("type")

		def TIMESTAMP(column):
			return column.label("t")

		def PRIORITY(priority):
			return sqlalchemy.literal(priority).label("priority")

		# Build Created
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("build-created"),

				# Timestamp
				TIMESTAMP(builds.Build.created_at),

				# Priority
				PRIORITY(4),

				# Build ID
				builds.Build.id.label("build_id"),

				# By User
				builds.Build.owner_id.label("by_user_id"),
			)
		))

		# Finished/Failed Builds
		events.append((
			sqlalchemy
			.select(
				# Type
				sqlalchemy.case(
					(builds.Build.failed == True, sqlalchemy.literal("build-failed")),
					else_=sqlalchemy.literal("build-finished"),
				).label("type"),

				# Timestamp
				TIMESTAMP(builds.Build.finished_at),

				# Priority
				sqlalchemy.case(
					(builds.Build.failed == True, 8),
					else_=4,
				).label("priority"),

				# Build ID
				builds.Build.id.label("build_id"),
			)
			.select_from(builds.Build)
			.where(
				builds.Build.finished_at != None,
			)
		))

		# Deleted Builds
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("build-deleted"),

				# Timestamp
				TIMESTAMP(builds.Build.deleted_at),

				# Priority
				PRIORITY(4),

				# Build ID
				builds.Build.id.label("build_id"),

				# Deleted By User
				builds.Build.deleted_by_id.label("by_user_id"),
			)
			.select_from(builds.Build)
			.where(
				builds.Build.deleted_at != None,
			)
		))

		# Deprecated Builds
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("build-deprecated"),

				# Timestamp
				TIMESTAMP(builds.Build.deprecated_at),

				# Priority
				PRIORITY(4),

				# Build ID
				builds.Build.id.label("build_id"),

				# By Build ID
				builds.Build.deprecating_build_id.label("by_build_id"),

				# By User ID
				builds.Build.deprecated_by_id.label("by_user_id"),
			)
			.select_from(builds.Build)
			.where(
				builds.Build.deprecated_at != None,
			)
		))

		# Build Comments
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("build-comment"),

				# Timestamp
				TIMESTAMP(builds.BuildComment.created_at),

				# Priority
				PRIORITY(5),

				# Build ID
				builds.BuildComment.build_id.label("build_id"),

				# Build Comment ID
				builds.BuildComment.id.label("build_comment_id"),

				# User ID
				builds.BuildComment.user_id.label("user_id"),
			)
			.select_from(builds.BuildComment)
			.where(
				builds.BuildComment.deleted_at == None,
			)
		))

		# Build Watchers added
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("build-watcher-added"),

				# Timestamp
				TIMESTAMP(builds.BuildWatcher.added_at),

				# Priority
				PRIORITY(1),

				# Build ID
				builds.BuildWatcher.build_id.label("build_id"),

				# User ID
				builds.BuildWatcher.user_id.label("user_id"),
			)
			.select_from(builds.BuildWatcher)
		))

		# Build Watchers removed
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("build-watcher-removed"),

				# Timestamp
				TIMESTAMP(builds.BuildWatcher.deleted_at),

				# Priority
				PRIORITY(1),

				# Build ID
				builds.BuildWatcher.build_id.label("build_id"),

				# User ID
				builds.BuildWatcher.user_id.label("user_id"),
			)
			.select_from(builds.BuildWatcher)
			.where(
				builds.BuildWatcher.deleted_at != None,
			)
		))

		# Bugs added to builds
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("build-bug-added"),

				# Timestamp
				TIMESTAMP(builds.BuildBug.added_at),

				# Priority
				PRIORITY(4),

				# Build ID
				builds.BuildBug.build_id.label("build_id"),

				# By User ID
				builds.BuildBug.added_by_id.label("by_user_id"),

				# Bug ID
				builds.BuildBug.bug_id.label("bug_id"),
			)
			.select_from(builds.BuildBug)
		))

		# Bugs removed from builds
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("build-bug-removed"),

				# Timestamp
				TIMESTAMP(builds.BuildBug.removed_at),

				# Priority
				PRIORITY(4),

				# Build ID
				builds.BuildBug.build_id.label("build_id"),

				# By User ID
				builds.BuildBug.removed_by_id.label("by_user_id"),

				# Bug ID
				builds.BuildBug.bug_id.label("bug_id"),
			)
			.select_from(builds.BuildBug)
			.where(
				builds.BuildBug.removed_at != None,
			)
		))

		src_repo = sqlalchemy.orm.aliased(repos.RepoBuild)
		dst_repo = sqlalchemy.orm.aliased(repos.RepoBuild)

		# Build added to/moved repository
		events.append((
			sqlalchemy
			.select(
				# Type
				sqlalchemy.case(
					(src_repo == None, "repository-build-added"),
					else_="repository-build-moved",
				).label("type"),

				# Timestamp
				TIMESTAMP(dst_repo.added_at),

				# Priority
				PRIORITY(5),

				# Build ID
				dst_repo.build_id.label("build_id"),

				# By User ID
				dst_repo.added_by_id.label("by_user_id"),

				# Repo ID
				dst_repo.repo_id.label("repo_id"),
			)
			.select_from(dst_repo)
			.join(
				src_repo,
				sqlalchemy.and_(
					src_repo.build_id == dst_repo.build_id,
					src_repo.repo_id != dst_repo.repo_id,
					src_repo.removed_at == dst_repo.added_at,
				),
				isouter=True,
			)
		))

		# Build removed from repository
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("repository-build-removed"),

				# Timestamp
				TIMESTAMP(src_repo.removed_at),

				# Priority
				PRIORITY(5),

				# Build ID
				src_repo.build_id.label("build_id"),

				# By User ID
				src_repo.removed_by_id.label("by_user_id"),

				# Repo ID
				src_repo.repo_id.label("repo_id"),
			)
			.select_from(src_repo)
			.join(
				dst_repo,
				sqlalchemy.and_(
					src_repo.build_id == dst_repo.build_id,
					src_repo.repo_id != dst_repo.repo_id,
					src_repo.removed_at == dst_repo.added_at,
				),
				isouter=True,
			)
			.where(
				src_repo.removed_at != None,
				dst_repo.repo_id == None,
			)
		))

		# Build Points
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("build-points"),

				# Timestamp
				TIMESTAMP(builds.BuildPoint.created_at),

				# Priority
				PRIORITY(1),

				# Build ID
				builds.BuildPoint.build_id.label("build_id"),

				# User ID
				builds.BuildPoint.user_id.label("by_user_id"),

				# Points
				builds.BuildPoint.points.label("points"),
			)
			.select_from(builds.BuildPoint)
		))

		# Test Builds
		events.append((
			sqlalchemy
			.select(
				# Type
				sqlalchemy.case(
					(builds.BuildGroup.failed == True, "test-builds-failed"),
					else_="test-builds-succeeded",
				).label("type"),

				# Timestamp
				TIMESTAMP(builds.BuildGroup.finished_at),

				# Priority
				PRIORITY(4),

				# Build Group ID
				builds.BuildGroup.id.label("build_group_id"),
			)
			.select_from(builds.Build)
			.join(
				builds.BuildGroup,
				builds.BuildGroup.tested_build_id == builds.Build.id,
			)
			.where(
				builds.BuildGroup.deleted_at == None,
				builds.Build.deleted_at == None,
				builds.BuildGroup.finished_at != None,
			)
		))

		# Created Jobs
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("job-created"),

				# Timestamp
				TIMESTAMP(jobs.Job.created_at),

				# Priority
				PRIORITY(1),

				# Build ID
				jobs.Job.build_id.label("build_id"),

				# Job ID
				jobs.Job.id.label("job_id"),
			)
			.select_from(jobs.Job)
			.where(
				jobs.Job.deleted_at == None,
			)
		))

		# Failed Jobs
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("job-failed"),

				# Timestamp
				TIMESTAMP(jobs.Job.finished_at),

				# Priority
				PRIORITY(5),

				# Build ID
				jobs.Job.build_id.label("build_id"),

				# Job ID
				jobs.Job.id.label("job_id"),

				# Builder ID
				jobs.Job.builder_id.label("builder_id"),
			)
			.select_from(jobs.Job)
			.where(
				jobs.Job.deleted_at == None,
				jobs.Job.finished_at != None,
				jobs.Job.aborted == False,
				jobs.Job.failed == True,
			)
		))

		# Finished Jobs
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("job-finished"),

				# Timestamp
				TIMESTAMP(jobs.Job.finished_at),

				# Priority
				PRIORITY(4),

				# Build ID
				jobs.Job.build_id.label("build_id"),

				# Job ID
				jobs.Job.id.label("job_id"),

				# Builder ID
				jobs.Job.builder_id.label("builder_id"),
			)
			.select_from(jobs.Job)
			.where(
				jobs.Job.deleted_at == None,
				jobs.Job.finished_at != None,
				jobs.Job.aborted == False,
				jobs.Job.failed == False,
			)
		))

		# Aborted Jobs
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("job-aborted"),

				# Timestamp
				TIMESTAMP(jobs.Job.finished_at),

				# Priority
				PRIORITY(4),

				# Build ID
				jobs.Job.build_id.label("build_id"),

				# Job ID
				jobs.Job.id.label("job_id"),

				# Builder ID
				jobs.Job.builder_id.label("builder_id"),

				# By User ID
				jobs.Job.aborted_by_id.label("by_user_id"),
			)
			.select_from(jobs.Job)
			.where(
				jobs.Job.deleted_at == None,
				jobs.Job.aborted == True,
			)
		))

		# Dispatched Jobs
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("job-dispatched"),

				# Timestamp
				TIMESTAMP(jobs.Job.started_at),

				# Priority
				PRIORITY(1),

				# Build ID
				jobs.Job.build_id.label("build_id"),

				# Job ID
				jobs.Job.id.label("job_id"),

				# Builder ID
				jobs.Job.builder_id.label("builder_id"),
			)
			.select_from(jobs.Job)
			.where(
				jobs.Job.deleted_at == None,
				jobs.Job.started_at != None,
			)
		))

		superseeded_jobs = sqlalchemy.orm.aliased(jobs.Job)

		# Retried jobs
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("job-retry"),

				# Timestamp
				TIMESTAMP(jobs.Job.created_at),

				# Priority
				PRIORITY(4),

				# Build ID
				jobs.Job.build_id.label("build_id"),

				# Job ID
				jobs.Job.id.label("job_id"),
			)
			.select_from(jobs.Job)
			.join(
				superseeded_jobs,
				superseeded_jobs.id == jobs.Job.superseeded_by_id,
			)
			.where(
				jobs.Job.deleted_at == None,
			)
		))

		# Builders Created
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("builder-created"),

				# Timestamp
				TIMESTAMP(builders.Builder.created_at),

				# Priority
				PRIORITY(5),

				# Builder ID
				builders.Builder.id.label("builder_id"),

				# By User ID
				builders.Builder.created_by_id.label("by_user_id"),
			)
			.select_from(builders.Builder)
		))

		# Builders Deleted
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("builder-deleted"),

				# Timestamp
				TIMESTAMP(builders.Builder.deleted_at),

				# Priority
				PRIORITY(5),

				# Builder ID
				builders.Builder.id.label("builder_id"),

				# By User ID
				builders.Builder.deleted_by_id.label("by_user_id"),
			)
			.select_from(builders.Builder)
			.where(
				builders.Builder.deleted_at != None,
			)
		))

		# Releases Created
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("release-created"),

				# Timestamp
				TIMESTAMP(releases.Release.created_at),

				# Priority
				PRIORITY(1),

				# Release ID
				releases.Release.id.label("release_id"),

				# By User ID
				releases.Release.created_by_id.label("by_user_id"),
			)
			.select_from(releases.Release)
		))

		# Releases Deleted
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("release-deleted"),

				# Timestamp
				TIMESTAMP(releases.Release.deleted_at),

				# Priority
				PRIORITY(1),

				# Release ID
				releases.Release.id.label("release_id"),

				# By User ID
				releases.Release.deleted_by_id.label("by_user_id"),
			)
			.select_from(releases.Release)
			.where(
				releases.Release.deleted_at != None,
			)
		))

		# Releases Published
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("release-published"),

				# Timestamp
				TIMESTAMP(releases.Release.published_at),

				# Priority
				PRIORITY(5),

				# Release ID
				releases.Release.id.label("release_id"),
			)
			.select_from(releases.Release)
			.where(
				releases.Release.published_at != None,
				releases.Release.published_at <= sqlalchemy.func.current_timestamp(),
			)
		))

		# Mirrors Created
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("mirror-created"),

				# Timestamp
				TIMESTAMP(mirrors.Mirror.created_at),

				# Priority
				PRIORITY(5),

				# Mirror ID
				mirrors.Mirror.id.label("mirror_id"),

				# By User ID
				mirrors.Mirror.created_by_id.label("by_user_id"),
			)
			.select_from(mirrors.Mirror)
		))

		# Mirrors Deleted
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("mirror-created"),

				# Timestamp
				TIMESTAMP(mirrors.Mirror.deleted_at),

				# Priority
				PRIORITY(5),

				# Mirror ID
				mirrors.Mirror.id.label("mirror_id"),

				# By User ID
				mirrors.Mirror.deleted_by_id.label("by_user_id"),
			)
			.select_from(mirrors.Mirror)
			.where(
				mirrors.Mirror.deleted_at != None,
			)
		))

		mirror_status_changes = (
			sqlalchemy
			.select(
				mirrors.MirrorCheck.mirror_id.label("mirror_id"),
				mirrors.MirrorCheck.checked_at.label("checked_at"),
				mirrors.MirrorCheck.success.label("new_status"),
				sqlalchemy.func.lag(
					mirrors.MirrorCheck.success,
				)
				.over(
					partition_by=mirrors.MirrorCheck.mirror_id,
					order_by=mirrors.MirrorCheck.checked_at.asc(),
				)
				.label("old_status"),
				mirrors.MirrorCheck.error.label("error"),
			)
			.select_from(mirrors.MirrorCheck)
			.cte("mirror_status_changes")
		)

		# Mirror Status Changes
		events.append((
			sqlalchemy
			.select(
				# Type
				sqlalchemy.case(
					(mirror_status_changes.c.new_status == True, "mirror-online"),
					(mirror_status_changes.c.new_status == False, "mirror-offline"),
				).label("type"),

				# Timestamp
				TIMESTAMP(mirror_status_changes.c.checked_at),

				# Priority
				PRIORITY(4),

				# Mirror ID
				mirror_status_changes.c.mirror_id.label("mirror_id"),

				# Error
				mirror_status_changes.c.error.label("error"),
			)
			.select_from(mirror_status_changes)
			.where(
				mirror_status_changes.c.old_status != mirror_status_changes.c.new_status,
			)
		))

		# Release Monitoring Created
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("release-monitoring-created"),

				# Timestamp
				TIMESTAMP(monitorings.Monitoring.created_at),

				# Priority
				PRIORITY(4),

				# Package Name
				monitorings.Monitoring.name.label("package_name"),

				# By User ID
				monitorings.Monitoring.created_by_id.label("by_user_id"),
			)
			.select_from(monitorings.Monitoring)
		))

		# Release Monitoring Deleted
		events.append((
			sqlalchemy
			.select(
				# Type
				TYPE("release-monitoring-deleted"),

				# Timestamp
				TIMESTAMP(monitorings.Monitoring.deleted_at),

				# Priority
				PRIORITY(4),

				# Package Name
				monitorings.Monitoring.name.label("package_name"),

				# By User ID
				monitorings.Monitoring.deleted_by_id.label("by_user_id"),
			)
			.select_from(monitorings.Monitoring)
			.where(
				monitorings.Monitoring.deleted_at != None,
			)
		))

		# Discover all columns
		columns  = [c for c in sqlalchemy.inspection.inspect(Event).c]

		# Add any missing columns to keep the subqueries shorter
		events = [
			(
				sqlalchemy
				.select(
					*(
						query.columns.get(
							column.name,
							sqlalchemy.null().cast(column.type).label(column.name),
						)
						for column in columns
					),
				)
			) for query in events
		]

		# Create a new CTE with all events
		return sqlalchemy.union_all(*events).cte("events")

	async def __call__(self, priority=None, offset=None, limit=None,
			build=None, builder=None, mirror=None, user=None):
		"""
			Returns all events filtered by the given criteria
		"""
		# Create a subquery to map the model to the CTE
		events = (
			sqlalchemy
			.select(
				Event,
			)
			.add_cte(
				self.events,
			)
		).subquery()

		# Alias the subquery
		events = sqlalchemy.orm.aliased(Event, events)

		# Create a query to filter out the events we are interested in
		stmt = (
			sqlalchemy
			.select(
				events,
			)
			.order_by(
				events.t.desc(),
				events.priority.asc(),
			)
			.limit(limit)
			.offset(offset)
		)

		# Filter by build
		if build:
			stmt = stmt.where(
				events.build_id == build.id,
			)

		# Filter by builder
		if builder:
			stmt = stmt.where(
				events.builder_id == builder.id,
			)

		# Filter by mirror
		if mirror:
			stmt = stmt.where(
				events.mirror_id == mirror.id,
			)

		# Filter by user
		if user:
			stmt = stmt.where(
				events.user_id == user.id |
				events.by_user_id == user.id,
			)

		# Filter by priority
		if priority:
			stmt = stmt.where(
				events.priority >= priority,
			)

		# Run the query
		return await self.db.fetch_as_list(stmt)


class Event(database.Base):
	__tablename__ = "events"

	# Type

	type = Column(Text, primary_key=True, nullable=False)

	# Timestamp

	t = Column(DateTime(timezone=False), primary_key=True, nullable=False)

	# Priority

	priority = Column(Integer, nullable=False)

	# Build ID

	build_id = Column(Integer, ForeignKey("builds.id"))

	# Build

	build = sqlalchemy.orm.relationship(
		"Build", foreign_keys=[build_id], lazy="selectin",
	)

	# By Build ID

	by_build_id = Column(Integer, ForeignKey("builds.id"))

	# By Build

	by_build = sqlalchemy.orm.relationship(
		"Build", foreign_keys=[by_build_id], lazy="selectin",
	)

	# Build Comment ID

	build_comment_id = Column(Integer, ForeignKey("build_comments.id"))

	# Build Comment

	build_comment = sqlalchemy.orm.relationship(
		"BuildComment", foreign_keys=[build_comment_id], lazy="selectin",
	)

	# Build Group ID

	build_group_id = Column(Integer, ForeignKey("build_groups.id"))

	# Build Group

	build_group = sqlalchemy.orm.relationship(
		"BuildGroup", foreign_keys=[build_group_id], lazy="selectin",
	)

	# Job ID

	job_id = Column(Integer, ForeignKey("jobs.id"))

	# Job

	job = sqlalchemy.orm.relationship(
		"Job", foreign_keys=[job_id], lazy="selectin",
	)

	# Package Name

	package_name = Column(Text)

	# Mirror ID

	mirror_id = Column(Integer, ForeignKey("mirrors.id"))

	# Mirror

	mirror = sqlalchemy.orm.relationship(
		"Mirror", foreign_keys=[mirror_id], lazy="selectin",
	)

	# User ID

	user_id = Column(Integer, ForeignKey("users.id"))

	# User

	user = sqlalchemy.orm.relationship(
		"User", foreign_keys=[user_id], lazy="joined",
	)

	# By User ID

	by_user_id = Column(Integer, ForeignKey("users.id"))

	# By User

	by_user = sqlalchemy.orm.relationship(
		"User", foreign_keys=[by_user_id], lazy="joined",
	)

	# Builder ID

	builder_id = Column(Integer, ForeignKey("builders.id"))

	# Builder

	builder = sqlalchemy.orm.relationship(
		"Builder", foreign_keys=[builder_id], lazy="selectin",
	)

	# Repo ID

	repo_id = Column(Integer, ForeignKey("repositories.id"))

	# Repo

	repo = sqlalchemy.orm.relationship(
		"Repo", foreign_keys=[repo_id], lazy="selectin",
	)

	# Release ID

	release_id = Column(Integer, ForeignKey("releases.id"))

	# Release

	release = sqlalchemy.orm.relationship(
		"Release", foreign_keys=[release_id], lazy="selectin",
	)

	# Bug

	bug_id = Column(Integer)

	# Error

	error = Column(Text)

	# Points

	points = Column(Integer)
