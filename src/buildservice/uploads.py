#!/usr/bin/python3

import asyncio
import hashlib
import hmac
import logging
import os
import shutil
import sqlalchemy
import tempfile

from sqlalchemy import Column, ForeignKey
from sqlalchemy import BigInteger, DateTime, Integer, LargeBinary, Text, UUID

from . import base
from . import database
from . import builders
from . import users
from .constants import *

# Setup logging
log = logging.getLogger("pbs.uploads")

class Uploads(base.Object):
	def __aiter__(self):
		stmt = (
			sqlalchemy.select(Upload)

			# Order them by creation time
			.order_by(Upload.created_at)
		)

		# Fetch all objects
		return self.db.fetch(stmt)

	async def get_by_uuid(self, uuid):
		stmt = (
			sqlalchemy.select(Upload)
			.where(
				Upload.uuid == uuid,
				Upload.expires_at > sqlalchemy.func.current_timestamp(),
			)
		)

		return await self.db.fetch_one(stmt)

	async def create(self, filename, size, digest_blake2b512, owner=None):
		"""
			Creates a new upload
		"""
		builder, user = None, None

		# Check if the digest is set
		if not digest_blake2b512:
			raise ValueError("Empty digest")

		# Check uploader type
		if isinstance(owner, builders.Builder):
			builder = owner
		elif isinstance(owner, users.User):
			user = owner

		# Check quota for users
		#if user:
		#	# This will raise an exception if the quota has been exceeded
		#	await user.check_storage_quota(size)

		# Create a new upload
		upload = await self.db.insert(
			Upload,
			filename          = filename,
			size              = size,
			builder           = builder,
			user              = user,
			digest_blake2b512 = digest_blake2b512,
		)

		# Return the newly created upload object
		return upload

	async def create_from_local(self, path, filename=None, **kwargs):
		"""
			Imports a file from the local filesystem
		"""
		# Collect attributes
		if filename is None:
			filename = os.path.basename(path)

		# Fetch the size
		size = os.path.getsize(path)

		# Create the new upload object
		upload = await self.create(
			filename = filename,
			size     = size,
			**kwargs,
		)

		# Import the data
		with open(path, "rb") as f:
			await upload.copyfrom(f)

		return upload

	async def cleanup(self):
		# Find all expired uploads
		stmt = (
			sqlalchemy
			.select(Upload)
			.where(
				Upload.expires_at <= sqlalchemy.func.current_timestamp(),
			)
			.order_by(
				Upload.created_at
			)
		)

		# Delete them all
		async for upload in self.db.fetch(stmt):
			async with await self.db.transaction():
				await upload.delete()


class Upload(database.Base, database.BackendMixin):
	__tablename__ = "uploads"

	def __str__(self):
		return "%s" % self.uuid

	# ID

	id = Column(Integer, primary_key=True)

	# UUID

	uuid = Column(UUID, unique=True, nullable=False,
		server_default=sqlalchemy.func.gen_random_uuid())

	# Filename

	filename = Column(Text, nullable=False)

	# Extension

	@property
	def extension(self):
		filename, ext = os.path.splitext(self.filename)

		return ext.removeprefix(".")

	# Path

	path = Column(Text, nullable=False)

	# Size

	size = Column(BigInteger, nullable=False)

	# Digest

	digest_blake2b512 = Column(LargeBinary, nullable=False)

	# Builder ID

	builder_id = Column(Integer, ForeignKey("builders.id"))

	# Builder

	builder = sqlalchemy.orm.relationship("Builder", foreign_keys=[builder_id], lazy="joined")

	# User ID

	user_id = Column(Integer, ForeignKey("users.id"))

	# User

	user = sqlalchemy.orm.relationship("User", foreign_keys=[user_id], lazy="joined")

	# Has Perms?

	def has_perm(self, who):
		"""
			Returns True if "who" has permissions to use this upload
		"""
		if self.builder == who or self.user == who:
			return True

		# No permission
		return False

	# Delete!

	async def delete(self):
		log.info("Deleting upload %s (%s)" % (self, self.filename))

		# Remove the uploaded data
		if await self.has_payload():
			await self.backend.unlink(self.path)

		# Delete the upload from the database
		await self.db.delete(self)

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Expires At

	expires_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.text("CURRENT_TIMESTAMP + INTERVAL '24 hours'"))

	# Has Payload?

	async def has_payload(self):
		"""
			Returns True if this upload has received its payload
		"""
		# We must have a path
		if not self.path:
			return False

		# The data must exist on disk
		return await self.backend.exists(self.path)

	# Copy the payload from somewhere

	async def copyfrom(self, src):
		"""
			Copies the content of this upload from the source file descriptor
		"""
		# Cannot do this if we already have a payload
		if await self.has_payload():
			raise FileExistsError("We already have the payload")

		# Copy the entire content to a new temporary file
		with self.backend.tempfile(prefix="upload-", sync=True, delete=False) as dst:
			await asyncio.to_thread(shutil.copyfileobj, src, dst)

		# Store the path
		self.path = dst.name

	# Copy the payload to somewhere else

	async def copyinto(self, dst):
		"""
			Copies the content of this upload into the destination file descriptor.
		"""
		return await asyncio.to_thread(self._copyinto, dst)

	def _copyinto(self, dst):
		assert os.path.exists(self.path), "Upload has been deleted - %s" % self

		# Open the source file and copy it into the destination file descriptor
		with open(self.path, "rb") as src:
			shutil.copyfileobj(src, dst)
