###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import datetime
import functools
import logging

import sqlalchemy
from sqlalchemy import Column, Computed, ForeignKey
from sqlalchemy import ARRAY, Boolean, DateTime, Integer, Text

from . import database
from . import images
from . import registry

# Setup logging
log = logging.getLogger("pbs.releases")

class Release(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "releases"

	def __str__(self):
		return self.name

	def has_perm(self, *args, **kwargs):
		# Inherit all permissions from the distribution
		return self.distro.has_perm(*args, **kwargs)

	# ID

	id = Column(Integer, primary_key=True)

	# Distro ID

	distro_id = Column(Integer, ForeignKey("distributions.id"), nullable=False)

	# Distro

	distro = sqlalchemy.orm.relationship("Distro", lazy="joined", innerjoin=True)

	# Name

	name = Column(Text, nullable=False)

	# Slug

	slug = Column(Text, unique=True, nullable=False)

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Created By ID

	created_by_id = Column(Integer, ForeignKey("users.id"), nullable=False)

	# Created By

	created_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[created_by_id], lazy="joined", innerjoin=True,
	)

	# Deleted By ID

	deleted_by_id = Column(Integer, ForeignKey("users.id"))

	# Deleted By

	deleted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deleted_by_id], lazy="selectin",
	)

	# Stable?

	stable = Column(Boolean, nullable=False)

	# Announcement

	announcement = Column(Text, nullable=False)

	# URL

	@property
	def url(self):
		return "/distros/%s/releases/%s" % (self.distro.slug, self.slug)

	# Publish

	def is_published(self):
		if self.published_at and self.published_at <= datetime.datetime.utcnow():
			return True

		return False

	# Published At

	published_at = Column(DateTime)

	async def publish(self, when=None):
		"""
			Called to publish the release
		"""
		self.published_at = when or sqlalchemy.func.current_timestamp()

		# XXX TODO

	# Delete

	async def delete(self, *args, **kwargs):
		"""
			Deletes this release
		"""
		await super().delete(*args, **kwargs)

		# Delete all images, too
		for image in self.images:
			await image.delete(*args, **kwargs)

	# Images

	images = sqlalchemy.orm.relationship(
		"Image", back_populates="release", lazy="selectin",
	)

	# OCI Images

	@functools.cached_property
	def oci_images(self):
		"""
			Returns all OCI images
		"""
		images = []

		for image in self.images:
			if not image.type == "oci":
				continue

			# Make it an OCI image
			image = registry.OCIImage(self.backend, image)
			images.append(image)

		return images

	# Path

	@property
	def path(self):
		"""
			Make the path
		"""
		return "%s/%s" % (self.distro.slug, self.slug)

	# Local Path

	def local_path(self, arch=None):
		"""
			Path to the releases
		"""
		path = self.backend.path("releases", self.path)

		# Append the architecture
		if arch:
			path = os.path.join(path, arch)

		return path

	# Is this release mirrored?

	def is_mirrored(self):
		"""
			Returns True if this release should be mirrored
		"""
		# We currently only mirror stable releases
		if not self.stable:
			return False

		# If there is no publish date, we don't mirror
		if self.published_at is None:
			return False

		# What time is it now?
		now = datetime.datetime.now()

		# We start mirroring 24 hours before the release
		return self.published_at <= now - datetime.timedelta(hours=24)
