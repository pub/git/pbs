#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import boto3
import botocore.config
import logging

from . import base
from .decorators import *

# Make this all a little bit less verbose
for logger in ("boto3", "botocore"):
	logging.getLogger(logger).setLevel(logging.INFO)

class AWS(base.Object):
	def init(self):
		self._session_args = {
			"region_name"           : self.backend.config.get("aws", "region"),
			"aws_access_key_id"     : self.backend.config.get("aws", "access-key"),
			"aws_secret_access_key" : self.backend.config.get("aws", "access-secret"),
		}

	@property
	def session(self):
		return boto3.session.Session(**self._session_args)

	@property
	def ec2(self):
		return self.session.resource("ec2")
