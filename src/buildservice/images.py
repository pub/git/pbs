###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import logging
import os.path

import sqlalchemy
from sqlalchemy import Column, ForeignKey
from sqlalchemy import BigInteger, DateTime, Integer, LargeBinary, Text

from . import database

# Setup logging
log = logging.getLogger("pbs.images")

class Image(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "images"

	# Sorting

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.arch < other.arch

		return NotImplemented

	# ID

	id = Column(Integer, primary_key=True)

	# Release ID

	release_id = Column(Integer, ForeignKey("releases.id"), nullable=False)

	# Release

	release = sqlalchemy.orm.relationship("Release", foreign_keys=[release_id],
		back_populates="images", lazy="joined")

	# Type

	type = Column(Text, nullable=False)

	# Arch

	arch = Column(Text, nullable=False)

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Created By ID

	created_by_id = Column(Integer, ForeignKey("users.id"))

	# Created By

	created_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[created_by_id], lazy="joined",
	)

	# Deleted By ID

	deleted_by_id = Column(Integer, ForeignKey("users.id"))

	# Deleted By

	deleted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deleted_by_id], lazy="selectin",
	)

	# Path

	path = Column(Text, nullable=False)

	# Filename

	@property
	def filename(self):
		return os.path.basename(self.path)

	# Link

	@property
	def link(self):
		"""
			Returns a link where clients can download this image from
		"""
		return self.backend.path_to_url(self.path, mirrored=self.release.is_mirrored())

	# Size

	size = Column(BigInteger, nullable=False)

	# Checksums

	@property
	def checksums(self):
		"""
			A dictionary with all available checksums
		"""
		checksums = {
			"sha3-512"   : self.checksum_sha3_512,
			"sha3-256"   : self.checksum_sha3_256,
			"blake2b512" : self.checksum_blake2b512,
			"blake2s256" : self.checksum_blake2s256,
			"sha2-512"   : self.checksum_sha2_512,
			"sha2-256"   : self.checksum_sha2_256,
		}

		# Filter out any empty checksums
		return { algo : checksums[algo] for algo in checksums if checksums[algo] }

	# Checksum SHA3-512

	checksum_sha3_512 = Column(LargeBinary, nullable=False)

	# Checksum SHA3-256

	checksum_sha3_256 = Column(LargeBinary, nullable=False)

	# Checksum BLAKE2B512

	checksum_blake2b512 = Column(LargeBinary, nullable=False)

	# Checksum BLAKE2S256

	checksum_blake2s256 = Column(LargeBinary, nullable=False)

	# Checksum SHA2-512

	checksum_sha2_512 = Column(LargeBinary, nullable=False)

	# Checksum SHA2-256

	checksum_sha2_256 = Column(LargeBinary, nullable=False)
