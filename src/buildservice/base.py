#!/usr/bin/python

import functools

class Object(object):
	"""
		Main object where all other objects inherit from.

		This is used to access the global instance of Pakfire
		and hold the database connection.
	"""
	def __init__(self, backend, *args, **kwargs):
		self.backend = backend

		# Call custom constructor
		self.init(*args, **kwargs)

	def init(self, *args, **kwargs):
		"""
			Custom constructor to be overwritten by inheriting class
		"""
		pass

	@functools.cached_property
	def db(self):
		"""
			Shortcut to database
		"""
		return self.backend.db
