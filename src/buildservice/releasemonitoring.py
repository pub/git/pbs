###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import asyncio
import difflib
import json
import logging
import os
import pakfire
import re
import shutil
import urllib.parse

import sqlalchemy
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, Text

from . import base
from . import config
from . import database
from .decorators import *

# Setup logging
log = logging.getLogger("pbs.releasemonitoring")

# Only send up to four simultaneous API requests
ratelimiter = asyncio.Semaphore(value=4)

BUG_DESCRIPTION = """\
A new version of %(name)s has been released: %(version)s

This is an automatically created bug report by our release monitoring.
"""

BUG_DESCRIPTION_WITH_BUILD = BUG_DESCRIPTION + """
A build has been created to test this release:

  %(url)s
"""

# This comment is being added to the bug report when the build has been successful
BUG_BUILD_SUCCESSFUL = "The build has been successful."

# This command is being added to the bug report when the build has failed.
BUG_BUILD_FAILED = """\
The build has failed.

Log files of the failed jobs have been attached for you to investigate why.
"""

class ReleaseExistsError(Exception):
	"""
		Raised if a release already exists
	"""
	pass


class BuildExistsError(Exception):
	"""
		Raised if the build or a newer one already exists
	"""
	pass


class MonitoringRelease(database.Base, database.BackendMixin):
	__tablename__ = "release_monitoring_releases"

	def __str__(self):
		return "%s %s" % (self.monitoring.name, self.version)

	# ID

	id = Column(Integer, primary_key=True)

	# Monitoring ID

	monitoring_id = Column(Integer, ForeignKey("release_monitorings.id"))

	# Monitoring

	monitoring = sqlalchemy.orm.relationship("Monitoring", lazy="joined")

	# Version

	version = Column(Text, nullable=False)

	# Created At

	created_at = Column(
		DateTime(timezone=False), nullable=False, server_default=sqlalchemy.func.current_timestamp(),
	)

	# Delete

	async def delete(self, user=None):
		"""
			Deletes this release
		"""
		async with asyncio.TaskGroup() as tasks:
			# Delete the repository
			if self.repo:
				await self.repo.delete()

			# Delete the build
			if self.build:
				tasks.create_task(self.build.delete(user=user))

			# Close the bug
			tasks.create_task(
				self._close_bug(
					resolution="WONTFIX",
					comment="Release Monitoring for this package has been terminated",
				),
			)

	# Bug

	async def _create_bug(self):
		"""
			Creates a new bug report about this release
		"""
		args = {
			# Product, Version & Component
			"product"     : self.monitoring.distro.bugzilla_product,
			"version"     : self.monitoring.distro.bugzilla_version,
			"component"   : self.monitoring.name,

			# Summary & Description
			"summary"     : "%s has been released" % self,
			"description" : BUG_DESCRIPTION % \
				{
					"name"    : self.monitoring.name,
					"version" : self.version
				},

			# Keywords
			"keywords"    : [
				# Mark this bug as created automatically
				"Monitoring",

				# Mark this bug as a new release
				"NewRelease",
			],
		}

		# If we have a build, include it in the bug description
		if self.build:
			args |= {
				"description" : BUG_DESCRIPTION_WITH_BUILD % \
					{
						"name"    : self.monitoring.name,
						"version" : self.version,
						"url"     : await self.backend.url_to(self.build.url),
					},

				# Set the URL to point to the build
				"url" : await self.backend.url_to(self.build.url),
			}

		# Create the bug
		bug = await self.backend.bugzilla.create_bug(**args)

		# Store the bug ID
		self.bug_id = bug.id

		# Attach the diff (if we have one)
		if self.diff:
			await bug.attach(
				filename="%s.patch" % self,
				data=self.diff,
				summary="Patch for %s" % self,
				is_patch=True,
			)

		return bug

	async def _close_bug(self, *args, **kwargs):
		# Fetch the bug
		bug = await self.get_bug()

		if bug and not bug.is_closed():
			await bug.close(*args, **kwargs)

	# Bug ID

	bug_id = Column(Integer)

	async def get_bug(self):
		"""
			Fetches the bug from Bugzilla
		"""
		if self.bug_id:
			return await self.backend.bugzilla.get_bug(self.bug_id)

	# Repo ID

	repo_id = Column(Integer, ForeignKey("repositories.id"))

	# Repo

	repo = sqlalchemy.orm.relationship("Repo")

	# Build ID

	build_id = Column(Integer, ForeignKey("builds.id"))

	build = sqlalchemy.orm.relationship("Build")

	# Diff

	diff = Column(Text)

	async def _create_build(self, build, owner):
		"""
			Creates a build
		"""
		repo = None

		if self.build:
			raise RuntimeError("Build already exists")

		log.info("Creating build for %s from %s" % (self, build))

		try:
			# Create a new temporary space for the
			async with self.backend.tempdir() as target:
				# Create a new source package
				file = await self._update_source_package(build.pkg, target)

				if file:
					# Create a new repository
					repo = await self.backend.repos.create(
						self.monitoring.distro, "Test Build for %s" % self, owner=owner)

					# Upload the file
					upload = await self.backend.uploads.create_from_local(file)

					try:
						# Create a package
						package = await self.backend.packages.create(upload)

						# Create the build
						build = await self.backend.builds.create(repo, package, owner=owner)

					finally:
						await upload.delete()

		# If anything went wrong, then remove the repository
		except Exception as e:
			if repo:
				await repo.delete()

			raise e

		else:
			# Store the objects
			self.build = build
			self.repo  = repo

		# Launch the build
		await self.backend.builds.launch([build])

	async def _update_source_package(self, package, target):
		"""
			Takes a package and recreates it with this release
		"""
		if not package.is_source():
			raise RuntimeError("%s is not a source package" % package)

		# Capture Pakfire's log
		logger = config.PakfireLogger()

		# Create temporary directory to extract the package to
		try:
			async with self.backend.tempdir() as tmp:
				# Path to downloaded files
				files = os.path.join(tmp, "files")

				# Path to the makefile
				makefile = os.path.join(tmp, "%s.nm" % package.name)

				# Create a Pakfire instance from this distribution
				async with self.monitoring.distro.pakfire(logger=logger) as p:
					# Open the archive
					archive = await asyncio.to_thread(p.open, package.path)

					# Extract the archive into the temporary space
					await asyncio.to_thread(archive.extract, path=tmp)

					# XXX directories are being created with the wrong permissions
					os.system("chmod a+x -R %s" % tmp)

					# Remove any downloaded files
					await asyncio.to_thread(shutil.rmtree, files)

					# Update the makefile & store the diff
					self.diff = await self._update_makefile(makefile)

					# Log the diff
					log.info("Generated diff:\n%s" % self.diff)

					# Generate a new source package
					return await asyncio.to_thread(p.dist, makefile, target)

		# If we could not create a new source package, this is okay and we will continue
		# without. However, we will log the exception...
		except Exception as e:
			log.error("Could not create source package for %s" % self, exc_info=True)

			return None

		# Store the Pakfire log
		finally:
			self.log = "%s" % logger

	async def _update_makefile(self, path):
		"""
			Reads the makefile in path and updates it with the newer version
			returning a diff between the two.
		"""
		filename = os.path.basename(path)

		# Read the makefile
		with open(path, "r") as f:
			orig = f.readlines()

		# Replace the version & release
		updated = self._update_makefile_version(orig)

		# Write the new file
		with open(path, "w") as f:
			f.writelines(updated)

		# Generate a diff
		return "".join(
			difflib.unified_diff(orig, updated, fromfile=filename, tofile=filename),
		)

	def _update_makefile_version(self, lines, release=1):
		result = []

		# Walk through the file line by line and replace everything that
		# starts with version or release.
		for line in lines:
			if line and not line.startswith("#"):
				# Replace version
				m = re.match(r"^(version\s*=)\s*(.*)$", line)
				if m:
					line = "%s %s\n" % (m.group(1), self.version)

				# Replace release
				m = re.match(r"^(release\s*=)\s*(.*)$", line)
				if m:
					line = "%s %s\n" % (m.group(1), release)

			result.append(line)

		return result

	async def _build_finished(self):
		"""
			Called when the build has finished
		"""
		# Fetch the bug report
		bug = await self.get_bug()

		# Do nothing if there is no bug
		if not bug:
			return

		# If the build has been successful, ...
		if self.build.is_successful():
			await bug.update(comment=BUG_BUILD_SUCCESSFUL)

		# If the build failed, ...
		elif self.build.has_failed():
			# Say that the build has failed
			await bug.update(comment=BUG_BUILD_FAILED)

			# Append any logfiles from failed jobs
			for job in self.build.jobs:
				if not job.has_failed():
					continue

				# Open the logfile
				try:
					log = await job.open_log()
				except FileNotFoundError as e:
					log.warning("Could not open log file for %s" % job)
					continue

				# Attach it to the bug
				await bug.attach(summary="Log file for %s" % job, filename="%s.log" % job,
					data=log, content_type="text/plain")


class Monitorings(base.Object):
	baseurl = "https://release-monitoring.org"

	async def _request(self, method, url, data=None):
		body = {}

		# Fetch the API key
		api_key = self.backend.config.get("release-monitoring", "api-key")

		# Authenticate to the API
		headers = {
			"Authorization" : "Token %s" % api_key,
		}

		# Compose the url
		url = urllib.parse.urljoin(self.baseurl, url)

		if method == "GET":
			url = "%s?%s" % (url, urllib.parse.urlencode(data))

			# Reset data
			data = None

		# For POST requests, encode the payload in JSON
		elif method == "POST":
			data = urllib.parse.urlencode(data)

		# Send the request and wait for a response
		res = await self.backend.httpclient.fetch(url, method=method,
			headers=headers, body=data)

		# Decode JSON response
		if res.body:
			body = json.loads(res.body)

		# Check if we have received an error
		error = body.get("error")

		# Raise the error
		if error:
			raise RuntimeError(error)

		return body

	async def get_by_distro_and_name(self, distro, name):
		stmt = (
			sqlalchemy
			.select(Monitoring)
			.where(
				Monitoring.deleted_at == None,

				# Filter by the given distro and name
				Monitoring.distro == distro,
				Monitoring.name == name,
			)
		)

		return await self.db.fetch_one(stmt)

	async def create(self, distro, name, created_by, project_id,
			follow="stable", create_builds=True):
		monitoring = await self._get_monitoring("""
			INSERT INTO
				release_monitorings
			(
				distro_id,
				name,
				created_by,
				project_id,
				follow,
				create_builds
			)
			VALUES(
				%s, %s, %s, %s, %s, %s
			)
			RETURNING
				*
			""", distro, name, created_by, project_id, follow, create_builds,

			# Populate cache
			distro=distro,
		)

		return monitoring

	async def search(self, name):
		"""
			Returns a bunch of packages that match the given name
		"""
		# Send the request
		response = await self._request("GET", "/api/v2/projects",
			{
				"name"           : name,
				"items_per_page" : 250,
			},
		)

		# Return all packages
		return [database.Row(item) for item in response.get("items")]

	async def check(self, limit=None):
		"""
			Perform checks on monitorings
		"""
		# Fetch all monitorings that were never checked or checked longer than 24 hours ago
		monitorings = self._get_monitorings("""
			SELECT
				*
			FROM
				release_monitorings
			WHERE
				deleted_at IS NULL
			AND
				(
					last_check_at IS NULL
				OR
					last_check_at <= CURRENT_TIMESTAMP - INTERVAL '24 hours'
				)
			ORDER BY
				last_check_at ASC NULLS FIRST
			LIMIT
				%s
			""", limit,
		)

		async for monitoring in monitorings:
			await monitoring.check()


class Monitoring(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "release_monitorings"

	def __str__(self):
		return "%s - %s" % (self.distro, self.name)

	# ID

	id = Column(Integer, primary_key=True)

	@property
	def url(self):
		return "/distros/%s/monitorings/%s" % (self.distro.slug, self.name)

	# Distro ID

	distro_id = Column(Integer, ForeignKey("distributions.id"), nullable=False)

	# Distro

	distro = sqlalchemy.orm.relationship("Distro", lazy="joined")

	# Name

	name = Column(Text, nullable=False)

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Created By ID

	created_by_id = Column(Integer, ForeignKey("users.id"), nullable=False)

	# Created By

	created_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[created_by_id], lazy="joined",
	)

	# Deleted By ID

	deleted_by_id = Column(Integer, ForeignKey("users.id"))

	# Deleted By

	deleted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deleted_by_id], lazy="joined",
	)

	# Project ID

	project_id = Column(Integer, nullable=False)

	# Last Check At

	last_check_at = Column(DateTime(timezone=False))

	# Follow?

	follow = Column(Text, nullable=False)

	# Create Builds

	create_builds = Column(Boolean, nullable=False, default=True)

	# Permissions

	def has_perm(self, user=None):
		# Anonymous users can't perform any actions
		if user is None:
			return False

		# Users must be admins
		return user.is_admin()

	# Delete

	async def delete(self, user=None):
		# Mark as deleted
		await self._set_attribute_now("deleted_at")
		if user:
			await self._set_attribute("deleted_by", user)

		# Delete all releases
		async with asyncio.TaskGroup() as tasks:
			for release in self.releases:
				tasks.create_task(release.delete())

	# Check

	async def check(self):
		log.info("Checking for new releases for %s" % self)

		release = None

		# Fetch the current versions
		versions = await self._fetch_versions()

		# Fetch the latest release
		# XXX ???

		# Fetch the latest build
		latest_build = await self.get_latest_build()

		async with await self.db.transaction():
			# Store timestamp of this check
			self.last_check_at = sqlalchemy.func.current_timestamp()

			try:
				if self.follow == "latest":
					release = await self._follow_latest(versions)
				elif self.follow == "stable":
					release = await self._follow_stable(versions)
				elif self.follow == "current-branch":
					release = await self._follow_current_branch(versions, latest_build)
				else:
					raise ValueError("Cannot handle follow: %s" % self.follow)

			# If the release exists, do nothing
			except ReleaseExistsError as e:
				log.debug("Release %s already exists" % e)

			# The latest build is newer than this release
			except BuildExistsError as e:
				log.debug("Latest build is newer")

		# Dispatch any jobs
		await self.backend.jobs.queue.dispatch()

	async def _fetch_versions(self):
		"""
			Fetches all versions for this project
		"""
		# Wait until we are allowed to send an API request
		async with ratelimiter:
			response = await self.backend.monitorings._request(
				"GET", "/api/v2/versions/", {
					"project_id" : self.project_id,
				},
			)

		# Parse the response as JSON and return it
		return database.Row(response)

	async def _follow_stable(self, versions, *, build):
		"""
			This will follow "stable" i.e. the latest stable version
		"""
		for version in versions.stable_versions:
			return await self.create_release(version, build=build)

	async def _follow_latest(self, versions, * build):
		"""
			This will follow the latest version (including pre-releases)
		"""
		return await self.create_release(versions.latest_version, build=build)

	async def _follow_current_branch(self, versions, *, build):
		"""
			This will follow any minor releases in the same branch
		"""
		# We cannot perform this if there is no recent build
		if not build:
			return

		# Find the next version
		next_version = self._find_next_version(
			latest_build.pkg.evr, versions.stable_versions)

		# Create a new release with the next version
		if next_version:
			return await self.create_release(next_version, build=build)

	def _find_next_version(self, current_version, available_versions):
		# Remove epoch
		if ":" in current_version:
			epoch, delim, current_version = current_version.partition(":")

		# Remove release
		current_version, delim, release = current_version.rpartition("-")

		# Split the current version into parts
		current_version_parts = self._split_version(current_version)

		versions = {}

		# Find all versions that are interesting for us and store them with
		# how many parts are matching against the current version
		for version in available_versions:
			# Only consider later versions
			if pakfire.version_compare(current_version, version) >= 0:
				continue

			# Split the version into parts
			parts = self._split_version(version)

			# Count the number of parts that match at the beginning
			for i, (a, b) in enumerate(zip(current_version_parts, parts)):
				if not a == b:
					break

				# Store the number of matching parts
				versions[version] = i + 1

		# Fetch all versions with the highest number of matches
		versions = [v for v in versions if versions[v] == max(versions.values())]

		# Return the latest version
		for version in versions:
			return version

	@staticmethod
	def _split_version(version):
		"""
			Splits a version into its parts by any punctuation characters
		"""
		return re.split(r"[\.\-_]", version)

	# Releases

	def get_releases(self):
		stmt = (
			sqlalchemy
			.select(MonitoringRelease)
			.where(
				MonitoringRelease.monitoring == self,
			)
			.order_by(
				MonitoringRelease.created_at.desc(),
			)
		)

		return self.db.fetch(stmt)

	# Latest Release

	latest_release = sqlalchemy.orm.relationship("MonitoringRelease",
		order_by=MonitoringRelease.created_at.desc(), uselist=False, viewonly=True, lazy="joined",
	)

	async def _release_exists(self, version):
		"""
			Returns True if this version already exists
		"""
		return version in [release.version async for release in self.releases]

	async def create_release(self, version, *, build):
		"""
			Creates a new release for this package
		"""
		# XXX Do we need to check whether we are going backwards?

		# Raise an error if the release already exists
		if await self._release_exists(version):
			raise ReleaseExistsError(version)

		# Raise an error if we already have a newer build
		elif self._build_exists(version):
			raise BuildExistsError(version)

		log.info("%s: Creating new release %s" % (self, version))

		# Insert into database
		release = await self.db.insert(
			MonitoringRelease,
			monitoring = self,
			version    = version,
		)

		# Create a build
		if self.create_builds:
			await release._create_build(
				build = build,
				owner = self.backend.users.pakfire,
			)

		# Create a bug report
		await release._create_bug()

		# Return the release
		return release

	# Builds

	def _build_exists(self, version):
		"""
			Returns True if a build with this version already exists
		"""
		# If there is no build to check against we return False
		if not self.latest_build:
			return False

		# Compare the versions
		if pakfire.version_compare(self.latest_build.pkg.evr, version) > 0:
			return True

		return False

	async def get_latest_build(self):
		for build in await self.distro.get_builds(name=self.name, limit=1):
			return build
