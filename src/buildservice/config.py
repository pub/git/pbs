#!/usr/bin/python3

import asyncio
import collections
import configparser
import io
import logging
import logging.handlers
import pakfire

from . import base

# Setup logging
log = logging.getLogger("pbs.config")

class PakfireConfig(base.Object):
	def init(self, distro=None, repos=None, vendor=None, contact=None,
			arch=None, include_source=False, mirrored=True, build=None, logger=None):
		self.distro = distro
		self.repos  = repos

		# Overwritten for user repositories
		self.vendor = vendor
		self.contact = contact

		# Architecture
		self.arch = arch

		# Should the configuration include the source repositories?
		self.include_source = include_source

		# Should the repositories use any mirrors?
		self.mirrored = mirrored

		# Is this for a build?
		self.build = build

		# Log messages to here
		self.logger = logger

	# Repositories

	async def fetch_repos(self):
		"""
			Collects all repositories that are used in this configuration
		"""
		repos = set(self.repos or [])

		# Add all repositories belonging to the distribution
		if self.distro:
			repos |= await self.distro.get_repos()

		# Add all parent repositories
		for repo in repos:
			repos |= await repo.get_parents()

		return sorted(repos)

	# Context

	async def __aenter__(self):
		# Setup a new context
		ctx = pakfire.Ctx()

		# Set the cache path
		ctx.cache_path = "/pub/pakfire/.cache"

		# Configure the logger
		if self.logger:
			ctx.set_logger(self.logger)

		# Make configuration
		config = await self.config(local=True)

		# Log action
		log.debug("Launching Pakfire:")
		if config:
			log.debug(config)

		# Launch a new Pakfire instance (in a separate thread)
		return await asyncio.to_thread(
			pakfire.Pakfire,
			stub   = True,
			ctx    = ctx,
			arch   = self.arch,
			config = config,
		)

	async def __aexit__(self, type, value, traceback):
		pass

	async def config(self, local=False):
		"""
			Generates the configuration file
		"""
		config = configparser.ConfigParser(
			interpolation   = None,
			default_section = "general",
		)

		# Fetch all repositories
		repos = await self.fetch_repos()

		# Add the distro configuration
		if self.distro:
			self.distro.write_config(
				config,
				local   = local,
				vendor  = self.vendor,
				contact = self.contact,
			)

		# Add the repository configurations
		for repo in sorted(repos):
			repo.write_config(
				config,
				local          = local,
				include_source = self.include_source,
				mirrored       = self.mirrored,
				build          = self.build,
			)

		# Allocate a buffer and write the configuration to it
		f = io.StringIO()
		config.write(f)

		return f.getvalue()


class PakfireLogger(object):
	"""
		This is a simple logger instance which collects all log
		messages which can in the end be exported to for example being stored in the database.
	"""
	id = 0

	class QueueHandler(logging.handlers.QueueHandler):
		"""
			This calls overwrites the enqueue() method so that
			we can use an alternative queue which allows us to
			iterate over its data without removing anything.
		"""
		def enqueue(self, record):
			self.queue.append(record)

	def __init__(self, level=logging.DEBUG):
		# Create a new child logger
		self.log = log.getChild("logger-%s" % self.id)
		self.log.setLevel(level)

		# Increment the ID
		self.id += 1

		# Propagate messages to the parent logger
		self.log.propagate = True

		# Create a new queue to buffer any messages
		self.queue = collections.deque()

		# Create a log formatter
		formatter = logging.Formatter(
			"%(asctime)s %(levelname)-8s %(message)s"
		)

		# Create a queue handler
		handler = self.QueueHandler(self.queue)
		handler.setFormatter(formatter)

		# Register the queue with the logger
		self.log.addHandler(handler)

	def __str__(self):
		"""
			Returns the entire log as a string
		"""
		return "\n".join((record.getMessage() for record in self.queue))

	def log(self, *args, **kwargs):
		"""
			Logs a message
		"""
		return self.log.log(*args, **kwargs)
