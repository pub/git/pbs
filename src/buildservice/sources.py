#!/usr/bin/python

import asyncio
import datetime
import fnmatch
import functools
import logging
import os
import re
import sqlalchemy
import tempfile

from sqlalchemy import Column, ForeignKey
from sqlalchemy import DateTime, Integer, Text

from . import base
from . import database
from . import config
from . import misc

from .constants import *

# Setup logging
log = logging.getLogger("pbs.sources")

# Fetch only up to the last N commits
GIT_DEPTH = 256

VALID_TAGS = (
	"Acked-by",
	"Cc",
	"Fixes",
	"Reported-by",
	"Reviewed-by",
	"Signed-off-by",
	"Suggested-by",
	"Tested-by",
)

class Sources(base.Object):
	def __aiter__(self):
		sources = self._get_sources("""
			SELECT
				*
			FROM
				sources
			WHERE
				deleted_at IS NULL
			ORDER BY
				created_at
			""",
		)

		return aiter(sources)

	async def get_by_slug(self, slug):
		stmt = (
			sqlalchemy
			.select(Source)
			.where(
				Source.deleted_at == None,
				Source.slug == slug,
			)
		)

		return await self.db.fetch_one(stmt)

	async def create(self, repo, name, url, user):
		# Make slug
		slug = self._make_slug(name)

		# Insert into the database
		source = await self.db.insert(
			Source,
			name       = name,
			url        = url,
			created_by = created_by,
			repo       = repo,
		)

		return source

	def _make_slug(self, name):
		for i in misc.infinity():
			slug = misc.normalize(name, iteration=i)

			# Check if the source already exists
			source = self.get_by_slug(slug)
			if source:
				continue

			return slug

	# Fetch

	async def fetch(self, run_jobs=True):
		"""
			Fetches any new commits from all sources
		"""
		# Fetch all sources concurrently
		async with asyncio.TaskGroup() as tg:
			for source in self:
				tg.create_task(source.fetch())

		# Process any pending jobs
		if run_jobs:
			await self.run_jobs()

	# Run jobs

	@property
	def pending_jobs(self):
		"""
			Returns a generator of all pending jobs
		"""
		return self._get_jobs("""
			SELECT
				source_commit_jobs.*
			FROM
				sources
			JOIN
				source_commits ON sources.id = source_commits.source_id
			JOIN
				source_commit_jobs ON source_commits.id = source_commit_jobs.commit_id
			WHERE
				sources.deleted_at IS NULL
			AND
				source_commits.finished_at IS NULL
			AND
				source_commit_jobs.finished_at IS NULL
			ORDER BY
				sources.created_at, source_commits.created_at
			""",
		)

	async def run_jobs(self):
		"""
			Runs all unfinished jobs of all sources
		"""
		# Run jobs one after the other
		async for job in self.pending_jobs:
			await job.run()


class Source(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "sources"

	def __str__(self):
		return self.name

	# ID

	id = Column(Integer, primary_key=True)

	# Name

	name = Column(Text, nullable=False)

	# Slug

	slug = Column(Text, unique=True, nullable=False)

	# URL

	url = Column(Text, nullable=False)

	# Gitweb

	gitweb = Column(Text)

	# Revision

	revision = Column(Text)

	# Branch

	branch = Column(Text, nullable=False)

	# Last Fetched At

	last_fetched_at = Column(DateTime(timezone=False))

	# Repo ID

	repo_id = Column(Integer, ForeignKey("repositories.id"), nullable=False)

	# Repo

	repo = sqlalchemy.orm.relationship(
		"Repo", foreign_keys=[repo_id], lazy="joined", innerjoin=True,
	)

	# Distro

	@property
	def distro(self):
		return self.repo.distro

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Created By ID

	created_by_id = Column(Integer, ForeignKey("users.id"))

	# Created By

	created_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[created_by_id], lazy="joined", innerjoin=True,
	)

	# Deleted By ID

	deleted_by_id = Column(Integer, ForeignKey("users.id"))

	# Deleted By

	deleted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deleted_by_id], lazy="joined",
	)

	# Path

	@property
	def path(self):
		"""
			Returns a unique path this repository lives in
		"""
		return self.backend.path(
			"sources",
			self.repo.path,
			self.slug,
		)

	@functools.cached_property
	def git(self):
		# Setup the Git repository
		return Git(self.backend, self.path, self.url, self.branch)

	# Commits

	async def _create_commit(self, revision, initial_commit=False):
		"""
			Imports the commit with the given revision
		"""
		log.info("%s: Creating commit %s" % (self, revision))

		# Fetch the author's name and email address
		author = await self.git.show_attribute(revision, r"%an <%ae>")

		# Fetch the committer's name and email address
		committer = await self.git.show_attribute(revision, r"%cn <%ce>")

		# Subject
		subject = await self.git.show_attribute(revision, r"%s")

		# Body
		body = await self.git.show_attribute(revision, r"%b")

		# Date
		date = datetime.datetime.fromisoformat(
			await self.git.show_attribute(revision, r"%aI"),
		)

		# Create a new build group
		group = self.backend.builds.create_group()

		# Insert into the database
		commit = await self.db.insert(
			Commit,
			source    = self,
			revision  = revision,
			author    = author,
			committer = committer,
			subject   = subject,
			body      = body,
			date      = date,
			group     = group,
		)

		# If we are processing the initial commit, we get a list of all files in the tree
		if initial_commit:
			changed_files = [("A", f) for f in await self.git.ls_tree(revision, "*/*.nm")]

		# Find changed files
		else:
			changed_files = await self.git.changed_files(revision, filter="*/*.nm")

		# Create jobs for each file
		for status, changed_file in changed_files:
			# Find the package name
			name = os.path.dirname(changed_file)

			log.debug("%s: %s: Processing '%s' (status = %s)" % (self, revision, name, status))

			# Check that the file part matches
			if not changed_file.endswith("/%s.nm" % name):
				raise ValueError("Invalid package name")

			# If a makefile has been added or modified, we will run "dist"
			if status == "A" or status == "M":
				action = "dist"

			# If a makefile has been removed, we deprecate all former builds
			elif status == "D":
				action = "deprecate"

			# Break on any other
			else:
				raise RuntimeError("Unhandled status %s for file %s" % (status, filename))

			# Create job
			commit._create_job(action, name)

		return commit

	async def get_commits(self, limit=None):
		# XXX using the ID is an incorrect way to sort them
		stmt = (
			sqlalchemy
			.select(
				SourceCommit,
			)
			.where(
				SourceCommit.source == self,
			)
			.order_by(
				SourceCommit.id.desc(),
			)
			.limit(limit)
		)

		return await self.db.fetch_as_list(stmt)

	async def get_commit(self, revision):
		stmt = (
			sqlalchemy
			.select(
				SourceCommit,
			)
			.where(
				SourceCommit.source == self,
				SourceCommit.revision == revision,
			)
		)

		return await self.db.fetch_one(stmt)

	# Fetch

	async def fetch(self):
		"""
			Fetches any new commits from this source
		"""
		log.debug("Fetching %s" % self)

		# Update or clone the repository
		if self.git.is_cloned():
			await self.git.fetch()
		else:
			await self.git.clone()

		with self.db.transaction():
			# Did we already import something?
			initial_commit = not self.revision

			# Determine which commits there are to process
			revisions = await self.git.revisions(self.revision, self.branch)

			# Import all revisions
			for revision in revisions:
				# Import the commit
				await self._create_commit(revision, initial_commit=initial_commit)

				# Store the updated revision
				self._set_attribute("revision", revision)

				# Only the first revision would the initial commit
				initial_commit = False

			# Store when we fetched
			self._set_attribute_now("last_fetched_at")


class SourceCommit(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "source_commits"

	def __str__(self):
		return self.subject or self.revision

	# ID

	id = Column(Integer, primary_key=True)

	# Source ID

	source_id = Column(Integer, ForeignKey("sources.id"), nullable=False)

	# Source

	source = sqlalchemy.orm.relationship(
		"Source", foreign_keys=[source_id], lazy="joined",
	)

	# Revision

	revision = Column(Text, nullable=False)

	# Author

	author = Column(Text, nullable=False)

	# Committer

	committer = Column(Text, nullable=False)

	# Date

	date = Column(DateTime(timezone=False), nullable=False)

	# Subject

	subject = Column(Text, nullable=False)

	# Body

	body = Column(Text, nullable=False)

	@functools.cached_property
	def message(self):
		"""
			Returns the message without Git tags
		"""
		lines = []

		# Remove any Git tags
		for line in self.body.splitlines():
			m = re.match(r"^[A-Za-z\-]+:", line)
			if m:
				continue

			lines.append(line)

		# Join the message back together again
		message = "\n".join(lines)

		# Remove excess whitespace
		message = message.strip()

		return message

	@functools.cached_property
	def tags(self):
		tags = {}

		# For Fixes:, we only want to extract the bug ID
		results = re.findall(r"""
			# Lines must start with Fixes:
			^Fixes:

			# Consume any extra whitespace
			\s*

			# There might be a leading # for the ID
			\#?

			# The ID
			([0-9]+)
		""", self.body, re.IGNORECASE|re.MULTILINE|re.VERBOSE)

		if results:
			tags["Fixes"] = results

		# Match anything else
		results = re.findall(r"""
			# The tag must start at the beginning of the line
			# only containing letters and hyphens followed by colon
			^((?!Fixes)[A-Za-z\-]+):

			# Consume any extra whitespace
			\s*

			# Consume the rest of the line in a non-greedy fashion
			(.*?)

			# Ignore any trailing whitespace
			\s*

			# We must have read the entire line
			$
		""", self.body, re.IGNORECASE|re.MULTILINE|re.VERBOSE)

		# Store all tags
		for tag, value in results:
			try:
				tags[tag].append(value)
			except KeyError:
				tags[tag] = [value]

		return tags

	def get_watchers(self):
		"""
			Returns a list of all known users involved in this commit
			which should be added as watchers to builds.
		"""
		tags = (
			"Acked-by",
			"Cc",
			"Reported-by",
			"Reviewed-by",
			"Signed-off-by",
			"Suggested-by",
			"Tested-by",
		)

		# Add the author
		users = set((
			self.data.author,
		))

		# Add users from tags
		for tag in tags:
			users.update(self.tags.get(tag, []))

		return self.backend.users._search_by_email(users)

	async def get_fixed_bugs(self):
		"""
			Returns a list of all fixed bugs
		"""
		bug_ids = self.tags.get("Fixes")

		if not bug_ids:
			return []

		return await self.backend.bugzilla.get_bugs(bug_ids)

	# Build Group ID

	build_group_id = Column(Integer, ForeignKey("build_groups.id"))

	# Build Group

	builds = sqlalchemy.orm.relationship(
		"BuildGroup", foreign_keys=[build_group_id], lazy="selectin",
	)

	# Jobs

	async def _create_job(self, action, name):
		"""
			Creates a new job
		"""
		log.info("%s: %s: Created '%s' job for '%s'" \
			% (self.source, self.revision, action, name))

		job = await self.backend.sources._get_job("""
			INSERT INTO
				source_commit_jobs
			(
				commit_id,
				action,
				name
			)
			VALUES
			(
				%s, %s, %s
			)
			RETURNING *
			""", self.id, action, name,
		)

		# Append to cache
		self.jobs.add(job)

		return job

	@functools.cached_property
	def jobs(self):
		jobs = self.backend.sources._get_jobs("""
			SELECT
				*
			FROM
				source_commit_jobs
			WHERE
				commit_id = %s
			""", self.id,
		)

		return set(jobs)

	async def _job_has_finished(self, job):
		"""
			Called when a job has finished
		"""
		# Find any pending/failed jobs
		for job in self.jobs:
			if job.is_pending():
				return

			if job.has_failed():
				return

		# If we get here, all jobs must have finished successfully
		self.finished_at = sqlalchemy.func.current_timestamp()


class SourceJob(database.Base, database.BackendMixin):
	__tablename__ = "source_commit_jobs"

	# ID

	id = Column(Integer, primary_key=True)

	# Commit ID

	commit_id = Column(Integer, ForeignKey("source_commits.id"), nullable=False)

	# Commit

	commit = sqlalchemy.orm.relationship(
		"SourceCommit", foreign_keys=[commit_id], lazy="joined",
	)

	# Action

	action = Column(Text, nullable=False)

	# Name

	name = Column(Text, nullable=False)

	# Finished At

	finished_at = Column(DateTime(timezone=False))

	# Error

	error = Column(Text)

	# Status

	def is_pending(self):
		return not self.has_finished()

	def has_finished(self):
		if self.finished_at:
			return True

		return False

	def has_failed(self):
		return self.has_finished() and self.error

	# Run

	async def run(self):
		"""
			Runs the job
		"""
		build = None

		log.debug("Running %s..." % self)

		with self.db.transaction():
			# Run action
			if self.action == "dist":
				await self._run_dist()

			elif self.action == "deprecate":
				await self._run_deprecate()

			else:
				raise RuntimeError("Unhandled action: %s" % self.action)

			# Mark as finished
			self.finished_at = sqlalchemy.func.current_timezone()

			# Report that this job has finished if there is no error
			if not self.error:
				await self.commit._job_has_finished(job=self)

		# Launch all jobs (in the background)
		if build:
			self.backend.run_task(self.backend.builds.launch, [build])

	async def _run_deprecate(self):
		"""
			Called to deprecate any builds that match any of the name
		"""
		builds = self.backend.builds._get_builds("""
			SELECT
				builds.*
			FROM
				repositories
			LEFT JOIN
				repository_builds ON repositories.id = repository_builds.repo_id
			LEFT JOIN
				builds ON repository_builds.build_id = builds.id
			LEFT JOIN
				packages ON builds.pkg_id = packages.id
			WHERE
				repositories.deleted_at IS NULL
			AND
				repositories.distro_id = %s
			AND
				repositories.owner_id IS NULL
			AND
				repository_builds.removed_at IS NULL
			AND
				builds.deleted_at IS NULL
			AND
				builds.deprecated_by IS NULL
			AND
				packages.deleted_at IS NULL
			AND
				packages.name = %s
			""", self.source.distro, self.name,
		)

		# Deprecate all matching builds
		for build in builds:
			build.deprecate()

	async def _run_dist(self):
		"""
			Called to run dist on the given package
		"""
		upload = None

		# Collect the log from Pakfire
		logger = config.PakfireLogger()

		try:
			# Create a new temporary directory and check out the requested revision
			async with self.backend.tempdir() as path:
				# Checkout sources
				await self.source.git.checkout(self.commit.revision, path, self.name)

				# Create a path for the source packages
				target = os.path.join(path, ".target")

				# Find the absolute path of the makefile
				makefile = os.path.join(path, "%s/%s.nm" % (self.name, self.name))

				# Break if there is no makefile
				if not await self.backend.exists(makefile):
					raise RuntimeError("Could not find makefile for %s" % self.name)

				# Launch a Pakfire instance with the repository configuration
				async with self.source.repo.pakfire(logger=logger) as p:
					log.debug("Running dist for %s..." % makefile)

					# Run dist() in a separate thread
					file = await asyncio.to_thread(p.dist, makefile, target)

				# Upload the file
				upload = await self.backend.uploads.create_from_local(file)

			# Create the package
			package = await self.backend.packages.create(upload, commit=self.commit)

			# Create a new build (without updating the repository immediately)
			build = await self.backend.builds.create(
				self.source.repo, package, group=self.commit.builds)

			# Add any watchers
			for user in self.commit.get_watchers():
				await build.add_watcher(user)

			# Launch the build
			await self.backend.builds.launch([build])

		# Catch any exceptions and log them
		except Exception as e:
			log.error("Error running %s: " % self, exc_info=True)

			# Store the error
			self.error = "%s" % e

		# Always delete the upload & store the log
		finally:
			if upload:
				await upload.delete()

			# Store log
			self.log = "%s" % logger


class Git(object):
	"""
		A simple abstraction layer to deal with Git repositories
	"""
	def __init__(self, backend, path, url, branch):
		self.backend = backend
		self.path = path
		self.url = url
		self.branch = branch

	def __repr__(self):
		return "<%s %s>" % (self.__class__.__name__, self.path)

	# Locks so that we will access a repository once at a time
	__locks = {}

	@property
	def lock(self):
		try:
			lock = self.__locks[self.path]
		except KeyError:
			lock = self.__locks[self.path] = asyncio.Lock()

		return lock

	async def command(self, *args, **kwargs):
		"""
			Executes a Git command
		"""
		cwd = None

		# Operate inside the repository once it has been cloned
		if self.is_cloned():
			cwd = self.path

		async with self.lock:
			return await self.backend.command("git", *args, cwd=cwd, **kwargs)

	def is_cloned(self):
		"""
			Returns True if this repository has already been cloned
		"""
		return os.path.exists(self.path)

	async def clone(self):
		"""
			Clones the Git repository
		"""
		if self.is_cloned():
			log.debug("%s is already cloned" % self)
			return

		log.info("Cloning %s to %s" % (self.url, self.path))

		await self.command(
			"clone",

			# Keep a bare copy
			"--bare",

			# Make it shallow
			"--depth", "%s" % GIT_DEPTH,

			# Only copy the branch we are interested in
			"--single-branch",
			"--branch", self.branch,

			# URL
			self.url,

			# Path
			self.path,
		)

	async def fetch(self):
		"""
			Fetches any changes
		"""
		await self.command("fetch", self.url, "%s:%s" % (self.branch, self.branch))

	async def show_attribute(self, revision, format):
		return await self.command(
			"show",
			"--no-patch",
			"--format=format:%s" % format,
			revision,
			return_output=True,
		)

	async def show_ref(self, branch):
		"""
			Resolves a ref into a hash
		"""
		return await self.command("show-ref", "--hash", branch, return_output=True)

	async def revisions(self, start, end):
		"""
			Returns a list with all revisions between start and end
		"""
		if start is None:
			return [await self.show_ref(end)]

		# Fetch the hashes of all revisions
		revisions = await self.command("log", "--format=%H",
			"%s..%s" % (start, end), return_output=True)

		# Return them in reverse order from oldest to newest
		return reversed(revisions.splitlines())

	async def changed_files(self, revision, filter=None):
		"""
			Returns a list of files that has been changed
		"""
		changed_files = []

		output = await self.command(
			"show",

			# Don't show anything else
			"--format=format:",

			# Show the status of each changed file
			"--name-status",

			# Revision
			revision,

			# Ask for the output to be returned
			return_output=True,
		)

		for line in output.splitlines():
			m = re.match(r"^([A-Z])\s+(.*)$", line)
			if not m:
				raise ValueError("Could not parse line: %s" % line)

			# Extract values
			status, filename = m.groups()

			# Filter out anything unwanted
			if filter and not fnmatch.fnmatch(filename, filter):
				continue

			# Append the file to the list
			changed_files.append((status, filename))

		return changed_files

	async def ls_tree(self, revision, filter=None):
		"""
			Returns the paths of all files in the given tree
		"""
		files = []

		output = await self.command(
			"ls-tree",

			# Give us the filenames only
			"--name-only",

			# Recurse into sub-trees
			"-r",

			# Revision
			revision,

			# Ask for the output to be returned
			return_output=True,
		)

		for filename in output.splitlines():
			# Filter out anything we don't want
			if filter and not fnmatch.fnmatch(filename, filter):
				continue

			files.append(filename)

		return files

	async def checkout(self, revision, destination, path=None):
		"""
			Creates a working directory at the revision in path
		"""
		log.debug("Checking out %s into %s" % (revision, path))

		await self.command(
			# Set the destination path as work tree
			"--work-tree", destination,

			# Perform a checkout of the specified revision
			"checkout", revision, path or ".",
		)
