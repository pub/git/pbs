#!/usr/bin/python

import aiofiles
import asyncio
import configparser
import datetime
import functools
import logging
import magic
import os
import shutil
import ssl
import stat
import systemd.journal
import tempfile
import urllib.parse

from . import aws
from . import bugtracker
from . import builders
from . import builds
from . import cache
from . import config
from . import database
from . import distros
from . import events
from . import httpclient
from . import jobs
from . import keys
from . import logstreams
from . import messages
from . import mirrors
from . import packages
from . import ratelimiter
from . import releasemonitoring
from . import repos
from . import sessions
from . import sources
from . import uploads
from . import users

# Setup logging
log = logging.getLogger("pbs")

# Import version
from .__version__ import VERSION as __version__

from .constants import *
from .decorators import *

class Backend(object):
	version = __version__

	# A list of any background tasks
	__tasks = set()

	def __init__(self, config_file, test=False):
		self.test = test

		# Read configuration file
		self.config = self.read_config(config_file)

		# Fetch the base path
		self.basepath = self.config.get("global", "basepath")

		# Initialize the HTTP Client
		self.httpclient = httpclient.HTTPClient(self)

		self.cache       = cache.Cache(self)
		self.aws         = aws.AWS(self)
		self.builds      = builds.Builds(self)
		self.jobs        = jobs.Jobs(self)
		self.builders    = builders.Builders(self)
		self.distros     = distros.Distros(self)
		self.events      = events.Events(self)
		self.keys        = keys.Keys(self)
		self.logstreams  = logstreams.LogStreams(self)
		self.messages    = messages.Messages(self)
		self.mirrors     = mirrors.Mirrors(self)
		self.packages    = packages.Packages(self)
		self.monitorings = releasemonitoring.Monitorings(self)
		self.ratelimiter = ratelimiter.RateLimiter(self)
		self.repos       = repos.Repos(self)
		self.sessions    = sessions.Sessions(self)
		self.sources     = sources.Sources(self)
		self.uploads     = uploads.Uploads(self)
		self.users       = users.Users(self)

		# Open a connection to bugzilla.
		self.bugzilla          = bugtracker.Bugzilla(self)

		# Create a temporary directory
		self._create_tmp_path()

		log.info("Pakfire Build Service initialized at %s" % self.basepath)

	def launch_background_tasks(self):
		# Launch some initial tasks
		self.run_task(self.builders.autoscale)

		# Regularly sync data to the mirrors
		self.run_periodic_task(300, self.sync)

		# Regularly check the mirrors
		self.run_periodic_task(300, self.mirrors.check)

		# Regularly fetch sources
		#self.run_periodic_task(300, self.sources.fetch)

		# Regularly check for new releases
		# XXX Disabled for now
		#self.run_periodic_task(300, self.monitorings.check)

		# Regularly try sending messages
		self.run_periodic_task(600, self.messages.queue.send)

		# Cleanup regularly
		self.run_periodic_task(3600, self.cleanup)

		# Automatically abort any jobs that run for forever
		self.run_periodic_task(60, self.jobs.abort)

	def read_config(self, path):
		c = configparser.ConfigParser()

		# Read configuration from file
		if path:
			c.read(path)

		return c

	@functools.cached_property
	def db(self):
		try:
			name     = self.config.get("database", "name")
			hostname = self.config.get("database", "hostname")
			user     = self.config.get("database", "user")
			password = self.config.get("database", "password")
		except configparser.Error as e:
			log.error("Error parsing the config: %s" % e.message)

		log.debug("Connecting to database %s @ %s" % (name, hostname))

		return database.Connection(self, hostname, name, user=user, password=password)

	def _create_tmp_path(self):
		"""
			This function will create some temporary space with the correct permissions.
		"""
		path = self.path("tmp")

		try:
			os.mkdir(path, mode=0o1777)

		# Ignore if the directory already exists
		except FileExistsError:
			pass

	def path(self, *args):
		"""
			Takes a relative path and makes it absolute
		"""
		# Make the path
		path = os.path.join(self.basepath, *args)

		# Make the path absolute
		path = os.path.abspath(path)

		# Ensure that the path is inside the base path
		if not path.startswith("%s/" % self.basepath):
			raise ValueError("Invalid path: %s" % path)

		return path

	def relpath(self, path):
		"""
			Does the reverse of path()
		"""
		return os.path.relpath(path, self.basepath)

	def url_to(self, *args):
		"""
			Takes a relative URL and makes it absolute
		"""
		# The base URL
		baseurl = self.config.get("global", "baseurl")

		# Join the paths together first
		url = os.path.join(*args)

		# Make the URL
		return urllib.parse.urljoin(baseurl, url)

	def path_to_url(self, path, mirrored=False):
		"""
			Takes a path to a file on the file system and converts it into a URL
		"""
		# Make the path relative
		path = self.relpath(path)

		# Send to the downloads load balancer
		if mirrored:
			return self.url_to("downloads", path)

		# Send them to the local file store
		return self.url_to("files", path)

	def pakfire(self, *args, **kwargs):
		"""
			Launches a new Pakfire instance with the given configuration
		"""
		return config.PakfireConfig(self, *args, **kwargs)

	# Functions to run something in background

	def run_task(self, callback, *args, **kwargs):
		"""
			Runs the given coroutine in the background
		"""
		# Create a new task
		task = asyncio.create_task(callback(*args, **kwargs))

		# Keep a reference to the task and remove it when the task has finished
		self.__tasks.add(task)
		task.add_done_callback(self.__tasks.discard)

		return task

	def run_periodic_task(self, delay, callback, *args):
		"""
			Calls the given callback periodically in the background
		"""
		self.run_task(self._periodic_task, delay, callback, *args)

	async def _periodic_task(self, delay, callback, *args):
		"""
			Helper function for run_periodic_task() that will call the given
			callback regulary after the timer has expired.
		"""
		log.debug("Periodic callback %r started" % callback)

		while True:
			# Run the callback in a separate task
			self.run_task(callback, *args)

			# Wait a little moment
			await asyncio.sleep(delay)

	# Commands

	async def command(self, *command, krb5_auth=False, **kwargs):
		"""
			Runs this shell command
		"""
		async with self.tempdir() as tmp:
			# Create a minimal environment
			env = {
				"HOME"       : os.environ.get("HOME", tmp),

				# Tell the system where to put temporary files
				"TMPDIR"     : tmp,

				# Store any Kerberos credentials here
				"KRB5CCNAME" : os.path.join(tmp, ".krb5cc"),
			}

			# Authenticate using Kerberos
			if krb5_auth:
				await self._krb5_auth(env=env)

			# Run the command
			return await self._command(*command, env=env, **kwargs)

	async def _command(self, *command, return_output=False, input=None, **kwargs):
		log.debug("Running command: %s" % " ".join(command))

		# Fork child process
		process = await asyncio.create_subprocess_exec(
			*command,
			stdin=asyncio.subprocess.PIPE if input else asyncio.subprocess.DEVNULL,
			stdout=asyncio.subprocess.PIPE,
			stderr=asyncio.subprocess.PIPE,
			**kwargs,
		)

		# Send input
		if input:
			# Convert to bytes
			if not isinstance(input, bytes):
				input = input.encode()

			# Write the entire data chunk by chunk
			while input:
				chunk, input = input[0:64], input[64:]
				if not chunk:
					break

				process.stdin.write(chunk)
				await process.stdin.drain()

			# Close the input once we are done
			process.stdin.close()

		stdout = []

		# Fetch output of command and send it to the logger
		while True:
			line = await process.stdout.readline()
			if not line:
				break

			# Decode line
			line = line.decode()

			# Strip newline
			line = line.rstrip()

			# Log the output
			log.debug(line)

			# Store the output if requested
			if return_output:
				stdout.append(line)

		# Wait until the process has finished
		returncode = await process.wait()

		# Check the return code
		if returncode:
			# Fetch any output from the standard error output
			stderr = await process.stderr.read()
			stderr = stderr.decode()

			# Log the error
			log.error("Error running command: %s (code=%s)" % (
				" ".join(command), returncode,
			))
			if stderr:
				log.error(stderr)

			raise CommandExecutionError(returncode, stderr)

		# Return output if requested
		if return_output:
			return "\n".join(stdout)

	async def _krb5_auth(self, **kwargs):
		log.debug("Performing Kerberos authentication...")

		# Fetch path to keytab
		keytab = self.config.get("krb5", "keytab")
		if not keytab:
			log.warning("No keytab configured")
			return

		# Fetch Kerberos principal
		principal = self.config.get("krb5", "principal")
		if not principal:
			log.warning("No Kerberos principal configured")
			return

		# Fetch a Kerberos ticket
		await self._command(
			"kinit", "-k", "-t", keytab, principal, **kwargs,
		)

	@run_in_thread
	def exists(self, path):
		"""
			Checks whether a file exists
		"""
		# Make the path absolute
		if not path.startswith("/"):
			path = self.path(path)

		return os.path.exists(path)

	@run_in_thread
	def stat(self, path, fmt=None):
		"""
			stat()s a file
		"""
		# Make the path absolute
		if not path.startswith("/"):
			path = self.path(path)

		# Stat the file
		try:
			s = os.stat(path)

		# Return nothing if the file does not exist
		except FileNotFoundError as e:
			return

		# Check if the format matches
		if fmt:
			# Return nothing if the format does not match
			if not stat.S_IFMT(s.st_mode) == fmt:
				return

		return s

	# Initialize libmagic
	magic = magic.Magic(mime=True, uncompress=False)

	@run_in_thread
	def mimetype(self, path):
		# Make the path absolute
		if not path.startswith("/"):
			path = self.path(path)

		return self.magic.from_file(path)

	@run_in_thread
	def makedirs(self, path, **kwargs):
		"""
			Creates a new directory
		"""
		return os.makedirs(path, **kwargs)

	async def make_parent_directory(self, path):
		"""
			Creates the parent directory of path
		"""
		path = os.path.dirname(path)

		return await self.makedirs(path, exist_ok=True)

	async def copy(self, src, dst, mode=None):
		"""
			Copies a file from src to dst
		"""
		log.debug("Copying %s to %s" % (src, dst))

		# Create parent directory
		await self.make_parent_directory(dst)

		# Copy data without any metadata
		await asyncio.to_thread(shutil.copyfile, src, dst)

		# Set mode
		if mode:
			await self.chmod(dst, mode)

	async def move(self, src, dst, **kwargs):
		"""
			Moves something from src to dst
		"""
		log.debug("Moving %s to %s" % (src, dst))

		# Create parent directory
		await self.make_parent_directory(dst)

		# Move!
		await asyncio.to_thread(shutil.move, src, dst, **kwargs)

	@run_in_thread
	def unlink(self, path):
		"""
			Unlinks path
		"""
		# Normalize the path
		path = os.path.abspath(path)

		# Check if the path is within our base directory
		if not path.startswith(self.basepath):
			raise OSError("Cannot delete %s which is outside %s" % (path, self.basepath))

		log.debug("Unlinking %s" % path)

		# Unlink the file we were asked to unlink
		try:
			os.unlink(path)
		except OSError as e:
			return

		# Try to delete any empty parent directories
		while True:
			# Get the parent directory
			path = os.path.dirname(path)

			# Break if we reached the base path
			if path == self.basepath or path == self.path("tmp"):
				break

			# Call rmdir()
			try:
				os.rmdir(path)
			except OSError as e:
				break

			log.debug("  Cleaned up %s..." % path)

	@run_in_thread
	def rmtree(self, path):
		"""
			Removes a directory recursively
		"""
		# Normalize the path
		path = os.path.abspath(path)

		# Check if the path is within our base directory
		if not path.startswith(self.basepath):
			raise OSError("Cannot delete %s which is outside %s" % (path, self.basepath))

		log.debug("Removing %s..." % path)

		try:
			shutil.rmtree(path)

		# Ignore if path didn't exist in the first place
		except FileNotFoundError:
			pass

	@run_in_thread
	def chmod(self, *args, **kwargs):
		return os.chmod(*args, **kwargs)

	def tempfile(self, mode="w+b", sync=False, **kwargs):
		"""
			Returns an open file handle to a new temporary file
		"""
		path = self.path("tmp")

		# If requested, return a sync temporary file
		if sync:
			return tempfile.NamedTemporaryFile(mode=mode, dir=path, **kwargs)

		return aiofiles.tempfile.NamedTemporaryFile(mode=mode, dir=path, **kwargs)

	def tempdir(self, **kwargs):
		"""
			Asynchronously creates a new temporary directory
		"""
		# Default to our own tmp directory
		path = self.path("tmp")

		return aiofiles.tempfile.TemporaryDirectory(dir=path, **kwargs)

	async def open(self, path):
		"""
			Opens a package and returns the archive
		"""
		log.debug("Opening %s..." % path)

		# Open the archive
		async with self.pakfire() as p:
			return await asyncio.to_thread(p.open, path)

	@property
	def ssl_context(self):
		"""
			Returns a SSL context with our client certificate
		"""
		# Create SSL context
		context = ssl.create_default_context()

		# Fetch client certificate
		certificate = self.config.get("ssl", "certificate")
		key         = self.config.get("ssl", "key")

		# Raise an error if we don't have certificates
		if not certificate or not key:
			raise RuntimeError("Missing SSL configuration")

		# Load the certificate chain
		context.load_cert_chain(certificate, key)

		return context

	async def cleanup(self):
		"""
			Called regularly to cleanup any left-over resources
		"""
		# Cache
		await self.cache.cleanup()

		# Messages
		await self.messages.queue.cleanup()

		# Ratelimiter
		await self.ratelimiter.cleanup()

		# Sessions
		await self.sessions.cleanup()

		# Uploads
		await self.uploads.cleanup()

	async def sync(self):
		"""
			Syncs any repository that should be mirrored
		"""
		log.info("Syncing mirrors...")

		# Fetch the sync target
		target = self.config.get("sync", "target")
		if not target:
			log.warning("No sync target configured")
			return 0

		# Update the timestamp
		await self._update_timestamp()

		commandline = [
			"rsync",

			# Show what is being transferred
			"--verbose",

			# Compress any transferred data
			"--compress",

			# Enable archive mode
			"--archive",

			# Preserve hardlinks, ACLs & XATTRs
			"--hard-links",
			"--acls",
			"--xattrs",

			# Delete any files that we have deleted
			"--delete",
			"--delete-excluded",

			# Remove any empty directories
			"--prune-empty-dirs",

			# Make the transaction atomic
			"--delay-updates",

			# Add source & target
			"%s/" % self.basepath,
			target,

			# Sync the .timestamp
			"--include=.timestamp",
		]

		paths = []

		# Add all mirrored releases
		async for distro in self.distros:
			releases = await distro.get_releases()

			for release in releases:
				if not release.is_mirrored():
					continue

				paths.append(
					release.local_path(),
				)

		# Add all mirrored repositories
		async for repo in await self.repos.mirrored:
			paths.append(
				repo.local_path(),
			)

		# Add all paths
		for path in paths:
			commandline.append("--include=%s***" % self.relpath(path))

		# Exclude everything that hasn't been included
		commandline += ("--include=*/", "--exclude=*")

		# Run command
		await self.command(*commandline, krb5_auth=True)

	async def _update_timestamp(self):
		"""
			Updates the .timestamp file in the root of the exported files
		"""
		# Make filename
		path = os.path.join(self.basepath, ".timestamp")

		t = datetime.datetime.utcnow()

		# Write the current time as seconds since epoch
		async with aiofiles.open(path, mode="w") as f:
			await f.write(t.strftime("%s"))


def setup_logging():
	"""
		Configures the logger for the buildservice backend
	"""
	# Log everything to journal
	handler = systemd.journal.JournalHandler(
		SYSLOG_IDENTIFIER="pakfire-build-service",
	)
	log.addHandler(handler)


# Setup logging
setup_logging()
