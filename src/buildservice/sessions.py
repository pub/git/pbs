#!/usr/bin/python

import datetime
import logging
import sqlalchemy

from sqlalchemy import Column, DateTime, ForeignKey, Integer, Text
from sqlalchemy.dialects.postgresql import INET

from . import base
from . import database
from . import misc

# Setup logging
log = logging.getLogger("pbs.sessions")

SESSION_LIFETIME = datetime.timedelta(days=14)

class Sessions(base.Object):
	async def create(self, user, address, user_agent=None):
		"""
			Creates a new session in the data.

			The user is not checked and it is assumed that the user exists
			and has the right to log in.
		"""
		session_id = misc.generate_random_string(48)

		session = await self.db.insert(
			Session,
			session_id = session_id,
			user       = user,
			address    = address,
			user_agent = user_agent,
		)

		# Log what we have done
		log.info("Created new session %s" % session)

		return session

	async def get_by_session_id(self, session_id):
		stmt = (
			sqlalchemy
			.select(Session)
			.where(
				Session.session_id == session_id,
				Session.expires_at > sqlalchemy.func.current_timestamp(),
			)
		)

		return await self.db.fetch_one(stmt)

	# Alias function
	get = get_by_session_id

	async def cleanup(self):
		"""
			Deletes all sessions that are not valid any more
		"""
		stmt = (
			sqlalchemy
			.delete(
				Session,
			)
			.where(
				Session.expires_at <= sqlalchemy.func.current_timestamp(),
			)
		)

		# Run the query
		async with await self.db.transaction():
			await self.db.execute(stmt)


class Session(database.Base):
	__tablename__ = "sessions"

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.user < other.user

		return NotImplemented

	# ID

	id = Column(Integer, primary_key=True)

	# Session ID

	session_id = Column(Text, unique=True, nullable=False)

	# User

	user_id = Column(Integer, ForeignKey("users.id"), nullable=False)

	user = sqlalchemy.orm.relationship("User", back_populates="sessions",
		lazy="joined", innerjoin=True)

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Expires At

	expires_at = Column(DateTime(timezone=False), nullable=False,
		default=sqlalchemy.func.current_timestamp() + SESSION_LIFETIME)

	# Address

	address = Column(INET(), nullable=False)

	# User Agent

	user_agent = Column(Text)

	# Logout!

	async def logout(self):
		"""
			Destroys this session
		"""
		await self.db.delete(self)
