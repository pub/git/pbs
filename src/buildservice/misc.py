#!/usr/bin/python3

import random
import re
import string
import unicodedata

# A list of possible random characters.
random_chars = string.ascii_letters + string.digits

def generate_random_string(length=16):
	"""
		Return a string with random chararcters A-Za-z0-9 with given length.
	"""
	return "".join([random.choice(random_chars) for i in range(length)])

def infinity():
        i = 0

        while True:
                i += 1
                yield i

def normalize(*args, iteration=1):
	s = "-".join((e for e in args if e))

	# Append iteration when larger than one
	if iteration > 1:
		s += "-%s" % iteration

	# Remove any non-ASCII characters
	try:
		s = unicodedata.normalize("NFKD", s)
	except TypeError:
		pass

	# Encode into ASCII and back again
	s = s.encode("ascii", errors="ignore").decode()

	# Make everything lowercase
	s = s.lower()

	# Remove excessive whitespace
	s = re.sub(r"[^\w]+", " ", s)

	return "-".join(s.split())

async def group(items, key):
	"""
		This function takes some iterable and returns it grouped by key.
	"""
	result = {}

	async for item in items:
		if callable(key):
			value = key(item)
		else:
			value = getattr(item, key)

		try:
			result[value].append(item)
		except KeyError:
			result[value] = [item]

	return result
