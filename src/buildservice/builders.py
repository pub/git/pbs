#!/usr/bin/python3

import asyncio
import botocore.exceptions
import datetime
import functools
import json
import logging

import sqlalchemy
from sqlalchemy import BigInteger, Boolean, Column, DateTime, Double, ForeignKey, Integer, Text
from sqlalchemy.dialects.postgresql import INET

from . import base
from . import database
from . import jobs
from . import uploads

from .decorators import *
from .errors import *

# Setup logging
log = logging.getLogger("pbs.builders")

class BuilderStat(database.Base):
	__tablename__ = "builder_stats"

	# Builder ID

	builder_id = Column(Integer, ForeignKey("builders.id"), primary_key=True, nullable=False)

	# Builder

	builder = sqlalchemy.orm.relationship(
		"Builder", foreign_keys=[builder_id], lazy="joined", innerjoin=True,
	)

	# Created At

	created_at = Column(DateTime(timezone=False), primary_key=True, nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# CPU User

	cpu_user = Column(Double, nullable=False)

	# CPU Nice

	cpu_nice = Column(Double, nullable=False)

	# CPU System

	cpu_system = Column(Double, nullable=False)

	# CPU Idle

	cpu_idle = Column(Double, nullable=False)

	# CPU IO Wait

	cpu_iowait = Column(Double, nullable=False)

	# CPU IRQ

	cpu_irq = Column(Double, nullable=False)

	# CPU Soft IRQ

	cpu_softirq = Column(Double, nullable=False)

	# CPU Steal

	cpu_steal = Column(Double, nullable=False)

	# CPU Guest

	cpu_guest = Column(Double, nullable=False)

	# CPU Guest Nice

	cpu_guest_nice = Column(Double, nullable=False)

	@property
	def cpu_usage(self):
		"""
			Returns the CPU usage in percent
		"""
		return 1 - self.cpu_idle

	# Load Average 1

	loadavg1 = Column(Double, nullable=False)

	# Load Average 5

	loadavg5 = Column(Double, nullable=False)

	# Load Average 15

	loadavg15 = Column(Double, nullable=False)

	# Memory Total

	mem_total = Column(BigInteger, nullable=False)

	# Memory Available

	mem_available = Column(BigInteger, nullable=False)

	# Memory Used

	mem_used = Column(BigInteger, nullable=False)

	# Memory Free

	mem_free = Column(BigInteger, nullable=False)

	# Memory Active

	mem_active = Column(BigInteger, nullable=False)

	# Memory Inactive

	mem_inactive = Column(BigInteger, nullable=False)

	# Memory Buffers

	mem_buffers = Column(BigInteger, nullable=False)

	# Memory Cached

	mem_cached = Column(BigInteger, nullable=False)

	# Memory Shared

	mem_shared = Column(BigInteger, nullable=False)

	@property
	def mem_usage(self):
		"""
			Returns the amount of used memory in percent
		"""
		return (self.mem_total - self.mem_available) / (self.mem_total * 100)

	# Swap Total

	swap_total = Column(BigInteger, nullable=False)

	# Swap Used

	swap_used = Column(BigInteger, nullable=False)

	# Swap Free

	swap_free = Column(BigInteger, nullable=False)

	@property
	def swap_usage(self):
		"""
			Returns the amount of used swap in percent
		"""
		if not self.swap_total:
			return 0

		return self.swap_used / self.swap_total * 100


class Builders(base.Object):
	# Stores any control connections to builders
	connections = {}

	def __aiter__(self):
		stmt = (
			sqlalchemy
			.select(Builder)
			.where(
				Builder.deleted_at == None,
			)
			.order_by(
				Builder.name,
			)
		)

		# Fetch the builders
		return self.db.fetch(stmt)

	def init(self):
		# Initialize stats
		self.stats = BuildersStats(self.backend)

	async def create(self, **kwargs):
		"""
			Creates a new builder
		"""
		builder = await self.db.insert(
			Builder,
			**kwargs,
		)

		return builder

	async def get_by_name(self, name):
		stmt = (
			sqlalchemy
			.select(Builder)
			.where(
				Builder.deleted_at == None,

				# Match by name
				Builder.name == name,
			)
		)

		return await self.db.fetch_one(stmt)

	@property
	def least_busy(self):
		"""
			Returns all builders in order of busyness (i.e. running the least jobs)
		"""
		builder_jobs = (
			sqlalchemy
			.select(
				Builder.id.label("builder_id"),
				sqlalchemy.func.count().label("running_jobs"),
			)
			.join(
				jobs.Job,
				jobs.Job.builder_id == Builder.id,
			)
			.where(
				Builder.deleted_at == None,

				# Jobs cannot be deleted
				jobs.Job.deleted_at == None,

				# Jobs must be running
				jobs.Job.started_at != None,
				jobs.Job.finished_at == None,
			)
			.group_by(
				Builder.id,
			)
			.cte("builder_jobs")
		)

		stmt = (
			sqlalchemy
			.select(
				Builder,
			)
			.join(
				builder_jobs,
				builder_jobs.c.builder_id == Builder.id,
				isouter=True,
			)
			.where(
				Builder.deleted_at == None,
			)
			.order_by(
				(
					builder_jobs.c.running_jobs
					/
					Builder.max_jobs
				).asc(),
			)
		)

		return self.db.fetch(stmt)

	async def autoscale(self, wait=False):
		"""
			This method performs two tasks:

			* It will launch any new builders if more are required

			* It will shutdown any builders which are no longer required
		"""
		log.debug("Running autoscaling schedulder")

		builders = {}

		# Fetch all builders and their value for sorting
		async with asyncio.TaskGroup() as tg:
			async for builder in self:
				task = tg.create_task(
					builder._autoscale_sort(),
				)

				builders[builder] = task

		# Fetch the results
		builders = { builder : builders[builder].result() for builder in builders }

		# Sort all builders after their priority
		builders = sorted(builders, key=lambda b: (-builders[b], b))

		# Store the length of the queue for each builder
		queue = {
			builder : 0 for builder in builders
		}

		# Run through all build jobs and allocate them to a builder.
		# If a builder is full (i.e. reaches the threshold of its build time),
		# we move on to the next builder until that is full and so on.
		async for job in self.backend.jobs.queue:
			log.debug("Processing job %s..." % job)

			for builder in builders:
				# Skip disabled builders
				if not builder.enabled:
					continue

				# Check if this builder is already at capacity
				if queue[builder] >= builder.max_jobs:
					log.debug("Builder %s is already full" % builder)
					continue

				# Skip if this builder cannot build this job
				if not builder.can_build(job):
					continue

				log.debug("Builder %s can build %s" % (builder, job))

				# Store the job
				queue[builder] += 1
				break

		# Find all builders that should be running
		builders_to_be_launched = [
			builder for builder in builders if queue[builder]
		]

		# Find all builders that are no longer needed and can be shut down
		builders_to_be_shut_down = [
			builder for builder in builders if not queue[builder] and await builder.is_idle()
		]

		async with asyncio.TaskGroup() as tasks:
			# Start all builders that have been allocated at least one job
			for builder in builders_to_be_launched:
				tasks.create_task(
					builder.start(auto=True, wait=wait),
				)

			# Shutdown the rest
			for builder in builders_to_be_shut_down:
				tasks.create_task(
					builder.stop(auto=True, wait=wait),
				)

	# Stats

	async def get_total_build_time(self):
		"""
			Returns the total build time
		"""
		stmt = (
			sqlalchemy
			.select(
				sqlalchemy.func.sum(
					sqlalchemy.func.coalesce(
						jobs.Job.finished_at,
						sqlalchemy.func.current_timestamp()
					)
					- jobs.Job.started_at,
				).label("total_build_time")
			)
			.where(
				jobs.Job.started_at != None,
			)
		)

		return await self.db.select_one(stmt, "total_build_time")

	async def get_total_build_time_by_arch(self):
		"""
			Returns a dict with the total build times grouped by architecture
		"""
		stmt = (
			sqlalchemy
			.select(
				jobs.Job.arch,

				sqlalchemy.func.sum(
					sqlalchemy.func.coalesce(
						jobs.Job.finished_at,
						sqlalchemy.func.current_timestamp()
					)
					- jobs.Job.started_at,
				).label("total_build_time")
			)
			.where(
				jobs.Job.started_at != None,
			)
			.group_by(
				jobs.Job.arch,
			)
			.order_by(
				jobs.Job.arch,
			)
		)

		return { row.arch : row.total_build_time async for row in self.db.select(stmt) }


class BuildersStats(base.Object):
	builders = {}

	def join(self, builder, connection):
		try:
			self.builders[builder.name].append(connection)
		except KeyError:
			self.builders[builder.name] = [connection]

	def leave(self, connection):
		# Find and remove the connection
		for name in self.builders.copy():
			try:
				self.builders[name].remove(connection)
			except ValueError:
				continue

	async def submit_stats(self, builder, stats):
		"""
			Called when a builder sends new stats
		"""
		try:
			connections = self.builders[builder.name]
		except KeyError:
			return

		# Send the stats to all connections
		async with asyncio.TaskGroup() as tasks:
			for c in connections:
				tasks.create_task(
					c.submit_stats(stats),
				)


class Builder(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "builders"

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.name < other.name

		return NotImplemented

	def __str__(self):
		return self.name

	# ID

	id = Column(Integer, primary_key=True)

	# Description

	description = Column(Text, nullable=False, default="")

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Created By ID

	created_by_id = Column(Integer, ForeignKey("users.id"), nullable=False)

	# Created By

	created_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[created_by_id], lazy="joined", innerjoin=True,
	)

	# Deleted By ID

	deleted_by_id = Column(Integer, ForeignKey("users.id"))

	# Deleted By

	deleted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deleted_by_id], lazy="selectin",
	)

	def is_online(self):
		"""
			Returns True if the builder is online
		"""
		if self.connection:
			return True

		return False

	# Maintenance

	maintenance = Column(Boolean, nullable=False, default=False)

	# Enabled

	enabled = Column(Boolean, nullable=False, default=False)

	# Permissions

	def has_perm(self, user):
		# Anonymous users have no permissions
		if not user:
			return False

		# Admins have all permissions
		return user.is_admin()

	@property
	def supported_arches(self):
		"""
			Returns a list of all architectures this builder can natively build for
		"""
		# Every builder supports noarch
		arches = ["noarch"]

		# We can always build our native architeture
		if self.arch:
			arches.append(self.arch)

		return sorted(arches)

	def can_build(self, job):
		return job.arch in self.supported_arches

	# Running Jobs

	@functools.cached_property
	def running_jobs(self):
		"""
			A CTE with all jobs currently running on this builder
		"""
		return (
			sqlalchemy
			.select(
				self.backend.jobs.running_jobs,
			)
			.where(
				self.backend.jobs.running_jobs.c.builder_id == self.id,
			)
			.cte("builder_running_jobs")
		)

	async def get_running_jobs(self):
		"""
			Returns the currently running jobs
		"""
		running_jobs = sqlalchemy.orm.aliased(jobs.Job, self.running_jobs)

		stmt = (
			sqlalchemy
			.select(
				running_jobs,
			)
		)

		return await self.db.fetch_as_list(stmt)

	async def num_running_jobs(self):
		"""
			Returns the number of currently running jobs
		"""
		stmt = (
			sqlalchemy
			.select(
				sqlalchemy.func.count().label("running_jobs"),
			)
			.select_from(
				self.running_jobs,
			)
		)

		# Run the query
		return await self.db.select_one(stmt, "running_jobs") or 0

	# Max Jobs

	max_jobs = Column(Integer, nullable=False, default=1)

	async def is_full(self):
		"""
			Returns True if the builder has enough jobs running
		"""
		return await self.num_running_jobs() >= self.max_jobs

	async def is_idle(self):
		"""
			Return True if the builder has no active jobs
		"""
		return await self.num_running_jobs() == 0

	# Name

	name = Column(Text, unique=True, nullable=False)

	# (Pakfire) Version

	version = Column(Text, nullable=False, default="")

	# OS Name

	os_name = Column(Text, nullable=False, default="")

	# System Vendor

	sys_vendor = Column(Text)

	# System Name

	sys_name = Column(Text)

	# CPU Model

	cpu_model = Column(Text, nullable=False, default="")

	# CPU Count

	cpu_count = Column(Integer, nullable=False, default=1)

	# Architecture

	arch = Column(Text)

	# AWS - Instance ID

	instance_id = Column(Text)

	# AWS - Instance Type

	instance_type = Column(Text)

	@functools.cached_property
	def instance(self):
		if self.instance_id:
			return self.backend.aws.ec2.Instance(self.instance_id)

	async def is_running(self):
		"""
			Returns True if this builder is currently running
		"""
		# Shortcut, because the online check is much cheaper
		if self.is_online():
			return True

		state = await asyncio.to_thread(self._fetch_state)

		return state in ("pending", "running")

	async def is_shut_down(self):
		"""
			Returns True if this builder is shut down
		"""
		state = await asyncio.to_thread(self._fetch_state)

		return state == "stopped"

	async def is_shutting_down(self):
		"""
			Returns True if this builder is currently shutting down
		"""
		state = await asyncio.to_thread(self._fetch_state)

		return state in ("shutting-down", "stopping")

	def _fetch_state(self):
		"""
			Returns the current state of this instance
		"""
		if self.instance:
			return self.instance.state.get("Name")

	async def start(self, auto=False, wait=True):
		"""
			Starts the instance on AWS
		"""
		# Do nothing if the builder is already running
		if await self.is_running():
			return

		# Don't start automatically when in maintenance mode
		if auto and self.maintenance:
			log.warning("Won't start %s in maintenance mode" % self)
			return

		await asyncio.to_thread(self._start, wait=wait)

	def _start(self, wait):
		# Requires an instance
		if not self.instance:
			return

		log.info("Starting %s" % self)

		# Set correct instance type
		self._set_instance_type()

		# Send the start signal
		self.instance.start()

		# End here if we don't want to wait
		if not wait:
			return

		log.debug("Waiting until %s has started" % self)

		# And wait until the instance is running
		self.instance.wait_until_running()

		log.debug("%s has been started" % self)

	def _set_instance_type(self):
		"""
			Changes the type of this instance
		"""
		# Don't try setting instance type if nothing is configured
		if not self.instance_type:
			return

		# Check if this needs changing at all
		if self.instance.instance_type == self.instance_type:
			return

		log.debug("Changing instance type of %s to %s" % (self, self.instance_type))

		# Send the change
		try:
			self.instance.modify_attribute(
				InstanceType={
					"Value" : self.instance_type,
				}
			)

		# Log an error if this request wasn't successful
		except botocore.exceptions.ClientError as e:
			log.warning("Could not change instance type of %s: %s" % (self, e))

	async def stop(self, auto=False, wait=True):
		"""
			Stops this instance on AWS
		"""
		# Do nothing if the builder is already shut down
		if await self.is_shutting_down() or await self.is_shut_down():
			return

		# Don't stop automatically when in maintenance mode
		if auto and self.maintenance:
			log.warning("Won't stop %s in maintenance mode" % self)
			return

		await asyncio.to_thread(self._stop, wait=wait)

	def _stop(self, wait):
		# Requires an instance
		if not self.instance:
			return

		log.info("Stopping %s" % self)

		# Send the stop signal
		self.instance.stop()

		# End here if we don't want to wait
		if not wait:
			return

		log.debug("Waiting until %s has stopped" % self)

		# And wait until the instance has been stopped
		self.instance.wait_until_stopped()

		log.debug("%s has been stopped" % self)

	async def _autoscale_sort(self):
		"""
			This function returns a number that tells the autoscaling algorithm
			in which order a builder should be considered.

			The higher the number, the sooner the builder is considered.
		"""
		# Return something neutral if this isn't an AWS instance
		if not self.instance:
			return 0

		# Move disabled builders towards the end
		if not self.enabled:
			return -1

		# Running builders should come first
		if await self.is_running():
			return 1

		# Put builders that are shutting down at the end
		elif await self.is_shutting_down():
			return -1

		# Otherwise return a neutral preference
		return 0

	# Stats

	async def get_stats(self):
		"""
			Fetch the latest stats
		"""
		stmt = (
			sqlalchemy
			.select(
				BuilderStat,
			)
			.where(
				BuilderStat.builder == self,
			)
			.order_by(
				BuilderStat.created_at.desc(),
			)
			.limit(1)
		)

		return await self.db.fetch_one(stmt)

	async def log_stats(self, **kwargs):
		"""
			Logs some stats about this builder
		"""
		stats = await self.db.insert(
			BuilderStat, builder=self, **kwargs,
		)

		# Send out the stats
		await self.backend.builders.stats.submit_stats(self, stats)

	async def get_total_build_time(self):
		"""
			Returns the total build time
		"""
		stmt = (
			sqlalchemy
			.select(
				sqlalchemy.func.sum(
					sqlalchemy.func.coalesce(
						jobs.Job.finished_at,
						sqlalchemy.func.current_timestamp()
					)
					- jobs.Job.started_at,
				).label("total_build_time")
			)
			.where(
				jobs.Job.builder == self,
				jobs.Job.started_at != None,
			)
		)

		return await self.db.select_one(stmt, "total_build_time")

	# Connections

	def connected(self, connection, address=None):
		"""
			Called when a builder has connected
		"""
		# Replace any old connections
		if self.connection:
			log.warning("Closing connection to %s because it is being replaced" % self)

			# Close the previous connection
			self.connection.close(code=1000, reason="Replaced by a new connection")

		log.info("%s has connected" % self)

		# Store the connection
		self.backend.builders.connections[self.name] = connection

		# Remember when it last connected
		self.connected_at = sqlalchemy.func.current_timestamp()

		# Store the IP address the builder connected from
		self.connected_from = address

	def disconnected(self):
		"""
			Called when a builder has disconnected
		"""
		log.info("%s has disconnected" % self)

		try:
			del self.backend.builders.connections[self.name]
		except KeyError:
			pass

	@property
	def connection(self):
		"""
			Easy access to the control connection to a builder
		"""
		try:
			return self.backend.builders.connections[self.name]
		except KeyError:
			pass

	# Connected At

	connected_at = Column(DateTime(timezone=False))

	# Connected From

	connected_from = Column(INET)

	# Send Message

	async def send_message(self, message):
		"""
			Sends a message to the builder
		"""
		# Raise an error if the builder is not online
		if not self.connection:
			raise BuilderNotOnlineError(self)

		# Log action
		log.debug("Sending message to %s:\n%s",
			self, json.dumps(message, indent=4, sort_keys=True))

		# Write the message to the control connection
		return self.connection.write_message(message)

	# Uploads

	def get_uploads(self):
		"""
			Returns all uploads that belong to this builder
		"""
		stmt = (
			sqlalchemy
			.select(uploads.Upload)
			.where(
				uploads.Upload.builder == self,
				uploads.Upload.expires_at > sqlalchemy.func.current_timestamp(),
			)
			.order_by(
				uploads.Upload.created_at.desc(),
			)
		)

		return self.db.fetch(stmt)
