#!/usr/bin/python

import asyncio
import functools

def run_in_thread(func):
	"""
		Decorator to run a synchronous function in a separate thread.
	"""
	@functools.wraps(func)
	async def wrapper(*args, **kwargs):
		return await asyncio.to_thread(func, *args, **kwargs)

	return wrapper
