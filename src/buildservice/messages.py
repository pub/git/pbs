#!/usr/bin/python

import asyncio
import datetime
import email
import email.charset
import email.mime.text
import email.policy
import email.utils
import functools
import logging
import re
import smtplib
import socket
import tornado.locale
import tornado.template

import sqlalchemy
from sqlalchemy import Boolean, Column, DateTime, Integer, Text

from . import base
from . import database

from .constants import TEMPLATESDIR
from .decorators import *

# Setup logging
log = logging.getLogger("pbs.messages")

# Encode emails in UTF-8 by default
email.charset.add_charset("utf-8", email.charset.SHORTEST, email.charset.QP, "utf-8")

# Default policy for internal email handling
policy = email.policy.HTTP

class Message(database.Base, database.BackendMixin):
	"""
		A simple class that represents a message that needs to be sent
	"""
	__tablename__ = "messages"

	# ID

	id = Column(Integer, primary_key=True)

	# Message - The actual payload

	message = Column(Text, nullable=False)

	# Queued At

	queued_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Sent At

	sent_at = Column(DateTime(timezone=False))

	# Priority

	priority = Column(Integer, default=0)

	# Error Message

	error_message = Column(Text)


class Messages(base.Object):
	def init(self):
		self.template_loader = tornado.template.Loader(TEMPLATESDIR, autoescape=None)

	@functools.cached_property
	def queue(self):
		"""
			The message queue
		"""
		return Queue(self.backend)

	async def send(self, message, priority=None):
		# Check if To is set
		if not "To" in message:
			raise ValueError("Message has no To: header")

		# Add a message ID if non existant
		if not "Message-ID" in message:
			message.add_header("Message-ID", self.make_msgid())

		# Add date if the message doesn't have one already
		if "Date" not in message:
			message.add_header("Date", email.utils.formatdate())

		# Add From header if it does not exist
		if not "From" in message:
			sender = self.backend.config.get("mail", "sender",
				fallback="Pakfire Build Service <no-reply@ipfire.org>")

			message.add_header("From", sender)

		# Send any errors to the bounce address
		bounce_address = self.backend.config.get("mail", "bounce-address", fallback=None)
		if bounce_address:
			message.add_header("Errors-To", "<%s>" % bounce_address)

		# Send the message
		message = await self.queue.enqueue(message, priority=priority)

		# Launch a new task to send all queued messages
		self.backend.run_task(self.queue.send)

		return message

	def make_msgid(self):
		"""
			Generates a random Message ID
		"""
		return email.utils.make_msgid("pakfire-build-service", domain="pakfire.ipfire.org")

	def render(self, template_name, priority=None,
			attachments=[], locale=None, headers=None, **kwargs):
		"""
			Renders a message based on the given template
		"""
		locale = locale or tornado.locale.get("en_US")

		# Create the required namespace to render the message
		namespace = {
			# Generic Stuff
			"backend"       : self.backend,

			# Locale
			"locale"        : locale,
			"_"             : locale.translate,
		}

		# Add custom variables
		namespace.update(kwargs)

		# Collect all content from the templates
		message_parts = []
		_headers = {}

		# Load the template
		t = self.template_loader.load(template_name)

		# Render the message
		try:
			body = t.generate(**namespace)

		# Reset the rendered template when it could not be rendered
		except:
			self.template_loader.reset()
			raise

		# Parse the message and extract the header
		body = email.message_from_string(body.decode(), policy=policy)

		for header in body:
			value = body[header]

			# Make sure addresses are properly encoded
			realname, address = email.utils.parseaddr(value)
			if realname and address:
				value = email.utils.formataddr((realname, address))

			_headers[header] = value

		# Get the payload
		payload = body.get_payload()

		# Replace any multiple chains of newlines with only one newline
		payload = re.sub(r"\n{2,}", "\n\n", payload)

		# Create a MIMEText object out of it
		body = email.mime.text.MIMEText(payload, "text")

		# Collect all parts of this message
		message_parts = [
			body,
		]

		# Add any attachments
		#for attachment in attachments:
		#	if attachment.mimetype:
		#		maintype, slash, subtype = attachment.mimetype.partition("/")
		#	else:
		#		maintype, subtype = "application", "octet-stream"
		#
		#	a = email.mime.application.MIMEApplication(attachment.blob, subtype)
		#	if attachment.filename:
		#		a.add_header("Content-Disposition", "attachment", filename=attachment.filename)
		#
		#	# Append the attachment to the message
		#	message_parts.append(a)

		# Use first value if we only have one part
		if len(message_parts) == 1:
			message = message_parts[0]

		# Generate multipart message
		else:
			message = email.mime.base.MIMEBase("multipart", "mixed", charset="utf-8")

			for message_part in message_parts:
				message.attach(message_part)

		# Add any custom headers
		if headers:
			_headers.update(headers)

		# Apply any headers to the outer message
		for k, v in _headers.items():
			if not v:
				continue

			try:
				message.replace_header(k, v)
			except KeyError:
				message.add_header(k, v)

		return message

	async def send_template(self, template_name, priority=None, **kwargs):
		# Render message
		message = self.render(template_name, **kwargs)

		# Send the message
		return await self.send(message, priority=priority)


class Queue(base.Object):
	"""
		Queues and delivers any emails
	"""
	async def get_messages(self, limit=None):
		stmt = (
			sqlalchemy
			.select(
				Message,
			)
			.where(
				Message.sent_at == None,
			)
			.order_by(
				Message.priority.desc(),
				Message.queued_at.asc(),
			)
			.limit(limit)
		)

		return await self.db.fetch_as_list(stmt)

	async def enqueue(self, message, priority=0):
		"""
			Enqueues a new message
		"""
		# Insert into the database
		message = await self.db.insert(
			Message,
			message  = message.as_string(policy=policy),
			priority = priority,
		)

		log.debug("Message queued with ID %s" % message.id)

	def connect(self):
		"""
			Connection to the local mail relay
		"""
		hostname = socket.getfqdn()

		# Open SMTP connection
		conn = smtplib.SMTP(hostname)

		# Start TLS connection
		conn.starttls(context=self.backend.ssl_context)

		return conn

	_send_lock = asyncio.Lock()

	async def send(self, **kwargs):
		"""
			Sends all pending messages from the queue
		"""
		relay = None

		async with self._send_lock:
			while True:
				messages = await self.get_messages(limit=10)

				# If there are no messages left, we can quit
				if not messages:
					break

				# Connect to the relay
				if relay is None:
					relay = self.connect()

				# Send the messages one by one
				for message in messages:
					# Send the message
					await self._send(relay, message, **kwargs)

					# Commit to the database after each message
					await self.db.commit()

		log.debug("All messages sent")

	async def _send(self, relay, message):
		"""
			Delivers the given message to the local mail relay
		"""
		# Parse the message from what is in the database
		msg = email.message_from_string(message.message)

		log.debug("Sending a message %s to: %s" % (
			msg.get("Subject"), msg.get("To"),
		))

		error_messages = []
		rejected_recipients = {}

		# Try delivering the email
		try:
			# Run this in an extra thread because smtplib blocks
			rejected_recipients = await asyncio.to_thread(relay.send_message, msg)

		except smtplib.SMTPRecipientsRefused as e:
			rejected_recipients = e.recipients

		except smtplib.SMTPException as e:
			log.error("SMTP Exception: %s" % e)
			error_messages.append("%s" % e)

		# Log all emails that could not be delivered
		for recipient in rejected_recipients:
			code, reason = rejected_recipients[recipient]

			error_messages.append("Recipient refused: %s - %s (%s)" % \
				(recipient, code, reason.decode()))

		# Store any errors
		if error_messages:
			log.error("Could not send email: %s" % message.id)

			message.error_message = "; ".join(error_messages)

			for line in error_messages:
				log.error(line)

		# After the email has been successfully sent, we mark it as such
		message.sent_at = sqlalchemy.func.current_timestamp()

	async def cleanup(self):
		"""
			Deletes all successfully sent emails
		"""
		log.debug("Cleaning up message queue")

		stmt = (
			sqlalchemy
			.delete(
				Message,
			)
			.where(
				Message.sent_at <= sqlalchemy.func.current_timestamp() - datetime.timedelta(days=30),
			)
		)

		# Run the query
		async with await self.db.transaction():
			await self.db.execute(stmt)
