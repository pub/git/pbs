#!/usr/bin/python

import asyncio
import configparser
import contextlib
import datetime
import functools
import io
import logging
import os.path
import shutil

import sqlalchemy
from sqlalchemy import Column, ForeignKey
from sqlalchemy import Boolean, DateTime, Integer, Text

from . import base
from . import builds
from . import database
from . import jobs
from . import misc
from . import packages
from . import sources

from .constants import *
from .decorators import *

# Setup logging
log = logging.getLogger("pbs.repositories")

class RepoBuild(database.Base):
	__tablename__ = "repository_builds"

	# ID

	id = Column(Integer, primary_key=True)

	# Repo ID

	repo_id = Column(Integer, ForeignKey("repositories.id"), nullable=False)

	# Repo

	repo = sqlalchemy.orm.relationship("Repo", foreign_keys=[repo_id],
		lazy="joined", innerjoin=True)

	# Build ID

	build_id = Column(Integer, ForeignKey("builds.id"), nullable=False)

	# Build

	build = sqlalchemy.orm.relationship("Build", foreign_keys=[build_id],
		lazy="joined", innerjoin=True)

	# Added At

	added_at = Column(DateTime(timezone=False), nullable=False,
		default=sqlalchemy.func.current_timestamp())

	# Added By ID

	added_by_id = Column(Integer, ForeignKey("users.id"), nullable=False)

	# Added By

	added_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[added_by_id], lazy="selectin",
	)

	# Removed At

	removed_at = Column(DateTime(timezone=False))

	# Removed By ID

	removed_by_id = Column(Integer, ForeignKey("users.id"))

	# Removed By

	removed_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[removed_by_id], lazy="selectin",
	)

	# Remove!

	def remove(self, removed_by=None):
		"""
			Removes the build from the repository
		"""
		# Mark as removed
		self.removed_at = sqlalchemy.func.current_timestamp()
		if removed_by:
			self.removed_by = removed_by

		log.debug("%s has been removed from %s by %s" % (self.build, self.repo, self.removed_by))


class Repos(base.Object):
	def __aiter__(self):
		stmt = (
			sqlalchemy
			.select(Repo)
			.where(
				Repo.deleted_at == None,
			)

			# Order them by distro & name
			.order_by(
				Repo.distro_id,
				Repo.name,
			)
		)

		# Fetch the repos
		return self.db.fetch(stmt)

	@property
	async def mirrored(self):
		"""
			Lists all repositories that should be mirrored
		"""
		stmt = (
			sqlalchemy
			.select(Repo)
			.where(
				Repo.deleted_at == None,

				# Filter by those who should be mirrored
				Repo.mirrored == True,
			)
		)

		# Fetch the repositories
		return self.db.fetch(stmt)

	async def create(self, distro, name, owner=None):
		"""
			Creates a new repository
		"""
		# Generate a slug
		slug = await self._make_slug(distro, name, owner=owner)

		# Generate a comment for the key
		comment = "%s - %s" % (distro, name)
		if owner:
			comment = "%s - %s" % (owner, comment)

		# Create a key for this repository
		key = await self.backend.keys.create(
			comment = comment,
			user    = owner,
		)

		# Create the repository
		repo = await self.db.insert(
			Repo,
			distro = distro,
			owner  = owner,
			name   = name,
			slug   = slug,
			key    = key,
		)

		return repo

	async def _make_slug(self, distro, name, owner=None):
		for i in misc.infinity():
			slug = misc.normalize(name, iteration=i)

			stmt = (
				sqlalchemy
				.select(
					Repo
				)
				.where(
					Repo.deleted_at == None,
					Repo.distro == distro,
					Repo.slug == slug,
				)
			)

			# If a repository with this slug exists, we need to try again
			if await self.db.select_one(stmt):
				continue

			return slug

	async def write(self):
		"""
			Write/re-write all repositories
		"""
		async for repo in self:
			await repo.write()


class Repo(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "repositories"

	def __str__(self):
		return self.name

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return self.name < other.name

		return NotImplemented

	def to_json(self):
		ret = {
			"name"        : self.name,
			"slug"        : self.slug,
			"description" : self.description,
			"created_at"  : self.created_at.isoformat(),

			# Distribution
			"distro"      : self.distro.slug,
		}

		if self.owner:
			ret["owner"] = self.owner.to_json()

		return ret

	# ID

	id = Column(Integer, primary_key=True)

	# Distro

	distro_id = Column(Integer, ForeignKey("distributions.id"), nullable=False)

	distro = sqlalchemy.orm.relationship("Distro", lazy="joined", innerjoin=True)

	# Created At

	created_at = Column(
		DateTime(timezone=False), nullable=False, server_default=sqlalchemy.func.current_timestamp(),
	)

	# Repo Types

	def is_user(self):
		"""
			Returns True if this is a user repository
		"""
		if self.owner:
			return True

		return False

	def is_distro(self):
		"""
			Returns True if this is a distro repository
		"""
		return not self.is_user()

	# Priority

	priority = Column(Integer, nullable=False)

	# Owner ID

	owner_id = Column(Integer, ForeignKey("users.id"))

	# Owner

	owner = sqlalchemy.orm.relationship("User", lazy="selectin")

	def has_perm(self, user):
		"""
			Returns True if the user has permission to manage this repository
		"""
		# Anonymous users have no permissions
		if not user:
			return False

		# Admins can edit anything
		if user.is_admin():
			return True

		# If the user owns this repository, they may edit it
		if self.owner == user:
			return True

		# Nope
		return False

	# Slug

	slug = Column(Text, unique=True, nullable=False)

	@property
	def path(self):
		parts = []

		# Add owner
		if self.owner:
			parts.append("~%s" % self.owner.name)

		# Add distro & slug
		parts += (
			self.distro.slug,
			self.slug,
		)

		# Join everything together
		return os.path.join(*parts)

	def local_path(self, arch=None):
		"""
			Path to the repository
		"""
		path = self.backend.path("repos", self.path)

		# Append the architecture
		if arch:
			path = os.path.join(path, arch)

		return path

	@property
	def url(self):
		if self.owner:
			return "/users/%s/repos/%s/%s" % (self.owner.name, self.distro.slug, self.slug)

		return "/distros/%s/repos/%s" % (self.distro.slug, self.slug)

	@property
	def download_url(self):
		return os.path.join(
			self.backend.config.get("global", "baseurl", fallback="https://pakfire.ipfire.org"),
			"files",
			"repos",
			self.path,
			"%{arch}",
		)

	@property
	def mirrorlist(self):
		return os.path.join(
			self.backend.config.get("global", "baseurl", fallback="https://pakfire.ipfire.org"),
			"distros",
			self.distro.slug,
			"repos",
			self.slug,
			"mirrorlist?arch=%{arch}"
		)

	def write_config(self, config, include_source=False, **kwargs):
		# Write the default configuration
		self._write_config(config, self.slug, "%{arch}", **kwargs)

		# Add another entry for the source repository
		if include_source:
			self._write_config(config, "%s-source" % self.slug, "src", **kwargs)

	def _write_config(self, config, name, arch, local=False, mirrored=True, build=False):
		# Disable mirroring if local or not mirrored
		if local or not self.mirrored:
			mirrored = False

		args = {
			# Description
			"description" : "%s - %s" % (self.distro.name, self.name),

			# Base URL
			"baseurl" : "file://%s" % self.local_path(arch) if local else self.download_url,

			# Key
			"key" : self.key.public_key if self.key else "",
		}

		# Mirrors
		if mirrored:
			args |= {
				"mirrors" : self.mirrorlist,
			}

		# Priority
		if self.priority:
			args |= {
				"priority" : "%s" % self.priority,
			}

		# In build environments force refreshing every time
		if build:
			args |= {
				"refresh" : "0",
			}

		# Store in configuration
		config["repo:%s" % name] = args

	# Name

	name = Column(Text, nullable=False)

	# Description

	description = Column(Text, nullable=False, default="")

	# Key ID

	key_id = Column(Integer, ForeignKey("keys.id"))

	# Key

	key = sqlalchemy.orm.relationship("Key", foreign_keys=[key_id], lazy="joined")

	# Architectures

	@property
	def arches(self):
		return self.distro.arches + ["src"]

	# Mirrored

	mirrored = Column(Boolean, nullable=False, default=False)

	# Listed

	listed = Column(Boolean, nullable=False, default=False)

	# Sibling repositories

	async def get_parents(self):
		"""
			Returns all parent repositories
		"""
		# User repositories have the distro repositories as parents
		if self.is_user():
			return await self.distro.get_repos()

		# Distro repositories don't have any parents
		elif self.is_distro():
			return set()

	async def get_children(self):
		"""
			Returns all repositories that depend on this
		"""
		# User repositories don't have any children
		if self.is_user():
			return set()

		# Distro repositories are parents for all user repositories
		elif self.is_distro():
			stmt = (
				sqlalchemy
				.select(
					Repo,
				)
				.where(
					Repo.deleted_at == None,
					Repo.distro == self.distro,
					Repo.owner != None,
				)
			)

			return await self.db.fetch_as_set(stmt)

	# Builds

	async def get_builds(self, **kwargs):
		"""
			Returns builds in this repository
		"""
		return await self.backend.builds.get(repo=self, **kwargs)

	# Has Build?

	async def has_build(self, build):
		"""
			Checks if this build is part of this repository
		"""
		stmt = (
			sqlalchemy
			.select(RepoBuild)
			.where(
				RepoBuild.repo == self,
				RepoBuild.build == build,
				RepoBuild.removed_at == None,
			)
		)

		return await self.db.fetch_one(stmt)

	# Add/Remove Builds

	async def add_build(self, build, user=None):
		"""
			Adds a build to this repository
		"""
		await self.db.insert(
			RepoBuild,
			repo     = self,
			build    = build,
			added_by = user,
		)

		# Update bug status
		# XXX TODO

		# Update the repository (in the background)
		await self.changed()

	async def remove_build(self, build, user=None):
		"""
			Removes a build from this repository
		"""
		repo_build = await self.has_build(build)

		# Raise an exception if we don't have this build
		if not repo_build:
			raise ValueError("%s is not part of %s" % (build, self))

		# Remove the build
		repo_build.remove()

		# Update the repository (in the background)
		await self.changed()

	# Sources

	@functools.cached_property
	def sources(self):
		sources = self.backend.sources._get_sources("""
			SELECT
				*
			FROM
				sources
			WHERE
				deleted_at IS NULL
			AND
				repo_id = %s
			ORDER BY
				name
			""", self.id,
		)

		return list(sources)

	async def get_source_by_slug(self, slug):
		stmt = (
			sqlalchemy
			.select(
				sources.Source,
			)
			.where(
				sources.Source.deleted_at == None,
				sources.Source.repo_id == self.id,
				sources.Source.slug == slug,
			)
		)

		return await self.db.fetch_one(stmt)

	@functools.cached_property
	def builds(self):
		"""
			Returns a CTE with all builds in this repository
		"""
		return (
			sqlalchemy
			.select(
				builds.Build,
			)
			.select_from(
				RepoBuild,
			)
			.join(
				builds.Build,
				builds.Build.id == RepoBuild.build_id,
			)
			.where(
				# Builds should not be deleted
				builds.Build.deleted_at == None,

				# Builds must be in this repository
				RepoBuild.repo == self,
				RepoBuild.removed_at == None,
			)
			.cte("_builds")
		)

	@functools.cached_property
	def packages(self):
		"""
			Returns a CTE with all packages in this repository
		"""
		# Source packages
		source_packages = (
			sqlalchemy
			.select(
				packages.Package,
			)
			.select_from(
				self.builds,
			)
			.join(
				packages.Package,
				packages.Package.id == self.builds.c.pkg_id,
			)
			.where(
				packages.Package.deleted_at == None,
			)
		)

		# Binary Packages
		binary_packages = (
			sqlalchemy
			.select(
				packages.Package,
			)
			.select_from(
				self.builds,
			)
			.join(
				jobs.Job,
				jobs.Job.build_id == self.builds.c.id,
			)
			.join(
				jobs.JobPackage,
				jobs.JobPackage.job_id == jobs.Job.id,
			)
			.join(
				packages.Package,
				packages.Package.id == jobs.JobPackage.pkg_id,
			)
			.where(
				packages.Package.deleted_at == None,
				jobs.Job.deleted_at == None,
			)
		)

		return sqlalchemy.union_all(source_packages, binary_packages).cte("_packages")

	async def get_total_builds(self):
		"""
			Return the total number of builds in this repository
		"""
		stmt = (
			sqlalchemy
			.select(
				sqlalchemy.func.count().label("total_builds"),
			)
			.select_from(self.builds)
		)

		return await self.db.select_one(stmt, "total_builds")

	def get_packages(self, arch):
		"""
			Returns an iterator over all packages that match the given arch
		"""
		arches = [arch]

		# Include noarch packages unless we are looking for source packages only
		if not arch == "src":
			arches.append("noarch")

		# Alias the CTE
		all_packages = sqlalchemy.orm.aliased(packages.Package, self.packages)

		# Fetch all packages that match the architecture
		stmt = (
			sqlalchemy
			.select(
				all_packages,
			)
			.where(
				all_packages.arch.in_(arches),
			)
		)

		return self.db.fetch(stmt)

	@property
	def pending_jobs(self):
		"""
			Returns an iterator of all pending jobs that use
			this repository as their build repository.
		"""
		stmt = (
			sqlalchemy
			.select(
				jobs.Job,
			)
			.join(
				builds.Build,
				builds.Build.id == jobs.Job.build_id,
			)
			.where(
				builds.Build.deleted_at == None,
				jobs.Job.deleted_at == None,

				# Must be in this repository
				builds.Build.repo == self,

				# Skip superseeded jobs
				jobs.Job.superseeded_by_id == None,

				# Jobs must not have started and not be finished
				jobs.Job.started_at == None,
				jobs.Job.finished_at == None,
			)
		)

		return self.db.fetch(stmt)

	# Stats

	async def get_total_size(self):
		"""
			Returns the total size of the repository
		"""
		stmt = (
			sqlalchemy
			.select(
				sqlalchemy.func.sum(
					self.packages.c.size,
				).label("total_size"),
			)
			.select_from(
				self.packages,
			)
		)

		return await self.db.select_one(stmt, "total_size") or 0

	# Pakfire

	def pakfire(self, **kwargs):
		"""
			Returns a Pakfire configuration for this repository
		"""
		# Overwrite the distro's vendor/contact for user repositories
		if self.owner:
			kwargs |= {
				"vendor"  : "%s" % self.owner,
				"contact" : self.owner.email,
			}

		return self.backend.pakfire(distro=self.distro, repos=[self], **kwargs)

	# Has Changed?

	__has_changed = {}

	@property
	def has_changed(self):
		"""
			This is set when the repository has been changed and needs an update
		"""
		try:
			event = self.__has_changed[self.id]
		except KeyError:
			event = self.__has_changed[self.id] = asyncio.Event()

		return event

	async def changed(self):
		"""
			Convenience function that should be called when a package has been
			added/removed from this repository.
		"""
		# Run the update process at the next convenient moment
		self.backend.run_task(self.update)

	__locks = {}

	@property
	def lock(self):
		"""
			Locks the repository when it is being rewritten
		"""
		try:
			lock = self.__locks[self.id]
		except KeyError:
			lock = self.__locks[self.id] = asyncio.Lock()

		return lock

	async def update(self):
		"""
			Called to perform an update of this repository
		"""
		# If the repository is currently being rewritten,
		# we will mark it as changed and return.
		if self.lock.locked():
			log.debug("%s is currently being updated" % self)

			# Set flag
			self.has_changed.set()
			return

		# Otherwise, try to acquire the lock
		async with self.lock:
			# Clear the "has changed" flag
			self.has_changed.clear()

			# Write/rewrite the repository
			await self.write()

		# Check if the repository has been changed in the meantime
		# and if so, call ourselves again
		if self.has_changed.is_set():
			await self.update()

		log.info("Finished updating %s" % self)

		# Relaunch any pending jobs
		await self.relaunch_pending_jobs()

	async def relaunch_pending_jobs(self):
		"""
			Relaunches all pending jobs
		"""
		# Perform this on this repository
		await self._relaunch_pending_jobs()

		# Perform this for all child repositories
		async with asyncio.TaskGroup() as tasks:
			for repo in await self.get_children():
				tasks.create_task(
					repo._relaunch_pending_jobs(),
				)

	async def _relaunch_pending_jobs(self):
		"""
			This function needs to be called whenever the repository has to be
			changed so that all pending jobs will have their dependencies re-checked.
		"""
		log.debug("%s: Relaunching pending jobs" % self)

		# Find all jobs that previously failed their installation check
		jobs = [
			job async for job in self.pending_jobs if job.is_pending(installcheck=False)
		]

		# Perform installcheck on all pending jobs
		if jobs:
			await self.installcheck(jobs)

		# Request dispatch
		await self.backend.jobs.queue.dispatch()

	async def installcheck(self, jobs):
		"""
			Performs an installation check for all given jobs
		"""
		# We create a new context which allows us to enter many (but unknown) numbers of
		# Pakfire instances at a time which leads to Pakfire only being initialized once
		# per architecture. We are then able to run all dependency checks one after the
		# other.
		async with contextlib.AsyncExitStack() as stack:
			ps = {}

			success = False

			for job in jobs:
				# Fetch the Pakfire instance of the architecture or create a new one
				try:
					p = ps[job.arch]
				except KeyError:
					p = ps[job.arch] = await stack.enter_async_context(
						self.pakfire(arch=job.arch)
					)

				# Perform the check for the job
				async with await self.db.transaction():
					if await job._installcheck(p):
						success = True

			return success

	# Write repository

	async def write(self):
		"""
			Called to write/rewrite/update the repository metadata
		"""
		log.info("Writing repository %s..." % self)
		key = None

		# Create a new pakfire instance
		async with self.backend.tempdir(prefix="pakfire-repo-") as t:
			async with self.pakfire() as p:
				# Fetch the key
				if self.key:
					key = self.key._make_key(p)

				for arch in self.arches:
					# Destination path for this architecture
					path = os.path.join(t, arch)

					# Fetch packages
					packages = self.get_packages(arch)

					# Make filelist (if they exist)
					files = [
						p.path async for p in packages \
							if p.path and await self.backend.exists(p.path)
					]

					# Write repository metadata
					await asyncio.to_thread(p.repo_compose, path=path, key=key, files=files)

			# Mark the repository as read-only
			await self.backend.chmod(t, 0o755)

			# Perform an atomic replace of the repository directory
			async with self.backend.tempdir(prefix="pakfire-repo-") as bin:
				# Local path
				path = self.local_path()

				# Move the old data to the temporary space
				try:
					await self.backend.move(path, bin)

				# Ignore if this repository is being created for the first time
				except FileNotFoundError:
					pass

				# Move the new repository data to its destination
				await self.backend.move(t, path)

	# Clear!

	async def clear(self, removed_by=None):
		"""
			Removes all builds from this repository
		"""
		stmt = (
			sqlalchemy
			.update(
				RepoBuild,
			)
			.where(
				RepoBuild.removed_at == None,
				RepoBuild.repo == self,
			)
			.values(
				removed_at    = sqlalchemy.func.current_timestamp(),
				removed_by_id = removed_by.id if removed_by else None,
			)
		)

		# Run the query
		await self.db.execute(stmt)

	# Delete!

	async def delete(self, deleted_by=None):
		"""
			Deletes this repository
		"""
		# Mark as deleted
		await super().delete(deleted_by)

		# Delete the key
		if self.key:
			await self.key.delete()

		# Remove all builds from this repository
		await self.clear(deleted_by)

		# Local path
		path = self.local_path()

		# Lock the repository and remove all files
		async with self.lock:
			await self.backend.rmtree(path)
