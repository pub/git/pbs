#!/usr/bin/python

import asyncio
import collections
import datetime
import functools
import gzip
import logging
import os
import pakfire
import queue
import sqlalchemy

from sqlalchemy import Column, ForeignKey
from sqlalchemy import BigInteger, Boolean, DateTime, Integer, Interval, LargeBinary, Text, UUID

from . import base
from . import builders
from . import builds
from . import database
from . import misc
from . import users

from .constants import *
from .decorators import *
from .errors import *

# Setup logging
log = logging.getLogger("pbs.jobs")

class JobPackage(database.Base):
	__tablename__ = "job_packages"

	# Job ID

	job_id = Column(Integer, ForeignKey("jobs.id"), primary_key=True, nullable=False)

	# Job

	job = sqlalchemy.orm.relationship("Job", foreign_keys=[job_id],
		viewonly=True, lazy="joined", innerjoin=True)

	# Package ID

	pkg_id = Column(Integer, ForeignKey("packages.id"), primary_key=True, nullable=False)

	# Package

	pkg = sqlalchemy.orm.relationship("Package", foreign_keys=[pkg_id],
		viewonly=True, lazy="joined", innerjoin=True)


class Jobs(base.Object):
	def init(self):
		# Setup queue
		self.queue = Queue(self.backend)

	async def create(self, build, arch, superseeds=None, timeout=None):
		"""
			Create a new job
		"""
		# Insert into the database
		job = await self.db.insert(
			Job,
			build   = build,
			arch    = arch,
			timeout = timeout,
		)

		# Mark if the new job superseeds some other job
		if superseeds:
			superseeds.superseeded_by = job

		return job

	async def get_by_uuid(self, uuid):
		"""
			Fetch a job by its UUID
		"""
		stmt = (
			sqlalchemy
			.select(Job)
			.where(
				Job.deleted_at == None,
				Job.uuid == uuid,
			)
		)

		return await self.db.fetch_one(stmt)

	@functools.cached_property
	def running_jobs(self):
		"""
			Returns a CTE with all currently running jobs
		"""
		return (
			sqlalchemy
			.select(
				Job,
			)
			.where(
				Job.deleted_at == None,
				Job.started_at != None,
				Job.finished_at == None,
			)
			.order_by(
				Job.started_at.desc(),
			)
			.cte("running_jobs")
		)

	def get_running(self):
		"""
			Returns all currently running jobs
		"""
		running_jobs = sqlalchemy.orm.aliased(Job, self.running_jobs)

		stmt = (
			sqlalchemy
			.select(
				running_jobs,
			)
		)

		return self.backend.db.fetch(stmt)

	def get_finished(self, failed_only=False, limit=None, offset=None):
		"""
			Returns a list of all finished jobs
		"""
		# This will include deleted jobs
		stmt = (
			sqlalchemy
			.select(Job)
			.where(
				# Get finished jobs only
				Job.finished_at != None,
			)
			.order_by(
				Job.finished_at.desc(),
			)
			.limit(limit)
			.offset(offset)
		)

		# Only show failed?
		if failed_only:
			stmt = stmt.where(Job.failed == True)

		return self.backend.db.fetch(stmt)

	async def launch(self, jobs):
		"""
			Called to launch all given jobs
		"""
		repos = {}

		# Group jobs by their build repository
		for job in jobs:
			try:
				repos[job.build.repo].append(job)
			except KeyError:
				repos[job.build.repo] = [job]

		# Run the dependency check for all jobs
		for repo, jobs in repos.items():
			await repo.installcheck(jobs)

		# Request dispatch
		await self.backend.jobs.queue.dispatch()

	async def abort(self):
		"""
			This is periodically called to abort any jobs that have crashed on the
			builders for unknown reasons.
		"""
		log.debug("Aborting timed-out jobs...")

		stmt = (
			sqlalchemy
			.select(
				Job
			)
			.where(
				# Don't care about deleted jobs
				Job.deleted_at == None,

				# Jobs must be running
				Job.started_at != None,
				Job.finished_at == None,

				# Jobs must have a timeout
				Job.timeout != None,

				# The timeout must have passed
				Job.started_at + Job.timeout < sqlalchemy.func.current_timestamp(),
			)
		)

		# Abort them all...
		async for job in self.db.fetch(stmt):
			await job.abort()


class Queue(base.Object):
	# Locked when the queue is being processed
	lock = asyncio.Lock()

	@functools.cached_property
	def queue(self):
		# XXX Need to filter out any jobs from users that have reached their quotas

		return (
			sqlalchemy

			# Collect all jobs and order them by priority
			.select(
				Job.id.label("job_id"),

				# Number the jobs by their priority
				sqlalchemy.func.rank()
				.over(
					order_by = (
						builds.Build.priority.desc(),

						# Put test builds at the end of the queue
						sqlalchemy.case(
							(builds.Build.test == True, 1), else_=0,
						),

						# Order by when the installcheck succeeded
						Job.installcheck_performed_at,

						# Otherwise use the creation time
						Job.created_at,
					),
				).label("rank"),
			)
			.select_from(Job)

			# Join builds
			.join(
				builds.Build,
				builds.Build.id == Job.build_id,
			)

			# Filter out any deleted objects
			.where(
				builds.Build.deleted_at == None,
				Job.deleted_at == None,
			)

			# Filter out any jobs that are not pending
			.where(
				Job.started_at == None,
				Job.finished_at == None,
			)

			# The installcheck must have succeeded
			.where(
				Job.installcheck_succeeded == True,
			)

			# Order everything by its rank
			.order_by("rank")

			# Name the cte
			.cte("job_queue")
		)

	def __aiter__(self):
		return self.get_jobs()

	async def length(self):
		"""
			The total length of the job queue
		"""
		stmt = (
			sqlalchemy
			.select(
				sqlalchemy.func.count(
					self.queue.c.job_id,
				).label("jobs")
			)
			.select_from(self.queue)
		)

		# Run the query
		return await self.db.select_one(stmt, "jobs")

	def get_jobs(self, limit=None):
		"""
			Returns all or a limited number of jobs ordered by their priority
		"""
		stmt = (
			sqlalchemy
			.select(
				Job,
			)
			.select_from(
				self.queue,
			)
			.join(
				Job,
				Job.id == self.queue.c.job_id,
			)
			.limit(limit)
		)

		return self.db.fetch(stmt)

	def get_jobs_for_builder(self, builder, limit=None):
		"""
			Returns all jobs that the given builder can process.
		"""
		stmt = (
			sqlalchemy
			.select(
				Job,
			)
			.select_from(
				self.queue,
			)
			.join(
				Job,
				Job.id == self.queue.c.job_id,
			)
			.where(
				Job.arch.in_(builder.supported_arches),
			)
		)

		return self.db.fetch(stmt)

	async def dispatch(self):
		"""
			Will be called regularly and will dispatch any pending jobs to any
			available builders
		"""
		# Just request dispatching when we are already running
		if self.lock.locked():
			self._dispatch_requested.set()
			return

		# Schedule dispatch
		self.backend.run_task(self._dispatch)

	# Set when a dispatch is requested
	_dispatch_requested = asyncio.Event()

	async def _dispatch(self):
		async with self.lock:
			log.debug("Dispatching jobs...")

			# Clear the request
			self._dispatch_requested.clear()

			builders = queue.SimpleQueue()

			# Add all builders to the queue in ascending order of running jobs
			async for builder in self.backend.builders.least_busy:
				# Skip any builders that are not enabled
				if not builder.enabled:
					log.debug("%s is not enabled" % builder)
					continue

				# Skip any builders that are not connected
				elif not builder.is_online():
					log.debug("%s is not online" % builder)
					continue

				# Skip any builders that are already full
				elif await builder.is_full():
					log.debug("%s is full" % builder)
					continue

				# Add it to the queue
				builders.put(builder)

			# Log if there are no builders
			if builders.empty():
				log.warning("There are no builders to dispatch any jobs to")

			# Run for as long as we have unprocessed builders
			while not builders.empty():
				# Take the first builder
				builder = builders.get()

				log.debug("  Processing builder %s" % builder)

				async with await self.db.transaction():
					try:
						# We are ready for a new job
						async for job in self.get_jobs_for_builder(builder):
							log.debug("Looking at %s for %s..." % (job, builder))

							# Perform installcheck (just to be sure)
							if not await job.installcheck():
								log.debug("Job %s failed its installcheck" % job)
								continue

							# If we have a job, we dispatch it to the builder
							await job.dispatch(builder)

							# Once we dispatched a job, we will commit the database
							# session so that the data is accessible if the builder
							# connects really quick and in case something goes wrong
							# dispatching any later jobs
							await self.db.commit()

							break

						# If not suitable jobs could be found for this builder,
						# we skip the full check below
						else:
							continue

					# Ignore if the builder suddenly went away
					except BuilderNotOnlineError:
						continue

				# If the builder has not any free slots remaining, we put it back in the
				# queue to fill it up, but we give priority to builders who have fewer
				# jobs to run.
				if not await builder.is_full():
					builders.put(builder)

			# Auto-scale builders in the background
			self.backend.run_task(self.backend.builders.autoscale)

		# Did we get asked to run again?
		if self._dispatch_requested.is_set():
			await self._dispatch()


class Job(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "jobs"

	def __str__(self):
		return self.name

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			# Order by architecture (alphabetically)
			return self.arch < other.arch

		return NotImplemented

	@property
	def name(self):
		return "%s-%s.%s" % (self.pkg.name, self.pkg.evr, self.arch)

	# ID

	id = Column(Integer, primary_key=True)

	# UUID

	uuid = Column(UUID, nullable=False, server_default=sqlalchemy.func.gen_random_uuid())

	# Build ID

	build_id = Column(Integer, ForeignKey("builds.id"), nullable=False)

	# Build

	build = sqlalchemy.orm.relationship("Build", back_populates="alljobs", lazy="selectin")

	# Arch

	arch = Column(Text, nullable=False)

	def is_test(self):
		"""
			Returns True if this job belongs to a test build
		"""
		return self.build.is_test()

	def has_perm(self, user):
		"""
			Check permissions
		"""
		# Check if this builder can modify the job
		if isinstance(user, builders.Builder):
			return self.builder == user

		# This is the same as for builds
		return self.build.has_perm(user)

	# Package

	@property
	def pkg(self):
		return self.build.pkg

	# Binary Packages

	packages = sqlalchemy.orm.relationship("Package", secondary="job_packages", lazy="selectin")

	async def _import_packages(self, uploads):
		"""
			Will take a list of uploads and import them as packages
		"""
		packages = []

		# Do not allow importing any packages for test builds
		if self.is_test():
			raise RuntimeError("Cannot import packages for test builds")

		# Import all packages
		for upload in uploads:
			packages.append(
				await self.backend.packages.create(upload),
			)

		# Flush the packages to the database
		await self.db.flush()

		# Associate the packages with this job
		await self.db.insert_many(
			JobPackage, ({
				"job_id" : self.id,
				"pkg_id" : pkg.id,
			} for pkg in packages),
		)

		# Consume all packages
		for upload in uploads:
			await upload.delete()

	@property
	def size(self):
		return sum((p.size for p in self.packages))

	# Distro

	@property
	def distro(self):
		return self.build.distro

	def is_superseeded(self):
		"""
			Returns True if this job has been superseeded by another one
		"""
		if self.superseeded_by:
			return True

		return False

	# Superseeded By ID

	superseeded_by_id = Column(Integer, ForeignKey("jobs.id"))

	# Superseeded By

	superseeded_by = sqlalchemy.orm.relationship(
		"Job", remote_side=[id], lazy="selectin",
	)

	@property
	def preceeding_jobs(self):
		"""
			A list of jobs that came before this one
		"""
		jobs = []

		for job in self.build.alljobs:
			if not job.superseeded_by == self:
				continue

			jobs.append(job)
			jobs.extend(job.preceeding_jobs)

		return sorted(jobs)

	@property
	def all_jobs(self):
		"""
			This includes this job and all preceeding jobs
		"""
		return [self] + self.preceeding_jobs

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Started At

	started_at = Column(DateTime(timezone=False), nullable=False)

	# Finished At

	finished_at = Column(DateTime(timezone=False))

	# Date

	@property
	def date(self):
		if self.finished_at:
			return self.finished_at.date()

		# Fall back on to today
		return datetime.date.today()

	# Timeout

	timeout = Column(Interval)

	@property
	def times_out_in(self):
		"""
			The remaining time until the timeout ends
		"""
		if not self.is_running():
			return

		# Return nothing if no timeout is configured
		if not self.timeout:
			return

		# Return the remaining time
		return self.timeout - self.duration

	async def dispatch(self, builder):
		"""
			Called to dispatch this job to the given builder
		"""
		log.debug("Dispatching %s to %s" % (self, builder))

		# Store the builder
		self.builder = builder

		# Store the time
		self.started_at = sqlalchemy.func.current_timestamp()

		# Launch Pakfire in build mode without any mirrors
		pakfire = self.pakfire(build=True, mirrored=False)

		# Generate the configuration file
		config = await pakfire.config()

		# Send a message to the builder
		await builder.send_message({
			"type" : "job",

			"data" : {
				# Add job information
				"id"      : "%s" % self.uuid,
				"name"    : "%s" % self,
				"arch"    : self.arch,

				# ccache path
				"ccache"  : {
					"enabled" : self.ccache_enabled,
					"path"    : self.ccache_path,
				},

				# Is this a test job?
				"test"    : self.is_test(),

				# Configuration
				"config"  : config,

				# URL to the package
				"pkg"     : self.pkg.download_url,
			},
		})

	async def finished(self, success, packages=None, message=None, logfile=None):
		"""
			Called when this job has finished
		"""
		if success:
			log.info("Job %s has finished successfully" % self)
		else:
			log.error("Job %s has finished with an error" % self)

		# Store the time
		self.finished_at = sqlalchemy.func.current_timestamp()

		# Flush to the database and read back the timestamp
		await self.db.flush_and_refresh(self)

		# Import log
		if logfile:
			await self._import_logfile(logfile)

		# Import packages
		if success and packages and not self.is_test():
			await self._import_packages(packages)

		# Store message
		self.message = message

		# Mark as failed
		self.failed = not success

		# On success, update all repositories
		if success:
			await self.build._update_repos()

		# Handle any unsuccessful jobs
		else:
			# Notify the owner
			if self.build.owner:
				# Send an email
				await self.build.owner.send_email(
					"jobs/messages/failed.txt",
					job=self,
					build=self.build,
					log=await self.tail_log(100),
				)

		# Propagate any changes to the build
		return await self.build._job_finished(job=self)

	def is_pending(self, installcheck=None):
		if self.has_finished():
			return False

		if self.is_aborted():
			return False

		if self.is_running():
			return False

		# Did the caller ask for the installcheck to be successful?
		if installcheck is True and not self.installcheck_succeeded:
			return False

		# Did the caller ask for the installcheck to have failed?
		elif installcheck is False and self.installcheck_succeeded:
			return False

		# Otherwise just return True
		return True

	def is_queued(self):
		"""
			The installcheck has been successful and the job is waiting to be dispatched
		"""
		return self.is_pending(installcheck=True)

	async def is_halted(self):
		# Only scratch builds can be halted
		if not self.build.is_scratch():
			return False

		# If the job is either running, finished or aborted, it cannot be halted
		if self.is_running() or self.has_finished() or self.is_aborted():
			return False

		# Halt if users have exceeded their quota
		return await self.build.owner.has_exceeded_build_quota()

	def is_running(self):
		"""
			Returns True if this job is running
		"""
		return self.started_at and not self.finished_at and not self.aborted_at

	def has_finished(self):
		"""
			Returns True if this job has finished, been aborted, etc.
		"""
		if self.finished_at:
			return True

		return False

	# Failed

	failed = Column(Boolean, nullable=False, default=False)

	# Failed?

	def has_failed(self):
		"""
			Returns True if this job has failed
		"""
		if self.has_finished():
			return self.failed

	# Aborted

	aborted = Column(Boolean, nullable=False, default=False)

	# Abort!

	async def abort(self, aborted_by=None):
		"""
			Aborts the job
		"""
		# Do nothing if already aborted
		if self.is_aborted():
			return

		# Tell the builder to abort the job
		try:
			await self.builder.send_message({
				"job_id"  : "%s" % self.uuid,
				"command" : "abort",
			})
		except builders.BuilderNotOnlineError as e:
			pass

		# Mark as finished
		self.finished_at = sqlalchemy.func.current_timestamp()

		# Mark as aborted
		self.aborted = True
		if aborted_by:
			self.aborted_by = aborted_by

		# Try to dispatch more jobs in the background
		await self.backend.jobs.queue.dispatch()

	# Aborted?

	def is_aborted(self):
		"""
			Returns True if this job has been aborted
		"""
		if self.aborted:
			return True

		return False

	# Aborted At

	@property
	def aborted_at(self):
		if self.is_aborted():
			return self.finished_at

	# Aborted By ID

	aborted_by_id = Column(Integer, ForeignKey("users.id"))

	# Aborted By

	aborted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[aborted_by_id], lazy="selectin",
	)

	# Log Streaming

	async def enable_log_streaming(self):
		"""
			Enables log streaming for this job.
		"""
		# Don't run this when the job has already finished
		if self.has_finished():
			return

		# Send a message to the builder
		if self.builder:
			try:
				await self.builder.send_message({
					"job_id"  : "%s" % self.uuid,
					"command" : "launch-log-stream",
				})

			# Ignore if the builder is not online
			except BuilderNotOnlineError:
				pass

	async def disable_log_streaming(self):
		"""
			Disables log streaming for this job.
		"""
		# Don't run this when the job has already finished
		if self.has_finished():
			return

		# Send a message to the builder
		if self.builder:
			try:
				await self.builder.send_message({
					"job_id"  : "%s" % self.uuid,
					"command" : "terminate-log-stream",
				})

			# Ignore if the builder is not online
			except BuilderNotOnlineError:
				pass

	# Message

	message = Column(Text, nullable=False, default="")

	async def delete(self, deleted_by=None):
		"""
			Deletes a job
		"""
		# Delete all binary packages
		for pkg in self.packages:
			await pkg.delete(deleted_by=deleted_by)

		# Mark as deleted
		await super().delete(deleted_by=deleted_by)

		# Delete the log
		await self._delete_log()

	# Clone!

	async def clone(self):
		"""
			Clones this build job
		"""
		job = await self.backend.jobs.create(
			build      = self.build,
			arch       = self.arch,
			superseeds = self,
		)

		# Log action
		log.debug("Cloned job %s as %s" % (self, job))

		return job

	# Retry

	def can_be_retried(self):
		# Cannot be retried if deleted
		if self.is_deleted():
			return False

		# The job cannot have been superseeded by another job
		if self.superseeded_by:
			return False

		# The job must have failed or been aborted
		return self.has_failed() or self.is_aborted()

	async def retry(self, user=None):
		"""
			Creates a copy of a job
		"""
		# Check
		if not self.can_be_retried():
			raise RuntimeError("Job %s cannot be retried" % self)

		# Clone the job
		return await self.clone()

	# Log

	def has_log(self):
		if self.log_path:
			return True

		return False

	def show_log(self):
		"""
			Helper function because we cannot use any() in the templates.

			Is returns True if we should show the "View Log" option.
		"""
		if self.is_running():
			return True

		return any(job.has_log() for job in self.all_jobs)

	@property
	def log_url(self):
		"""
			Creates the URL to the log file without the compression extension.
		"""
		if self.log_path:
			log_path, extension = os.path.splitext(self.log_path)

			return self.backend.path_to_url(log_path)

	# Log Path

	log_path = Column(Text)

	# Log Size

	log_size = Column(BigInteger)

	# Log Digest (blake2s)

	log_digest_blake2s = Column(LargeBinary)

	# Open Log

	@run_in_thread
	def open_log(self):
		"""
			Opens the log file, and returns an open file handle
		"""
		# Raise an error, if we don't have a log file
		if not self.has_log():
			raise FileNotFoundError

		# Open gzip-compressed files
		if self.log_path.endswith(".gz"):
			return gzip.open(self.log_path)

		# Open uncompressed files
		return open(self.log_path, "rb")

	# Tail Log

	tail_ratelimiter = asyncio.Semaphore(1)

	async def tail_log(self, limit):
		"""
			Tails the log file (i.e. returns the N last lines)
		"""
		async with self.tail_ratelimiter:
			# Open the log file
			try:
				with await self.open_log() as f:
					return await asyncio.to_thread(collections.deque, f, limit)

			# Return nothing if the log could not be opened
			except FileNotFoundError as e:
				return []

	# Import the logfile

	async def _import_logfile(self, upload):
		uuid = "%s" % self.uuid

		# Create some destination path
		path = self.backend.path(
			"logs",
			"jobs",
			uuid[0:2],
			uuid[2:4],
			"%s.log.gz" % uuid[4:],
		)

		# Create parent directory
		await self.backend.make_parent_directory(path)

		# Open the destination path
		f = open(path, "wb")

		# If the upload was not compressed already, compress it
		if not upload.extension == "gz":
			f = gzip.GzipFile(fileobj=f, compresslevel=9)

		# Copy the payload
		with f:
			await upload.copyinto(f)

		# Store everything in the database
		self.log_path           = path
		self.log_size           = upload.size
		self.log_digest_blake2s = upload.digest_blake2b512

		# Consume the upload object
		await upload.delete()

	async def _delete_log(self):
		"""
			Called to delete the log file
		"""
		# Nothing to do if there is no log file
		if not self.has_log():
			return

		# Delete the file from disk
		await self.backend.unlink(self.log_path)

		# Reset all database attributes
		self.log_path           = None
		self.log_size           = None
		self.log_digest_blake2s = None

	# Builder ID

	builder_id = Column(Integer, ForeignKey("builders.id"))

	# Builder

	builder = sqlalchemy.orm.relationship("Builder", foreign_keys=[builder_id], lazy="joined")

	@property
	def ccache_enabled(self):
		"""
			Should the ccache be enabled for this build?
		"""
		# Enable the cache for scratch builds
		if self.build.owner:
			return True

		# Enable the cache for test builds
		if self.is_test():
			return True

		# Otherwise disable the ccache
		return False

	@property
	def ccache_path(self):
		"""
			Returns the path to a private ccache
		"""
		# Return nothing if the ccache should not be used
		if not self.ccache_enabled:
			return

		# Include the name of the package and the architecture
		path = os.path.join(
			self.build.pkg.name,
			self.arch,
		)

		# Have a large shared cache for all test builds
		if self.is_test():
			path = os.path.join(
				"test",
				path,
			)

		# Give each user their own private ccache
		elif self.build.owner:
			path = os.path.join(
				"~%s" % self.build.owner.name,
				path,
			)

		return path

	@property
	def duration(self):
		"""
			Returns the total build duration or elapsed time
		"""
		if self.has_finished():
			try:
				return self.finished_at - self.started_at
			except TypeError:
				return datetime.timedelta(0)

		else:
			return datetime.datetime.utcnow() - self.started_at

	def pakfire(self, **kwargs):
		"""
			Generate the Pakfire configuration for this job
		"""
		return self.build.repo.pakfire(arch=self.arch, **kwargs)

	async def installcheck(self):
		"""
			Perform install check
		"""
		await self.build.repo.installcheck([self])

		# Return whether the check succeeded or not
		return self.installcheck_succeeded

	async def _installcheck(self, p):
		"""
			Implementation that takes an active Pakfire instance and
			performs a check on whether the source package can be installed
		"""
		log.info("Performing install check for %s (%s)" % (self, self.uuid))

		# Fetch the commandline repository
		repo = p.get_repo("@commandline")

		# Perform the installcheck
		try:
			# Check if the source package exists
			if not self.pkg.path:
				raise pakfire.DependencyError("Source package does not exist")

			# Open the archive
			archive = await asyncio.to_thread(p.open, self.pkg.path)

			# Fetch the package
			package = await asyncio.to_thread(archive.get_package, repo)

			await asyncio.to_thread(package.installcheck)

		# Store any dependency errors
		except pakfire.DependencyError as e:
			self.installcheck_succeeded = False

			# Store the message
			self.message = "%s" % e

		# Raise any other exceptions
		except Exception as e:
			raise e

		# Everything OK
		else:
			self.installcheck_succeeded = True

		# Store the timestamp
		self.installcheck_performed_at = sqlalchemy.func.current_timestamp()

	# Installcheck Succeeded?

	installcheck_succeeded = Column(Boolean)

	# Installcheck Performed At

	installcheck_performed_at = Column(DateTime(timezone=False))

	# Reverse Requires

	async def _reverse_requires(self):
		log.debug("%s: Searching for reverse requires" % self)

		# Skip this if we don't have any packages
		if not self.packages:
			return []

		packages = []

		async with self.pakfire(include_source=True) as p:
			for package in self.packages:
				for pkg in p.whatprovides("%s = %s" % (package.name, package.evr)):
					# XXX we should search straight away for the UUID here,
					# but that doesn't seem to work right now
					if not ("%s" % package.uuid) == ("%s" % pkg.uuid):
						continue

					# Find all packages that depend on the current package
					for r in pkg.reverse_requires:
						# Skip packages that are provided by ourselves
						if r.name in (p.name for p in self.packages):
							continue

						# Rebuild this package!
						packages.append(r.uuid)

		# Skip the query if there are no packages
		if not packages:
			return []

		# Return any builds that generated those packages
		return await self.backend.builds.get_by_package_uuids(packages)
