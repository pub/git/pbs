###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import alembic.migration
import asyncio
import functools
import logging
import queue
import time

import sqlalchemy
import sqlalchemy.ext.asyncio
from sqlalchemy import Column, DateTime

from . import base

# Setup logging
log = logging.getLogger("pbs.database")

#@sqlalchemy.event.listens_for(sqlalchemy.Engine, "before_cursor_execute")
#def before_cursor_execute(conn, cursor, statement, parameters, context, executemany):
#	now = time.time()
#
#	# Create a queue to store start times
#	try:
#		q = conn.info["query_start_time"]
#	except KeyError:
#		q = conn.info["query_start_time"] = queue.LifoQueue()
#
#	# Push the start time of the query
#	q.put(now)
#
#	# Log the statement
#	log.debug("Start Query: %s %r", statement, parameters)
#
#@sqlalchemy.event.listens_for(sqlalchemy.Engine, "after_cursor_execute")
#def after_cursor_execute(conn, cursor, statement, parameters, context, executemany):
#	time_end = time.time()
#
#	# Fetch the latest start time
#	time_start = conn.info["query_start_time"].get()
#
#	# Compute the total time
#	t = time_end - time_start
#
#	# Log the total runtime
#	log.debug("Query completed in %.02fms", t * 1000)

class Base(sqlalchemy.ext.asyncio.AsyncAttrs, sqlalchemy.orm.DeclarativeBase):
	"""
		This is the declarative base for this application
	"""
	pass


class Connection(object):
	"""
		This is a convenience wrapper around SQLAlchemy.
	"""
	def __init__(self, backend, host, database, user=None, password=None):
		self.backend = backend

		# Make the URL
		self.url = "postgresql+asyncpg://%s:%s@%s/%s" % (user, password, host, database)

		# Create the engine
		self.engine = sqlalchemy.ext.asyncio.create_async_engine(
			self.url,

			# Use our own logger
			logging_name=log.name,

			# Be more verbose
			echo=False, #echo_pool="debug",

			# Increase the pool size
			pool_size=128,
		)

		# Create a session maker
		self.sessionmaker = sqlalchemy.orm.sessionmaker(
			self.engine,
			expire_on_commit = False,
			class_           = sqlalchemy.ext.asyncio.AsyncSession,
			info             = {
				"backend" : self.backend,
			},
		)

		# Stores sessions assigned to tasks
		self.__sessions = {}

	async def check_schema(self):
		"""
			This method checks if the current database schema matches with this application
		"""
		# Run a schema check
		async with self.engine.connect() as c:
			return await c.run_sync(self._check_schema)

	def _check_schema(self, engine):
		# Create a new context
		context = alembic.migration.MigrationContext.configure(engine)

		# Compare the metadata
		diff = alembic.autogenerate.compare_metadata(context, Base.metadata)

		# If we have a difference, lets log it
		if diff:
			log.warning("The database schema does not match:")

			# Product migrations
			migrations = alembic.autogenerate.produce_migrations(context, Base.metadata)

			# Show the differences
			for op in migrations.upgrade_ops.ops:
				self._show_op(op)

		else:
			log.debug("Database schema matches")

	def _show_op(self, op):
		if isinstance(op, alembic.operations.ops.DropTableOp):
			log.warning("Unknown table '%s'" % op.table_name)

		elif isinstance(op, alembic.operations.ops.ModifyTableOps):
			log.warning("Table %s:" % op.table_name)

			# Show all sub-operations
			for _op in op.ops:
				self._show_op(_op)

		elif isinstance(op, alembic.operations.ops.AddColumnOp):
			args = []

			# Is Nullable?
			if not op.column.nullable:
				args.append("NOT NULL")

			log.warning("  Missing column %s (%s)%s %s" %
				(op.column.name, op.column.type, ":" if args else "", " ".join(args)))

		elif isinstance(op, alembic.operations.ops.AlterColumnOp):
			log.warning("  Incorrect column: %s" % op.column_name)

			if not op.modify_type is False and not op.existing_type == op.modify_type:
				log.warning("    Type %s -> %s" % (op.existing_type, op.modify_type))

			if not op.modify_nullable is False and not op.existing_nullable == op.modify_nullable:
				log.warning("    %s -> %s" % (
					"NULL" if op.existing_nullable else "NOT NULL",
					"NULL" if op.modify_nullable else "NOT NULL",
				))

			if not op.modify_comment is False and not op.existing_comment == op.modify_comment:
				log.warning("    Comment: %s -> %s" % (op.existing_comment, op.modify_comment))

		elif isinstance(op, alembic.operations.ops.CreateIndexOp):
			log.warning("  Missing index: %s" % op.index_name)

		elif isinstance(op, alembic.operations.ops.CreateForeignKeyOp):
			log.warning("  Missing foreign key %s : %s -> %s(%s)" %
				(op.constraint_name or "N/A", op.local_cols, op.referent_table, op.remote_cols))

		elif isinstance(op, alembic.operations.ops.CreateUniqueConstraintOp):
			constraint = op.to_constraint()

			log.warning("  Missing unique constraint %s" % constraint.name)

		elif isinstance(op, alembic.operations.ops.DropColumnOp):
			log.warning("  Unknown column: %s" % op.column_name)

		elif isinstance(op, alembic.operations.ops.DropConstraintOp):
			log.warning("  Unknown constraint: %s" % op.constraint_name)

		elif isinstance(op, alembic.operations.ops.DropIndexOp):
			log.warning("  Unknown index: %s" % op.index_name)

		else:
			raise NotImplementedError(
				"Unknown migration operation: %s" % op,
			)

	async def session(self):
		"""
			Returns a session from the engine
		"""
		# Fetch the current task
		task = asyncio.current_task()

		assert task, "Could not determine task"

		# Try returning the same session to the same task
		try:
			return self.__sessions[task]
		except KeyError:
			pass

		# Fetch a new session from the engine
		session = self.__sessions[task] = self.sessionmaker()

		log.debug("Assigning database session %s to %s" % (session, task))

		# When the task finishes, release the connection
		task.add_done_callback(self.release_session)

		return session

	def release_session(self, task):
		"""
			Called when a task that requested a session has finished.

			This method will schedule that the session is being closed.
		"""
		self.backend.run_task(self.__release_session, task)

	async def __release_session(self, task):
		# Retrieve the session
		try:
			session = self.__sessions[task]
		except KeyError:
			return

		# Fetch any exception
		exception = task.exception()

		# If there is no exception, we can commit
		if exception is None:
			await session.commit()

		log.debug("Releasing database session %s of %s" % (session, task))

		# Delete it
		del self.__sessions[task]

		# Close the session
		await session.close()

		# Re-raise the exception
		if exception:
			raise exception

	async def transaction(self):
		"""
			Opens a new transaction
		"""
		# Fetch our session
		session = await self.session()

		# If we are already in a transaction, begin a nested one
		if session.in_transaction():
			return session.begin_nested()

		# Otherwise begin the first transaction of the session
		return session.begin()

	async def insert(self, cls, **kwargs):
		"""
			Inserts a new object into the database
		"""
		# Fetch our session
		session = await self.session()

		# Create a new object
		object = cls(**kwargs)

		# Add it to the database
		session.add(object)

		# Return the object
		return object

	async def insert_many(self, cls, args):
		"""
			Inserts many new objects into the database
		"""
		# Fetch our session
		session = await self.session()

		# Create the new objects
		objects = [
			cls(**kwargs) for kwargs in args
		]

		# Add them to the database
		session.add_all(objects)

		# Return the objects
		return objects

	async def get(self, object, pkey, **kwargs):
		"""
			Fetches an object by its primary key
		"""
		# Fetch our session
		session = await self.session()

		# Return the object
		return await session.get(object, pkey, **kwargs)

	async def delete(self, object):
		"""
			Marks an object as deleted
		"""
		# Fetch our session
		session = await self.session()

		# Mark it as deleted
		await session.delete(object)

	async def execute(self, stmt):
		"""
			Executes a statement and returns a result object
		"""
		# Fetch our session
		session = await self.session()

		# Execute the statement
		result = await session.execute(stmt)

		return result

	async def fetch(self, stmt, batch_size=128):
		"""
			Fetches objects of the given type
		"""
		result = await self.execute(stmt)

		# Process the result in batches
		if batch_size:
			result = result.yield_per(batch_size)

		# Apply unique filtering
		result = result.unique()

		# Return all objects
		for object in result.scalars():
			yield object

	async def fetch_one(self, stmt):
		result = await self.execute(stmt)

		# Apply unique filtering
		result = result.unique()

		# Return exactly one object or none, but fail otherwise
		return result.scalar_one_or_none()

	async def fetch_as_list(self, *args, **kwargs):
		"""
			Fetches objects and returns them as a list instead of an iterator
		"""
		objects = self.fetch(*args, **kwargs)

		# Return as list
		return [o async for o in objects]

	async def fetch_as_set(self, *args, **kwargs):
		"""
			Fetches objects and returns them as a set instead of an iterator
		"""
		objects = self.fetch(*args, **kwargs)

		# Return as set
		return set([o async for o in objects])

	async def select(self, stmt):
		"""
			Returns the raw result after a SELECT statement
		"""
		result = await self.execute(stmt)

		# Process mappings
		result = result.mappings()

		for object in result.fetchall():
			yield object

	async def select_one(self, stmt, attr=None):
		"""
			Returns exactly one row
		"""
		result = await self.execute(stmt)

		# Process mappings
		result = result.mappings()

		# Extract exactly one result (or none)
		result = result.one_or_none()

		# Return if we have no result
		if result is None:
			return

		# Return the whole result if no attribute was requested
		elif attr is None:
			return result

		# Otherwise return the attribute only
		return getattr(result, attr)

	async def commit(self):
		"""
			Manually triggers a database commit
		"""
		# Fetch our session
		session = await self.session()

		# Commit!
		await session.commit()

	async def flush(self, *objects):
		"""
			Manually triggers a flush
		"""
		# Fetch our session
		session = await self.session()

		# Flush!
		await session.flush(objects)

	async def refresh(self, o):
		"""
			Refreshes the given object
		"""
		# Fetch our session
		session = await self.session()

		# Refresh!
		await session.refresh(o)

	async def flush_and_refresh(self, *objects):
		"""
			Flushes and refreshes in one go.
		"""
		# Fetch our session
		session = await self.session()

		# Flush!
		await session.flush(objects)

		# Refresh!
		for o in objects:
			await session.refresh(o)


class BackendMixin:
	@functools.cached_property
	def backend(self):
		# Fetch the session
		session = sqlalchemy.orm.object_session(self)

		# Return the backend
		return session.info.get("backend")

	@functools.cached_property
	def db(self):
		return self.backend.db


class SoftDeleteMixin:
	"""
		A mixin that will automatically add a deleted_at column with a timestamp
		of when an object has been deleted.
	"""

	# Deleted At

	deleted_at = Column(DateTime(timezone=False), nullable=True)

	# Deleted?

	def is_deleted(self):
		if self.deleted_at:
			return True

		return False

	# Delete!

	async def delete(self, deleted_by=None):
		"""
			Called to delete this object
		"""
		self.deleted_at = sqlalchemy.func.current_timestamp()

		# Optionally set deleted_by
		if deleted_by:
			self.deleted_by = deleted_by
