#!/usr/bin/python3

import asyncio
import datetime
import io
import logging
import pakfire
import sqlalchemy

from sqlalchemy import Column, ForeignKey
from sqlalchemy import DateTime, Integer, Numeric, Text

from . import base
from . import database

# Setup logging
log = logging.getLogger("pbs.keys")

DEFAULT_ALGORITHM = pakfire.PAKFIRE_KEY_ALGO_ED25519

class Keys(base.Object):
	async def create(self, comment, user=None):
		"""
			Creates a new key
		"""
		# Launch a new Pakfire instance
		async with self.backend.pakfire() as p:
			# Generate the new key
			key = p.generate_key(DEFAULT_ALGORITHM, comment)

			# Export the secret key
			secret_key = key.export(True)

			# Export the public key
			public_key = key.export()

			# Store the key in the database
			key = await self.db.insert(
				Key,
				created_by = user,
				public_key = public_key,
				secret_key = secret_key,
				key_id     = key.id,
				comment    = comment,
			)

			return key

class Key(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "keys"

	# ID

	id = Column(Integer, primary_key=True)

	# Created At

	created_at = Column(DateTime(timezone=False), nullable=False,
		server_default=sqlalchemy.func.current_timestamp())

	# Created By ID

	created_by_id = Column(Integer, ForeignKey("users.id"))

	# Created By

	created_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[created_by_id], lazy="selectin",
	)

	# Deleted By ID

	deleted_by_id = Column(Integer, ForeignKey("users.id"))

	# Deleted By

	deleted_by = sqlalchemy.orm.relationship(
		"User", foreign_keys=[deleted_by_id], lazy="selectin",
	)

	# Public Key

	public_key = Column(Text, nullable=False)

	# Secret Key

	secret_key = Column(Text, nullable=False)

	# Key ID

	key_id = Column(Numeric, nullable=False)

	# Comment

	comment = Column(Text)

	def _make_key(self, pakfire):
		"""
			Parses the key and returns a Key object
		"""
		return pakfire.import_key(self.secret_key)

	def has_perm(self, user):
		# Anonymous users have no permission
		if not user:
			return False

		# Admins have all permissions
		return user.is_admin()
