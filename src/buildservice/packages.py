#!/usr/bin/python

import asyncio
import datetime
import logging
import mimetypes
import os
import shutil
import stat

import sqlalchemy
from sqlalchemy import Column, Computed, ForeignKey
from sqlalchemy import ARRAY, BigInteger, Boolean, DateTime, Integer, LargeBinary, Text, UUID
from sqlalchemy.dialects.postgresql import TSVECTOR

import pakfire

from . import base
from . import builds
from . import database
from . import jobs
from . import misc

from .constants import *
from .decorators import *
from .errors import *

# Setup logging
log = logging.getLogger("pbs.packages")

class Packages(base.Object):
	async def list(self):
		"""
			Returns a list with all package names and the summary line
			that have at one time been part of the distribution
		"""
		stmt = \
			sqlalchemy.select(
				Package.name,
				Package.summary,
				Package.created_at,
			) \
			.distinct(Package.name) \
			.select_from(Package) \
			.join(
				builds.Build,
				Package.id == builds.Build.pkg_id,
				isouter=True,
			) \
			.where(
				Package.deleted_at == None,
				builds.Build.deleted_at == None,
				Package.arch == "src",
			) \
			.order_by(
				Package.name,
				Package.created_at.desc(),
			)

		return self.db.select(stmt)

	async def get_by_uuid(self, uuid):
		stmt = (
			sqlalchemy
			.select(Package)
			.where(
				Package.deleted_at == None,

				# Match by UUID
				Package.uuid == uuid,
			)
		)

		return await self.db.fetch_one(stmt)

	async def get_by_buildid(self, buildid):
		"""
			Fetches the debug information for the given BuildID
		"""
		# Make the path
		path = buildid_to_path(buildid)

		# Search for the package containing this file
		async for package in self.search_by_filename(path, limit=1):
			log.debug("Found debuginfo for %s in %s" % (buildid, package))

			return package

	async def create(self, upload, distro=None, commit=None):
		"""
			Creates a new package from an uploaded file
		"""
		if not self.backend.test and distro:
			raise RuntimeError("Cannot alter distro when not in test mode")

		# Upload the archive
		archive = await self.backend.open(upload.path)

		# Extract package metadata from archive
		package = await asyncio.to_thread(archive.get_package)

		# Check if a package with this UUID exists
		pkg = await self.get_by_uuid(package.uuid)
		if pkg:
			return pkg

		log.debug("Importing package %s..." % package)

		# Find a matching distribution
		if not distro:
			distro = await self.backend.distros.get_by_tag(package.distribution)
			if not distro:
				log.error("Could not find distribution '%s'" % package.distribution)

				raise NoSuchDistroError(package.distribution)

		# Extract the checksum
		hash_type, checksum = package.checksum

		# Insert into database
		pkg = await self.db.insert(
			Package,
			name         = package.name,
			evr          = package.evr,
			arch         = package.arch,
			uuid         = package.uuid,
			groups       = package.groups,
			distro       = distro,
			packager     = package.packager,
			license      = package.license,
			url          = package.url,
			summary      = package.summary or "",
			description  = package.description or "",
			prerequires  = package.prerequires,
			requires     = package.requires,
			provides     = package.provides,
			conflicts    = package.conflicts,
			obsoletes    = package.obsoletes,
			recommends   = package.recommends,
			suggests     = package.suggests,
			size         = package.installsize,
			build_arches = package.build_arches,
			commit       = commit,
			build_id     = package.build_id,
			build_host   = package.buildhost,
			build_time   = datetime.datetime.fromtimestamp(package.buildtime),
			filesize     = package.downloadsize,
			digest_type  = hash_type,
			digest       = checksum,
		)

		# Import filelist
		await pkg._import_filelist(archive)

		# Import package data
		await pkg._import_archive(archive)

		return pkg

	async def search(self, q, limit=None):
		"""
			Searches for packages that do match the query.

			This function does not work for UUIDs or filenames.
		"""
		source_packages = sqlalchemy.orm.aliased(Package)

		_source_packages = (
			sqlalchemy
			.select(
				source_packages.id.label("pkg_id"),
				source_packages.search,
			)
			.join(builds.Build, source_packages.id == builds.Build.pkg_id)
			.where(
				# Objects must exist
				source_packages.deleted_at == None,
				builds.Build.deleted_at == None,

				# Ignore test builds
				builds.Build.test == False,
			)
		)

		_binary_packages = (
			sqlalchemy
			.select(
				source_packages.id.label("pkg_id"),
				Package.search,
			)
			.select_from(builds.Build)
			.join(jobs.Job)
			.join(Package)
			.join(source_packages)
			.where(
				# Objects must exist
				source_packages.deleted_at == None,
				builds.Build.deleted_at == None,
				jobs.Job.deleted_at == None,
				Package.deleted_at == None,

				# Ignore test builds
				builds.Build.test == False,
			)
		)

		search_index = (
			sqlalchemy
			.union(
				_source_packages,
				_binary_packages,
			)
			.cte("search_index")
		)

		stmt = (
			sqlalchemy
			.select(Package)
			.select_from(search_index)
			.join(Package, search_index.c.pkg_id == Package.id)
			.where(
				sqlalchemy.func.websearch_to_tsquery(
					"english", q,
				)
				.op("@@")(search_index.c.search),
			)
			.order_by(
				sqlalchemy.func.ts_rank(
					search_index.c.search,
					sqlalchemy.func.websearch_to_tsquery(
						"english", q,
					),
				).desc(),
			)
			.limit(limit)
		)

		return await self.db.fetch_as_list(stmt)

	async def search_by_filename(self, filename, limit=None):
		stmt = (
			sqlalchemy
			.select(Package)
			.distinct(Package.name)
			.join(
				File,
				Package.id == File.pkg_id,
			)
			.where(
				File.path.like(filename),
			)
			.order_by(
				Package.name,
				Package.build_time.desc(),
			)
			.limit(limit)
		)

		# Run the query
		return await self.db.fetch_as_list(stmt)


class Package(database.Base, database.BackendMixin, database.SoftDeleteMixin):
	__tablename__ = "packages"

	def __repr__(self):
		return "<%s %s>" % (self.__class__.__name__, self.nevra)

	def __str__(self):
		return self.nevra

	def __lt__(self, other):
		if isinstance(other, self.__class__):
			return pakfire.version_compare(self.nevra, other.nevra) < 0

		return NotImplemented

	# ID

	id = Column(Integer, primary_key=True)

	# UUID

	uuid = Column(UUID, nullable=False)

	# Link

	@property
	def link(self):
		return "/packages/%s" % self.uuid

	# Created At

	created_at = Column(
		DateTime(timezone=False), nullable=False, server_default=sqlalchemy.func.current_timestamp(),
	)

	async def delete(self, deleted_by=None):
		"""
			Deletes this package
		"""
		# Check if this package can be deleted
		if not await self.can_be_deleted():
			log.debug("Won't delete package %s as it is still in use" % self.nevra)
			return

		log.info("Deleting package %s" % self)

		# Delete from the database
		await super().delete(deleted_by=deleted_by)

		# Unlink the payload
		if self.path:
			await self.backend.unlink(self.path)

		# Reset path
		self.path = None

	async def can_be_deleted(self):
		"""
			Checks if this package is being used somewhere else and
			therefore cannot be deleted.
		"""
		# If there any builds using this package, it cannot be deleted
		if await self.get_builds():
			return False

		# This package can be deleted
		return True

	# Name

	name = Column(Text, nullable=False)

	# EVR

	evr = Column(Text, nullable=False)

	# Arch

	arch = Column(Text, nullable=False)

	# Source?

	def is_source(self):
		"""
			Returns True if this is a source package
		"""
		return self.arch == "src"

	# NEVRA

	@property
	def nevra(self):
		return "%s-%s.%s" % (self.name, self.evr, self.arch)

	# Groups

	groups = Column(ARRAY(Text), nullable=False, default=[])

	# Packager

	packager = Column(Text, nullable=False, default="")

	# License

	license = Column(Text, nullable=False)

	# URL

	url = Column(Text, nullable=False)

	# Summary

	summary = Column(Text, nullable=False)

	# Description

	description = Column(Text, nullable=False)

	# Build Arches

	build_arches = Column(ARRAY(Text), nullable=False)

	# Size

	size = Column(BigInteger, nullable=False)

	# Dependencies

	prerequires = Column(ARRAY(Text), nullable=False, default=[])

	requires = Column(ARRAY(Text), nullable=False, default=[])

	provides = Column(ARRAY(Text), nullable=False, default=[])

	conflicts = Column(ARRAY(Text), nullable=False, default=[])

	obsoletes = Column(ARRAY(Text), nullable=False, default=[])

	suggests = Column(ARRAY(Text), nullable=False, default=[])

	recommends = Column(ARRAY(Text), nullable=False, default=[])

	# Commit

	@functools.cached_property
	def commit(self):
		if self.data.commit_id:
			return self.backend.sources.get_commit_by_id(self.data.commit_id)

	# Distro ID

	distro_id = Column(Integer, ForeignKey("distributions.id"), nullable=False)

	# Distro

	distro = sqlalchemy.orm.relationship("Distro",
		foreign_keys=[distro_id], lazy="joined", innerjoin=True)

	# Build ID

	build_id = Column(UUID)

	# Build Host

	build_host = Column(Text, nullable=False)

	# Build Time

	build_time = Column(DateTime(timezone=False), nullable=False)

	# Path

	path = Column(Text)

	# Download URL

	@property
	def download_url(self):
		return self.backend.path_to_url(self.path)

	# Filename

	@property
	def filename(self):
		return os.path.basename(self.path)

	# Digest Type

	digest_type = Column(Text, nullable=False)

	# Digest

	digest = Column(LargeBinary, nullable=False)

	# File Size

	filesize = Column(BigInteger, nullable=False)

	async def _import_archive(self, archive):
		"""
			Imports the package into the filesystem
		"""
		uuid = "%s" % self.uuid

		# Determine the new path
		path = self.backend.path(
			"packages",
			uuid[0:2],
			uuid[2:4],
			uuid[4:],
			"%s.pfm" % self.nevra,
		)

		log.debug("Importing %s to %s..." % (self, path))

		# Store the path
		self.path = path

		# Copy the file if it doesn't exist, yet
		if not await self.backend.exists(path):
			await self.backend.copy(archive.path, path, mode=0o444)

	async def _import_filelist(self, archive):
		log.debug("Importing filelist for %s" % self)

		# Fetch the filelist
		filelist = await asyncio.to_thread(lambda a: a.filelist, archive)

		await self.db.insert_many(
			File, ({
				"package"           : self,
				"path"              : file.path,
				"size"              : file.size,
				"mode"              : file.mode,
				"uname"             : file.uname,
				"gname"             : file.gname,
				"ctime"             : file.ctime,
				"mtime"             : file.mtime,
				"mimetype"          : file.mimetype,
				"digest_sha2_512"   : file.checksum("sha2-512"),
				"digest_sha2_256"   : file.checksum("sha2-256"),
				"digest_blake2b512" : file.checksum("blake2b512"),
				"digest_blake2s256" : file.checksum("blake2s256"),
				"digest_sha3_512"   : file.checksum("sha3-512"),
				"digest_sha3_256"   : file.checksum("sha3-256"),
				"capabilities"      : file.capabilities,
			} for file in filelist),
		)

	async def get_builds(self, limit=None, offset=None):
		stmt = (
			sqlalchemy
			.select(
				builds.Build,
			)
			.where(
				builds.Build.deleted_at == None,
				builds.Build.pkg == self,
			)
			.order_by(
				builds.Build.created_at.desc(),
			)
			.limit(limit)
			.offset(offset)
		)

		return await self.db.fetch_as_list(stmt)

	# Files

	async def get_files(self):
		"""
			Returns the filelist of this package
		"""
		stmt = (
			sqlalchemy
			.select(File)

			# Only select files from this package
			.where(
				File.pkg_id == self.id,
			)

			# Order by path
			.order_by(
				File.path,
			)
		)

		return self.db.fetch(stmt)

	async def get_file(self, path):
		"""
			Fetches a single file of this package
		"""
		stmt = (
			sqlalchemy
			.select(File)

			# Only select files from this package and match by path
			.where(
				File.pkg_id == self.id,
				File.path == path,
			)
		)

		return await self.db.fetch_one(stmt)

	async def get_debuginfo(self, buildid):
		path = buildid_to_path(buildid)

		return await self.get_file(path)

	# Open

	async def open(self):
		"""
			Opens the package archive and returns a handle
		"""
		return await self.backend.open(self.path)

	# Search

	search = Column(
		TSVECTOR, Computed(
			"""
				(
					setweight(
						to_tsvector('simple'::regconfig, name),
						'A'::"char"
					)
					||
					setweight(
						to_tsvector('english'::regconfig, summary),
						'B'::"char"
					)
					||
					setweight(
						to_tsvector('english'::regconfig, description),
						'C'::"char"
					)
				)
			""",
            persisted=True,
        )
	)


class File(database.Base):
	__tablename__ = "package_files"

	def __str__(self):
		return self.path

	# Package ID

	pkg_id = Column(Integer, ForeignKey("packages.id"), primary_key=True, nullable=False)

	# Package

	package = sqlalchemy.orm.relationship("Package", foreign_keys=[pkg_id], lazy="selectin")

	# Path

	path = Column(Text, primary_key=True, nullable=False)

	# Size

	size = Column(BigInteger, nullable=False)

	# Config

	config = Column(Boolean, nullable=False, default=False)

	# Mode

	mode = Column(Integer, nullable=False)

	# Type

	@property
	def type(self):
		return stat.S_IFMT(self.mode)

	# uname

	uname = Column(Text, nullable=False)

	# gname

	gname = Column(Text, nullable=False)

	# Creation Time

	ctime = Column(DateTime(timezone=False), nullable=False)

	# Modification Time

	mtime = Column(DateTime(timezone=False), nullable=False)

	# MIME Type

	mimetype = Column(Text)

	# Capabilities

	capabilities = Column(ARRAY(Text))

	# Digest SHA-2 512

	digest_sha2_512 = Column(LargeBinary)

	# Digest SHA-2 256

	digest_sha2_256 = Column(LargeBinary)

	# Digest Blake2b512

	digest_blake2b512 = Column(LargeBinary)

	# Digest Blake2s256

	digest_blake2s256 = Column(LargeBinary)

	# Digest SHA-3 512

	digest_sha3_512 = Column(LargeBinary)

	# Digest SHA-3 256

	digest_sha3_256 = Column(LargeBinary)

	# Downloadable?

	def is_downloadable(self):
		"""
			Returns True if this file is downloadable
		"""
		# All regular files are downloadable
		return self.type == stat.S_IFREG

	# Viewable?

	def is_viewable(self):
		# Empty files cannot be viewed.
		if self.size == 0:
			return False

		# Files that are of type text/* are viewable
		if self.mimetype and self.mimetype.startswith("text/"):
			return True

		for ext in FILE_EXTENSIONS_VIEWABLE:
			if self.path.endswith(ext):
				return True

		return False

	# Send Payload

	async def open(self):
		# Open the package
		p = await self.package.open()

		# Create a helper function to return a file-like object
		func = lambda: p.read(self.path)

		# Read the payload in a separate thread
		return await asyncio.to_thread(func)

	async def sendfile(self, dst, chunk_size=65536):
		"""
			Sends the payload of the file into the given file descriptor
		"""
		src = await self.open()

		return await asyncio.to_thread(self._sendfile, src, dst, chunk_size=chunk_size)

	def _sendfile(self, src, dst, chunk_size=65536):
		while True:
			chunk = src.read(chunk_size)
			if not chunk:
				break

			dst.write(chunk)

	@property
	async def payload(self):
		"""
			Returns the entire payload at once
		"""
		# Open the file
		f = await self.open()

		# Read everything in a separate thread
		return await asyncio.to_thread(f.readall)


def buildid_to_path(buildid):
	"""
		Returns the path where we expect to find the BuildID
	"""
	return "/usr/lib/debug/.build-id/%s/%s.debug" % (buildid[0:2], buildid[2:])
