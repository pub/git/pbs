/*
	Pakfire Build Service

	Custom JS
*/

$(document).ready(function() {
	// Send our XSRF token with all requests
	$.ajaxSetup({
		headers: {
			"X-Xsrftoken" : $("meta[name=xsrf-token]").attr("content"),
		}
	});
});

/*
	Navigation
*/

var burger = document.querySelector(".navbar-burger");
var menu = document.querySelector("#" + burger.dataset.target);
burger.addEventListener('click', function () {
	burger.classList.toggle('is-active');
	menu.classList.toggle('is-active');
});

/*

	Dropdowns

*/

// Get all dropdowns on the page that aren't hoverable
const dropdowns = document.querySelectorAll('.dropdown:not(.is-hoverable)');

if (dropdowns.length > 0) {
	// For each dropdown, add event handler to open on click.
	dropdowns.forEach(function(el) {
		el.addEventListener('click', function(e) {
			closeDropdowns();
			e.stopPropagation();
			el.classList.toggle('is-active');
		});
	});

	// If user clicks outside dropdown, close it.
	document.addEventListener('click', function(e) {
		closeDropdowns();
	});
}

/*
 * Close dropdowns by removing the "is-active" class
 */
function closeDropdowns() {
	dropdowns.forEach(function(el) {
		el.classList.remove('is-active');
	});
}

// Close dropdowns if ESC pressed
document.addEventListener('keydown', function(e) {
	let event = e || window.event;
	if (event.key === 'Esc' || event.key === 'Escape') {
		closeDropdowns();
	}
});

/*
 * Modals
 */
document.addEventListener("DOMContentLoaded", () => {
	// Functions to open and close a modal
	function openModal($el) {
		$el.classList.add("is-active");
	}

	function closeModal($el) {
		$el.classList.remove("is-active");
	}

	function closeAllModals() {
		(document.querySelectorAll(".modal") || []).forEach(($modal) => {
			closeModal($modal);
		});
	}

	// Add a click event on buttons to open a specific modal
	(document.querySelectorAll(".modal-toggle") || []).forEach(($trigger) => {
		const modal = $trigger.dataset.target;
		const $target = document.getElementById(modal);

		$trigger.addEventListener("click", () => {
			openModal($target);
		});
	});

	// Add a click event on various child elements to close the parent modal
	(document.querySelectorAll(".modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button") || []).forEach(($close) => {
		const $target = $close.closest(".modal");

		$close.addEventListener("click", () => {
			closeModal($target);
		});
	});

	// Add a keyboard event to close all modals
	document.addEventListener("keydown", (event) => {
		if (event.key === "Escape") {
			closeAllModals();
		}
	});
});

/*
 * Push Subscriptions
 *
 * Request permission when the button is being clicked
 */

// Check if the browser supports notifications
$(function() {
	// Nothing to do if the browser supports notifications
	if ("serviceWorker" in navigator && "PushManager" in window)
		return;

	// If not, we will disable the button
	$("#push-subscribe-button").prop("disabled", true);
});

// Handle button click
$("#push-subscribe-button").on("click", function() {
	console.debug("Subscribe button clicked!");

	// Fetch our application server key
	const application_server_key = $(this).data("application-server-key");

	// Request permission from the user
	const request = new Promise(function (resolve, reject) {
		const result = Notification.requestPermission(function (result) {
			resolve(result);
		});

		if (result) {
			result.then(resolve, reject);
		}
	}).then(function (result) {
		if (result !== 'granted') {
			throw new Error("We weren't granted permission.");
		}
	});

	// Show some activity
	$(this).addClass("is-loading");

	// Register our service worker
	var registration = navigator.serviceWorker.register("/static/js/notification-worker.min.js");

	// Register with the push service
	registration = registration.then(function (registration) {
		return registration.pushManager.subscribe({
			userVisibleOnly: true,
			applicationServerKey: application_server_key,
		});
	})

	// Fetch the PushSubscription
	const subscription = registration.then(function (subscription) {
		console.debug("Received PushSubscription: ", JSON.stringify(subscription));

		// Send the PushSubscription to our server
		$.post({
			"url" : "/users/push/subscribe",

			// Payload
			"contentType" : "application/json",
			"data" : JSON.stringify(subscription),
		});

		return subscription;
    });
});

/*
 * Builder Stats
 */

$(".builders-stats").each(function() {
	// Fetch the name of the builder
	const name = $(this).data("name");

	const cpu_usage  = $(this).find("#cpu-usage");
	const mem_usage  = $(this).find("#mem-usage");
	const swap_usage = $(this).find("#swap-usage");

	// Make the URL
	const url = "wss://" + window.location.host + "/builders/" + name + "/stats";

	// Try to connect to the stream
	const stream = new WebSocket(url);

	// Updates the progressbar and sets a colour
	updateProgressBar = function(e, percentage) {
		// Set the value
		e.val(percentage * 100);

		// Remove all classes
		e.removeClass("is-dark is-light is-danger is-warning is-success");

		if (percentage >= 0.9) {
			e.addClass("is-danger");

		} else if (percentage >= 0.75) {
			e.addClass("is-warning");

		} else {
			e.addClass("is-success");
		}
	}

	stream.addEventListener("message", (event) => {
		// Parse message as JSON
		const data = JSON.parse(event.data);

		console.debug("Message from server: ", data);

		// Set CPU usage
		if (cpu_usage)
			updateProgressBar(cpu_usage, data.cpu_usage);

		// Set memory usage
		if (mem_usage)
			updateProgressBar(mem_usage, data.mem_usage);

		// Set swap usage
		if (swap_usage)
			updateProgressBar(swap_usage, data.swap_usage);
	});
});

/*
 * Log Stream
 */
$(".jobs-log-stream").each(function() {
	// Fetch the UUID of the job
	const uuid = $(this).data("uuid");

	// Fetch the limit
	const limit = $(this).data("limit");

	// Find where we are appending lines to
	const log = $(this);

	// Make the URL
	var url = "wss://" + window.location.host + "/api/v1/jobs/" + uuid + "/log/stream";

	if (limit > 0)
		url += "?limit=" + limit;

	// Try to connect to the stream
	const stream = new WebSocket(url);

	stream.addEventListener("message", (event) => {
		// Parse message as JSON
		const data = JSON.parse(event.data);

		console.log("Message from server: ", data);

		// If there is a limit, reduce the number of rows first
		while (limit > 0 && log.children().length >= limit) {
			log.children().first().remove();
		}

		// Create a new line
		var line = $("<li></li>");

		// Set the log level
		line.addClass(data.level);

		// Set the content
		line.text(data.message);

		// Append it to the log window
		log.append(line);
	});
});
