'use strict';

/*
	Handle an incoming push notification
*/
self.addEventListener("push", function(event) {
	var data = {};

	// Fetch the data as JSON
	try {
		data = event.data.json();
	} catch (e) {
		// Nothing
	}

	// Pass the data on for follow-up events
	data.data = data;

	// Log what we have received
	console.debug("Push notification has been received: " + data);

	// Extract the title
	const title = data.title || event.data.text();

	// Show the notification
	const notification = self.registration.showNotification(title, data);

    event.waitUntil(notification);
});

/*
	Handle when the user clicks the notification
*/
self.addEventListener("notificationclick", function(event) {
	// Close the notification
	event.notification.close();

	const url = event.notification.data.url;

	// Open the URL if any
	if (url) {
		event.waitUntil(
			clients.openWindow(url)
		);
	}
});

// pushsubscriptionchange Handle this?
