#!/usr/bin/python

import errno
import tornado.web

from ..errors import NoSuchDistroError
from .. import misc

from . import base

class APIv1IndexHandler(base.APIMixin, base.BaseHandler):
	# Allow users to create builds
	allow_users = True

	@base.negotiate
	async def post(self):
		# Fetch the upload
		upload = await self.get_argument_upload("upload")
		if not upload:
			raise tornado.web.HTTPError(404, "Could not find upload")

		# Check permissions of the upload
		if not upload.has_perm(self.current_user):
			raise base.APIError(errno.ENOPERM, "No permission for using upload %s" % upload)

		# Fetch the repository
		repo_name = self.get_argument("repo", None)

		# Did the uploader request to disable test builds?
		disable_test_builds = self.get_argument_bool("disable_test_builds")

		async with await self.db.transaction():
			# Import the package
			try:
				package = await self.backend.packages.create(upload)

			# If the distribution that is coded into the package could not be found,
			# we will send that error to the user...
			except NoSuchDistroError as e:
				raise base.APIError(errno.ENOENT, "Could not find distribution: %s" % e)

			try:
				# Find the repository
				repo = await self.current_user.get_repo(package.distro, repo_name)
				if not repo:
					raise base.APIError(errno.ENOENT, "Could not find repository")

				# Create a new build
				build = await self.backend.builds.create(repo, package,
					owner=self.current_user, disable_test_builds=disable_test_builds)

			# If anything goes wrong, we will try to delete the package again
			except Exception as e:
				await package.delete(self.current_user)

				raise e

			# Delete the upload
			await upload.delete()

			# Launch the build
			await self.backend.builds.launch([build])

		# Send some data about the build
		self.finish({
			"uuid" : "%s" % build.uuid,
			"name" : "%s" % build,
		})


class IndexHandler(base.BaseHandler):
	async def get(self):
		# Pagination
		offset = self.get_argument_int("offset", None) or 0
		limit  = self.get_argument_int("limit", None) or 25

		# Filters
		name = self.get_argument("name", None)
		user = await self.get_argument_user("user", None)

		# Fetch the most recent builds
		builds = await self.backend.builds.get(
			name   = name,
			user   = user,
			limit  = limit,
			offset = offset,
		)

		await self.render("builds/index.html", builds=builds,
			name=name, user=user, limit=limit, offset=offset)


class ShowHandler(base.BaseHandler):
	async def get(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		await self.render("builds/show.html", build=build,
			pkg=build.pkg, distro=build.distro)


class ApproveHandler(base.BaseHandler):
	@base.authenticated
	def get(self, uuid):
		build = self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Check if this can be approved at all
		if not build.can_be_approved(self.current_user):
			raise tornado.web.HTTPError(400)

		self.render("builds/approve.html", build=build)

	@base.authenticated
	async def post(self, uuid):
		build = self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Check if this can be approved at all
		if not build.can_be_approved(self.current_user):
			raise tornado.web.HTTPError(400)

		# Approve the build
		with self.db.transaction():
			await build.approve(self.current_user)

		self.redirect("/builds/%s" % build.uuid)


class CloneHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		await self.render("builds/clone.html", build=build)

	@base.authenticated
	async def post(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Fetch the repository
		repo = await self.get_argument_repo(
			"repo",
			user   = self.current_user,
			distro = build.distro,
		)

		# Clone the build
		clone = await build.clone(
			owner = self.current_user,
			repo  = repo,
		)

		# Launch all jobs (in the background)
		self.backend.run_task(self.backend.builds.launch, [clone])

		self.redirect("/builds/%s" % clone.uuid)


class DeleteHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Check permissions
		if not build.can_be_deleted(self.current_user):
			raise tornado.web.HTTPError(403, "%s cannot delete build %s" \
				% (self.current_user, build))

		await self.render("builds/delete.html", build=build)

	@base.authenticated
	async def post(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Delete the build
		try:
			await build.delete(deleted_by=self.current_user)
		except PermissionError as e:
			raise tornado.web.HTTPError(403) from e

		self.redirect("/builds")


class WatchHandler(base.BaseHandler):
	@base.authenticated
	async def post(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Add the watcher
		await build.add_watcher(self.current_user)

		self.redirect("/builds/%s" % build.uuid)


class UnwatchHandler(base.BaseHandler):
	@base.authenticated
	async def post(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Remove the watcher
		await build.remove_watcher(self.current_user)

		self.redirect("/builds/%s" % build.uuid)


class CommentHandler(base.BaseHandler):
	@base.authenticated
	async def post(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Add a new comment to the build
		async with await self.db.transaction():
			await build.comment(
				text = self.get_argument("text"),
				user = self.current_user,
			)

		# Redirect to the build
		self.redirect("/builds/%s" % build.uuid)


class BugHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Fetch fields
		fields = await self.backend.bugzilla.fields

		await self.render("builds/bug.html", build=build, fields=fields)

	@base.authenticated
	async def post(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Is the user connected to Bugzilla?
		if not self.current_user.bugzilla:
			raise tornado.web.HTTPError(400, "%s is not connected to Bugzilla" \
				% self.current_user)

		kwargs = {
			# Summary & Description
			"summary"     : self.get_argument("summary"),
			"description" : self.get_argument("description", None),
		} | build.bugzilla_fields

		# Create the bug
		bug = await self.current_user.bugzilla.create_bug(**kwargs)

		# Send the attachments
		for job in build.jobs:
			if not self.get_argument_bool("attach_log_%s" % job.uuid):
				continue

			# Open the logfile
			try:
				log = await job.open_log()
			except FileNotFoundError as e:
				log.warning("Could not open log file for %s" % job)
				continue

			# Attach it to the bug
			await bug.attach(
				summary      = "Log file for %s" % job,
				filename     = "%s.log" % job,
				data         = log,
				content_type = "text/plain",
			)

		await self.render("builds/bug-created.html", build=build, bug=bug)


class ReposAddHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Fetch all available repositories
		try:
			repos = await self.current_user.get_repos(build.distro)
		except KeyError:
			repos = None

		await self.render("builds/repos/add.html", build=build, repos=repos)

	@base.authenticated
	async def post(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Fetch the repository
		repo = await self.get_argument_repo("repo", distro=build.distro, user=self.current_user)

		# Add the build to the repository
		async with await self.db.transaction():
			await repo.add_build(build, user=self.current_user)

		self.redirect("/builds/%s" % build.uuid)


class ReposRemoveHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Raise error when the build is in to repositories
		if not build.repos:
			raise tornado.web.HTTPError(400)

		await self.render("builds/repos/remove.html", build=build)

	@base.authenticated
	async def post(self, uuid):
		build = await self.backend.builds.get_by_uuid(uuid)
		if not build:
			raise tornado.web.HTTPError(404, "Could not find build %s" % uuid)

		# Fetch the repository
		repo = await self.get_argument_repo("repo", distro=build.distro, user=self.current_user)

		# Remove build from the repository
		async with await self.db.transaction():
			await repo.remove_build(build, user=self.current_user)

		self.redirect("/builds/%s" % build.uuid)


class GroupShowHandler(base.BaseHandler):
	async def get(self, uuid):
		group = await self.backend.builds.get_group_by_uuid(uuid)
		if not group:
			raise tornado.web.HTTPError(404, "Could not find build group %s" % uuid)

		await self.render("builds/groups/show.html", group=group)
