#!/usr/bin/python

import asyncio
import os.path
import tornado.web

from .. import misc

from . import base

class IndexHandler(base.BaseHandler):
	async def get(self):
		packages = await self.backend.packages.list()

		await self.render("packages/index.html", packages=packages)


class NameHandler(base.BaseHandler):
	async def get(self, name):
		build = await self.backend.builds.get_latest_by_name(name)
		if not build:
			raise tornado.web.HTTPError(404, "Package '%s' was not found" % name)

		# Fetch all distributions
		distros = {}

		# Collect all scratch builds by distro
		scratch_builds = {}

		# Collect data
		async with asyncio.TaskGroup() as tasks:
			async for distro in self.backend.distros:
				# Fetch bugs
				distros[distro] = tasks.create_task(
					self.backend.bugzilla.search(component=name, **distro.bugzilla_fields),
				)

				# Fetch scratch builds
				if self.current_user:
					scratch_builds[distro] = tasks.create_task(
						self.backend.builds.get(
							user=self.current_user, scratch=True, name=name, distro=distro),
					)

		# Map all bugs
		bugs = { distro : await distros[distro] for distro in distros }

		# Map all scratch builds
		scratch_builds = { distro : await scratch_builds[distro] for distro in distros }

		await self.render("packages/name/index.html",
			package=build.pkg, distros=distros, scratch_builds=scratch_builds, bugs=bugs)


class NameBuildsHandler(base.BaseHandler):
	async def get(self, name):
		build = await self.backend.builds.get_latest_by_name(name)
		if not build:
			raise tornado.web.HTTPError(404, "Package '%s' was not found" % name)

		# Fetch the limit
		limit = self.get_argument_int("limit", 5)

		# Find the latest release builds
		release_builds = self.backend.builds.get_release_builds_by_name(name)

		# Group them by distribution
		distros = await misc.group(release_builds, lambda build: build.distro)

		# Find the latest scratch builds
		scratch_builds = self.backend.builds.get_scratch_builds_by_name(name)

		# Group them by user
		users = await misc.group(scratch_builds, lambda build: build.owner)

		await self.render("packages/name/builds.html", limit=limit,
			package=build.pkg, distros=distros, users=users)


class ShowHandler(base.BaseHandler):
	async def get(self, uuid):
		package = await self.backend.packages.get_by_uuid(uuid)
		if not package:
			raise tornado.web.HTTPError(404, "Could not find package: %s" % uuid)

		await self.render("packages/show.html", package=package)


class FileDownloadHandler(base.BaseHandler):
	@base.ratelimit(limit=100, minutes=60, key="files")
	async def get(self, uuid, path):
		package = await self.backend.packages.get_by_uuid(uuid)
		if not package:
			raise tornado.web.HTTPError(404, "Could not find package: %s" % uuid)

		# Fetch the file
		file = await package.get_file(path)
		if not file:
			raise tornado.web.HTTPError(404, "Could not find file %s in %s" % (path, package))

		# Is this file downloadable?
		if not file.is_downloadable():
			raise tornado.web.HTTPError(400, "%s cannot be downloaded" % file)

		# Send Content-Length
		self.set_header("Content-Length", file.size)

		# Send MIME type
		if file.mimetype:
			self.set_header("Content-Type", file.mimetype)

		# Send the filename
		self.set_header("Content-Disposition",
			"attachment; filename=%s" % os.path.basename(file.path))

		# These pages should not be indexed
		self.add_header("X-Robots-Tag", "noindex")

		# Send the payload
		await file.sendfile(self)


class FileViewHandler(base.BaseHandler):
	@base.ratelimit(limit=100, minutes=60, key="files")
	async def get(self, uuid, path):
		package = await self.backend.packages.get_by_uuid(uuid)
		if not package:
			raise tornado.web.HTTPError(404, "Could not find package: %s" % uuid)

		# Fetch the file
		file = await package.get_file(path)
		if not file:
			raise tornado.web.HTTPError(404, "Could not find file %s in %s" % (path, package))

		# Is this file viewable?
		if not file.is_viewable():
			raise tornado.web.HTTPError(400, "%s cannot be viewed" % file)

		# Fetch the payload
		try:
			payload = await file.payload
		except FileNotFoundError as e:
			raise tornado.web.HTTPError(404, "Could not open file %s" % path) from e

		# These pages should not be indexed
		self.add_header("X-Robots-Tag", "noindex")

		await self.render("packages/view-file.html", package=package, file=file, payload=payload)
