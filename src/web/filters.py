###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import babel
import babel.dates
import datetime
import email.utils
import jinja2
import markdown
import pygments
import re
import stat
import urllib.parse

def avatar_url(user, size=None):
	"""
		Returns the avatar URL
	"""
	return user.avatar(size)

def email_address(e):
	"""
		Extracts the raw email address
	"""
	name, address = email.utils.parseaddr(e)

	return address

def email_name(e):
	"""
		Shows the name of the email address (of if there is none the plain email address)
	"""
	name, address = email.utils.parseaddr(e)

	return name or address

def file_mode(mode):
	"""
		Converts the file mode into a string
	"""
	return stat.filemode(mode)

@jinja2.pass_context
def format_asn(ctx, asn):
	backend = ctx.get("backend")

	# Lookup the Autonomous System
	o = backend.mirrors.location.get_as(asn)

	return "%s" % (o or asn)

@jinja2.pass_context
def format_country_code(ctx, country_code):
	# Fetch locale
	locale = ctx.get("locale")

	# Fetch the translation function
	_ = locale.translate

	# Fetch the Babel locale
	locale = babel.Locale(locale.code)

	try:
		return locale.territories[country_code]
	except KeyError:
		return _("- Unknown Country -")

@jinja2.pass_context
def format_date(ctx, *args, **kwargs):
	# Fetch locale
	locale = ctx.get("locale")

	return locale.format_date(*args, **kwargs)

@jinja2.pass_context
def format_day(ctx, *args, **kwargs):
	# Fetch locale
	locale = ctx.get("locale")

	return locale.format_day(*args, **kwargs)

@jinja2.pass_context
def format_time(ctx, delta, shorter=False):
	# Fetch locale
	locale = ctx.get("locale")

	return babel.dates.format_timedelta(
		delta,
		locale = locale.code,
		format = "short" if shorter else "long",
	)

def highlight(text, filename=None):
	# Find a lexer
	try:
		if filename:
			lexer = pygments.lexers.guess_lexer_for_filename(filename, text)
		else:
			lexer = pygments.lexers.guess_lexer(text)
	except pygments.util.ClassNotFound as e:
		lexer = pygments.lexers.special.TextLexer()

	# Format to HTML
	formatter = pygments.formatters.HtmlFormatter()

	return pygments.highlight(text, lexer, formatter)

def hostname(url):
	# Parse the URL
	url = urllib.parse.urlparse(url)

	# Return only the hostname
	return url.hostname

class PrettyLinksExtension(markdown.extensions.Extension):
	def extendMarkdown(self, md):
		md.preprocessors.register(BugzillaLinksPreprocessor(md), "bugzilla", 10)
		md.preprocessors.register(CVELinksPreprocessor(md), "cve", 10)


class BugzillaLinksPreprocessor(markdown.preprocessors.Preprocessor):
	regex = re.compile(r"(?:#(\d{5,}))", re.I)

	def run(self, lines):
		for line in lines:
			yield self.regex.sub(
				r"[#\1](https://bugzilla.ipfire.org/show_bug.cgi?id=\1)", line)


class CVELinksPreprocessor(markdown.preprocessors.Preprocessor):
	regex = re.compile(r"(?:CVE)[\s\-](\d{4}\-\d+)")

	def run(self, lines):
		for line in lines:
			yield self.regex.sub(
				r"[CVE-\1](https://cve.mitre.org/cgi-bin/cvename.cgi?name=\1)", line)

# Create the renderer
markdown_processor = markdown.Markdown(
	extensions=[
		PrettyLinksExtension(),
		"codehilite",
		"fenced_code",
		"sane_lists",
	],
)

def _markdown(text):
	"""
		Implements a simple markdown processor
	"""
	# Pass the text through a markdown processor
	return markdown_processor.convert(text)


@jinja2.pass_context
def static_url(ctx, *args, **kwargs):
	# Fetch the handler
	handler = ctx.get("handler")

	return handler.static_url(*args, **kwargs)

def summary(summary):
	"""
		Removes any trailing full stops
	"""
	return summary.removesuffix(".")
