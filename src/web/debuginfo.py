#!/usr/bin/python3

import tornado.web

from . import base

class DebugInfoHandler(base.BaseHandler):
	async def get(self, buildid):
		package = await self.backend.packages.get_by_buildid(buildid)
		if not package:
			raise tornado.web.HTTPError(404, "Could not find package providing BuildID %s" % buildid)

		# Fetch the debuginfo
		file = package.get_debuginfo(buildid)
		if not file:
			raise tornado.web.HTTPError(404, "Could not find debuginfo in %s" % package)

		# Send the payload
		await file.sendfile(self)
