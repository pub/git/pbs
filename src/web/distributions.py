#!/usr/bin/python

import asyncio
import tornado.web

from . import base

class IndexHandler(base.BaseHandler):
	async def get(self):
		await self.render("distros/index.html", distros=self.backend.distros)


class ShowHandler(base.BaseHandler):
	async def get(self, slug):
		distro = await self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % slug)

		await self.render("distros/show.html", distro=distro)


class EditHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, slug):
		distro = await self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distribution: %s" % slug)

		# Fetch available Bugzilla products
		bugzilla_products = await self.backend.bugzilla.get_products()

		# Check for permissions
		if not distro.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		self.render("distros/edit.html", distro=distro, bugzilla_products=bugzilla_products)

	@base.authenticated
	async def post(self, slug):
		distro = await self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distribution: %s" % slug)

		with self.db.transaction():
			distro.name    = self.get_argument("name")
			distro.vendor  = self.get_argument("vendor", None)
			distro.contact = self.get_argument("contact", None)
			distro.slogan  = self.get_argument("slogan", None)
			distro.arches  = self.get_arguments("arches")

			# Bugzilla Product/Version
			distro.bugzilla_product, delim, distro.bugzilla_version = \
				self.get_argument("bugzilla", "").partition(":")

		self.redirect("/distros/%s" % distro.slug)


class ReleasesIndexHandler(base.BaseHandler):
	async def get(self, distro_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Pagination
		limit  = self.get_argument_int("limit", 20)
		offset = self.get_argument_int("offset", None)

		# Fetch releases
		releases = await distro.get_releases(limit=limit, offset=offset)

		await self.render("distros/releases/index.html", distro=distro, releases=releases)


class ReleasesShowHandler(base.BaseHandler):
	async def get(self, distro_slug, release_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Fetch the release
		release = await distro.get_release(release_slug)
		if not release:
			raise tornado.web.HTTPError(404, "Could not find release %s" % release_slug)

		# XXX check permissions

		await self.render("distros/releases/show.html", distro=distro, release=release)


class ReleasesCreateHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, distro_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Check permissions
		if not distro.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("distros/releases/edit.html", release=None, distro=distro)

	@base.authenticated
	async def post(self, distro_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Check permissions
		if not distro.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Create the new release
		async with await self.db.transaction():
			release = await distro.create_release(
				name         = self.get_argument("name"),
				stable       = self.get_argument_bool("stable"),
				created_by   = self.current_user,
				announcement = self.get_argument("announcement", None)
			)

		# Redirect to the new release page
		self.redirect(release.url)


class ReleasesEditHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, distro_slug, release_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Fetch the release
		release = await distro.get_release(release_slug)
		if not release:
			raise tornado.web.HTTPError(404, "Could not find release %s" % release_slug)

		# Check permissions
		if not release.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("distros/releases/edit.html", release=release, distro=distro)

	@base.authenticated
	async def post(self, distro_slug, release_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Fetch the release
		release = await distro.get_release(release_slug)
		if not release:
			raise tornado.web.HTTPError(404, "Could not find release %s" % release_slug)

		# Check permissions
		if not release.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		async with await self.db.transaction():
			# Store name
			release.name = self.get_argument("name")

			# Store stable?
			release.stable = self.get_argument_bool("stable")

			# Store announcement
			release.announcement = self.get_argument("announcement", None)

		# Redirect back to the release
		self.redirect(release.url)


class ReleasesDeleteHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, distro_slug, release_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Fetch the release
		release = await distro.get_release(release_slug)
		if not release:
			raise tornado.web.HTTPError(404, "Could not find release %s" % release_slug)

		# Check permissions
		if not release.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("distros/releases/delete.html", release=release, distro=distro)

	@base.authenticated
	async def post(self, distro_slug, release_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Fetch the release
		release = await distro.get_release(release_slug)
		if not release:
			raise tornado.web.HTTPError(404, "Could not find release %s" % release_slug)

		# Check permissions
		if not release.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Delete the release
		async with await self.db.transaction():
			await release.delete(self.current_user)

		# Redirect back to all releases
		self.redirect("/distros/%s/releases" % distro.slug)


class ReleasesPublishHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, distro_slug, release_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Fetch the release
		release = await distro.get_release(release_slug)
		if not release:
			raise tornado.web.HTTPError(404, "Could not find release %s" % release_slug)

		# Check permissions
		if not release.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("distros/releases/publish.html", release=release, distro=distro)

	@base.authenticated
	async def post(self, distro_slug, release_slug):
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Fetch the release
		release = await distro.get_release(release_slug)
		if not release:
			raise tornado.web.HTTPError(404, "Could not find release %s" % release_slug)

		# Check permissions
		if not release.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Publish the release
		async with await self.db.transaction():
			await release.publish()

		# Redirect back to the release
		self.redirect(release.url)
