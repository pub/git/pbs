###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2025 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

# This file implements the OCI Registry API
#   https://github.com/opencontainers/distribution-spec/blob/main/spec.md#api

import hashlib
import json
import tornado.web

from . import base
from ..decorators import run_in_thread

class NotFoundError(tornado.web.HTTPError):
	def __init__(self):
		super().__init__(status_code=404)


class BlobUnknownError(NotFoundError):
	"""
		Raised when we could not find the blob
	"""
	pass


class ManifestUnknownError(NotFoundError):
	"""
		Raised when we could not find the manifest
	"""
	pass


class NameUnknownError(NotFoundError):
	"""
		Raised when we could not find the distro
	"""
	pass


class BaseHandler(base.BaseHandler):
	"""
		A base handler for any registry stuff
	"""
	def hash(self, data):
		if isinstance(data, str):
			data = data.encode()

		h = hashlib.new("sha256")
		h.update(data)

		return "sha256:%s" % h.hexdigest()

	async def get_blob(self, distro, digest):
		"""
			Returns a blob if we have it.
		"""
		# Fetch all releases
		releases = await distro.get_releases()

		# This is a super naive approach because we don't have an index
		# for the files. Considering how rarely we are running through this code,
		# it might work out perfectly fine for us to just walk through all images
		# until we find the correct file. As we are starting with the latest release,
		# chances should be high that we don't have to iterate through all of this
		# for too long.
		for release in releases:
			for image in release.oci_images:
				blob = await image.get_blob(digest)
				if blob:
					return blob

	@run_in_thread
	def stream_blob(self, blob):
		"""
			Streams the blob from a separate thread
		"""
		while True:
			chunk = blob.read(1024 * 1024)
			if not chunk:
				break

			self.write(chunk)

	async def write_error(self, *args, **kwargs):
		# Fetch the exception information
		exc_info = kwargs.get("exc_info")

		# Send nothing if we don't know enough about this error
		if not exc_info:
			return

		# Unpack exception info
		type, exception, traceback = exc_info

		# Match our own API errors
		if isinstance(exception, BlobUnknownError):
			self.finish({
				"errors" : [
					{ "code" : "BLOB_UNKNOWN" },
				],
			})

		elif isinstance(exception, ManifestUnknownError):
			self.finish({
				"errors" : [
					{ "code" : "MANIFEST_UNKNOWN" },
				],
			})

		elif isinstance(exception, NameUnknownError):
			self.finish({
				"errors" : [
					{ "code" : "NAME_UNKNOWN" },
				],
			})

		# Don't send anything for other errors
		else:
			pass


class NotFoundHandler(BaseHandler):
	"""
		A custom 404 error which does not any body.
	"""
	def prepare(self):
		raise tornado.web.HTTPError(404)


class IndexHandler(BaseHandler):
	"""
		This simply has to return 200 OK
	"""
	def get(self):
		pass


class ManifestLabelHandler(BaseHandler):
	# Docker is pulling the same content more than once (god knows why). After the
	# ManifestLabelHandler has responded, the client will ask for the same file again
	# by its digest. Therefore we simply cache them and will send them again upon request.

	async def head(self, *args, **kwargs):
		return await self.get(*args, **kwargs, send_body=False)

	async def get(self, distro_slug, label, send_body=True):
		# Fetch the distribution
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise NameUnknownError

		# Fetch the release
		if label == "latest":
			release = await distro.get_latest_release()
		else:
			release = await distro.get_release(label)

		# Fail if we could not find a release
		if not release:
			raise ManifestUnknownError

		# Check if the client supports the index format
		if not self.client_accepts("application/vnd.oci.image.index.v1+json"):
			raise tornado.web.HTTPError(406, "Client does not support OCI index format")

		# Fail if there are no OCI images
		if not release.oci_images:
			raise ManifestUnknownError

		manifests = []

		# Collect all manifests
		for image in release.oci_images:
			manifests += await image.get_manifests()

		# Serialise the JSON (because self.finish() resets Content-Type)
		index = json.dumps({
			"schemaVersion" : 2,
			"mediaType"     : "application/vnd.oci.image.index.v1+json",
			"manifests"     : manifests,
		})

		# Set Content-* headers
		self.set_header("Content-Type", "application/vnd.oci.image.index.v1+json")
		self.set_header("Content-Length", len(index))

		# Hash and cache
		await self.backend.cache.set(self.hash(index), index)

		# Send the response
		if send_body:
			self.finish(index)


class ManifestHandler(BaseHandler):
	async def head(self, *args, **kwargs):
		return await self.get(*args, **kwargs, send_body=False)

	async def get(self, distro_slug, digest, send_body=True):
		# If we have this item in the cache, we serve it straight away
		manifest = await self.backend.cache.get(digest)
		if manifest:
			# Set Content-Type
			self.set_header("Content-Type", "application/vnd.oci.image.index.v1+json")
			self.set_header("Content-Length", len(manifest))

			# Send the blob
			if send_body:
				self.finish(manifest)

			# We are done if we found a cached response
			return

		# Fetch the distribution
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise NameUnknownError

		# Fetch the blob
		blob = await self.get_blob(distro, digest)
		if not blob:
			raise ManifestUnknownError

		# Set Content-Type
		self.set_header("Content-Type", "application/vnd.oci.image.manifest.v1+json")

		# Send the blob!
		if send_body:
			await self.stream_blob(blob)


class BlobHandler(BaseHandler):
	async def head(self, *args, **kwargs):
		return await self.get(*args, **kwargs, send_body=False)

	async def get(self, distro_slug, digest, send_body=True):
		# Fetch the distribution
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise NameUnknownError

		# Fetch the blob
		blob = await self.get_blob(distro, digest)
		if not blob:
			raise BlobUnknownError

		# Set Content-Type
		self.set_header("Content-Type", "application/octet-stream")

		# Send the blob!
		if send_body:
			await self.stream_blob(blob)


class TagsHandler(BaseHandler):
	async def get(self, distro_slug):
		# Fetch the distribution
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise NameUnknownError

		tags = [
			"latest",
		]

		for release in await distro.get_releases():
			tags.append(release.slug)

		self.finish({
			"name" : distro.slug,
			"tags" : tags,
		})
