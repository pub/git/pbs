#!/usr/bin/python

import re

from . import base

class SearchHandler(base.BaseHandler):
	async def get(self):
		q = self.get_argument("q", None)
		if not q:
			await self.render("search.html", q=None, packages=None, files=None, users=None)
			return

		# Check if the given search pattern is a UUID
		if re.match(r"^([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})$", q):
			# Search for a matching object and redirect to it

			# Search in packages
			package = await self.backend.packages.get_by_uuid(q)
			if package:
				self.redirect("/packages/%s" % package.uuid)
				return

			# Search in builds.
			build = await self.backend.builds.get_by_uuid(q)
			if build:
				self.redirect("/builds/%s" % build.uuid)
				return

			# Search in jobs.
			job = await self.backend.jobs.get_by_uuid(q)
			if job:
				self.redirect("/builds/%s" % job.build.uuid)
				return

		packages = files = users = []

		# If the query starting starts with / we are searching for a file
		if q.startswith("/"):
			files = await self.backend.packages.search_by_filename(q, limit=50)

		# Otherwise we are performing a search for packages & users
		else:
			packages = await self.backend.packages.search(q, limit=50)
			users    = await self.backend.users.search(q, limit=50)

		# Redirect if we have an exact match for a package
		if len(packages) == 1 and not files and not users:
			for package in packages:
				self.redirect("/packages/%s" % package.name)
				return

		# Render results
		await self.render("search.html", q=q, packages=packages, files=files, users=users)
