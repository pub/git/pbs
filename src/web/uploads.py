#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2011 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import asyncio
import errno
import hashlib
import hmac
import io
import tempfile
import tornado.web

from . import base
from .. import uploads
from .. import users

class APIv1IndexHandler(base.APIMixin, base.BaseHandler):
	# Allow users to perform uploads
	allow_users = True

	@base.negotiate
	async def get(self):
		uploads = []

		# Send information about all uploads
		async for upload in self.current_user.get_uploads():
			uploads.append({
				"id"         : "%s" % upload.uuid,
				"filename"   : upload.filename,
				"size"       : upload.size,

				"created_at" : upload.created_at.isoformat(),
				"expires_at" : upload.expires_at.isoformat(),

				# Show whether we have received the payload
				"has-payload": await upload.has_payload(),
			})

		self.finish({
			"uploads" : uploads,
		})

	@base.negotiate
	async def post(self):
		"""
			Creates a new upload and returns its UUID
		"""
		# Fetch the filename
		filename = self.get_argument("filename")

		# Fetch file size
		size = self.get_argument_int("size")

		# Fetch the digest
		hexdigest_blake2b512 = self.get_argument("hexdigest_blake2b512")

		# Convert hexdigest
		try:
			digest_blake2b512 = bytes.fromhex(hexdigest_blake2b512)
		except ValueError as e:
			raise tornado.web.HTTPError(400, "Invalid hexdigest") from e

		# Create a new upload
		async with await self.db.transaction():
			try:
				upload = await self.backend.uploads.create(
					filename          = filename,
					size              = size,
					owner             = self.current_user,
					digest_blake2b512 = digest_blake2b512,
				)

			except users.QuotaExceededError as e:
				raise base.APIError(errno.EDQUOT, "Quota exceeded for %s" % self.current_user) from e

			except ValueError as e:
				raise base.APIError(errno.EINVAL, "%s" % e) from e

		# Commit the session before sending the response to the client
		await self.db.commit()

		# Send the ID of the upload back to the client
		self.finish({
			"id"         : "%s" % upload.uuid,
			"expires_at" : upload.expires_at.isoformat(),

			# Filename
			"filename"   : upload.filename,

			# The URL the client should send the payload to
			"url"        : self.backend.url_to("/api/v1/uploads/%s" % upload.uuid),
		})


@tornado.web.stream_request_body
class APIv1DetailHandler(base.APIMixin, base.BaseHandler):
	# Allow users to perform uploads
	allow_users = True

	# Compute a hash while receiving the file
	hashes = (
		"blake2b512",
	)

	def initialize(self):
		# Create a temporary buffer in memory which will be flushed out to disk
		# once it has received more than 128 MiB of data
		self.f = tempfile.SpooledTemporaryFile(
			max_size = 128 * 1024 * 1024, # 128 MiB
			dir      = self.backend.path("tmp"),
		)

		# Initalize the hashers
		self.hashers = { h : hashlib.new(h) for h in self.hashes }

	async def data_received(self, data):
		"""
			Called when some data is being received
		"""
		# Hash the data
		for h in self.hashers:
			self.hashers[h].update(data)

		# Write the data to the buffer
		await asyncio.to_thread(self.f.write, data)

	@base.negotiate
	async def get(self, uuid):
		"""
			This is mostly for debugging so that we can download
			any uploaded content for inspection.
		"""
		# Only allow this for admins
		if not self.current_user.is_admin():
			raise tornado.web.HTTPError(403)

		# Fetch the upload
		upload = await self.backend.uploads.get_by_uuid(uuid)
		if not upload:
			raise tornado.web.HTTPError(404, "Could not find upload %s" % uuid)

		# XXX we should guess the mime type here

		# Send it to the browser
		await upload.copyinto(self)

	@base.negotiate
	async def put(self, uuid):
		"""
			Called to store the received payload
		"""
		# Fetch the upload
		upload = await self.backend.uploads.get_by_uuid(uuid)
		if not upload:
			raise tornado.web.HTTPError(404, "Could not find upload %s" % uuid)

		# XXX has perm?

		# Fail if we did not receive anything
		if not self.f.tell():
			raise base.APIError(errno.ENODATA, "No data received")

		# Rewind the file
		self.f.seek(0)

		# Finalize digests of received data
		digests = {
			h : self.hashers[h].digest() for h in self.hashers
		}

		# Check if digests match
		for algo, digest in digests.items():
			if algo == "blake2b512":
				if not hmac.compare_digest(upload.digest_blake2b512, digest):
					raise tornado.web.HTTPError(409, "%s digest does not match" % algo)

		# Import the payload from the buffer
		try:
			await upload.copyfrom(self.f)

		except ValueError as e:
			raise base.APIError(errno.EINVAL, "%s" % e) from e

		# Commit the session
		await self.db.commit()

		# Send no response
		self.set_status(204)
		self.finish()

	@base.negotiate
	async def delete(self, uuid):
		"""
			Deletes an upload with a certain UUID
		"""
		upload = await self.backend.uploads.get_by_uuid(uuid)
		if not upload:
			raise tornado.web.HTTPError(404, "Could not find upload %s" % uuid)

		# Check for permissions
		if not upload.has_perm(self.current_user):
			raise tornado.web.HTTPError(403, "%s has no permission to delete %s" \
				% (current_user, upload))

		# Delete the upload
		async with await self.db.transaction():
			await upload.delete()

		# Send no response
		self.set_status(204)
		self.finish()
