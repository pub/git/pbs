#!/usr/bin/python3

import logging
import tornado.web
import tornado.websocket

from .. import misc
from . import base

# Setup logging
log = logging.getLogger("pbs.web.jobs")

class APIv1FinishedHandler(base.APIMixin, base.BackendMixin, tornado.web.RequestHandler):
	@base.negotiate
	async def post(self, uuid):
		job = await self.backend.jobs.get_by_uuid(uuid)
		if not job:
			raise tornado.web.HTTPError(404, "Could not find job %s" % uuid)

		# Check permissions
		if not job.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Success Status
		success = self.get_argument_bool("success")

		# Fetch the logfile
		logfile = await self.get_argument_upload("logfile")

		# Fetch the packages
		packages = await self.get_argument_uploads("package")

		# If the job has been successful there must be packages
		if success and not packages:
			raise tornado.web.HTTPError(400, "No packages")

		# Mark the job as finished
		async with await self.db.transaction():
			builds = await job.finished(
				success  = success,
				logfile  = logfile,
				packages = packages,
			)

		# Send an empty response
		self.set_status(204)
		self.finish()

		# Try to dispatch the next job
		await self.backend.jobs.queue.dispatch()

		# Launch any (test) builds
		if builds:
			self.backend.run_task(self.backend.builds.launch, builds)


class APIv1LogStreamHandler(base.BackendMixin, tornado.websocket.WebSocketHandler):
	# No authentication required
	async def open(self, uuid):
		job = await self.backend.jobs.get_by_uuid(uuid)
		if not job:
			raise tornado.web.HTTPError(404, "Could not find job %s" % uuid)

		# How many messages should be initially sent?
		limit = self.get_argument_int("limit", None)

		# Join the stream
		self.stream = await self.backend.logstreams.join(job, self, limit=limit)
		if not self.stream:
			raise tornado.web.HTTPError(400, "Could not join stream for %s" % job)

		# Send messages without any delay
		self.set_nodelay(True)

	def on_close(self):
		"""
			Leave the stream
		"""
		self.stream.leave(self)

	async def message(self, message):
		"""
			Called when there is a new message to be sent to the client
		"""
		try:
			await self.write_message(message)

		# Ignore if the message could not be sent
		except tornado.websocket.WebSocketClosedError as e:
			pass


class APIv1CrashedHandler(base.APIMixin, base.BackendMixin):
	"""
		This handler is called by the daemon when a job has crashed.
	"""
	# Don't allow users to authenticate
	allow_users = False

	@base.negotiate
	async def post(self, job_id):
		job = await self.backend.jobs.get_by_uuid(job_id)
		if not job:
			raise tornado.web.HTTPError(404, "Could not find job %s" % job_id)

		# Check permissions
		if not job.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Signal
		signo = self.get_argument_int("signo")

		log.error("%s has been killed with signal %s" % (job, signo))

		# Automatically abort the job
		await job.abort()

		# Send an empty response
		self.set_status(204)
		self.finish()


class IndexHandler(base.BaseHandler):
	async def get(self):
		# Pagination
		offset = self.get_argument_int("offset", None) or 0
		limit  = self.get_argument_int("limit", None) or 50

		# Filter
		failed_only = self.get_argument_bool("failed_only")

		# Fetch all finished jobs
		jobs = self.backend.jobs.get_finished(
			failed_only = failed_only,
			limit       = limit,
			offset      = offset,
		)

		await self.render("jobs/index.html", jobs=jobs, limit=limit, offset=offset,
			failed_only=failed_only)


class QueueHandler(base.BaseHandler):
	async def get(self):
		# Fetch all jobs
		jobs = [job async for job in self.backend.jobs.queue]

		await self.render("jobs/queue.html", jobs=jobs)


class LogHandler(base.BaseHandler):
	@base.ratelimit(limit=60, minutes=60, key="log")
	async def get(self, uuid):
		job = await self.backend.jobs.get_by_uuid(uuid)
		if not job:
			raise tornado.web.HTTPError(404, "Could not find job %s" % uuid)

		# Stream the log if the job is running
		if job.is_running():
			return await self.render("jobs/log-stream.html", job=job)

		# Set Content-Type header
		self.set_header("Content-Type", "text/plain")

		# Should we tail the log file?
		tail = self.get_argument_int("tail", None)

		# Should we tail the log, or stream the entire file?
		if tail:
			try:
				log = await job.tail_log(tail)

				# Since we are processing this line-based,
				# we will send all lines that have been sent to us
				for line in log:
					self.write(line)

				# Done!
				return

			# Send 404 if there is no log file
			except FileNotFoundError as e:
				raise tornado.web.HTTPError(404, "Could not find log for %s" % job) from e

		# Send 404 if there is no log
		if not job.log_url:
			raise tornado.web.HTTPError(404)

		# Redirect the client
		self.redirect(job.log_url, status=307)


class AbortHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, uuid):
		job = await self.backend.jobs.get_by_uuid(uuid)
		if not job:
			raise tornado.web.HTTPError(404, "Job not found: %s" % uuid)

		# Check for permissions
		if not job.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("jobs/abort.html", job=job)

	@base.authenticated
	async def post(self, uuid):
		job = await self.backend.jobs.get_by_uuid(uuid)
		if not job:
			raise tornado.web.HTTPError(404, "Job not found: %s" % uuid)

		# Check for permissions
		if not job.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Abort the job
		async with await self.db.transaction():
			await job.abort(aborted_by=self.current_user)

		self.redirect("/builds/%s" % job.build.uuid)


class RetryHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, uuid):
		job = await self.backend.jobs.get_by_uuid(uuid)
		if not job:
			raise tornado.web.HTTPError(404, "Could not find job %s" % uuid)

		# Check for permissions
		if not job.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("jobs/retry.html", job=job)

	@base.authenticated
	async def post(self, uuid):
		job = await self.backend.jobs.get_by_uuid(uuid)
		if not job:
			raise tornado.web.HTTPError(404, "Could not find job %s" % uuid)

		# Retry the job
		job = await job.retry(self.current_user)

		# Launch the newly created job
		await self.backend.jobs.launch([job])

		# Redirect back to the build page
		self.redirect("/builds/%s" % job.build.uuid)
