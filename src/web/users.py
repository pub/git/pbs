#!/usr/bin/python

import json
import tornado.locale
import tornado.web

from . import base

class IndexHandler(base.BaseHandler):
	async def get(self):
		# Fetch the top users
		users = await self.backend.users.get_top()

		await self.render("users/index.html", users=users)


class ShowHandler(base.BaseHandler):
	async def get(self, name):
		user = await self.backend.users.get_by_name(name)
		if not user:
			raise tornado.web.HTTPError(404, "Could not find user: %s" % name)

		await self.render("users/show.html", user=user)


class DeleteHandler(base.BaseHandler):
	@base.authenticated
	def get(self, name):
		user = self.backend.users.get_by_name(name)
		if not user:
			raise tornado.web.HTTPError(404, "Could not find user %s" % name)

		# Check for permission
		if not user.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		self.render("users/delete.html", user=user)

	@base.authenticated
	def post(self, name):
		user = self.backend.users.get_by_name(name)
		if not user:
			raise tornado.web.HTTPError(404, "Could not find user %s" % name)

		# Check for permission
		if not user.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		with self.db.transaction():
			user.delete()

		self.redirect("/users")


class EditHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, name):
		user = await self.backend.users.get_by_name(name)
		if not user:
			raise tornado.web.HTTPError(404, "Could not find user %s" % name)

		# Check for permission
		if not user.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("users/edit.html", user=user)

	@base.authenticated
	async def post(self, name):
		user = await self.backend.users.get_by_name(name)
		if not user:
			raise tornado.web.HTTPError(404, "Could not find user %s" % name)

		# Check for permission
		if not user.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		async with await self.db.transaction():
			# Connect to Bugzilla
			bugzilla_api_key = self.get_argument("bugzilla_api_key", None)
			if bugzilla_api_key:
				await user.connect_to_bugzilla(bugzilla_api_key)

			# Need admin permissions for these
			if self.current_user.is_admin():
				user.storage_quota = self.get_argument_int("storage-quota", 0) * (1024 ** 2)

		self.redirect("/users/%s" % user.name)


class BuildsHandler(base.BaseHandler):
	def get(self, name):
		user = self.backend.users.get_by_name(name)
		if not user:
			raise tornado.web.HTTPError(404, "Could not find user: %s" % name)

		self.render("users/builds.html", user=user, builds=user.builds)


class PushSubscribeHandler(base.BaseHandler):
	@base.authenticated
	async def get(self):
		await self.render("users/subscribe.html")

	@base.authenticated
	async def post(self):
		# The request body must be JSON
		if not self.request.headers.get("Content-Type") == "application/json":
			raise tornado.web.HTTPError(400)

		# Parse the JSON blob
		try:
			blob = json.loads(self.request.body)
		except json.DecodeError as e:
			raise tornado.web.HTTPError(400, "Could not parse JSON: %s" % e) from e

		# Fetch all values
		args = {
			"endpoint"   : blob.get("endpoint"),
			"p256dh"     : blob.get("keys").get("p256dh"),
			"auth"       : blob.get("keys").get("auth"),

			# Add the user agent
			"user_agent" : self.user_agent,
		}

		async with await self.db.transaction():
			await self.current_user.subscribe(**args)

		# Send empty response
		self.set_status(204)
		self.finish()
