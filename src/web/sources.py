###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import tornado.web

from . import base

class ShowHandler(base.BaseHandler):
	async def _get_source(self, distro_slug, repo_slug, source_slug, user_slug=None):
		user = None

		# Find the user
		if user_slug:
			user = await self.backend.users.get_by_name(user_slug)
			if not user:
				raise tornado.web.HTTPError(404, "Could not find user: %s" % user_slug)

		# Find the distribution
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Find the repository
		if user:
			repo = await user.get_repo(distro, repo_slug)
		else:
			repo = await distro.get_repo(repo_slug)
		if not repo:
			raise tornado.web.HTTPError(404, "Could not find repo: %s" % repo_slug)

		# Find the source
		source = await repo.get_source_by_slug(source_slug)
		if not source:
			raise tornado.web.HTTPError(404, "Could not find source: %s" % source_slug)

		return source

	async def get(self, **kwargs):
		source = await self._get_source(**kwargs)

		await self.render("sources/show.html", source=source)


class ShowCommitHandler(base.BaseHandler):
	async def _get_commit(self, distro_slug, repo_slug, source_slug, commit_slug, user_slug=None):
		user = None

		# Find the user
		if user_slug:
			user = await self.backend.users.get_by_name(user_slug)
			if not user:
				raise tornado.web.HTTPError(404, "Could not find user: %s" % user_slug)

		# Find the distribution
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Find the repository
		if user:
			repo = await user.get_repo(distro, repo_slug)
		else:
			repo = await distro.get_repo(repo_slug)
		if not repo:
			raise tornado.web.HTTPError(404, "Could not find repo: %s" % repo_slug)

		# Find the source
		source = await repo.get_source_by_slug(source_slug)
		if not source:
			raise tornado.web.HTTPError(404, "Could not find source: %s" % source_slug)

		# Find the commit
		commit = await source.get_commit(commit_slug)
		if not commit:
			raise tornado.web.HTTPError(404, "Could not find commit %s in %s" % (commit_slug, source))

		return commit

	async def get(self, **kwargs):
		commit = await self._get_commit(**kwargs)

		# Fetch any fixed bugs
		fixed_bugs = await commit.get_fixed_bugs()

		await self.render("sources/commit.html", source=commit.source, commit=commit,
			fixed_bugs=fixed_bugs)
