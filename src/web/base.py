#!/usr/bin/python

import asyncio
import base64
import binascii
import datetime
import functools
import http.client
import ipaddress
import jinja2
import jinja2.ext
import json
import kerberos
import logging
import os
import re
import socket
import sys
import time
import tornado.concurrent
import tornado.escape
import tornado.locale
import tornado.web
import tornado.websocket
import tornado.util
import traceback
import urllib.parse

from .. import __version__
from .. import builders
from .. import users
from ..decorators import *

from . import filters

# Setup logging
log = logging.getLogger("pbs.web.base")

class KerberosAuthMixin(object):
	"""
		A mixin that handles Kerberos authentication
	"""
	@property
	def kerberos_realm(self):
		return "IPFIRE.ORG"

	@property
	def kerberos_service(self):
		return self.backend.config.get("krb5", "service", fallback="HTTP")


	@property
	def kerberos_principal(self):
		return self.backend.config.get(
			"krb5", "principal", fallback="pakfire/%s" % socket.getfqdn(),
		)

	def authenticate_redirect(self):
		"""
			Called when the application needs the user to authenticate.

			We will send a response with status code 401 and set the
			WWW-Authenticate header to ask the client to either initiate
			some Kerberos authentication, or to perform HTTP Basic authentication.
		"""
		# Ask the client to authenticate using Kerberos
		self.add_header("WWW-Authenticate", "Negotiate")

		# Ask the client to authenticate using HTTP Basic Auth
		self.add_header("WWW-Authenticate", "Basic realm=\"%s\"" % self.kerberos_realm)

		# Set status to 401
		self.set_status(401)

	@functools.cache
	def get_authenticated_user(self):
		auth_header = self.request.headers.get("Authorization", None)

		# No authentication header
		if not auth_header:
			return

		# Perform GSS API Negotiation
		if auth_header.startswith("Negotiate "):
			return self._auth_negotiate(auth_header)

		# Perform Basic Authentication
		elif auth_header.startswith("Basic "):
			return self._auth_basic(auth_header)

		# Fail on anything else
		else:
			raise tornado.web.HTTPError(400, "Unexpected Authentication attempt: %s" % auth_header)

	def _auth_negotiate(self, auth_header):
		auth_value = auth_header.removeprefix("Negotiate ")

		# Set keytab to use
		os.environ["KRB5_KTNAME"] = self.backend.config.get("krb5", "keytab")

		try:
			# Initialise the server session
			result, context = kerberos.authGSSServerInit(self.kerberos_service)

			if not result == kerberos.AUTH_GSS_COMPLETE:
				raise tornado.web.HTTPError(500, "Kerberos Initialization failed: %s" % result)

			# Check the received authentication header
			result = kerberos.authGSSServerStep(context, auth_value)

			# If this was not successful, we will fall back to Basic authentication
			if not result == kerberos.AUTH_GSS_COMPLETE:
				return self._auth_basic(auth_header)

			if not isinstance(self, tornado.websocket.WebSocketHandler):
				# Fetch the server response
				response = kerberos.authGSSServerResponse(context)

				# Send the server response
				self.set_header("WWW-Authenticate", "Negotiate %s" % response)

			# Return the user who just authenticated
			user = kerberos.authGSSServerUserName(context)

		except kerberos.GSSError as e:
			log.error("Kerberos Authentication Error: %s" % e)

			raise tornado.web.HTTPError(500, "Could not initialize the Kerberos context")

		finally:
			# Cleanup
			kerberos.authGSSServerClean(context)

		log.debug("Successfully authenticated %s" % user)

		return user

	def _auth_basic(self, auth_header):
		# Remove "Basic "
		auth_header = auth_header.removeprefix("Basic ")

		try:
			# Decode base64
			auth_header = base64.b64decode(auth_header).decode()

			username, password = auth_header.split(":", 1)
		except:
			raise tornado.web.HTTPError(400, "Authorization data was malformed")

		# Authenticate against Kerberos
		return self._auth_with_credentials(username, password)

	def _auth_with_credentials(self, username, password):

		# Set keytab to use
		os.environ["KRB5_KTNAME"] = self.backend.config.get("krb5", "keytab")

		# Check the credentials against the Kerberos database
		try:
			kerberos.checkPassword(username, password,
				self.kerberos_principal, self.kerberos_realm)

		# Catch any authentication errors
		except kerberos.BasicAuthError as e:
			log.error("Could not authenticate %s: %s" % (username, e))
			return

		# Create user principal name
		user = "%s@%s" % (username, self.kerberos_realm)

		log.debug("Successfully authenticated %s" % user)

		return user


class BaseHandler(tornado.web.RequestHandler):
	@property
	def backend(self):
		return self.application.backend

	@property
	def db(self):
		return self.backend.db

	async def get_session(self):
		"""
			Returns the user session (if logged in)
		"""
		if not hasattr(self, "_session"):
			# Get the session from the cookie
			session_id = self.get_cookie("session_id", None)

			# Fetch the session
			if session_id:
				session = await self.backend.sessions.get(session_id)
			else:
				session = None

			# Store the session
			self._session = session

		return self._session

	async def get_current_user(self):
		session = await self.get_session()

		# If logged in, return the user
		if session:
			return session.user

	async def get_user_locale(self):
		# Get the locale from the user settings
		current_user = await self.get_current_user()
		if current_user:
			return current_user.locale

		# If no locale was provided, we take what ever the browser requested
		return self.get_browser_locale()

	@property
	def current_address(self):
		"""
			Returns the IP address the request came from.
		"""
		return ipaddress.ip_address(self.request.remote_ip)

	@property
	def user_agent(self):
		"""
			Returns the HTTP user agent
		"""
		return self.request.headers.get("User-Agent", None)

	def client_accepts(self, type):
		"""
			Checks if type is in Accept-Encoding: header
		"""
		# Fetch the header
		header = self.request.headers.get("Accept", "*/*")

		# Assume everything is accepted
		if header == "*/*":
			return True

		# Check if the type is accepted
		return type in re.split(r",\s*", header)

	def client_accepts_encoding(self, encoding):
		"""
			Checks if type is in Accept-Encoding: header
		"""
		# Fetch the header
		header = self.request.headers.get("Accept-Encoding", "")
		if not header:
			return False

		# Check if the encoding is accepted
		return encoding in re.split(r",\s*", header)

	def format_date(self, date, relative=True, shorter=False, full_format=False):
		return self.locale.format_date(date, relative=relative,
			shorter=shorter, full_format=full_format)

	# Template Loader

	def create_template_loader(self, template_path):
		"""
			Creates a new template loader using Jinja
		"""
		env = jinja2.Environment(
			# Load templates from the filesystem
			loader       = jinja2.FileSystemLoader(template_path),

			# Automatically escape
			autoescape   = True,

			# Fail if we are trying to access undefined variables
			undefined    = jinja2.StrictUndefined,

			# Enable asyncio
			enable_async = True,

			# Cache templates for forever
			cache_size   = -1,

			# Load extensions
			extensions   = [
				jinja2.ext.debug,
				jinja2.ext.i18n,
				jinja2.ext.loopcontrols,
			],
		)

		# Configure global variables
		env.globals |= {
			"backend"  : self.backend,
			"version"  : __version__,

			# Python modules
			"datetime" : datetime,

			# Functions
			"make_url" : self.make_url,
		}

		# Custom Filters
		env.filters |= {
			"avatar_url"          : filters.avatar_url,
			"email_address"       : filters.email_address,
			"email_name"          : filters.email_name,
			"file_mode"           : filters.file_mode,
			"format_asn"          : filters.format_asn,
			"format_country_code" : filters.format_country_code,
			"format_date"         : filters.format_date,
			"format_day"          : filters.format_day,
			"format_time"         : filters.format_time,
			"highlight"           : filters.highlight,
			"hostname"            : filters.hostname,
			"markdown"            : filters._markdown,
			"static_url"          : filters.static_url,
			"summary"             : filters.summary,

			# Add some Python built-ins
			"dir"                 : dir,
			"enumerate"           : enumerate,
			"range"               : range,
		}

		return JinjaTemplateLoader(env)

	async def get_template_namespace(self):
		# Make a new XSRF token
		xsrf_token = await self._make_xsrf_token()

		ns = {
			"handler"         : self,
			"request"         : self.request,
			"current_address" : self.current_address,
			"current_user"    : self.current_user,
			"hostname"        : self.request.host,
			"now"             : datetime.datetime.now(),

			# i18n
			"locale"          : self.locale,
			"gettext"         : self.locale.translate,
			"ngettext"        : self.locale.translate,
			"pgettext"        : self.locale.pgettext,

			# XSRF Stuff
			"xsrf_token"      : xsrf_token,
			"xsrf_form_html"  : self.xsrf_form_html,
		}

		return ns

	def make_url(self, url, **kwargs):
		"""
			Formats any query arguments and append them to the URL
		"""
		if kwargs:
			# Filter out None
			args = { k : kwargs[k] for k in kwargs if kwargs[k] is not None }

			# Encode into URL format
			args = urllib.parse.urlencode(args)

			url = "%s?%s" % (url, args)

		return url

	async def render_string(self, template_name, **kwargs):
		"""
			Generates the given template with the given arguments.
		"""
		template_path = self.get_template_path()

		with self._template_loader_lock:
			try:
				loader = self._template_loaders[template_path]
			except KeyError:
				loader = self._template_loaders[template_path] = \
					self.create_template_loader(template_path)

		# Load the template
		template = loader.load(template_name)

		# Make the namespace
		namespace = await self.get_template_namespace()
		namespace.update(kwargs)

		return await template.render_async(**namespace)

	async def render(self, template_name, **kwargs):
		"""
			Generates the given template and sends it as the response.
		"""
		html = await self.render_string(template_name, **kwargs)

		# Send the response
		self.finish(html)

	async def _execute(self, transforms, *args, **kwargs):
		"""
			Executes this request
		"""
		self._transforms = transforms

		try:
			# Raise error if the method is not supported
			if self.request.method not in self.SUPPORTED_METHODS:
				raise HTTPError(405)

			# Parse arguments
			self.path_args = [self.decode_argument(arg) for arg in args]
			self.path_kwargs = {
				k : self.decode_argument(v, name=k) for (k, v) in kwargs.items()
			}

			# Automatically log the user
			self.current_user = await self.get_current_user()

			# Set the locale
			self.locale = await self.get_user_locale()

			# Check the XSRF cookie
			if not self.request.method in ("GET", "HEAD", "OPTIONS"):
				self.check_xsrf_cookie()

			# Prepare the request
			result = self.prepare()
			if result:
				await result

			# Tell the application we are now ready to receive the body
			if self._prepared_future:
				tornado.concurrent.future_set_result_unless_cancelled(
					self._prepared_future, None
				)
			if self._finished:
				return

			# In streaming mode, we have to wait until the entire body has been received
			if tornado.web._has_stream_request_body(self.__class__):
				try:
					await self.request._body_future
				except tornado.iostream.StreamClosedError:
					return

			# Fetch the implementation
			method = getattr(self, self.request.method.lower())

			# Call the method
			result = method(*self.path_args, **self.path_kwargs)
			if result:
				await result

			# Automatically finish?
			if self._auto_finish and not self._finished:
				self.finish()

		except Exception as e:
			try:
				await self._handle_request_exception(e)
			except Exception:
				log.error("Exception in exception handler", exc_info=True)

	async def _handle_request_exception(self, e):
		# Not really an error, just finish the request
		if isinstance(e, tornado.web.Finish):
			if not self._finished:
				self.finish(*e.args)
			return

		# Fetch more information about this exception
		exc_info = sys.exc_info()

		# Log the exception
		try:
			self.log_exception(*exc_info)
		except Exception:
			log.error("Error in exception logger", exc_info=True)

		# We cannot send an error if something has already been sent,
		# so we just log the exception and are done.
		if self._finished:
			return

		if isinstance(e, tornado.web.HTTPError):
			await self.send_error(e.status_code, exc_info=exc_info)
		else:
			await self.send_error(500, exc_info=exc_info)

	async def send_error(self, status_code=500, **kwargs):
		"""
			Sends a HTTP error to the browser.
		"""
		if self._headers_written:
			log.error("Cannot send error response after headers written")
			if not self._finished:
				try:
					self.finish()
				except Exception:
					log.error("Failed to flush partial response", exc_info=True)
			return

		# Clear any headers that have been set by the handler
		self.clear()

		# Fetch the reason
		reason = kwargs.get("reason")

		# Try to extract the reason from the exception
		try:
			type, exception, traceback = kwargs["exc_info"]
		except KeyError:
			pass
		else:
			if isinstance(exception, tornado.web.HTTPError) and exception.reason:
				reason = exception.reason

		# Set the status code
		self.set_status(status_code, reason=reason)

		# Render the error message
		try:
			await self.write_error(status_code, **kwargs)
		except Exception:
			log.error("Uncaught exception in write_error", exc_info=True)

		# Make sure we are finished now to release the socket
		if not self._finished:
			self.finish()

	async def write_error(self, code, exc_info=None, **kwargs):
		# Translate the HTTP status code
		try:
			message = http.client.responses[code]
		except KeyError:
			message = None

		tb = []

		# Collect more information about the exception if possible.
		if exc_info:
			if self.current_user and isinstance(self.current_user, users.User):
				if self.current_user.is_admin():
					tb += traceback.format_exception(*exc_info)

		await self.render("errors/error.html",
			code=code, message=message, traceback="".join(tb), **kwargs)

	# XSRF Token Stuff

	@property
	def xsrf_token(self):
		raise NotImplementedError("We don't use this any more")

	async def _make_xsrf_token(self):
		if not hasattr(self, "_xsrf_token"):
			version, token, timestamp = self._get_raw_xsrf_token()

			output_version = self.settings.get("xsrf_cookie_version", 2)
			cookie_kwargs  = self.settings.get("xsrf_cookie_kwargs", {})

			mask = os.urandom(4)
			self._xsrf_token = b"|".join(
				[
					b"2",
					binascii.b2a_hex(mask),
					binascii.b2a_hex(tornado.util._websocket_mask(mask, token)),
					tornado.escape.utf8(str(int(timestamp))),
				]
			)

			if version is None:
				if self.current_user and "expires_days" not in cookie_kwargs:
					cookie_kwargs["expires_days"] = 30
				cookie_name = self.settings.get("xsrf_cookie_name", "_xsrf")
				self.set_cookie(cookie_name, self._xsrf_token, **cookie_kwargs)

		return self._xsrf_token

	async def xsrf_form_html(self):
		# Fetch the token
		xsrf_token = await self._make_xsrf_token()

		# Escape the token
		xsrf_token = tornado.escape.xhtml_escape(xsrf_token)

		return "<input type=\"hidden\" name=\"_xsrf\" value=\"%s\" />" % xsrf_token

	# Typed Arguments

	def get_argument_bool(self, name):
		arg = self.get_argument(name, default=None)

		if arg:
			return arg.lower() in ("on", "true", "yes", "1")

		return False

	def get_argument_int(self, *args, **kwargs):
		arg = self.get_argument(*args, **kwargs)

		# Return nothing
		if not arg:
			return None

		try:
			return int(arg)
		except (TypeError, ValueError):
			raise tornado.web.HTTPError(400, "%s is not an integer" % arg)

	def get_argument_float(self, *args, **kwargs):
		arg = self.get_argument(*args, **kwargs)

		# Return nothing
		if not arg:
			return None

		try:
			return float(arg)
		except (TypeError, ValueError):
			raise tornado.web.HTTPError(400, "%s is not an float" % arg)

	async def get_argument_builder(self, *args, **kwargs):
		name = self.get_argument(*args, **kwargs)

		if name:
			return await self.backend.builders.get_by_name(name)

	async def get_argument_distro(self, *args, **kwargs):
		slug = self.get_argument(*args, **kwargs)

		if slug:
			return await self.backend.distros.get_by_slug(slug)

	async def get_argument_repo(self, *args, distro=None, user=None, **kwargs):
		if distro is None:
			raise ValueError("Distro required")

		# Fetch the slug of the repository
		slug = self.get_argument(*args, **kwargs)

		# Fetch the user repository
		if user:
			return await user.get_repo(slug=slug, distro=distro)

		# Or fetch it from the distribution
		return await distro.get_repo(slug=slug)

	# Uploads

	async def _get_upload(self, uuid):
		# Fetch the upload
		upload = await self.backend.uploads.get_by_uuid(uuid)

		# Check permissions
		if upload and not upload.has_perm(self.current_user):
			raise tornado.web.HTTPError(403, "%s has no permissions for upload %s" % (self.current_user, upload))

		return upload

	async def get_argument_upload(self, *args, **kwargs):
		"""
			Returns an upload
		"""
		uuid = self.get_argument(*args, **kwargs)

		if uuid:
			return await self._get_upload(uuid)

	async def get_argument_uploads(self, *args, **kwargs):
		"""
			Returns a list of uploads
		"""
		uuids = self.get_arguments(*args, **kwargs)

		# Return all uploads
		return [await self._get_upload(uuid) for uuid in uuids]

	async def get_argument_user(self, *args, **kwargs):
		name = self.get_argument(*args, **kwargs)

		if name:
			return await self.backend.users.get_by_name(name)


# XXX TODO
BackendMixin = BaseHandler

class APIError(tornado.web.HTTPError):
	"""
		Raised if there has been an error in the API
	"""
	def __init__(self, code, message):
		super().__init__(400, message)

		self.code = code
		self.message = message

	def __str__(self):
		return self.message


class APIMixin(KerberosAuthMixin):
	# Generally do not permit users to authenticate against the API
	allow_users = False

	# Allow builders to authenticate?
	allow_builders = True

	# Do not perform any XSRF cookie validation on API calls
	def check_xsrf_cookie(self):
		pass

	async def get_current_user(self):
		"""
			Authenticates a user or builder
		"""
		# Fetch the Kerberos ticket
		principal = self.get_authenticated_user()

		# Return nothing if we did not receive any credentials
		if not principal:
			return

		logging.debug("Searching for principal %s..." % principal)

		# Strip the realm
		principal, delimiter, realm = principal.partition("@")

		# Return any builders
		if self.allow_builders and principal.startswith("host/"):
			hostname = principal.removeprefix("host/")

			return await self.backend.builders.get_by_name(hostname)

		# Return any users
		if self.allow_users:
			return await self.backend.users.get_by_name(principal)

	async def get_user_locale(self):
		return self.get_browser_locale()

	def get_compression_options(self):
		# Enable maximum compression
		return {
			"compression_level" : 9,
			"mem_level" : 9,
		}

	async def write_error(self, code, exc_info):
		"""
			Sends a JSON-encoded error message
		"""
		type, error, traceback = exc_info

		# We only handle API errors here
		if not isinstance(error, APIError):
			return await super().write_error(code, exc_info)

		# We send errors as 200
		self.set_status(200)

		self.finish({
			"error" : {
				"code"    : error.code,
				"message" : error.message,
			},
		})

	def _decode_json_message(self, message):
		# Decode JSON message
		try:
			message = json.loads(message)

		except json.DecodeError as e:
			log.error("Could not decode JSON message", exc_info=True)
			raise e

		# Log message
		log.debug("Received message:")
		log.debug("%s" % json.dumps(message, indent=4))

		return message


class JinjaTemplateLoader(tornado.template.BaseLoader):
	"""
		Wraps around Jinja to allow using it as template engine
	"""
	def __init__(self, env):
		super().__init__()

		# Store the environment
		self.env = env

	def resolve_path(self, name, parent_path=None):
		"""
			A function to convert any untrusted, relative paths into absolute.

			We will leave this problem for Jinja.
		"""
		return self.env.join_path(name, parent_path)

	def _create_template(self, name):
		return self.env.get_template(name=name)


# An alias for Tornado's authentication decorator
authenticated = tornado.web.authenticated

def negotiate(method):
	"""
		Requires clients to use SPNEGO
	"""
	@functools.wraps(method)
	async def wrapper(self, *args, **kwargs):
		if not self.current_user:
			# Send the Negotiate header
			self.add_header("WWW-Authenticate", "Negotiate")

			# Respond with 401
			self.set_status(401)
			self.finish()

			return None

		# Call the wrapped method
		result = method(self, *args, **kwargs)

		# Await it if it is a coroutine
		if asyncio.iscoroutine(result):
			return await result

	return wrapper

class ratelimit(object):
	"""
		A decorator class which limits how often a function can be called
	"""
	def __init__(self, **kwargs):
		self.kwargs = kwargs

	def __call__(self, method):
		@functools.wraps(method)
		async def wrapper(handler, *args, **kwargs):
			# Pass the request to the rate limiter and get a request object
			req = handler.backend.ratelimiter(handler.request, handler, **self.kwargs)

			# If the rate limit has been reached, we won't allow
			# processing the request and therefore send HTTP error code 429.
			if await req.is_ratelimited():
				raise tornado.web.HTTPError(429, "Rate limit exceeded")

			# Call the wrapped method
			result = method(handler, *args, **kwargs)

			# Await it if it is a coroutine
			if asyncio.iscoroutine(result):
				return await result

			# Return the result
			return result

		return wrapper


class AdminHandler(BaseHandler):
	"""
		An extension of the base handler that can only be called by an admin
	"""
	@authenticated
	async def prepare(self):
		# Fail if we don't have admin right
		if not self.current_user.is_admin():
			raise tornado.web.HTTPError(403, "admin rights required")


class NoAuthMixin:
	"""
		Add this to skip the process of authentication.

		This is only useful to speed up a request when never authentication is required.
	"""
	async def get_session(self):
		pass
