###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import tornado.web

from . import base

class ShowHandler(base.BaseHandler):
	async def get(self, slug, name):
		# Fetch the distribution
		distro = await self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro %s" % slug)

		# Fetch the monitoring
		monitoring = await self.backend.monitorings.get_by_distro_and_name(distro, name)
		if not monitoring:
			raise tornado.web.HTTPError(404, "Could not find monitoring for %s in %s" % (name, distro))

		await self.render("monitorings/show.html", monitoring=monitoring)


class CreateHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, slug, name):
		# Fetch the distribution
		distro = self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro %s" % slug)

		# Fetch the monitoring
		monitoring = self.backend.monitorings.get_by_distro_and_name(distro, name)
		if monitoring:
			raise tornado.web.HTTPError(400, "Monitoring for %s in %s already exists" % (name, distro))

		# Check permissions
		if not self.current_user.is_admin():
			raise tornado.web.HTTPError(403)

		# Fetch the search query
		q = self.get_argument("q", name)

		# Search for projects
		projects = await self.backend.monitorings.search(q)

		self.render("monitorings/edit.html", monitoring=None, distro=distro, name=name,
			projects=projects)

	@base.authenticated
	async def post(self, slug, name):
		# Fetch the distribution
		distro = self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro %s" % slug)

		# Fetch the monitoring
		monitoring = self.backend.monitorings.get_by_distro_and_name(distro, name)
		if monitoring:
			raise tornado.web.HTTPError(400, "Monitoring for %s in %s already exists" % (name, distro))

		# Check permissions
		if not self.current_user.is_admin():
			raise tornado.web.HTTPError(403)

		# Collect parameters
		project_id    = self.get_argument_int("project_id")
		follow        = self.get_argument("follow")
		create_builds = self.get_argument_bool("create_builds")

		with self.db.transaction():
			try:
				# Create a new monitoring
				monitoring = await self.backend.monitorings.create(distro, name,
					created_by=self.current_user, project_id=project_id,
					follow=follow, create_builds=create_builds,
				)

			except ValueError as e:
				raise tornado.web.HTTPError(400, "%s" % e) from e

		# Perform check immediately
		with self.db.transaction():
			await monitoring.check()

		# Redirect to the newly created monitoring
		self.redirect(monitoring.url)


class EditHandler(base.BaseHandler):
	@base.authenticated
	def get(self, slug, name):
		# Fetch the distribution
		distro = self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro %s" % slug)

		# Fetch the monitoring
		monitoring = self.backend.monitorings.get_by_distro_and_name(distro, name)
		if not monitoring:
			raise tornado.web.HTTPError(404, "Could not find monitoring for %s in %s" % (name, distro))

		# Check permissions
		if not monitoring.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		self.render("monitorings/edit.html", monitoring=monitoring, distro=distro, name=name)

	@base.authenticated
	def post(self, slug, name):
		# Fetch the distribution
		distro = self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro %s" % slug)

		# Fetch the monitoring
		monitoring = self.backend.monitorings.get_by_distro_and_name(distro, name)
		if not monitoring:
			raise tornado.web.HTTPError(404, "Could not find monitoring for %s in %s" % (name, distro))

		# Check permissions
		if not monitoring.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		with self.db.transaction():
			monitoring.create_builds = self.get_argument_bool("create_builds")

		self.redirect(monitoring.url)


class DeleteHandler(base.BaseHandler):
	@base.authenticated
	def get(self, slug, name):
		# Fetch the distribution
		distro = self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro %s" % slug)

		# Fetch the monitoring
		monitoring = self.backend.monitorings.get_by_distro_and_name(distro, name)
		if not monitoring:
			raise tornado.web.HTTPError(404, "Could not find monitoring for %s in %s" % (name, distro))

		# Check permissions
		if not monitoring.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		self.render("monitorings/delete.html", monitoring=monitoring)

	@base.authenticated
	async def post(self, slug, name):
		# Fetch the distribution
		distro = self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro %s" % slug)

		# Fetch the monitoring
		monitoring = self.backend.monitorings.get_by_distro_and_name(distro, name)
		if not monitoring:
			raise tornado.web.HTTPError(404, "Could not find monitoring for %s in %s" % (name, distro))

		# Check permissions
		if not monitoring.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		with self.db.transaction():
			await monitoring.delete(self.current_user)

		self.redirect("/packages/%s" % monitoring.name)


class CheckHandler(base.BaseHandler):
	@base.authenticated
	async def post(self, slug, name):
		# Fetch the distribution
		distro = self.backend.distros.get_by_slug(slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro %s" % slug)

		# Fetch the monitoring
		monitoring = self.backend.monitorings.get_by_distro_and_name(distro, name)
		if not monitoring:
			raise tornado.web.HTTPError(404, "Could not find monitoring for %s in %s" % (name, distro))

		# Check permissions
		if not monitoring.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Perform check (it is already starting its own transaction)
		await monitoring.check()

		# Redirect back
		self.redirect(monitoring.url)
