#!/usr/bin/python3

import logging
import tornado.web

from . import base

# Setup logging
log = logging.getLogger("pbs.web.auth")

class LoginHandler(base.KerberosAuthMixin, base.BaseHandler):
	async def get(self, username=None, failed=False):
		if self.current_user:
			raise tornado.web.HTTPError(403, "Already logged in")

		await self.render("login.html", username=username, failed=failed)

	@base.ratelimit(limit=10, minutes=5)
	async def post(self):
		# Fetch credentials
		username = self.get_argument("username")
		password = self.get_argument("password")

		# Try to authenticate the user
		if not self._auth_with_credentials(username, password):
			return await self.get(username=username, failed=True)

		# If the authentication was successful, we create a new session
		async with await self.db.transaction():
			# Fetch the authenticated user
			user = await self.backend.users.get_by_name(username)
			if not user:
				raise tornado.web.HTTPError(500, "Could not find user %s" % username)

			# Create a new session
			session = await self.backend.sessions.create(user,
				self.current_address, user_agent=self.user_agent)

		# Send the session cookie to the browser
		self.set_cookie("session_id", session.session_id,
			secure=True, expires=session.expires_at)

		# If there is "next" given, we redirect the user accordingly
		next = self.get_argument("next", None)

		# Redirect the user
		self.redirect(next or "/")


class LogoutHandler(base.BaseHandler):
	@base.authenticated
	async def get(self):
		# Destroy the user's session.
		async with await self.db.transaction():
			# Fetch the session
			session = await self.get_session()

			# Destroy the session
			if session:
				session.logout()

		# Remove the session cookie
		self.clear_cookie("session_id")

		# Redirect the user to the front page.
		self.redirect("/")
