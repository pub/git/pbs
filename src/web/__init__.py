#!/usr/bin/python
# encoding: utf-8

import logging
import tornado.locale
import tornado.web

from .. import Backend
from ..constants import *

# Import all handlers
from . import handlers
from . import auth
from . import builders
from . import builds
from . import debuginfo
from . import distributions
from . import errors
from . import jobs
from . import mirrors
from . import monitorings
from . import packages
from . import registry
from . import repos
from . import search
from . import sources
from . import uploads
from . import users

class Application(tornado.web.Application):
	def __init__(self, **kwargs):
		settings = dict(
			login_url = "/login",
			template_path = TEMPLATESDIR,
			static_path = STATICDIR,
			xsrf_cookies = True,
			xsrf_cookie_kwargs = {
				"secure" : True,
			},

			# WebSocket
			websocket_ping_interval = 15,
			websocket_ping_timeout  = 60,
		)
		settings.update(kwargs)

		# Load translations.
		tornado.locale.load_gettext_translations(LOCALEDIR, PACKAGE_NAME)

		tornado.web.Application.__init__(self, [
			# Entry site that lead the user to index
			(r"/", handlers.IndexHandler),

			# Authentication
			(r"/login", auth.LoginHandler),
			(r"/logout", auth.LogoutHandler),

			# Users
			(r"/users", users.IndexHandler),
			(r"/users/(\w+)", users.ShowHandler),
			(r"/users/(\w+)/delete", users.DeleteHandler),
			(r"/users/(\w+)/edit", users.EditHandler),
			(r"/users/push/subscribe", users.PushSubscribeHandler),

			# User Repositories
			(r"/users/(\w+)/repos/create", repos.CreateCustomHandler),
			(r"/users/(?P<user_slug>\w+)/repos/(?P<distro_slug>[A-Za-z0-9\-\.]+)/(?P<repo_slug>[A-Za-z0-9\-]+)",
				repos.ShowHandler),
			(r"/users/(?P<user_slug>\w+)/repos/(?P<distro_slug>[A-Za-z0-9\-\.]+)/(?P<repo_slug>[A-Za-z0-9\-]+)\.repo",
				repos.ConfigHandler),
			(r"/users/(?P<user_slug>\w+)/repos/(?P<distro_slug>[A-Za-z0-9\-\.]+)/(?P<repo_slug>[A-Za-z0-9\-]+)/builds",
				repos.BuildsHandler),
			(r"/users/(?P<user_slug>\w+)/repos/(?P<distro_slug>[A-Za-z0-9\-\.]+)/(?P<repo_slug>[A-Za-z0-9\-]+)/delete",
				repos.DeleteHandler),
			(r"/users/(?P<user_slug>\w+)/repos/(?P<distro_slug>[A-Za-z0-9\-\.]+)/(?P<repo_slug>[A-Za-z0-9\-]+)/edit",
				repos.EditHandler),
			(r"/users/(?P<user_slug>\w+)/repos/(?P<distro_slug>[A-Za-z0-9\-\.]+)/(?P<repo_slug>[A-Za-z0-9\-]+)/mirrorlist",
				repos.MirrorlistHandler),
			(r"/users/(?P<user_slug>\w+)/repos/(?P<distro_slug>[A-Za-z0-9\-\.]+)/(?P<repo_slug>[A-Za-z0-9\-]+)/sources/(?P<source_slug>[A-Za-z0-9\-]+)",
				sources.ShowHandler),
			(r"/users/(?P<user_slug>\w+)/repos/(?P<distro_slug>[A-Za-z0-9\-\.]+)/(?P<repo_slug>[A-Za-z0-9\-]+)/sources/(?P<source_slug>[A-Za-z0-9\-]+)/commits/(?P<commit_slug>[a-f0-9]{40})",
				sources.ShowCommitHandler),

			# Packages
			(r"/packages", packages.IndexHandler),
			(r"/packages/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})", packages.ShowHandler),
			(r"/packages/([\w\-\+]+)", packages.NameHandler),
			(r"/packages/([\w\-\+]+)/builds", packages.NameBuildsHandler),
			(r"/packages/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/download(.*)",
				packages.FileDownloadHandler),
			(r"/packages/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/view(.*)",
				packages.FileViewHandler),

			# Builds
			(r"/builds", builds.IndexHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})", builds.ShowHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/approve", builds.ApproveHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/bug", builds.BugHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/clone", builds.CloneHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/comment", builds.CommentHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/delete", builds.DeleteHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/repos/add", builds.ReposAddHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/repos/remove", builds.ReposRemoveHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/watch", builds.WatchHandler),
			(r"/builds/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/unwatch", builds.UnwatchHandler),

			(r"/api/v1/builds", builds.APIv1IndexHandler),

			# Build Groups
			(r"/builds/groups/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})", builds.GroupShowHandler),

			# Jobs
			(r"/jobs", jobs.IndexHandler),
			(r"/jobs/queue", jobs.QueueHandler),
			(r"/jobs/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/abort", jobs.AbortHandler),
			(r"/jobs/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/log", jobs.LogHandler),
			(r"/jobs/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/retry", jobs.RetryHandler),
			(r"/api/v1/jobs/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/crashed",
				jobs.APIv1CrashedHandler),
			(r"/api/v1/jobs/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/finished",
				jobs.APIv1FinishedHandler),
			(r"/api/v1/jobs/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})/log/stream",
				jobs.APIv1LogStreamHandler),

			# Builders
			(r"/builders", builders.IndexHandler),
			(r"/builders/create", builders.CreateHandler),
			(r"/builders/([A-Za-z0-9\-\.]+)", builders.ShowHandler),
			(r"/builders/([A-Za-z0-9\-\.]+)/delete", builders.DeleteHandler),
			(r"/builders/([A-Za-z0-9\-\.]+)/edit", builders.EditHandler),
			(r"/builders/([A-Za-z0-9\-\.]+)/start", builders.StartHandler),
			(r"/builders/([A-Za-z0-9\-\.]+)/stats", builders.StatsHandler),
			(r"/builders/([A-Za-z0-9\-\.]+)/stop", builders.StopHandler),
			(r"/api/v1/builders/control", builders.APIv1ControlHandler),

			# Distributions
			(r"/distros", distributions.IndexHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)", distributions.ShowHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/edit", distributions.EditHandler),
			(r"/distros/(?P<distro_slug>[A-Za-z0-9\-\.]+)/repos/(?P<repo_slug>[A-Za-z0-9\-]+)",
				repos.ShowHandler),
			(r"/distros/(?P<distro_slug>[A-Za-z0-9\-\.]+)/repos/(?P<repo_slug>[A-Za-z0-9\-]+)\.repo",
				repos.ConfigHandler),
			(r"/distros/(?P<distro_slug>[A-Za-z0-9\-\.]+)/repos/(?P<repo_slug>[A-Za-z0-9\-]+)/builds",
				repos.BuildsHandler),
			(r"/distros/(?P<distro_slug>[A-Za-z0-9\-\.]+)/repos/(?P<repo_slug>[A-Za-z0-9\-]+)/delete",
				repos.DeleteHandler),
			(r"/distros/(?P<distro_slug>[A-Za-z0-9\-\.]+)/repos/(?P<repo_slug>[A-Za-z0-9\-]+)/edit",
				repos.EditHandler),
			(r"/distros/(?P<distro_slug>[A-Za-z0-9\-\.]+)/repos/(?P<repo_slug>[A-Za-z0-9\-]+)/mirrorlist",
				repos.MirrorlistHandler),
			(r"/distros/(?P<distro_slug>[A-Za-z0-9\-\.]+)/repos/(?P<repo_slug>[A-Za-z0-9\-]+)/sources/(?P<source_slug>[A-Za-z0-9\-]+)",
				sources.ShowHandler),
			(r"/distros/(?P<distro_slug>[A-Za-z0-9\-\.]+)/repos/(?P<repo_slug>[A-Za-z0-9\-]+)/sources/(?P<source_slug>[A-Za-z0-9\-]+)/commits/(?P<commit_slug>[a-f0-9]{40})",
				sources.ShowCommitHandler),

			# Distro Monitorings
			(r"/distros/([A-Za-z0-9\-\.]+)/monitorings/([\w\-_]+)", monitorings.ShowHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/monitorings/([\w\-_]+)/check", monitorings.CheckHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/monitorings/([\w\-_]+)/create", monitorings.CreateHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/monitorings/([\w\-_]+)/delete", monitorings.DeleteHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/monitorings/([\w\-_]+)/edit", monitorings.EditHandler),

			# Distro Releases
			(r"/distros/([A-Za-z0-9\-\.]+)/releases", distributions.ReleasesIndexHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/releases/create", distributions.ReleasesCreateHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/releases/([\w\-_]+)", distributions.ReleasesShowHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/releases/([\w\-_]+)/delete", distributions.ReleasesDeleteHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/releases/([\w\-_]+)/edit", distributions.ReleasesEditHandler),
			(r"/distros/([A-Za-z0-9\-\.]+)/releases/([\w\-_]+)/publish", distributions.ReleasesPublishHandler),

			# Downloads
			(r"/downloads/(.*)", mirrors.DownloadsHandler),

			# Mirrors
			(r"/mirrors",					 mirrors.IndexHandler),
			(r"/mirrors/create",             mirrors.CreateHandler),
			(r"/mirrors/([\w\-\.]+)",		 mirrors.ShowHandler),
			(r"/mirrors/([\w\-\.]+)/check",	 mirrors.CheckHandler),
			(r"/mirrors/([\w\-\.]+)/delete", mirrors.DeleteHandler),
			(r"/mirrors/([\w\-\.]+)/edit",	 mirrors.EditHandler),

			# Search
			(r"/search", search.SearchHandler),

			# Log
			(r"/log", handlers.LogHandler),

			# Debuginfo
			(r"/buildid/([0-9a-f]{40})/debuginfo", debuginfo.DebugInfoHandler),

			# Repositories
			(r"/api/v1/repos/([\w\d\-]+)", repos.APIv1IndexHandler),
			(r"/api/v1/repos/([\w\d\-]+)/([\w\d\-]+)", repos.APIv1ShowHandler),

			# Uploads
			(r"/api/v1/uploads", uploads.APIv1IndexHandler),
			(r"/api/v1/uploads/([\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12})",
				uploads.APIv1DetailHandler),
		], default_handler_class=errors.Error404Handler, **settings)

		# Add the registry
		self.add_handlers("registry.pakfire.ipfire.org", [
			# Redirect anyone who is lost
			(r"/", tornado.web.RedirectHandler, { "url" : "https://pakfire.ipfire.org/" }),

			# Version 2
			(r"/v2/", registry.IndexHandler),

			# Manifests
			(r"/v2/([a-z0-9]+(?:(?:\.|_|__|-+)[a-z0-9]+)*(?:\/[a-z0-9]+(?:(?:\.|_|__|-+)[a-z0-9]+)*)*)/manifests/([a-zA-Z0-9_][a-zA-Z0-9._-]{0,127})",
				registry.ManifestLabelHandler),

			# Blobs & Manifests referenced by digest
			(r"/v2/([a-z0-9]+(?:(?:\.|_|__|-+)[a-z0-9]+)*(?:\/[a-z0-9]+(?:(?:\.|_|__|-+)[a-z0-9]+)*)*)/manifests/(sha256:[a-f0-9]{64})",
				registry.ManifestHandler),
			(r"/v2/([a-z0-9]+(?:(?:\.|_|__|-+)[a-z0-9]+)*(?:\/[a-z0-9]+(?:(?:\.|_|__|-+)[a-z0-9]+)*)*)/blobs/(sha256:[a-f0-9]{64})",
				registry.ManifestHandler),

			# Tags
			(r"/v2/([a-z0-9]+(?:(?:\.|_|__|-+)[a-z0-9]+)*(?:\/[a-z0-9]+(?:(?:\.|_|__|-+)[a-z0-9]+)*)*)/tags/list",
				registry.TagsHandler),

			# Catch anything else
			(r"/v2/.*", registry.NotFoundHandler),
		])

		# Launch backend & background tasks
		self.backend = Backend("/etc/pakfire/pbs.conf")
		self.backend.launch_background_tasks()

		logging.info("Successfully initialied application")
