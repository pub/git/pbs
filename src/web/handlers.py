#!/usr/bin/python

import tornado.web

from . import base

class IndexHandler(base.BaseHandler):
	async def get(self):
		# Fetch all running jobs
		running_jobs  = self.backend.jobs.get_running()

		# Fetch finished jobs
		finished_jobs = self.backend.jobs.get_finished(limit=10)

		# Concactenate all jobs
		jobs = [job async for job in running_jobs] + [job async for job in finished_jobs]

		await self.render("index.html", jobs=jobs, queue=self.backend.jobs.queue)


class LogHandler(base.BaseHandler):
	async def get(self):
		kwargs = {
			# Pagination
			"offset"   : self.get_argument_int("offset", None) or 0,
			"limit"    : self.get_argument_int("limit", None) or 50,

			# Priority
			"priority" : self.get_argument_int("priority", None) or 5,

			# Filters
			"builder"  : await self.get_argument_builder("builder", None),
			"user"     : await self.get_argument_user("user", None),
		}

		await self.render("log.html", **kwargs)
