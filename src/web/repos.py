#!/usr/bin/python3
###############################################################################
#                                                                             #
# Pakfire - The IPFire package management system                              #
# Copyright (C) 2022 Pakfire development team                                 #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import configparser
import os.path
import pakfire
import tornado.web
import urllib.parse

from . import base

class APIv1IndexHandler(base.APIMixin, base.BaseHandler):
	# Only users can have repositories
	allow_users = True
	allow_builders = False

	@base.negotiate
	async def get(self, distro_slug):
		# Fetch distro
		distro = self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro '%s'" % distro_slug)

		# Fetch all repositories
		try:
			repos = self.current_user.repos[distro]
		except KeyError:
			repos = []

		self.finish({
			"repos" : [repo.to_json() for repo in repos],
		})

	@base.negotiate
	async def post(self, distro_slug):
		with self.db.transaction():
			# Fetch distro
			distro = self.backend.distros.get_by_slug(distro_slug)
			if not distro:
				raise tornado.web.HTTPError(404, "Could not find distro '%s'" % distro_slug)

			# Fetch name
			name = self.get_argument("name")

			# Create a new repository
			repo = await self.backend.repos.create(distro=distro,
				name=name, owner=self.current_user)

			# Set description
			repo.description = self.get_argument("description", None)

		# Return the result
		self.finish(repo.to_json())


class APIv1ShowHandler(base.APIMixin, base.BaseHandler):
	# Only users can have repositories
	allow_users = True
	allow_builders = False

	def _get_repo(self, distro_slug, repo_slug):
		# Fetch distro
		distro = self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro '%s'" % distro_slug)

		# Fetch repository
		repo = self.current_user.get_repo(distro, repo_slug)
		if not repo:
			raise tornado.web.HTTPError(404, "Could not find repository '%s" % repo_slug)

		return distro, repo

	@base.negotiate
	async def get(self, distro_slug, name):
		with self.db.transaction():
			distro, repo = self._get_repo(distro_slug, name)

		self.finish(repo.to_json())

	@base.negotiate
	async def delete(self, distro_slug, name):
		with self.db.transaction():
			distro, repo = self._get_repo(distro_slug, name)

			# XXX check permissions

			# Delete the repository
			await repo.delete(self.current_user)

		# Send a positive response
		self.finish({})


class BaseHandler(base.BaseHandler):
	async def _get_repo(self, distro_slug, repo_slug, user_slug=None):
		user = None

		# Find the user
		if user_slug:
			user = await self.backend.users.get_by_name(user_slug)
			if not user:
				raise tornado.web.HTTPError(404, "Could not find user: %s" % user_slug)

		# Find the distribution
		distro = await self.backend.distros.get_by_slug(distro_slug)
		if not distro:
			raise tornado.web.HTTPError(404, "Could not find distro: %s" % distro_slug)

		# Find the repository
		if user:
			repo = await user.get_repo(distro, repo_slug)
		else:
			repo = await distro.get_repo(repo_slug)
		if not repo:
			raise tornado.web.HTTPError(404, "Could not find repo: %s" % repo_slug)

		return repo


class ShowHandler(BaseHandler):
	async def get(self, **kwargs):
		# Fetch the repository
		repo = await self._get_repo(**kwargs)

		await self.render("repos/show.html", repo=repo, distro=repo.distro)


class BuildsHandler(BaseHandler):
	async def get(self, **kwargs):
		# Fetch the repository
		repo = await self._get_repo(**kwargs)

		await self.render("repos/builds.html", repo=repo, distro=repo.distro)


class CreateCustomHandler(BaseHandler):
	@base.authenticated
	async def get(self, user_slug):
		user = await self.backend.users.get_by_name(user_slug)
		if not user:
			raise tornado.web.HTTPError(404, "Could not find user: %s" % user_slug)

		# Check for permissions
		if not user.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("repos/create-custom.html", user=user, distros=self.backend.distros)

	@base.authenticated
	async def post(self, user_slug):
		user = await self.backend.users.get_by_name(user_slug)
		if not user:
			raise tornado.web.HTTPError(404, "Could not find user: %s" % user_slug)

		# Check for permissions
		if not user.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Create the repository
		async with await self.db.transaction():
			repo = await self.backend.repos.create(
				distro = await self.get_argument_distro("distro"),
				name   = self.get_argument("name"),
				owner  = user,
			)

			# Write repository metadata
			await repo.write()

		self.redirect(repo.url)


class ConfigHandler(BaseHandler):
	async def get(self, **kwargs):
		# Fetch the repository
		repo = await self._get_repo(**kwargs)

		# Generate configuration
		config = configparser.ConfigParser(interpolation=None)
		repo.write_config(config)

		# This is plain text
		self.set_header("Content-Type", "text/plain")

		# Send it to the client
		config.write(self)


class EditHandler(BaseHandler):
	@base.authenticated
	async def get(self, **kwargs):
		# Fetch the repository
		repo = await self._get_repo(**kwargs)

		# Check for permissions
		if not repo.has_perm(self.current_user):
			raise tornado.web.HTTPError(403, "%s cannot edit this repository" % self.current_user)

		await self.render("repos/edit.html", repo=repo)

	@base.authenticated
	async def post(self, **kwargs):
		# Fetch the repository
		repo = await self._get_repo(**kwargs)

		# Check for permissions
		if not repo.has_perm(self.current_user):
			raise tornado.web.HTTPError(403, "%s cannot edit this repository" % self.current_user)

		async with await self.db.transaction():
			repo.name        = self.get_argument("name")
			repo.description = self.get_argument("description", None)
			repo.priority    = self.get_argument_int("priority", None)

			# Attributes for custom repositories
			if repo.owner:
				repo.listed = self.get_argument_bool("listed")

			# Admin settings
			if self.current_user.is_admin():
				repo.mirrored = self.get_argument_bool("mirrored")

		self.redirect(repo.url)


class DeleteHandler(BaseHandler):
	@base.authenticated
	async def get(self, **kwargs):
		# Fetch the repository
		repo = await self._get_repo(**kwargs)

		# Check for permissions
		if not repo.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("repos/delete.html", repo=repo)

	@base.authenticated
	async def post(self, **kwargs):
		# Fetch the repository
		repo = await self._get_repo(**kwargs)

		# Check for permissions
		if not repo.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		async with await self.db.transaction():
			await repo.delete(deleted_by=self.current_user)

		if repo.owner:
			self.redirect("/users/%s" % repo.owner.name)
		else:
			self.redirect("/distros/%s" % repo.distro.slug)


class MirrorlistHandler(base.NoAuthMixin, BaseHandler):
	async def get(self, **kwargs):
		# Fetch the repository
		repo = await self._get_repo(**kwargs)

		# Send nothing if repository isn't supposed to be mirrored
		if not repo.mirrored:
			raise tornado.web.HTTPError(404)

		# Fetch architecture
		arch = self.get_argument("arch")

		# Check if we support the architecture
		if not arch in pakfire.supported_arches():
			raise tornado.web.HTTPError(400, "Unsupported architecture: %s" % arch)

		self.finish({
			"type"    : "mirrorlist",
			"version" : 1,
			"mirrors" : [
				{
					"url"      : mirror.make_url("repos", repo.path, arch),
					"location" : mirror.country_code,
				}
				for mirror in await self.backend.mirrors.get_mirrors_for_address(self.current_address)
			],
		})
