#!/usr/bin/python

import logging
import tornado.web

from . import base

# Setup logging
log = logging.getLogger("pbs.web.builders")

class APIv1ControlHandler(base.APIMixin, base.BackendMixin, tornado.websocket.WebSocketHandler):
	@base.negotiate
	async def prepare(self):
		# This is here to require authentication before
		# the websocket connection is being negotiated.

		# Fetch the builder
		self.builder = await self.get_current_user()

	async def open(self):
		# The builder has opened a new connection
		self.builder.connected(self, address=self.current_address)

		# Update the builder information
		async with await self.db.transaction():
			# Update arch
			self.builder.arch       = self.get_argument("arch")

			# Update system information
			self.builder.sys_vendor = self.get_argument("sys_vendor", None)
			self.builder.sys_name   = self.get_argument("sys_name", None)

			# Update CPU information
			self.builder.cpu_model  = self.get_argument("cpu_model", None)
			self.builder.cpu_count  = self.get_argument_int("cpu_count", None)

			# Update Pakfire & OS information
			self.builder.version    = self.get_argument("version")
			self.builder.os_name    = self.get_argument("os_name", None)

		# Manually perform a database commit because this won't happen
		# until the builder disconnects again.
		await self.db.commit()

		# After the builder has connected, try to dispatch some jobs
		await self.backend.jobs.queue.dispatch()

	def on_ping(self, data):
		log.debug("%s has pinged us" % self.builder)

	def on_close(self):
		# Drop the connection to the builder
		self.builder.disconnected()

	async def on_message(self, message):
		# Fetch the builder again, because this is being executed as a separate task
		# and therefore we cannot use the object from the previous session.
		builder = await self.backend.builders.get_by_name(self.builder.name)

		# Decode message
		message = self._decode_json_message(message)

		# Fetch the message type & data
		type = message.get("type")
		data = message.get("data")

		# Fast path for log messages
		if type == "log":
			return self.backend.logstreams.log(message)

		# Handle stats
		elif type == "stats":
			async with await self.db.transaction():
				await builder.log_stats(**data)

		# Log an error and ignore any other messages
		else:
			log.error("Received message of type '%s' which we cannot handle here" % type)


class StatsHandler(base.BaseHandler, tornado.websocket.WebSocketHandler):
	# No authentication required
	async def open(self, name):
		builder = await self.backend.builders.get_by_name(name)
		if not builder:
			raise tornado.web.HTTPError(404, "Could not find builder %s" % name)

		# Register to receive updates
		self.backend.builders.stats.join(builder=builder, connection=self)

		# Initially send the stats that we currently have
		stats = await builder.get_stats()
		if stats:
			await self.submit_stats(stats)

	def on_close(self):
		self.backend.builders.stats.leave(self)

	async def submit_stats(self, stats):
		await self.write_message({
			"cpu_usage"  : stats.cpu_usage,
			"mem_usage"  : stats.mem_usage,
			"swap_usage" : stats.swap_usage,
		})


class IndexHandler(base.BaseHandler):
	async def get(self):
		await self.render("builders/index.html", builders=self.backend.builders)


class ShowHandler(base.BaseHandler):
	async def get(self, hostname):
		builder = await self.backend.builders.get_by_name(hostname)
		if not builder:
			raise tornado.web.HTTPError(404, "Could not find builder %s" % hostname)

		# Fetch status
		args = {
			"is_running"       : await builder.is_running(),
			"is_shutting_down" : await builder.is_shutting_down(),
			"is_shut_down"     : await builder.is_shut_down(),
		}

		await self.render("builders/show.html", builder=builder, **args)


class CreateHandler(base.AdminHandler):
	async def get(self):
		await self.render("builders/create.html")

	async def post(self):
		# Create a new builder
		async with await self.db.transaction():
			builder = await self.backend.builders.create(
				name       = self.get_argument("name"),
				created_by = self.current_user,
			)

		self.redirect("/builders/%s/edit" % builder.name)


class EditHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, name):
		builder = await self.backend.builders.get_by_name(name)
		if not builder:
			raise tornado.web.HTTPError(404, "Builder not found")

		# Check permissions
		if not builder.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("builders/edit.html", builder=builder)

	@base.authenticated
	async def post(self, name):
		builder = await self.backend.builders.get_by_name(name)
		if not builder:
			raise tornado.web.HTTPError(404, "Builder not found: %s" % name)

		# Check permissions
		if not builder.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		async with await self.db.transaction():
			builder.enabled     = self.get_argument_bool("enabled")
			builder.maintenance = self.get_argument_bool("maintenance")
			builder.max_jobs    = self.get_argument_int("max_jobs")

		# Try to dispatch more jobs
		await self.backend.jobs.queue.dispatch()

		self.redirect("/builders/%s" % builder.name)


class DeleteHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, name):
		builder = await self.backend.builders.get_by_name(name)
		if not builder:
			raise tornado.web.HTTPError(404, "Builder not found: %s" % name)

		# Check permissions
		if not builder.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		await self.render("builders/delete.html", builder=builder)

	@base.authenticated
	async def post(self, name):
		builder = await self.backend.builders.get_by_name(name)
		if not builder:
			raise tornado.web.HTTPError(404, "Builder not found: %s" % hostname)

		# Check permissions
		if not builder.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Delete the builder
		async with await self.db.transaction():
			await builder.delete(deleted_by=self.current_user)

		self.redirect("/builders")


class StartHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, name):
		builder = await self.backend.builders.get_by_name(name)
		if not builder:
			raise tornado.web.HTTPError(404, "Builder not found: %s" % name)

		# Check permissions
		if not builder.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Builders must be in maintenance mode
		if not builder.maintenance:
			raise tornado.web.HTTPError(400, "%s is not in maintenance mode" % builder)

		await self.render("builders/start.html", builder=builder)

	@base.authenticated
	async def post(self, name):
		builder = await self.backend.builders.get_by_name(name)
		if not builder:
			raise tornado.web.HTTPError(404, "Builder not found: %s" % name)

		# Check permissions
		if not builder.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Builders must be in maintenance mode
		if not builder.maintenance:
			raise tornado.web.HTTPError(400, "%s is not in maintenance mode" % builder)

		# Start the builder
		try:
			await builder.start(wait=False)

		# XXX what do we do when this fails?
		except:
			raise

		self.redirect("/builders/%s" % builder.name)


class StopHandler(base.BaseHandler):
	@base.authenticated
	async def get(self, name):
		builder = await self.backend.builders.get_by_name(name)
		if not builder:
			raise tornado.web.HTTPError(404, "Builder not found: %s" % name)

		# Check permissions
		if not builder.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Builders must be in maintenance mode
		if not builder.maintenance:
			raise tornado.web.HTTPError(400, "%s is not in maintenance mode" % builder)

		await self.render("builders/stop.html", builder=builder)

	@base.authenticated
	async def post(self, name):
		builder = await self.backend.builders.get_by_name(name)
		if not builder:
			raise tornado.web.HTTPError(404, "Builder not found: %s" % name)

		# Check permissions
		if not builder.has_perm(self.current_user):
			raise tornado.web.HTTPError(403)

		# Builders must be in maintenance mode
		if not builder.maintenance:
			raise tornado.web.HTTPError(400, "%s is not in maintenance mode" % builder)

		# Stop the builder
		try:
			await builder.stop(wait=False)

		# XXX what do we do when this fails?
		except:
			raise

		self.redirect("/builders/%s" % builder.name)
