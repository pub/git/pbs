--
-- PostgreSQL database dump
--

-- Dumped from database version 15.10 (Debian 15.10-0+deb12u1)
-- Dumped by pg_dump version 15.10 (Debian 15.10-0+deb12u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

-- *not* creating schema, since initdb creates it


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: build_bugs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.build_bugs (
    id integer NOT NULL,
    build_id integer NOT NULL,
    bug_id integer NOT NULL,
    added_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    added_by_id integer,
    removed_at timestamp without time zone,
    removed_by_id integer
);


--
-- Name: build_bugs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.build_bugs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: build_bugs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.build_bugs_id_seq OWNED BY public.build_bugs.id;


--
-- Name: build_comments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.build_comments (
    id integer NOT NULL,
    build_id integer NOT NULL,
    user_id integer NOT NULL,
    text text DEFAULT ''::text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    deleted_at timestamp without time zone
);


--
-- Name: build_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.build_groups (
    id integer NOT NULL,
    uuid uuid DEFAULT gen_random_uuid() NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by_id integer,
    deleted_at timestamp without time zone,
    deleted_by_id integer,
    finished_at timestamp without time zone,
    failed boolean DEFAULT false NOT NULL,
    tested_build_id integer
);


--
-- Name: build_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.build_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: build_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.build_groups_id_seq OWNED BY public.build_groups.id;


--
-- Name: build_packages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.build_packages (
    build_id integer NOT NULL,
    package_id integer NOT NULL,
    job_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


--
-- Name: build_points; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.build_points (
    build_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    points integer DEFAULT 0 NOT NULL,
    user_id integer
);


--
-- Name: build_watchers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.build_watchers (
    build_id integer NOT NULL,
    user_id integer NOT NULL,
    added_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at timestamp without time zone
);


--
-- Name: builder_stats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.builder_stats (
    builder_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    cpu_user double precision NOT NULL,
    cpu_nice double precision NOT NULL,
    cpu_system double precision NOT NULL,
    cpu_idle double precision NOT NULL,
    cpu_iowait double precision NOT NULL,
    cpu_irq double precision NOT NULL,
    cpu_softirq double precision NOT NULL,
    cpu_steal double precision NOT NULL,
    cpu_guest double precision NOT NULL,
    cpu_guest_nice double precision NOT NULL,
    loadavg1 double precision NOT NULL,
    loadavg5 double precision NOT NULL,
    loadavg15 double precision NOT NULL,
    mem_total bigint NOT NULL,
    mem_available bigint NOT NULL,
    mem_used bigint NOT NULL,
    mem_free bigint NOT NULL,
    mem_active bigint NOT NULL,
    mem_inactive bigint NOT NULL,
    mem_buffers bigint NOT NULL,
    mem_cached bigint NOT NULL,
    mem_shared bigint NOT NULL,
    swap_total bigint NOT NULL,
    swap_used bigint NOT NULL,
    swap_free bigint NOT NULL
);


--
-- Name: builders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.builders (
    id integer NOT NULL,
    name text NOT NULL,
    description text,
    enabled boolean DEFAULT false NOT NULL,
    loadavg text DEFAULT '0'::character varying NOT NULL,
    maintenance boolean DEFAULT false NOT NULL,
    max_jobs bigint DEFAULT (1)::bigint NOT NULL,
    version text,
    os_name text,
    cpu_model text,
    cpu_count integer DEFAULT 1 NOT NULL,
    host_key_id text,
    arch text,
    instance_id text,
    instance_type text,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by_id integer NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id integer,
    sys_vendor text,
    sys_name text,
    connected_at timestamp without time zone,
    connected_from inet
);


--
-- Name: builders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.builders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: builders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.builders_id_seq OWNED BY public.builders.id;


--
-- Name: builds; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.builds (
    id integer NOT NULL,
    uuid uuid DEFAULT gen_random_uuid() NOT NULL,
    pkg_id integer NOT NULL,
    severity text,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    repo_id integer NOT NULL,
    owner_id integer,
    priority integer DEFAULT 0 NOT NULL,
    finished_at timestamp without time zone,
    failed boolean DEFAULT false NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id integer,
    group_id integer,
    deprecating_build_id integer,
    deprecated_at timestamp without time zone,
    deprecated_by_id integer,
    test boolean DEFAULT false NOT NULL,
    disable_test_builds boolean DEFAULT false NOT NULL,
    points integer DEFAULT 0 NOT NULL,
    commit_id integer
);


--
-- Name: builds_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.builds_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: builds_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.builds_comments_id_seq OWNED BY public.build_comments.id;


--
-- Name: builds_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.builds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: builds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.builds_id_seq OWNED BY public.builds.id;


--
-- Name: cache; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cache (
    key text NOT NULL,
    value bytea NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    expires_at timestamp without time zone
);


--
-- Name: distributions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.distributions (
    id integer NOT NULL,
    name text NOT NULL,
    distro_id text NOT NULL,
    slogan text DEFAULT ''::text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    vendor text DEFAULT ''::text NOT NULL,
    contact text DEFAULT ''::text NOT NULL,
    version_id integer NOT NULL,
    deleted boolean DEFAULT false NOT NULL,
    arches text[] DEFAULT ARRAY[]::text[] NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    custom_config text DEFAULT ''::text NOT NULL,
    codename text DEFAULT ''::text NOT NULL,
    bugzilla_product text DEFAULT ''::text NOT NULL,
    bugzilla_version text DEFAULT ''::text NOT NULL,
    deleted_at timestamp without time zone,
    tag text GENERATED ALWAYS AS ((distro_id || version_id)) STORED
);


--
-- Name: distributions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.distributions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: distributions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.distributions_id_seq OWNED BY public.distributions.id;


--
-- Name: images; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.images (
    id integer NOT NULL,
    release_id integer NOT NULL,
    type text NOT NULL,
    arch text NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by_id integer,
    deleted_at timestamp without time zone,
    deleted_by_id integer,
    path text NOT NULL,
    size bigint NOT NULL,
    checksum_sha3_512 bytea NOT NULL,
    checksum_sha3_256 bytea NOT NULL,
    checksum_blake2b512 bytea NOT NULL,
    checksum_blake2s256 bytea NOT NULL,
    checksum_sha2_512 bytea NOT NULL,
    checksum_sha2_256 bytea NOT NULL
);


--
-- Name: images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.images_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.images_id_seq OWNED BY public.images.id;


--
-- Name: images_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.images_types (
    id integer NOT NULL,
    type text NOT NULL
);


--
-- Name: images_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.images_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: images_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.images_types_id_seq OWNED BY public.images_types.id;


--
-- Name: job_packages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.job_packages (
    job_id integer NOT NULL,
    pkg_id integer NOT NULL
);


--
-- Name: jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.jobs (
    id integer NOT NULL,
    uuid uuid DEFAULT gen_random_uuid() NOT NULL,
    build_id integer NOT NULL,
    arch text NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    started_at timestamp without time zone,
    finished_at timestamp without time zone,
    builder_id integer,
    message text,
    superseeded_by_id integer,
    installcheck_succeeded boolean,
    installcheck_performed_at timestamp without time zone,
    failed boolean DEFAULT false NOT NULL,
    log_path text,
    log_size bigint,
    log_digest_blake2s bytea,
    deleted_at timestamp without time zone,
    deleted_by integer,
    aborted boolean DEFAULT false NOT NULL,
    aborted_by_id integer,
    timeout interval
);


--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.jobs_id_seq OWNED BY public.jobs.id;


--
-- Name: keys; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.keys (
    id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by_id integer,
    deleted_at timestamp without time zone,
    deleted_by_id integer,
    public_key text NOT NULL,
    secret_key text NOT NULL,
    key_id numeric NOT NULL,
    comment text
);


--
-- Name: keys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.keys_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.keys_id_seq OWNED BY public.keys.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.messages (
    id integer NOT NULL,
    message text NOT NULL,
    queued_at timestamp without time zone DEFAULT now() NOT NULL,
    sent_at timestamp without time zone,
    priority integer DEFAULT 0 NOT NULL,
    error_message text
);


--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.messages_id_seq OWNED BY public.messages.id;


--
-- Name: mirror_checks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mirror_checks (
    mirror_id integer NOT NULL,
    checked_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    success boolean DEFAULT false NOT NULL,
    response_time double precision,
    http_status integer,
    last_sync_at timestamp without time zone,
    error text
);


--
-- Name: mirrors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mirrors (
    id integer NOT NULL,
    hostname text NOT NULL,
    path text NOT NULL,
    owner text,
    contact text,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by_id integer NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id integer,
    last_check_success boolean,
    last_check_at timestamp without time zone,
    last_sync_at timestamp without time zone,
    country_code text,
    error text,
    asn integer,
    notes text DEFAULT ''::text NOT NULL,
    addresses_ipv6 inet[] DEFAULT '{}'::inet[] NOT NULL,
    addresses_ipv4 inet[] DEFAULT '{}'::inet[] NOT NULL
);


--
-- Name: mirrors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mirrors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mirrors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mirrors_id_seq OWNED BY public.mirrors.id;


--
-- Name: package_files; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.package_files (
    pkg_id integer NOT NULL,
    path text NOT NULL,
    size bigint NOT NULL,
    config boolean DEFAULT false NOT NULL,
    mode integer NOT NULL,
    uname text NOT NULL,
    gname text NOT NULL,
    ctime timestamp without time zone NOT NULL,
    mtime timestamp without time zone NOT NULL,
    digest_sha2_512 bytea,
    digest_sha2_256 bytea,
    digest_blake2b512 bytea,
    digest_blake2s256 bytea,
    digest_sha3_512 bytea,
    digest_sha3_256 bytea,
    mimetype text,
    capabilities text
);


--
-- Name: packages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.packages (
    id integer NOT NULL,
    name text NOT NULL,
    evr text NOT NULL,
    arch text NOT NULL,
    groups text[] NOT NULL,
    packager text,
    license text NOT NULL,
    url text NOT NULL,
    summary text NOT NULL,
    description text NOT NULL,
    size numeric NOT NULL,
    build_arches text[],
    uuid uuid NOT NULL,
    commit_id integer,
    build_id uuid,
    build_host text NOT NULL,
    build_time timestamp without time zone NOT NULL,
    path text,
    filesize numeric NOT NULL,
    prerequires text[] DEFAULT ARRAY[]::text[] NOT NULL,
    requires text[] DEFAULT ARRAY[]::text[] NOT NULL,
    provides text[] DEFAULT ARRAY[]::text[] NOT NULL,
    obsoletes text[] DEFAULT ARRAY[]::text[] NOT NULL,
    conflicts text[] DEFAULT ARRAY[]::text[] NOT NULL,
    recommends text[] DEFAULT ARRAY[]::text[] NOT NULL,
    suggests text[] DEFAULT ARRAY[]::text[] NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    digest_type text NOT NULL,
    digest bytea NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by integer,
    distro_id integer NOT NULL,
    search tsvector GENERATED ALWAYS AS (((setweight(to_tsvector('simple'::regconfig, name), 'A'::"char") || setweight(to_tsvector('english'::regconfig, summary), 'B'::"char")) || setweight(to_tsvector('english'::regconfig, description), 'C'::"char"))) STORED
);


--
-- Name: packages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.packages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: packages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.packages_id_seq OWNED BY public.packages.id;


--
-- Name: ratelimiter; Type: TABLE; Schema: public; Owner: -
--

CREATE UNLOGGED TABLE public.ratelimiter (
    key text NOT NULL,
    "timestamp" timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    address inet NOT NULL,
    requests integer DEFAULT 1 NOT NULL,
    expires_at timestamp without time zone NOT NULL
);


--
-- Name: relation_sizes; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.relation_sizes AS
 SELECT c.relname AS relation,
    pg_size_pretty(pg_relation_size((c.oid)::regclass)) AS size
   FROM (pg_class c
     LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace)))
  WHERE (n.nspname <> ALL (ARRAY['pg_catalog'::name, 'information_schema'::name]))
  ORDER BY (pg_relation_size((c.oid)::regclass)) DESC;


--
-- Name: release_images; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.release_images (
    id integer NOT NULL,
    release_id integer NOT NULL,
    type_id integer NOT NULL,
    arch text NOT NULL,
    filename text NOT NULL,
    path text
);


--
-- Name: release_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.release_images_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: release_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.release_images_id_seq OWNED BY public.release_images.id;


--
-- Name: release_monitoring_releases; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.release_monitoring_releases (
    id integer NOT NULL,
    monitoring_id integer NOT NULL,
    version text NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    bug_id integer,
    build_id integer,
    diff text,
    repo_id integer,
    log text
);


--
-- Name: release_monitoring_releases_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.release_monitoring_releases_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: release_monitoring_releases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.release_monitoring_releases_id_seq OWNED BY public.release_monitoring_releases.id;


--
-- Name: release_monitorings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.release_monitorings (
    id integer NOT NULL,
    distro_id integer NOT NULL,
    name text NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by_id integer NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id integer,
    project_id integer NOT NULL,
    follow text NOT NULL,
    last_check_at timestamp without time zone,
    create_builds boolean DEFAULT true NOT NULL
);


--
-- Name: release_monitorings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.release_monitorings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: release_monitorings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.release_monitorings_id_seq OWNED BY public.release_monitorings.id;


--
-- Name: releases; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.releases (
    id integer NOT NULL,
    distro_id integer NOT NULL,
    name text NOT NULL,
    slug text NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by_id integer NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id integer,
    published_at timestamp without time zone,
    stable boolean DEFAULT false NOT NULL,
    announcement text
);


--
-- Name: releases_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.releases_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: releases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.releases_id_seq OWNED BY public.releases.id;


--
-- Name: repositories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.repositories (
    id integer NOT NULL,
    name text NOT NULL,
    slug text NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    distro_id integer NOT NULL,
    key_id integer NOT NULL,
    mirrored boolean DEFAULT false NOT NULL,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone,
    priority integer,
    owner_id integer,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    listed boolean DEFAULT true NOT NULL,
    deleted_by integer
);


--
-- Name: repositories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.repositories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: repositories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.repositories_id_seq OWNED BY public.repositories.id;


--
-- Name: repository_builds; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.repository_builds (
    id integer NOT NULL,
    repo_id integer NOT NULL,
    build_id bigint NOT NULL,
    added_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    added_by_id integer,
    removed_at timestamp without time zone,
    removed_by_id integer
);


--
-- Name: repository_builds_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.repository_builds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: repository_builds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.repository_builds_id_seq OWNED BY public.repository_builds.id;


--
-- Name: sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sessions (
    id integer NOT NULL,
    session_id text DEFAULT gen_random_uuid() NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    expires_at timestamp without time zone NOT NULL,
    user_id integer NOT NULL,
    address inet,
    user_agent text
);


--
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sessions_id_seq OWNED BY public.sessions.id;


--
-- Name: source_commit_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.source_commit_jobs (
    id integer NOT NULL,
    commit_id integer NOT NULL,
    action text NOT NULL,
    name text NOT NULL,
    finished_at timestamp without time zone,
    success boolean,
    error text,
    log text
);


--
-- Name: source_commit_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.source_commit_jobs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: source_commit_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.source_commit_jobs_id_seq OWNED BY public.source_commit_jobs.id;


--
-- Name: source_commits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.source_commits (
    id integer NOT NULL,
    source_id integer NOT NULL,
    revision text NOT NULL,
    author text NOT NULL,
    committer text NOT NULL,
    subject text NOT NULL,
    body text NOT NULL,
    date timestamp without time zone NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at timestamp without time zone,
    build_group_id integer,
    finished_at timestamp without time zone
);


--
-- Name: source_commits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.source_commits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: source_commits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.source_commits_id_seq OWNED BY public.source_commits.id;


--
-- Name: sources; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sources (
    id integer NOT NULL,
    name text NOT NULL,
    slug text NOT NULL,
    url text NOT NULL,
    gitweb text,
    revision text,
    branch text NOT NULL,
    last_fetched_at timestamp without time zone,
    repo_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_by_id integer NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id integer
);


--
-- Name: sources_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sources_id_seq OWNED BY public.sources.id;


--
-- Name: uploads; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.uploads (
    id integer NOT NULL,
    uuid uuid DEFAULT gen_random_uuid() NOT NULL,
    user_id integer,
    builder_id integer,
    filename text NOT NULL,
    path text,
    size bigint NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    expires_at timestamp without time zone DEFAULT (CURRENT_TIMESTAMP + '24:00:00'::interval) NOT NULL,
    digest_blake2b512 bytea NOT NULL
);


--
-- Name: uploads_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.uploads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: uploads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.uploads_id_seq OWNED BY public.uploads.id;


--
-- Name: user_push_subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_push_subscriptions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    uuid uuid DEFAULT gen_random_uuid() NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    deleted_at timestamp without time zone,
    user_agent text,
    endpoint text NOT NULL,
    p256dh bytea NOT NULL,
    auth bytea NOT NULL
);


--
-- Name: user_push_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_push_subscriptions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_push_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_push_subscriptions_id_seq OWNED BY public.user_push_subscriptions.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name text NOT NULL,
    deleted_at timestamp without time zone,
    registered_at timestamp without time zone DEFAULT now() NOT NULL,
    admin boolean DEFAULT false NOT NULL,
    storage_quota bigint,
    perms text[] DEFAULT ARRAY[]::text[] NOT NULL,
    _attrs bytea,
    bugzilla_api_key text,
    daily_build_quota interval
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: build_bugs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_bugs ALTER COLUMN id SET DEFAULT nextval('public.build_bugs_id_seq'::regclass);


--
-- Name: build_comments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_comments ALTER COLUMN id SET DEFAULT nextval('public.builds_comments_id_seq'::regclass);


--
-- Name: build_groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_groups ALTER COLUMN id SET DEFAULT nextval('public.build_groups_id_seq'::regclass);


--
-- Name: builders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builders ALTER COLUMN id SET DEFAULT nextval('public.builders_id_seq'::regclass);


--
-- Name: builds id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds ALTER COLUMN id SET DEFAULT nextval('public.builds_id_seq'::regclass);


--
-- Name: distributions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.distributions ALTER COLUMN id SET DEFAULT nextval('public.distributions_id_seq'::regclass);


--
-- Name: images id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images ALTER COLUMN id SET DEFAULT nextval('public.images_id_seq'::regclass);


--
-- Name: images_types id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images_types ALTER COLUMN id SET DEFAULT nextval('public.images_types_id_seq'::regclass);


--
-- Name: jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs ALTER COLUMN id SET DEFAULT nextval('public.jobs_id_seq'::regclass);


--
-- Name: keys id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keys ALTER COLUMN id SET DEFAULT nextval('public.keys_id_seq'::regclass);


--
-- Name: messages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.messages_id_seq'::regclass);


--
-- Name: mirrors id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mirrors ALTER COLUMN id SET DEFAULT nextval('public.mirrors_id_seq'::regclass);


--
-- Name: packages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.packages ALTER COLUMN id SET DEFAULT nextval('public.packages_id_seq'::regclass);


--
-- Name: release_images id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_images ALTER COLUMN id SET DEFAULT nextval('public.release_images_id_seq'::regclass);


--
-- Name: release_monitoring_releases id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitoring_releases ALTER COLUMN id SET DEFAULT nextval('public.release_monitoring_releases_id_seq'::regclass);


--
-- Name: release_monitorings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitorings ALTER COLUMN id SET DEFAULT nextval('public.release_monitorings_id_seq'::regclass);


--
-- Name: releases id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.releases ALTER COLUMN id SET DEFAULT nextval('public.releases_id_seq'::regclass);


--
-- Name: repositories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repositories ALTER COLUMN id SET DEFAULT nextval('public.repositories_id_seq'::regclass);


--
-- Name: repository_builds id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repository_builds ALTER COLUMN id SET DEFAULT nextval('public.repository_builds_id_seq'::regclass);


--
-- Name: sessions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions ALTER COLUMN id SET DEFAULT nextval('public.sessions_id_seq'::regclass);


--
-- Name: source_commit_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.source_commit_jobs ALTER COLUMN id SET DEFAULT nextval('public.source_commit_jobs_id_seq'::regclass);


--
-- Name: source_commits id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.source_commits ALTER COLUMN id SET DEFAULT nextval('public.source_commits_id_seq'::regclass);


--
-- Name: sources id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sources ALTER COLUMN id SET DEFAULT nextval('public.sources_id_seq'::regclass);


--
-- Name: uploads id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.uploads ALTER COLUMN id SET DEFAULT nextval('public.uploads_id_seq'::regclass);


--
-- Name: user_push_subscriptions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_push_subscriptions ALTER COLUMN id SET DEFAULT nextval('public.user_push_subscriptions_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: build_bugs build_bugs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_bugs
    ADD CONSTRAINT build_bugs_pkey PRIMARY KEY (id);


--
-- Name: build_comments build_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_comments
    ADD CONSTRAINT build_comments_pkey PRIMARY KEY (id);


--
-- Name: build_groups build_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_groups
    ADD CONSTRAINT build_groups_pkey PRIMARY KEY (id);


--
-- Name: builds builds_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds
    ADD CONSTRAINT builds_pkey PRIMARY KEY (id);


--
-- Name: cache cache_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cache
    ADD CONSTRAINT cache_pkey PRIMARY KEY (key);


--
-- Name: distributions distributions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.distributions
    ADD CONSTRAINT distributions_pkey PRIMARY KEY (id);


--
-- Name: builders idx_2197954_primary; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builders
    ADD CONSTRAINT idx_2197954_primary PRIMARY KEY (id);


--
-- Name: images_types image_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images_types
    ADD CONSTRAINT image_types_pkey PRIMARY KEY (id);


--
-- Name: images images_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- Name: images images_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_unique UNIQUE (release_id, type, arch);


--
-- Name: jobs jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: keys keys_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keys
    ADD CONSTRAINT keys_pkey PRIMARY KEY (id);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: mirrors mirrors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mirrors
    ADD CONSTRAINT mirrors_pkey PRIMARY KEY (id);


--
-- Name: packages packages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.packages
    ADD CONSTRAINT packages_pkey PRIMARY KEY (id);


--
-- Name: ratelimiter ratelimiter_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ratelimiter
    ADD CONSTRAINT ratelimiter_pkey PRIMARY KEY (key, "timestamp", address);


--
-- Name: ratelimiter ratelimiter_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ratelimiter
    ADD CONSTRAINT ratelimiter_unique UNIQUE (key, "timestamp", address);


--
-- Name: release_images release_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_images
    ADD CONSTRAINT release_images_pkey PRIMARY KEY (id);


--
-- Name: release_monitoring_releases release_monitoring_releases_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitoring_releases
    ADD CONSTRAINT release_monitoring_releases_pkey PRIMARY KEY (id);


--
-- Name: release_monitorings release_monitorings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitorings
    ADD CONSTRAINT release_monitorings_pkey PRIMARY KEY (id);


--
-- Name: releases releases_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.releases
    ADD CONSTRAINT releases_pkey PRIMARY KEY (id);


--
-- Name: repositories repositories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repositories
    ADD CONSTRAINT repositories_pkey PRIMARY KEY (id);


--
-- Name: repository_builds repository_builds_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repository_builds
    ADD CONSTRAINT repository_builds_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_session_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_session_id_key UNIQUE (session_id);


--
-- Name: source_commit_jobs source_commit_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.source_commit_jobs
    ADD CONSTRAINT source_commit_jobs_pkey PRIMARY KEY (id);


--
-- Name: source_commits source_commits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.source_commits
    ADD CONSTRAINT source_commits_pkey PRIMARY KEY (id);


--
-- Name: sources sources_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT sources_pkey PRIMARY KEY (id);


--
-- Name: uploads uploads_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.uploads
    ADD CONSTRAINT uploads_id PRIMARY KEY (id);


--
-- Name: user_push_subscriptions user_push_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_push_subscriptions
    ADD CONSTRAINT user_push_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: build_bugs_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX build_bugs_unique ON public.build_bugs USING btree (build_id, bug_id) WHERE (removed_at IS NULL);


--
-- Name: build_groups_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX build_groups_uuid ON public.build_groups USING btree (uuid) WHERE (deleted_at IS NULL);


--
-- Name: build_packages_build_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX build_packages_build_id ON public.build_packages USING btree (build_id);


--
-- Name: build_points_build_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX build_points_build_id ON public.build_points USING btree (build_id);


--
-- Name: build_watchers_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX build_watchers_unique ON public.build_watchers USING btree (build_id, user_id) WHERE (deleted_at IS NULL);


--
-- Name: builders_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX builders_name ON public.builders USING btree (name) WHERE (deleted_at IS NULL);


--
-- Name: builds_build_group_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX builds_build_group_id ON public.builds USING btree (group_id) WHERE (deleted_at IS NULL);


--
-- Name: builds_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX builds_created_at ON public.builds USING btree (created_at DESC) WHERE (deleted_at IS NULL);


--
-- Name: builds_deleted; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX builds_deleted ON public.builds USING btree (deleted_at) WHERE (deleted_at IS NOT NULL);


--
-- Name: builds_deprecating_build_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX builds_deprecating_build_id ON public.builds USING btree (deprecating_build_id) WHERE ((deleted_at IS NULL) AND (deprecated_at IS NOT NULL));


--
-- Name: builds_not_tests; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX builds_not_tests ON public.builds USING btree (id) WHERE ((test IS FALSE) AND (deleted_at IS NULL));


--
-- Name: builds_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX builds_pkg_id ON public.builds USING btree (pkg_id) WHERE (deleted_at IS NULL);


--
-- Name: builds_scratch_builds; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX builds_scratch_builds ON public.builds USING btree (id) WHERE ((deleted_at IS NULL) AND (test IS FALSE) AND (owner_id IS NOT NULL));


--
-- Name: builds_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX builds_uuid ON public.builds USING btree (uuid) WHERE (deleted_at IS NULL);


--
-- Name: distributions_tag; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX distributions_tag ON public.distributions USING btree (tag) WHERE (deleted_at IS NULL);


--
-- Name: distributions_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX distributions_unique ON public.distributions USING btree (distro_id, version_id) WHERE (deleted IS FALSE);


--
-- Name: job_packages_job_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX job_packages_job_id ON public.job_packages USING btree (job_id);


--
-- Name: job_packages_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX job_packages_pkg_id ON public.job_packages USING btree (pkg_id);


--
-- Name: jobs_arch; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX jobs_arch ON public.jobs USING btree (arch);


--
-- Name: jobs_build_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX jobs_build_id ON public.jobs USING btree (build_id) WHERE (deleted_at IS NULL);


--
-- Name: jobs_finished_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX jobs_finished_at ON public.jobs USING btree (finished_at DESC) WHERE (finished_at IS NOT NULL);


--
-- Name: jobs_pending; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX jobs_pending ON public.jobs USING btree (id) WHERE ((deleted_at IS NULL) AND (started_at IS NULL) AND (finished_at IS NULL) AND (installcheck_succeeded IS TRUE));


--
-- Name: jobs_running; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX jobs_running ON public.jobs USING btree (started_at) WHERE ((deleted_at IS NULL) AND (started_at IS NOT NULL) AND (finished_at IS NULL));


--
-- Name: jobs_superseeded_by; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX jobs_superseeded_by ON public.jobs USING btree (superseeded_by_id);


--
-- Name: jobs_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX jobs_uuid ON public.jobs USING btree (uuid) WHERE (deleted_at IS NULL);


--
-- Name: messages_queued; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX messages_queued ON public.messages USING btree (priority DESC, queued_at) WHERE (sent_at IS NULL);


--
-- Name: mirror_checks_search; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX mirror_checks_search ON public.mirror_checks USING btree (mirror_id, checked_at);


--
-- Name: mirrors_hostname; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX mirrors_hostname ON public.mirrors USING btree (hostname) WHERE (deleted_at IS NULL);


--
-- Name: package_files_path; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX package_files_path ON public.package_files USING btree (path);


--
-- Name: package_files_pkg_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX package_files_pkg_id ON public.package_files USING btree (pkg_id);


--
-- Name: packages_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX packages_name ON public.packages USING btree (name);


--
-- Name: packages_search; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX packages_search ON public.packages USING gin (search) WHERE (deleted_at IS NULL);


--
-- Name: release_monitoring_releases_build_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX release_monitoring_releases_build_id ON public.release_monitoring_releases USING btree (build_id);


--
-- Name: release_monitoring_releases_search; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX release_monitoring_releases_search ON public.release_monitoring_releases USING btree (monitoring_id, created_at);


--
-- Name: release_monitoring_releases_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX release_monitoring_releases_unique ON public.release_monitoring_releases USING btree (monitoring_id, version);


--
-- Name: release_monitorings_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX release_monitorings_unique ON public.release_monitorings USING btree (distro_id, name) WHERE (deleted_at IS NULL);


--
-- Name: releases_distro_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX releases_distro_id ON public.releases USING btree (distro_id) WHERE (deleted_at IS NULL);


--
-- Name: releases_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX releases_unique ON public.releases USING btree (slug) WHERE (deleted_at IS NULL);


--
-- Name: repositories_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX repositories_unique ON public.repositories USING btree (owner_id, distro_id, slug) WHERE (deleted_at IS NULL);


--
-- Name: repository_builds_build_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX repository_builds_build_id ON public.repository_builds USING btree (build_id);


--
-- Name: repository_builds_repo_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX repository_builds_repo_id ON public.repository_builds USING btree (repo_id) WHERE (removed_at IS NULL);


--
-- Name: repository_builds_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX repository_builds_unique ON public.repository_builds USING btree (repo_id, build_id) WHERE (removed_at IS NULL);


--
-- Name: source_commit_jobs_commit_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX source_commit_jobs_commit_id ON public.source_commit_jobs USING btree (commit_id);


--
-- Name: source_commits_unique; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX source_commits_unique ON public.source_commits USING btree (source_id, revision);


--
-- Name: sources_repo_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sources_repo_id ON public.sources USING btree (repo_id) WHERE (deleted_at IS NULL);


--
-- Name: sources_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX sources_slug ON public.sources USING btree (slug) WHERE (deleted_at IS NULL);


--
-- Name: uploads_builder_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX uploads_builder_id ON public.uploads USING btree (builder_id);


--
-- Name: uploads_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX uploads_user_id ON public.uploads USING btree (user_id);


--
-- Name: uploads_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX uploads_uuid ON public.uploads USING btree (uuid);


--
-- Name: user_push_subscriptions_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_push_subscriptions_user_id ON public.user_push_subscriptions USING btree (user_id) WHERE (deleted_at IS NULL);


--
-- Name: users_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX users_name ON public.users USING btree (name) WHERE (deleted_at IS NULL);


--
-- Name: build_bugs build_bugs_added_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_bugs
    ADD CONSTRAINT build_bugs_added_by FOREIGN KEY (added_by_id) REFERENCES public.users(id);


--
-- Name: build_bugs build_bugs_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_bugs
    ADD CONSTRAINT build_bugs_build_id FOREIGN KEY (build_id) REFERENCES public.builds(id);


--
-- Name: build_bugs build_bugs_removed_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_bugs
    ADD CONSTRAINT build_bugs_removed_by FOREIGN KEY (removed_by_id) REFERENCES public.users(id);


--
-- Name: build_groups build_groups_created_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_groups
    ADD CONSTRAINT build_groups_created_by FOREIGN KEY (created_by_id) REFERENCES public.users(id);


--
-- Name: build_groups build_groups_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_groups
    ADD CONSTRAINT build_groups_deleted_by FOREIGN KEY (deleted_by_id) REFERENCES public.users(id);


--
-- Name: build_groups build_groups_tested_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_groups
    ADD CONSTRAINT build_groups_tested_build_id FOREIGN KEY (tested_build_id) REFERENCES public.builds(id);


--
-- Name: build_packages build_packages_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_packages
    ADD CONSTRAINT build_packages_build_id FOREIGN KEY (build_id) REFERENCES public.builds(id);


--
-- Name: build_packages build_packages_job_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_packages
    ADD CONSTRAINT build_packages_job_id FOREIGN KEY (job_id) REFERENCES public.jobs(id);


--
-- Name: build_packages build_packages_package_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_packages
    ADD CONSTRAINT build_packages_package_id FOREIGN KEY (package_id) REFERENCES public.packages(id);


--
-- Name: build_points build_points_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_points
    ADD CONSTRAINT build_points_build_id FOREIGN KEY (build_id) REFERENCES public.builds(id);


--
-- Name: build_points build_points_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_points
    ADD CONSTRAINT build_points_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: build_watchers build_watchers_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_watchers
    ADD CONSTRAINT build_watchers_build_id FOREIGN KEY (build_id) REFERENCES public.builds(id);


--
-- Name: build_watchers build_watchers_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_watchers
    ADD CONSTRAINT build_watchers_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: builder_stats builder_stats_builder_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builder_stats
    ADD CONSTRAINT builder_stats_builder_id FOREIGN KEY (builder_id) REFERENCES public.builders(id);


--
-- Name: builders builders_created_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builders
    ADD CONSTRAINT builders_created_by FOREIGN KEY (created_by_id) REFERENCES public.users(id);


--
-- Name: builders builders_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builders
    ADD CONSTRAINT builders_deleted_by FOREIGN KEY (deleted_by_id) REFERENCES public.users(id);


--
-- Name: build_comments builds_comments_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_comments
    ADD CONSTRAINT builds_comments_build_id FOREIGN KEY (build_id) REFERENCES public.builds(id);


--
-- Name: build_comments builds_comments_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.build_comments
    ADD CONSTRAINT builds_comments_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: builds builds_commit_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds
    ADD CONSTRAINT builds_commit_id FOREIGN KEY (commit_id) REFERENCES public.source_commits(id) NOT VALID;


--
-- Name: builds builds_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds
    ADD CONSTRAINT builds_deleted_by FOREIGN KEY (deleted_by_id) REFERENCES public.users(id);


--
-- Name: builds builds_deprecated_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds
    ADD CONSTRAINT builds_deprecated_by FOREIGN KEY (deprecated_by_id) REFERENCES public.users(id);


--
-- Name: builds builds_deprecating_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds
    ADD CONSTRAINT builds_deprecating_build_id FOREIGN KEY (deprecating_build_id) REFERENCES public.builds(id);


--
-- Name: builds builds_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds
    ADD CONSTRAINT builds_group_id FOREIGN KEY (group_id) REFERENCES public.build_groups(id);


--
-- Name: builds builds_owner_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds
    ADD CONSTRAINT builds_owner_id FOREIGN KEY (owner_id) REFERENCES public.users(id);


--
-- Name: builds builds_pkg_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds
    ADD CONSTRAINT builds_pkg_id FOREIGN KEY (pkg_id) REFERENCES public.packages(id);


--
-- Name: builds builds_repo_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.builds
    ADD CONSTRAINT builds_repo_id FOREIGN KEY (repo_id) REFERENCES public.repositories(id);


--
-- Name: images images_created_by_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_created_by_id FOREIGN KEY (created_by_id) REFERENCES public.users(id);


--
-- Name: images images_deleted_by_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_deleted_by_id FOREIGN KEY (deleted_by_id) REFERENCES public.users(id);


--
-- Name: images images_release_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.images
    ADD CONSTRAINT images_release_id FOREIGN KEY (release_id) REFERENCES public.releases(id);


--
-- Name: job_packages job_packages_job_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.job_packages
    ADD CONSTRAINT job_packages_job_id FOREIGN KEY (job_id) REFERENCES public.jobs(id);


--
-- Name: job_packages job_packages_pkg_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.job_packages
    ADD CONSTRAINT job_packages_pkg_id FOREIGN KEY (pkg_id) REFERENCES public.packages(id);


--
-- Name: jobs jobs_aborted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_aborted_by FOREIGN KEY (aborted_by_id) REFERENCES public.users(id);


--
-- Name: jobs jobs_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_build_id FOREIGN KEY (build_id) REFERENCES public.builds(id);


--
-- Name: jobs jobs_builder_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_builder_id FOREIGN KEY (builder_id) REFERENCES public.builders(id);


--
-- Name: jobs jobs_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_deleted_by FOREIGN KEY (deleted_by) REFERENCES public.users(id);


--
-- Name: jobs jobs_superseeded_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_superseeded_by FOREIGN KEY (superseeded_by_id) REFERENCES public.jobs(id);


--
-- Name: keys keys_created_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keys
    ADD CONSTRAINT keys_created_by FOREIGN KEY (created_by_id) REFERENCES public.users(id);


--
-- Name: keys keys_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keys
    ADD CONSTRAINT keys_deleted_by FOREIGN KEY (deleted_by_id) REFERENCES public.users(id);


--
-- Name: mirror_checks mirror_checks_mirror_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mirror_checks
    ADD CONSTRAINT mirror_checks_mirror_id FOREIGN KEY (mirror_id) REFERENCES public.mirrors(id);


--
-- Name: mirrors mirrors_created_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mirrors
    ADD CONSTRAINT mirrors_created_by FOREIGN KEY (created_by_id) REFERENCES public.users(id);


--
-- Name: mirrors mirrors_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mirrors
    ADD CONSTRAINT mirrors_deleted_by FOREIGN KEY (deleted_by_id) REFERENCES public.users(id);


--
-- Name: package_files package_files_pkg_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.package_files
    ADD CONSTRAINT package_files_pkg_id FOREIGN KEY (pkg_id) REFERENCES public.packages(id);


--
-- Name: packages packages_commit_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.packages
    ADD CONSTRAINT packages_commit_id FOREIGN KEY (commit_id) REFERENCES public.source_commits(id);


--
-- Name: packages packages_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.packages
    ADD CONSTRAINT packages_deleted_by FOREIGN KEY (deleted_by) REFERENCES public.users(id);


--
-- Name: packages packages_distro_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.packages
    ADD CONSTRAINT packages_distro_id FOREIGN KEY (distro_id) REFERENCES public.distributions(id);


--
-- Name: release_images release_images_release_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_images
    ADD CONSTRAINT release_images_release_id FOREIGN KEY (release_id) REFERENCES public.releases(id);


--
-- Name: release_images release_images_type_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_images
    ADD CONSTRAINT release_images_type_id FOREIGN KEY (type_id) REFERENCES public.images_types(id);


--
-- Name: release_monitoring_releases release_monitoring_releases_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitoring_releases
    ADD CONSTRAINT release_monitoring_releases_build_id FOREIGN KEY (build_id) REFERENCES public.builds(id);


--
-- Name: release_monitoring_releases release_monitoring_releases_monitoring_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitoring_releases
    ADD CONSTRAINT release_monitoring_releases_monitoring_id FOREIGN KEY (monitoring_id) REFERENCES public.release_monitorings(id);


--
-- Name: release_monitoring_releases release_monitoring_releases_repo_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitoring_releases
    ADD CONSTRAINT release_monitoring_releases_repo_id FOREIGN KEY (repo_id) REFERENCES public.repositories(id);


--
-- Name: release_monitorings release_monitorings_created_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitorings
    ADD CONSTRAINT release_monitorings_created_by FOREIGN KEY (created_by_id) REFERENCES public.users(id);


--
-- Name: release_monitorings release_monitorings_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitorings
    ADD CONSTRAINT release_monitorings_deleted_by FOREIGN KEY (deleted_by_id) REFERENCES public.users(id);


--
-- Name: release_monitorings release_monitorings_distro_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.release_monitorings
    ADD CONSTRAINT release_monitorings_distro_id FOREIGN KEY (distro_id) REFERENCES public.distributions(id);


--
-- Name: releases releases_created_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.releases
    ADD CONSTRAINT releases_created_by FOREIGN KEY (created_by_id) REFERENCES public.users(id);


--
-- Name: releases releases_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.releases
    ADD CONSTRAINT releases_deleted_by FOREIGN KEY (deleted_by_id) REFERENCES public.users(id);


--
-- Name: releases releases_distro_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.releases
    ADD CONSTRAINT releases_distro_id FOREIGN KEY (distro_id) REFERENCES public.distributions(id);


--
-- Name: repositories repositories_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repositories
    ADD CONSTRAINT repositories_deleted_by FOREIGN KEY (deleted_by) REFERENCES public.users(id);


--
-- Name: repositories repositories_distro_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repositories
    ADD CONSTRAINT repositories_distro_id FOREIGN KEY (distro_id) REFERENCES public.distributions(id);


--
-- Name: repositories repositories_key_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repositories
    ADD CONSTRAINT repositories_key_id FOREIGN KEY (key_id) REFERENCES public.keys(id);


--
-- Name: repositories repositories_owner_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repositories
    ADD CONSTRAINT repositories_owner_id FOREIGN KEY (owner_id) REFERENCES public.users(id);


--
-- Name: repository_builds repository_builds_added_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repository_builds
    ADD CONSTRAINT repository_builds_added_by FOREIGN KEY (added_by_id) REFERENCES public.users(id);


--
-- Name: repository_builds repository_builds_build_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repository_builds
    ADD CONSTRAINT repository_builds_build_id FOREIGN KEY (build_id) REFERENCES public.builds(id);


--
-- Name: repository_builds repository_builds_removed_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repository_builds
    ADD CONSTRAINT repository_builds_removed_by FOREIGN KEY (removed_by_id) REFERENCES public.users(id);


--
-- Name: repository_builds repository_builds_repo_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.repository_builds
    ADD CONSTRAINT repository_builds_repo_id FOREIGN KEY (repo_id) REFERENCES public.repositories(id);


--
-- Name: sessions sessions_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: source_commit_jobs source_commit_jobs_commit_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.source_commit_jobs
    ADD CONSTRAINT source_commit_jobs_commit_id FOREIGN KEY (commit_id) REFERENCES public.source_commits(id);


--
-- Name: source_commits source_commits_build_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.source_commits
    ADD CONSTRAINT source_commits_build_group_id FOREIGN KEY (build_group_id) REFERENCES public.build_groups(id);


--
-- Name: source_commits sources_commits_source_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.source_commits
    ADD CONSTRAINT sources_commits_source_id FOREIGN KEY (source_id) REFERENCES public.sources(id);


--
-- Name: sources sources_created_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT sources_created_by FOREIGN KEY (created_by_id) REFERENCES public.users(id);


--
-- Name: sources sources_deleted_by; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT sources_deleted_by FOREIGN KEY (deleted_by_id) REFERENCES public.users(id);


--
-- Name: sources sources_repo_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT sources_repo_id FOREIGN KEY (repo_id) REFERENCES public.repositories(id);


--
-- Name: uploads uploads_builder_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.uploads
    ADD CONSTRAINT uploads_builder_id FOREIGN KEY (builder_id) REFERENCES public.builders(id);


--
-- Name: uploads uploads_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.uploads
    ADD CONSTRAINT uploads_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: user_push_subscriptions user_push_subscriptions_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_push_subscriptions
    ADD CONSTRAINT user_push_subscriptions_user_id FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: -
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

